package tr.com.srdc.cda2fhir;

import org.junit.BeforeClass;
import org.junit.Test;
import org.openhealthtools.mdht.uml.cda.consol.ProcedureActivityObservation;
import org.openhealthtools.mdht.uml.cda.util.CDAUtil;
import tr.com.srdc.cda2fhir.testutil.CDAFactories;
import tr.com.srdc.cda2fhir.testutil.generator.ProcedureActivityObservationGenerator;
import tr.com.srdc.cda2fhir.transform.ResourceTransformerImpl;
import tr.com.srdc.cda2fhir.transform.entry.impl.EntryResult;
import tr.com.srdc.cda2fhir.transform.util.impl.BundleInfo;
import tr.com.srdc.cda2fhir.transform.util.impl.TransformationContextImpl;

public class ProcedureActivityObservationTest {
	private static final ResourceTransformerImpl rt = new ResourceTransformerImpl();
	private static CDAFactories factories;

	@BeforeClass
	public static void init() {
		CDAUtil.loadPackages();

		factories = CDAFactories.init();
	}

	@Test
	public void testTransformation() throws Exception {
		ProcedureActivityObservationGenerator generator = ProcedureActivityObservationGenerator.getDefaultInstance();
		ProcedureActivityObservation observation = generator.generate(factories);

		EntryResult entryResult = rt.tProcedureActivityObservation2Observation(observation, new BundleInfo(rt), new TransformationContextImpl());

		generator.verify(entryResult.getBundle());
		generator.verifyDeferredReferences(entryResult);
	}
}
