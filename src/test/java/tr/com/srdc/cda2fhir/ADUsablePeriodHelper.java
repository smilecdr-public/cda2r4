package tr.com.srdc.cda2fhir;

import org.hl7.fhir.r4.model.Period;
import org.junit.Assert;
import org.openhealthtools.mdht.uml.hl7.datatypes.AD;
import org.openhealthtools.mdht.uml.hl7.datatypes.IVL_TS;
import org.openhealthtools.mdht.uml.hl7.datatypes.PIVL_TS;
import org.openhealthtools.mdht.uml.hl7.datatypes.SXCM_TS;
import tr.com.srdc.cda2fhir.testutil.CDAFactories;

import java.text.MessageFormat;
import java.util.*;

public class ADUsablePeriodHelper {

    public static final String TIME_1 = "20190507";
    public static final String TIME_2 = "20200911";
    public static final String TIME_3 = "20201126";
    public static final String TIME_4 = "20210324";
    public static final String TIME_5 = "20230101";
    public static final Date EXPECTED_TIME_1 = new GregorianCalendar(2019, Calendar.MAY, 7).getTime();
    public static final Date EXPECTED_TIME_2 = new GregorianCalendar(2020, Calendar.SEPTEMBER, 11).getTime();
    public static final Date EXPECTED_TIME_3 = new GregorianCalendar(2020, Calendar.NOVEMBER, 26).getTime();
    public static final Date EXPECTED_TIME_4 = new GregorianCalendar(2021, Calendar.MARCH, 24).getTime();
    public static final Date EXPECTED_TIME_5 = new GregorianCalendar(2023, Calendar.JANUARY, 1).getTime();

    public ADUsablePeriodHelper() {
    }

    public class ADCase {
        private final List<SXCM_TS> actuals;
        private final Date expectedStart;
        private final Date expectedEnd;

        public ADCase(List<SXCM_TS> theActuals, Date theExpectedStart, Date theExpectedEnd) {
            actuals = theActuals;
            expectedStart = theExpectedStart;
            expectedEnd = theExpectedEnd;
        }

        public List<SXCM_TS> getActuals() {
            return actuals;
        }

        public Date getExpectedStart() {
            return expectedStart;
        }

        public Date getExpectedEnd() {
            return expectedEnd;
        }
    }

    List<ADCase> createCases() {
        List<ADCase> result = new ArrayList<>();

        // timestamps
        result.add(new ADCase(List.of(createTimestamp(null)), null, null));
        result.add(new ADCase(List.of(createTimestamp(TIME_1)), EXPECTED_TIME_1, null));
        result.add(new ADCase(List.of(createTimestamp(TIME_1), createTimestamp(TIME_2)), EXPECTED_TIME_1, EXPECTED_TIME_2));
        result.add(new ADCase(List.of(createTimestamp(TIME_1), createTimestamp(TIME_3)), EXPECTED_TIME_1, EXPECTED_TIME_3));

        // IVLs
        result.add(new ADCase(List.of(createIVL(TIME_1, null)), EXPECTED_TIME_1, null));
        result.add(new ADCase(List.of(createIVL(null, TIME_2)), null, EXPECTED_TIME_2));
        result.add(new ADCase(List.of(createIVL(TIME_1, TIME_2)), EXPECTED_TIME_1, EXPECTED_TIME_2));

        // PIVLs
        result.add(new ADCase(List.of(createPIVL(TIME_1, null)), EXPECTED_TIME_1, null));
        result.add(new ADCase(List.of(createPIVL(null, TIME_2)), null, null));
        result.add(new ADCase(List.of(createPIVL(TIME_1, TIME_2)), EXPECTED_TIME_1, null));

        // timestamps + IVLs
        result.add(new ADCase(List.of(createTimestamp(TIME_1), createIVL(TIME_2, null)), EXPECTED_TIME_1, null));
        result.add(new ADCase(List.of(createTimestamp(TIME_1), createIVL(null, TIME_2)), EXPECTED_TIME_1, EXPECTED_TIME_2));
        result.add(new ADCase(List.of(createIVL(TIME_1, null), createTimestamp(TIME_2)), EXPECTED_TIME_1, null));
        result.add(new ADCase(List.of(createIVL(null, TIME_1), createTimestamp(TIME_2)), EXPECTED_TIME_2, null));
        result.add(new ADCase(List.of(createTimestamp(TIME_1), createIVL(TIME_2, TIME_3)), EXPECTED_TIME_1, EXPECTED_TIME_3));
        result.add(new ADCase(List.of(createTimestamp(TIME_2), createIVL(TIME_1, TIME_3)), EXPECTED_TIME_1, EXPECTED_TIME_3));
        result.add(new ADCase(List.of(createTimestamp(TIME_3), createIVL(TIME_1, TIME_2)), EXPECTED_TIME_1, EXPECTED_TIME_3));

        // IVL + PIVL
        result.add(new ADCase(List.of(createIVL(TIME_1, null), createPIVL(TIME_2, null)), EXPECTED_TIME_1, null));
        result.add(new ADCase(List.of(createIVL(TIME_2, null), createPIVL(TIME_1, null)), EXPECTED_TIME_1, null));
        result.add(new ADCase(List.of(createIVL(null, TIME_1), createPIVL(TIME_2, null)), EXPECTED_TIME_2, null));
        result.add(new ADCase(List.of(createIVL(null, TIME_2), createPIVL(TIME_1, null)), EXPECTED_TIME_1, null));
        result.add(new ADCase(List.of(createIVL(TIME_1, null), createPIVL(null, TIME_2)), EXPECTED_TIME_1, null));
        result.add(new ADCase(List.of(createIVL(TIME_2, null), createPIVL(null, TIME_1)), EXPECTED_TIME_2, null));
        result.add(new ADCase(List.of(createIVL(null, TIME_1), createPIVL(null, TIME_2)), null, null));
        result.add(new ADCase(List.of(createIVL(null, TIME_2), createPIVL(null, TIME_1)), null, null));

        result.add(new ADCase(List.of(createIVL(TIME_2, TIME_3), createPIVL(TIME_1, null)), EXPECTED_TIME_1, null));
        result.add(new ADCase(List.of(createIVL(TIME_1, TIME_3), createPIVL(TIME_2, null)), EXPECTED_TIME_1, null));
        result.add(new ADCase(List.of(createIVL(TIME_1, TIME_3), createPIVL(TIME_2, null)), EXPECTED_TIME_1, null));
        result.add(new ADCase(List.of(createIVL(TIME_2, TIME_3), createPIVL(null, TIME_1)), EXPECTED_TIME_2, null));
        result.add(new ADCase(List.of(createIVL(TIME_1, TIME_3), createPIVL(null, TIME_2)), EXPECTED_TIME_1, null));
        result.add(new ADCase(List.of(createIVL(TIME_1, TIME_3), createPIVL(null, TIME_2)), EXPECTED_TIME_1, null));

        // timestamp + pivl
        result.add(new ADCase(List.of(createTimestamp(TIME_1), createPIVL(TIME_2, null)), EXPECTED_TIME_1, null));
        result.add(new ADCase(List.of(createTimestamp(TIME_1), createPIVL(null, TIME_2)), EXPECTED_TIME_1, null));
        result.add(new ADCase(List.of(createPIVL(TIME_1, null), createTimestamp(TIME_2)), EXPECTED_TIME_1, null));
        result.add(new ADCase(List.of(createPIVL(null, TIME_1), createTimestamp(TIME_2)), EXPECTED_TIME_2, null));
        result.add(new ADCase(List.of(createTimestamp(TIME_1), createPIVL(TIME_2, TIME_3)), EXPECTED_TIME_1, null));
        result.add(new ADCase(List.of(createTimestamp(TIME_2), createPIVL(TIME_1, TIME_3)), EXPECTED_TIME_1, null));
        result.add(new ADCase(List.of(createTimestamp(TIME_3), createPIVL(TIME_1, TIME_2)), EXPECTED_TIME_1, null));

        // other
        result.add(new ADCase(List.of(createTimestamp(TIME_1), createIVL(TIME_2, TIME_3), createPIVL(TIME_4, TIME_5)), EXPECTED_TIME_1, null));
        result.add(new ADCase(List.of(createTimestamp(TIME_1), createIVL(TIME_2, TIME_3), createIVL(TIME_4, TIME_5)), EXPECTED_TIME_1, EXPECTED_TIME_5));
        result.add(new ADCase(List.of(createTimestamp(TIME_3), createIVL(TIME_1, TIME_5), createIVL(TIME_2, TIME_4)), EXPECTED_TIME_1, EXPECTED_TIME_5));

        return result;
    }

    private Date getExpected(String time) {
        switch (time) {
            case TIME_1:
                return EXPECTED_TIME_1;
            case TIME_2:
                return EXPECTED_TIME_2;
            case TIME_3:
                return EXPECTED_TIME_3;
            case TIME_4:
                return EXPECTED_TIME_4;
            case TIME_5:
                return EXPECTED_TIME_5;
            default:
                return null;
        }
    }

    AD createADWithTimes(List<SXCM_TS> times) {
        CDAFactories factories = CDAFactories.init();
        AD ad = factories.datatype.createAD();
        ad.getUseablePeriods().addAll(times);
        return ad;
    }

    PIVL_TS createPIVL(String start, String end) {
        CDAFactories factories = CDAFactories.init();
        PIVL_TS result = factories.datatype.createPIVL_TS();
        result.setPhase(createIVL(start, end));
        return result;
    }

    IVL_TS createIVL(String start, String end) {
        CDAFactories factories = CDAFactories.init();
        return factories.datatype.createIVL_TS(start, end);
    }

    SXCM_TS createTimestamp(String time) {
        CDAFactories factories = CDAFactories.init();
        SXCM_TS result = factories.datatype.createSXCM_TS();
        result.setValue(time);
        return result;
    }

    void verifyPeriod(ADCase adCase, Period period) {
        Date expectedStart = adCase.getExpectedStart();
        Date actualStart = period.getStart();
        Assert.assertEquals(MessageFormat.format("Expected start: [{0}]; received: [{1}]", expectedStart, actualStart), expectedStart, actualStart);

        Date expectedEnd = adCase.getExpectedEnd();
        Date actualEnd = period.getEnd();
        Assert.assertEquals(MessageFormat.format("Expected end: [{0}]; received: [{1}]", expectedEnd, actualEnd), expectedEnd, actualEnd);
    }
}