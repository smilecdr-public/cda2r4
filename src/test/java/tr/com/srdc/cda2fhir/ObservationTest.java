package tr.com.srdc.cda2fhir;

import java.util.Map;

import org.hl7.fhir.r4.model.BooleanType;
import org.hl7.fhir.r4.model.Bundle;
import org.hl7.fhir.r4.model.CodeableConcept;
import org.hl7.fhir.r4.model.Observation;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import org.openhealthtools.mdht.uml.cda.util.CDAUtil;
import org.openhealthtools.mdht.uml.hl7.datatypes.BL;
import org.openhealthtools.mdht.uml.hl7.datatypes.CE;
import org.openhealthtools.mdht.uml.hl7.datatypes.ED;
import org.openhealthtools.mdht.uml.hl7.datatypes.TEL;

import com.helger.commons.collection.attr.StringMap;

import org.openhealthtools.mdht.uml.hl7.vocab.NullFlavor;
import tr.com.srdc.cda2fhir.testutil.BundleUtil;
import tr.com.srdc.cda2fhir.testutil.CDAFactories;
import tr.com.srdc.cda2fhir.testutil.generator.CaregiverCharacteristicObservationGenerator;
import tr.com.srdc.cda2fhir.testutil.generator.PregnancyObservationGenerator;
import tr.com.srdc.cda2fhir.testutil.generator.SocialHistoryObservationGenerator;
import tr.com.srdc.cda2fhir.transform.ResourceTransformerImpl;
import tr.com.srdc.cda2fhir.transform.util.ITransformationContext;
import tr.com.srdc.cda2fhir.transform.entry.IEntryResult;
import tr.com.srdc.cda2fhir.transform.util.impl.BundleInfo;
import tr.com.srdc.cda2fhir.transform.util.impl.TransformationContextImpl;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;

public class ObservationTest {
	private static final ResourceTransformerImpl rt = new ResourceTransformerImpl();
	private static CDAFactories factories;

	@BeforeClass
	public static void init() {
		CDAUtil.loadPackages();

		factories = CDAFactories.init();
	}

	public static void verifyBooleanValue(boolean value) throws Exception {
		org.openhealthtools.mdht.uml.cda.Observation observation = factories.base.createObservation();
		BL bl = factories.datatype.createBL(value);
		observation.getValues().add(bl);

		BundleInfo bundleInfo = new BundleInfo(rt);
		ITransformationContext transformationContext = new TransformationContextImpl();
		Bundle bundle = rt.tObservation2Observation(observation, bundleInfo, transformationContext).getBundle();
		org.hl7.fhir.r4.model.Observation fhirObservation = BundleUtil.findOneResource(bundle,
				org.hl7.fhir.r4.model.Observation.class);
		assertNotNull(fhirObservation);
		BooleanType bt = fhirObservation.getValueBooleanType();
		Assert.assertEquals("Pull back the observation " + value + " boolean value", value,
				bt.getValue());
	}

	@Test
	public void valueBooleanTest() throws Exception {
		verifyBooleanValue(true);
		verifyBooleanValue(false);
	}

	@Test
	public void testObservationOriginalText() throws Exception {

		org.openhealthtools.mdht.uml.cda.Observation observation = factories.base.createObservation();

		BundleInfo bundleInfo = new BundleInfo(rt);
		String expectedValue = "freetext entry";
		String referenceValue = "fakeid1";
		CE ce = factories.datatype.createCE();
		ED ed = factories.datatype.createED();
		TEL tel = factories.datatype.createTEL();
		tel.setValue("#" + referenceValue);
		ed.setReference(tel);
		ce.setCode("code");
		ce.setCodeSystem("codeSystem");
		ce.setOriginalText(ed);
		Map<String, String> idedAnnotations = new StringMap();
		idedAnnotations.put(referenceValue, expectedValue);
		bundleInfo.mergeIdedAnnotations(idedAnnotations);

		ITransformationContext transformationContext = new TransformationContextImpl();

		observation.setCode(ce);
		Bundle bundle = rt.tObservation2Observation(observation, bundleInfo, transformationContext).getBundle();
		Observation fhirObservation = BundleUtil.findOneResource(bundle, Observation.class);
		assertNotNull(fhirObservation);
		CodeableConcept cc = fhirObservation.getCode();
		Assert.assertEquals("Observation Code text value assigned", expectedValue, cc.getText());

	}

	@Test
	public void testPregnancyObservation() throws Exception {
		PregnancyObservationGenerator generator = PregnancyObservationGenerator.getDefaultInstance();

		IEntryResult entryResult = rt.tSocialHistoryObservation2Observation(generator.generate(factories), new BundleInfo(rt),
				new TransformationContextImpl());
		generator.verify(entryResult.getBundle());
	}

	@Test
	public void testCaregiverCharacteristicsObservation() throws Exception {
		CaregiverCharacteristicObservationGenerator generator = CaregiverCharacteristicObservationGenerator.getDefaultInstance();

		IEntryResult entryResult = rt.tSocialHistoryObservation2Observation(generator.generate(factories), new BundleInfo(rt),
				new TransformationContextImpl());
		generator.verify(entryResult.getBundle());
	}

	@Test
	public void testSocialHistoryObservation_withNullFlavor() throws Exception {
		SocialHistoryObservationGenerator generator = SocialHistoryObservationGenerator.getDefaultInstance();
		org.openhealthtools.mdht.uml.cda.Observation observation = generator.generate(factories);
		observation.setNullFlavor(NullFlavor.NI);

		IEntryResult entryResult = rt.tSocialHistoryObservation2Observation(observation,
				new BundleInfo(rt), new TransformationContextImpl());
		assertNotNull(entryResult);
		assertFalse(entryResult.hasResult());
	}


}
