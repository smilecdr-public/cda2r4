package tr.com.srdc.cda2fhir.util;

import java.util.ArrayList;
import java.util.List;

import org.hl7.fhir.r4.model.Coding;
import org.hl7.fhir.r4.model.Identifier;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

public class FHIRUtilTest {

	@Test
	public void testIsSameResource() {
		
		Identifier id1 = new Identifier();
		id1.setSystem("http://system1").setValue("11111");
		
		Identifier id2 = new Identifier();
		id2.setSystem("http://system2").setValue("222222");

		Identifier id3 = new Identifier();
		id3.setSystem("http://system3").setValue("3333333");

		Identifier id4 = new Identifier();
		id4.setSystem("http://system4").setValue("4444444");
		
		Identifier noSystem = new Identifier();
		noSystem.setValue("111111");
		
		Identifier noValue = new Identifier();
		noValue.setSystem("http://system1");
		
		//-- Case 1 different value
		//-- id1, id2
		List<Identifier> idList1 = new ArrayList<>();
		idList1.add(id1);
		idList1.add(id2);
		
		//-- id3, id4
		List<Identifier> idList2 = new ArrayList<>();
		idList2.add(id3);
		idList2.add(id4);

		Assert.assertFalse(FHIRUtil.isSameResource(idList1, idList2));
	
		//Case 2: Same order
		//-- id1, id2
		idList1 = new ArrayList<>();
		idList1.add(id1);
		idList1.add(id2);
		
		//-- id1, id4
		idList2 = new ArrayList<>();
		idList2.add(id1);
		idList2.add(id4);
		idList2.add(id3);
		
		Assert.assertTrue(FHIRUtil.isSameResource(idList1, idList2));
		
		//Case 3 Different order
		//-- id1, id2
		idList1 = new ArrayList<>();
		idList1.add(id1);
		idList1.add(id2);
		
		//-- id4, id1
		idList2 = new ArrayList<>();
		idList2.add(id4);
		idList2.add(id1);
		idList2.add(id3);
		
		Assert.assertTrue(FHIRUtil.isSameResource(idList1, idList2));
		
		// Case 4 : one of them null
		Assert.assertFalse(FHIRUtil.isSameResource(null, idList2));
		Assert.assertFalse(FHIRUtil.isSameResource(idList1, null));
	
		// Case 5: both null
		Assert.assertFalse(FHIRUtil.isSameResource(null, null));
		
		// Case 6: no system
		//-- id1, id2
		idList1 = new ArrayList<>();
		idList1.add(id1);
		idList1.add(id2);
		
		//-- id4, noSystem
		idList2 = new ArrayList<>();
		idList2.add(id4);
		idList2.add(noSystem);
		
		Assert.assertFalse(FHIRUtil.isSameResource(idList1, idList2));
		
		// Case 7: no value
		//-- id1, id2
		idList1 = new ArrayList<>();
		idList1.add(id1);
		idList1.add(id2);
		
		//-- id4, noSystem
		idList2 = new ArrayList<>();
		idList2.add(id4);
		idList2.add(noValue);
		
		Assert.assertFalse(FHIRUtil.isSameResource(idList1, idList2));
	}
}
