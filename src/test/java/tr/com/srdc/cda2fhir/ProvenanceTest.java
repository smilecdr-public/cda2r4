package tr.com.srdc.cda2fhir;

import java.security.MessageDigest;
import java.util.List;

import org.eclipse.emf.ecore.xml.type.internal.DataValue.Base64;
import org.hamcrest.CoreMatchers;
import org.hamcrest.MatcherAssert;
import org.hl7.fhir.r4.model.Bundle;
import org.hl7.fhir.r4.model.Bundle.BundleEntryComponent;
import org.hl7.fhir.r4.model.CodeableConcept;
import org.hl7.fhir.r4.model.Coding;
import org.hl7.fhir.r4.model.Composition;
import org.hl7.fhir.r4.model.Device;
import org.hl7.fhir.r4.model.DocumentReference;
import org.hl7.fhir.r4.model.IdType;
import org.hl7.fhir.r4.model.Identifier;
import org.hl7.fhir.r4.model.Medication;
import org.hl7.fhir.r4.model.Narrative.NarrativeStatus;
import org.hl7.fhir.r4.model.Organization;
import org.hl7.fhir.r4.model.Patient;
import org.hl7.fhir.r4.model.Provenance;
import org.hl7.fhir.r4.model.Provenance.ProvenanceAgentComponent;
import org.hl7.fhir.r4.model.Provenance.ProvenanceEntityRole;
import org.hl7.fhir.r4.model.codesystems.ProvenanceAgentRole;
import org.hl7.fhir.r4.model.codesystems.ProvenanceAgentType;
import org.junit.Assert;
import org.junit.Test;

import tr.com.srdc.cda2fhir.testutil.BundleUtil;
import tr.com.srdc.cda2fhir.transform.ResourceTransformerImpl;
import tr.com.srdc.cda2fhir.transform.util.ITransformationContext;
import tr.com.srdc.cda2fhir.transform.util.impl.TransformationContextImpl;

public class ProvenanceTest {

	private static final ResourceTransformerImpl rt = new ResourceTransformerImpl();

	@Test
	public void testProvenance() throws Exception {
		Bundle testBundle = new Bundle();
		IdType orgId = (new IdType("Organization/1"));
		IdType medId = (new IdType("Medication/1"));
		IdType patientId = (new IdType("Patient/1"));
		IdType compositionId = (new IdType("Composition/1"));

		Composition composition = new Composition();
		composition.setIdElement(compositionId);
		composition.getAuthorFirstRep().setReference("Practitioner/1ef404c0-4fb5-4f95-a763-7c34a867f4bc");
		composition.getCustodian().setReference("Organization/9fa06037-96da-4287-9e19-5a5dcb20a0b4");
		
		testBundle.addEntry(new BundleEntryComponent().setResource(composition));
		testBundle.addEntry(new BundleEntryComponent().setResource(new Organization().setIdElement(orgId)));
		testBundle.addEntry(new BundleEntryComponent().setResource(new Medication().setIdElement(medId)));
		testBundle.addEntry(new BundleEntryComponent().setResource(new Patient().setIdElement(patientId)));

		Identifier assemblerDevice = new Identifier();
		assemblerDevice.setValue("Higgs");
		assemblerDevice.setSystem("http://www.amida.com");

		ITransformationContext transformationContext = new TransformationContextImpl();

		String documentBody = "<ClinicalDoc>Meowmeowmeowmeow</ClinicalDoc>";
		testBundle = rt.tProvenance(testBundle, documentBody, assemblerDevice, transformationContext);
		
		// Verifies bundle contains the initial resources.
		BundleUtil.findOneResource(testBundle, Organization.class);
		BundleUtil.findOneResource(testBundle, Medication.class);
		BundleUtil.findOneResource(testBundle, Patient.class);

		DocumentReference docRef = BundleUtil.findOneResource(testBundle, DocumentReference.class);

		// Test doc reference.
		Assert.assertEquals(docRef.getStatus().toString(), "CURRENT");
		Assert.assertEquals(docRef.getContent().get(0).getAttachment().getContentType(), "text/plain");
		Assert.assertEquals(docRef.getType().getCoding().get(0).getCode(), "34133-9");

		// Test encoding.
		Assert.assertEquals(docRef.getContent().get(0).getAttachment().getDataElement().getValueAsString(),
				Base64.encode(documentBody.getBytes()));

		// Test hash.
		MessageDigest digest = MessageDigest.getInstance("SHA-1");
		byte[] encodedHash = digest.digest(documentBody.getBytes());
		Assert.assertEquals(docRef.getContent().get(0).getAttachment().getHashElement().getValueAsString(),
				Base64.encode(encodedHash));

		Device device = BundleUtil.findOneResource(testBundle, Device.class);
		Assert.assertEquals(device.getText().getStatusAsString().toLowerCase(),
				NarrativeStatus.GENERATED.toString().toLowerCase());
		Assert.assertEquals(device.getIdentifierFirstRep().getSystem().toLowerCase(),
				assemblerDevice.getSystem().toLowerCase());
		Assert.assertEquals(device.getIdentifierFirstRep().getValue().toLowerCase(),
				assemblerDevice.getValue().toLowerCase());

		Provenance provenance = BundleUtil.findOneResource(testBundle, Provenance.class);
		Assert.assertEquals(provenance.getTarget().get(0).getReference(), compositionId.getValue());
		Assert.assertEquals(provenance.getTarget().get(1).getReference(), orgId.getValue());
		Assert.assertEquals(provenance.getTarget().get(2).getReference(), medId.getValue());
		Assert.assertEquals(provenance.getTarget().get(3).getReference(), patientId.getValue());
		Assert.assertEquals(provenance.getTarget().get(4).getReference().substring(0, 17), "DocumentReference");
		Assert.assertEquals(provenance.getTarget().get(5).getReference().substring(0, 6), "Device");

		//-- Author
		ProvenanceAgentComponent authorPac = getAgent(provenance.getAgent(), ProvenanceAgentType.AUTHOR.toCode());
		
		//-- author.type
		Coding authorType = authorPac.getType().getCodingFirstRep();
		Assert.assertNull(authorType.getId());
		Assert.assertEquals(authorType.getSystem(), ProvenanceAgentType.AUTHOR.getSystem());
		Assert.assertEquals(authorType.getCode(), ProvenanceAgentType.AUTHOR.toCode());
		Assert.assertEquals(authorType.getDisplay(), ProvenanceAgentType.AUTHOR.getDisplay());

		// author.role
		List<CodeableConcept> authorRoleList = authorPac.getRole();
		Assert.assertEquals(0, authorRoleList.size());
		
		Assert.assertEquals(authorPac.getWho().getReference(), "Practitioner/1ef404c0-4fb5-4f95-a763-7c34a867f4bc");
		Assert.assertEquals(authorPac.getOnBehalfOf().getReference(), "Organization/9fa06037-96da-4287-9e19-5a5dcb20a0b4");
		
		//-- transmitter
		ProvenanceAgentComponent transmitterPac = getAgent(provenance.getAgent(), "transmitter");
		
		// transmitter.type
		Coding transmitterType = transmitterPac.getType().getCodingFirstRep();
		Assert.assertNull(transmitterType.getId());
		Assert.assertEquals(transmitterType.getSystem(), "http://hl7.org/fhir/us/core/CodeSystem/us-core-provenance-participant-type");
		Assert.assertEquals(transmitterType.getCode(), "transmitter");
		Assert.assertEquals(transmitterType.getDisplay(), "Transmitter");

		Assert.assertEquals(transmitterPac.getWho().getReference(), "Practitioner/1ef404c0-4fb5-4f95-a763-7c34a867f4bc");
		Assert.assertEquals(transmitterPac.getOnBehalfOf().getReference(), "Organization/9fa06037-96da-4287-9e19-5a5dcb20a0b4");

		//-- Device
		ProvenanceAgentComponent devicePac = getAgent(provenance.getAgent(), ProvenanceAgentType.ASSEMBLER.toCode());
		
		// device.type
		Coding deviceType = devicePac.getType().getCodingFirstRep();
		Assert.assertNull(deviceType.getId());
		Assert.assertEquals(deviceType.getSystem(), ProvenanceAgentType.ASSEMBLER.getSystem());
		Assert.assertEquals(deviceType.getCode(), ProvenanceAgentType.ASSEMBLER.toCode());
		Assert.assertEquals(deviceType.getDisplay(), ProvenanceAgentType.ASSEMBLER.getDisplay());

		// device role
		Coding deviceRole = devicePac.getRoleFirstRep().getCodingFirstRep();
		Assert.assertNull(deviceRole.getId());
		Assert.assertEquals(deviceRole.getSystem(), ProvenanceAgentRole.ASSEMBLER.getSystem());
		Assert.assertEquals(deviceRole.getCode(), ProvenanceAgentRole.ASSEMBLER.toCode());
		Assert.assertEquals(deviceRole.getDisplay(), ProvenanceAgentRole.ASSEMBLER.getDisplay());

		Assert.assertNull(devicePac.getOnBehalfOf().getReference());
		MatcherAssert.assertThat(devicePac.getWho().getReference(), CoreMatchers.containsString("Device"));

		Assert.assertEquals(provenance.getEntityFirstRep().getWhat().getReference().substring(0, 17),
				"DocumentReference");
		Assert.assertEquals(provenance.getEntityFirstRep().getRole(), ProvenanceEntityRole.SOURCE);
	}
	
	@Test
	public void testProvenanceWithoutAssemblerDevice() throws Exception {
		Bundle testBundle = new Bundle();
		IdType orgId = (new IdType("Organization/1"));
		IdType medId = (new IdType("Medication/1"));
		IdType patientId = (new IdType("Patient/1"));
		IdType compositionId = (new IdType("Composition/1"));

		Composition composition = new Composition();
		composition.setIdElement(compositionId);
		composition.getAuthorFirstRep().setReference("Practitioner/1ef404c0-4fb5-4f95-a763-7c34a867f4bc");
		composition.getCustodian().setReference("Organization/9fa06037-96da-4287-9e19-5a5dcb20a0b4");
			
		testBundle.addEntry(new BundleEntryComponent().setResource(composition));
		testBundle.addEntry(new BundleEntryComponent().setResource(new Organization().setIdElement(orgId)));
		testBundle.addEntry(new BundleEntryComponent().setResource(new Medication().setIdElement(medId)));
		testBundle.addEntry(new BundleEntryComponent().setResource(new Patient().setIdElement(patientId)));

		ITransformationContext transformationContext = new TransformationContextImpl();

		String documentBody = "<ClinicalDoc>Meowmeowmeowmeow</ClinicalDoc>";
		testBundle = rt.tProvenance(testBundle, documentBody, null, transformationContext);

		// Verifies bundle contains the initial resources.
		BundleUtil.findOneResource(testBundle, Organization.class);
		BundleUtil.findOneResource(testBundle, Medication.class);
		BundleUtil.findOneResource(testBundle, Patient.class);

		DocumentReference docRef = BundleUtil.findOneResource(testBundle, DocumentReference.class);

		// Test doc reference.
		Assert.assertEquals(docRef.getStatus().toString(), "CURRENT");
		Assert.assertEquals(docRef.getContent().get(0).getAttachment().getContentType(), "text/plain");
		Assert.assertEquals(docRef.getType().getCoding().get(0).getCode(), "34133-9");

		// Test encoding.
		Assert.assertEquals(docRef.getContent().get(0).getAttachment().getDataElement().getValueAsString(),
				Base64.encode(documentBody.getBytes()));

		// Test hash.
		MessageDigest digest = MessageDigest.getInstance("SHA-1");
		byte[] encodedHash = digest.digest(documentBody.getBytes());
		Assert.assertEquals(docRef.getContent().get(0).getAttachment().getHashElement().getValueAsString(),
				Base64.encode(encodedHash));

		Provenance provenance = BundleUtil.findOneResource(testBundle, Provenance.class);
		Assert.assertEquals(provenance.getTarget().get(0).getReference(), compositionId.getValue());
		Assert.assertEquals(provenance.getTarget().get(1).getReference(), orgId.getValue());
		Assert.assertEquals(provenance.getTarget().get(2).getReference(), medId.getValue());
		Assert.assertEquals(provenance.getTarget().get(3).getReference(), patientId.getValue());
		Assert.assertEquals(provenance.getTarget().get(4).getReference().substring(0, 17), "DocumentReference");

		//-- Author
		ProvenanceAgentComponent authorPac = getAgent(provenance.getAgent(), ProvenanceAgentType.AUTHOR.toCode());
		
		//-- author.type
		Coding authorType = authorPac.getType().getCodingFirstRep();
		Assert.assertNull(authorType.getId());
		Assert.assertEquals(authorType.getSystem(), ProvenanceAgentType.AUTHOR.getSystem());
		Assert.assertEquals(authorType.getCode(), ProvenanceAgentType.AUTHOR.toCode());
		Assert.assertEquals(authorType.getDisplay(), ProvenanceAgentType.AUTHOR.getDisplay());

		// author.role
		List<CodeableConcept> authorRoleList = authorPac.getRole();
		Assert.assertEquals(0, authorRoleList.size());
		
		Assert.assertEquals(authorPac.getWho().getReference(), "Practitioner/1ef404c0-4fb5-4f95-a763-7c34a867f4bc");
		Assert.assertEquals(authorPac.getOnBehalfOf().getReference(), "Organization/9fa06037-96da-4287-9e19-5a5dcb20a0b4");

		//-- transmitter
		ProvenanceAgentComponent transmitterPac = getAgent(provenance.getAgent(), "transmitter");
		
		// transmitter.type
		Coding transmitterType = transmitterPac.getType().getCodingFirstRep();
		Assert.assertNull(transmitterType.getId());
		Assert.assertEquals(transmitterType.getSystem(), "http://hl7.org/fhir/us/core/CodeSystem/us-core-provenance-participant-type");
		Assert.assertEquals(transmitterType.getCode(), "transmitter");
		Assert.assertEquals(transmitterType.getDisplay(), "Transmitter");

		Assert.assertEquals(transmitterPac.getWho().getReference(), "Practitioner/1ef404c0-4fb5-4f95-a763-7c34a867f4bc");
		Assert.assertEquals(transmitterPac.getOnBehalfOf().getReference(), "Organization/9fa06037-96da-4287-9e19-5a5dcb20a0b4");


		//-- device, no device agent
		ProvenanceAgentComponent devicePac = getAgent(provenance.getAgent(), ProvenanceAgentType.ASSEMBLER.toCode());
		Assert.assertNull(devicePac);
		
		Assert.assertEquals(provenance.getEntityFirstRep().getWhat().getReference().substring(0, 17),
				"DocumentReference");
		Assert.assertEquals(provenance.getEntityFirstRep().getRole(), ProvenanceEntityRole.SOURCE);
	}
	
	private ProvenanceAgentComponent getAgent(List<ProvenanceAgentComponent> agents, String type) {
		
		for (ProvenanceAgentComponent agent : agents) {
			if (type.equals(agent.getType().getCodingFirstRep().getCode()))
				return agent;
		}
		return null;
	}
}
