package tr.com.srdc.cda2fhir;

import java.util.List;
import java.util.Map;

import org.eclipse.emf.common.util.BasicDiagnostic;
import org.eclipse.emf.common.util.DiagnosticChain;
import org.hl7.fhir.r4.model.Bundle;
import org.hl7.fhir.r4.model.CodeableConcept;
import org.hl7.fhir.r4.model.Coding;
import org.hl7.fhir.r4.model.Condition;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import org.openhealthtools.mdht.uml.cda.consol.ProblemConcernAct;
import org.openhealthtools.mdht.uml.cda.consol.impl.ConsolFactoryImpl;
import org.openhealthtools.mdht.uml.cda.consol.impl.ProblemConcernActImpl;
import org.openhealthtools.mdht.uml.cda.consol.impl.ProblemObservationImpl;
import org.openhealthtools.mdht.uml.cda.util.CDAUtil;
import org.openhealthtools.mdht.uml.hl7.datatypes.CD;
import org.openhealthtools.mdht.uml.hl7.datatypes.CS;
import org.openhealthtools.mdht.uml.hl7.datatypes.DatatypesFactory;
import org.openhealthtools.mdht.uml.hl7.datatypes.ED;
import org.openhealthtools.mdht.uml.hl7.datatypes.IVL_TS;
import org.openhealthtools.mdht.uml.hl7.datatypes.TEL;
import org.openhealthtools.mdht.uml.hl7.datatypes.impl.CDImpl;
import org.openhealthtools.mdht.uml.hl7.datatypes.impl.DatatypesFactoryImpl;
import org.openhealthtools.mdht.uml.hl7.vocab.NullFlavor;
import org.openhealthtools.mdht.uml.hl7.vocab.x_ActRelationshipEntryRelationship;

import com.bazaarvoice.jolt.JsonUtils;
import com.helger.commons.collection.attr.StringMap;

import tr.com.srdc.cda2fhir.testutil.BundleUtil;
import tr.com.srdc.cda2fhir.transform.ResourceTransformerImpl;
import tr.com.srdc.cda2fhir.transform.util.ITransformationContext;
import tr.com.srdc.cda2fhir.transform.util.impl.BundleInfo;
import tr.com.srdc.cda2fhir.transform.util.impl.TransformationContextImpl;

import static tr.com.srdc.cda2fhir.transform.ResourceTransformerImpl.CODE_SYSTEM_CONDITION_CATEGORY;

public class ProblemConcernActTest {
	private static final ResourceTransformerImpl rt = new ResourceTransformerImpl();

	private static ConsolFactoryImpl cdaObjFactory;
	private static DatatypesFactory cdaTypeFactory;

	private static Map<String, Object> verificationStatusMap = JsonUtils
			.filepathToMap("src/test/resources//value-maps/ConditionVerificationStatus.json");

	@BeforeClass
	public static void init() {
		CDAUtil.loadPackages();

		cdaObjFactory = (ConsolFactoryImpl) ConsolFactoryImpl.init();
		cdaTypeFactory = DatatypesFactoryImpl.init();
	}

	private static ProblemConcernActImpl createProblemConcernAct() {
		ProblemConcernActImpl act = (ProblemConcernActImpl) cdaObjFactory.createProblemConcernAct();
		ProblemObservationImpl observation = (ProblemObservationImpl) cdaObjFactory.createProblemObservation();
		act.addObservation(observation);
		act.getEntryRelationships().stream().filter(r -> (r.getObservation() == observation))
				.forEach(r -> r.setTypeCode(x_ActRelationshipEntryRelationship.SUBJ));
		return act;
	}

	static private void verifyCoding(Coding coding, String code, String displayName, String system) {
		Assert.assertEquals("Unexpected Coding code", code, coding.getCode());
		Assert.assertEquals("Unexpected Coding display name", displayName, coding.getDisplay());
		Assert.assertEquals("Unexpected Coding system", system, coding.getSystem());
	}

	@Test
	public void testProblemObservationCode() throws Exception {
		ProblemConcernActImpl act = createProblemConcernAct();
		ProblemObservationImpl observation = (ProblemObservationImpl) act.getEntryRelationships().get(0)
				.getObservation();
		DiagnosticChain dxChain = new BasicDiagnostic();
		BundleInfo bundleInfo = new BundleInfo(rt);
		ITransformationContext transformationContext = new TransformationContextImpl();

		String code = "404684003"; // From CCDA Specification
		String displayName = "Finding";
		CDImpl cd = (CDImpl) cdaTypeFactory.createCD(code, "2.16.840.1.113883.6.96", "SNOMED CT", displayName);
		observation.setCode(cd);

		Boolean validation = act.validateProblemConcernActProblemObservation(dxChain, null);
		Assert.assertTrue("Invalid Problem Concern Act in Test", validation);

		Bundle bundle = rt.tProblemConcernAct2Condition(act, bundleInfo, transformationContext).getBundle();
		Condition condition = BundleUtil.findOneResource(bundle, Condition.class);
		List<Coding> category = condition.getCategory().get(0).getCoding();
		Assert.assertEquals("Unexpected number of category codings", 1, category.size());
		verifyCoding(category.get(0), "problem-list-item", "Problem List Item",
				CODE_SYSTEM_CONDITION_CATEGORY);

		String translationCode = "75321-0"; // From CCDA Specification
		String translationDisplayName = "Clinical finding HL7.CCDAR2";
		CD translationCd = cdaTypeFactory.createCD(translationCode, "2.16.840.1.113883.6.1", "LOINC",
				translationDisplayName);
		cd.getTranslations().add(translationCd);

		Boolean validation2 = act.validateProblemConcernActProblemObservation(dxChain, null);
		Assert.assertTrue("Invalid Problem Concern Act in Test", validation2);

		Bundle bundle2 = rt.tProblemConcernAct2Condition(act, bundleInfo, transformationContext).getBundle();
		Condition condition2 = BundleUtil.findOneResource(bundle2, Condition.class);
		List<Coding> category2 = condition2.getCategory().get(0).getCoding();
		Assert.assertEquals("Unexpected number of category codings", 1, category2.size());
		verifyCoding(category2.get(0), "problem-list-item", "Problem List Item",
				CODE_SYSTEM_CONDITION_CATEGORY);

	}

	@Test
	public void testProblemObservationProblemStatusInactive() throws Exception {
		ProblemConcernActImpl act = createProblemConcernAct();
		act.setStatusCode(cdaTypeFactory.createCS("completed"));

		BundleInfo bundleInfo = new BundleInfo(rt);
		ITransformationContext transformationContext = new TransformationContextImpl();
		Bundle bundle = rt.tProblemConcernAct2Condition(act, bundleInfo, transformationContext).getBundle();
		Condition condition = BundleUtil.findOneResource(bundle, Condition.class);
		CodeableConcept clinicalStatus = condition.getClinicalStatus();
		String actual = clinicalStatus.getCodingFirstRep().getCode();
		Assert.assertEquals("Inactive Problem with completed status", "inactive", actual);

	}

	@Test
	public void testProblemObservationProblemStatusActive() throws Exception {
		ProblemConcernActImpl act = createProblemConcernAct();
		ProblemObservationImpl observation = (ProblemObservationImpl) act.getEntryRelationships().get(0)
				.getObservation();

		String low = "2018-01-01";

		IVL_TS interval = cdaTypeFactory.createIVL_TS(low);

		observation.setEffectiveTime(interval);
		BundleInfo bundleInfo = new BundleInfo(rt);
		ITransformationContext transformationContext = new TransformationContextImpl();
		Bundle bundle = rt.tProblemConcernAct2Condition(act, bundleInfo, transformationContext).getBundle();
		Condition condition = BundleUtil.findOneResource(bundle, Condition.class);
		CodeableConcept clinicalStatus = condition.getClinicalStatus();
		String actual = clinicalStatus.getCodingFirstRep().getCode();
		Assert.assertEquals("Active Problem without high value", "active", actual);

	}

	@Test
	public void testProblemObservationProblemStatusActiveNoDate() throws Exception {
		ProblemConcernActImpl act = createProblemConcernAct();

		BundleInfo bundleInfo = new BundleInfo(rt);
		ITransformationContext transformationContext = new TransformationContextImpl();
		Bundle bundle = rt.tProblemConcernAct2Condition(act, bundleInfo, transformationContext).getBundle();
		Condition condition = BundleUtil.findOneResource(bundle, Condition.class);
		CodeableConcept clinicalStatus = condition.getClinicalStatus();
		String actual = clinicalStatus.getCodingFirstRep().getCode();
		Assert.assertEquals("Active Problem without no value defaults to active", "active", actual);

	}

	static private void verifyConditionVerificationStatus(ProblemConcernAct act, String expected) throws Exception {
		BundleInfo bundleInfo = new BundleInfo(rt);
		ITransformationContext transformationContext = new TransformationContextImpl();
		Bundle bundle = rt.tProblemConcernAct2Condition(act, bundleInfo, transformationContext).getBundle();
		Condition condition = BundleUtil.findOneResource(bundle, Condition.class);

		CodeableConcept verificationStatus = condition.getVerificationStatus();
		String actual = verificationStatus == null ? null : verificationStatus.getCodingFirstRep().getCode();

		Assert.assertEquals(expected, actual);
	}

	@Test
	public void testVerificationStatusCode() throws Exception {
		ProblemConcernActImpl act = (ProblemConcernActImpl) cdaObjFactory.createProblemConcernAct();
		ProblemObservationImpl observation = (ProblemObservationImpl) cdaObjFactory.createProblemObservation();
		observation.setNegationInd(Boolean.FALSE);
		act.addObservation(observation);

		verifyConditionVerificationStatus(act, "confirmed");

		observation.setNegationInd(Boolean.TRUE);
		verifyConditionVerificationStatus(act, "refuted");
	}

	@Test
	public void testCodeOriginalText() throws Exception {

		ProblemConcernActImpl act = (ProblemConcernActImpl) cdaObjFactory.createProblemConcernAct();
		ProblemObservationImpl observation = (ProblemObservationImpl) cdaObjFactory.createProblemObservation();

		BundleInfo bundleInfo = new BundleInfo(rt);
		String expectedValue = "freetext entry";
		String referenceValue = "fakeid1";
		CD cd = cdaTypeFactory.createCD();
		ED ed = cdaTypeFactory.createED();
		TEL tel = cdaTypeFactory.createTEL();
		tel.setValue("#" + referenceValue);
		ed.setReference(tel);
		cd.setCode("code");
		cd.setCodeSystem("codeSystem");
		cd.setOriginalText(ed);
		Map<String, String> idedAnnotations = new StringMap();
		idedAnnotations.put(referenceValue, expectedValue);
		bundleInfo.mergeIdedAnnotations(idedAnnotations);
		ITransformationContext transformationContext = new TransformationContextImpl();

		observation.getValues().add(cd);
		act.addObservation(observation);
		Bundle bundle = rt.tProblemConcernAct2Condition(act, bundleInfo, transformationContext).getBundle();
		Condition condition = BundleUtil.findOneResource(bundle, Condition.class);
		CodeableConcept cc = condition.getCode();
		Assert.assertEquals("Condition Code text value assigned", expectedValue, cc.getText());

	}

	@Test
	public void testCode_noKnownProblem() throws Exception {

		ProblemConcernActImpl act = (ProblemConcernActImpl) cdaObjFactory.createProblemConcernAct();
		ProblemObservationImpl observation = (ProblemObservationImpl) cdaObjFactory.createProblemObservation();

		BundleInfo bundleInfo = new BundleInfo(rt);
		ITransformationContext transformationContext = new TransformationContextImpl();

		CD diseaseCode = cdaTypeFactory.createCD();
		diseaseCode.setCodeSystem("2.16.840.1.113883.6.96");
		diseaseCode.setCode("64572001");
		diseaseCode.setCodeSystemName("SNOMED CT");
		diseaseCode.setDisplayName("Disease (disorder)");

		observation.getValues().add(diseaseCode);
		observation.setNegationInd(true);
		act.addObservation(observation);
		Bundle bundle = rt.tProblemConcernAct2Condition(act, bundleInfo, transformationContext).getBundle();
		Condition condition = BundleUtil.findOneResource(bundle, Condition.class);
		CodeableConcept cc = condition.getCode();
		Assert.assertEquals("Condition Code code", "160245001", cc.getCodingFirstRep().getCode());
		Assert.assertEquals("Condition Code system", "http://snomed.info/sct", cc.getCodingFirstRep().getSystem());
		Assert.assertEquals("Condition Code display", "No current problems or disability (situation)", cc.getCodingFirstRep().getDisplay());

	}

	@Test
	public void testCode_noKnownProblem_alternateCode() throws Exception {

		ProblemConcernActImpl act = (ProblemConcernActImpl) cdaObjFactory.createProblemConcernAct();
		ProblemObservationImpl observation = (ProblemObservationImpl) cdaObjFactory.createProblemObservation();

		BundleInfo bundleInfo = new BundleInfo(rt);
		ITransformationContext transformationContext = new TransformationContextImpl();

		CD diseaseCode = cdaTypeFactory.createCD();
		diseaseCode.setCodeSystem("2.16.840.1.113883.6.96");
		diseaseCode.setCode("55607006");
		diseaseCode.setCodeSystemName("SNOMED CT");
		diseaseCode.setDisplayName("Problem");

		observation.getValues().add(diseaseCode);
		observation.setNegationInd(true);
		act.addObservation(observation);
		Bundle bundle = rt.tProblemConcernAct2Condition(act, bundleInfo, transformationContext).getBundle();
		Condition condition = BundleUtil.findOneResource(bundle, Condition.class);
		CodeableConcept cc = condition.getCode();
		Assert.assertEquals("Condition Code code", "160245001", cc.getCodingFirstRep().getCode());
		Assert.assertEquals("Condition Code system", "http://snomed.info/sct", cc.getCodingFirstRep().getSystem());
		Assert.assertEquals("Condition Code display", "No current problems or disability (situation)", cc.getCodingFirstRep().getDisplay());

	}
}
