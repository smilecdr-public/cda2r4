package tr.com.srdc.cda2fhir;

/*
 * #%L
 * CDA to FHIR Transformer Library
 * %%
 * Copyright (C) 2016 SRDC Yazilim Arastirma ve Gelistirme ve Danismanlik Tic. A.S.
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import ca.uhn.fhir.model.api.IResource;
import com.google.common.collect.Lists;
import org.eclipse.emf.common.util.EList;
import org.hl7.fhir.r4.model.*;
import org.hl7.fhir.r4.model.Bundle.BundleEntryComponent;
import org.hl7.fhir.r4.model.Device;
import org.hl7.fhir.r4.model.Encounter;
import org.hl7.fhir.r4.model.Observation;
import org.hl7.fhir.r4.model.Organization;
import org.hl7.fhir.r4.model.Patient;
import org.hl7.fhir.r4.model.Composition.SectionComponent;
import org.hl7.fhir.r4.model.Patient.ContactComponent;
import org.hl7.fhir.r4.model.Procedure;
import org.hl7.fhir.r4.model.Reference;
import org.hl7.fhir.r4.model.Patient.PatientCommunicationComponent;
import org.hl7.fhir.r4.model.codesystems.ConditionVerStatus;
import org.hl7.fhir.r4.model.codesystems.ObservationCategory;
import org.hl7.fhir.r4.model.codesystems.V3ActCode;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import org.openhealthtools.mdht.uml.cda.Author;
import org.openhealthtools.mdht.uml.cda.LanguageCommunication;
import org.openhealthtools.mdht.uml.cda.PatientRole;
import org.openhealthtools.mdht.uml.cda.Place;
import org.openhealthtools.mdht.uml.cda.*;
import org.openhealthtools.mdht.uml.cda.consol.*;
import org.openhealthtools.mdht.uml.cda.util.CDAUtil;
import org.openhealthtools.mdht.uml.hl7.datatypes.*;
import org.openhealthtools.mdht.uml.hl7.vocab.NullFlavor;
import org.openhealthtools.mdht.uml.hl7.vocab.ParticipationType;
import org.openhealthtools.mdht.uml.hl7.vocab.x_ActClassDocumentEntryOrganizer;
import org.openhealthtools.mdht.uml.hl7.vocab.x_ActRelationshipEntryRelationship;
import tr.com.srdc.cda2fhir.testutil.CDAFactories;
import tr.com.srdc.cda2fhir.testutil.generator.*;
import tr.com.srdc.cda2fhir.transform.DataTypesTransformerImpl;
import tr.com.srdc.cda2fhir.transform.IDataTypesTransformer;
import tr.com.srdc.cda2fhir.transform.ResourceTransformerImpl;
import tr.com.srdc.cda2fhir.transform.ValueSetsTransformerImpl;
import tr.com.srdc.cda2fhir.transform.entry.IEntryResult;
import tr.com.srdc.cda2fhir.transform.entry.impl.EntryResult;
import tr.com.srdc.cda2fhir.transform.util.ITransformationContext;
import tr.com.srdc.cda2fhir.transform.util.ITransformationMessage;
import tr.com.srdc.cda2fhir.transform.util.impl.BundleInfo;
import tr.com.srdc.cda2fhir.transform.util.impl.TransformationContextImpl;
import tr.com.srdc.cda2fhir.util.FHIRUtil;

import javax.annotation.Nonnull;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.math.BigDecimal;
import java.nio.charset.StandardCharsets;
import java.text.MessageFormat;
import java.util.*;
import java.util.stream.Collectors;

import static org.hl7.fhir.r4.model.Narrative.NarrativeStatus.ADDITIONAL;
import static tr.com.srdc.cda2fhir.transform.DataTypesTransformerImpl.*;
import static tr.com.srdc.cda2fhir.transform.ImplantedDeviceHelper.*;
import static tr.com.srdc.cda2fhir.transform.ResourceTransformerImpl.*;
import static tr.com.srdc.cda2fhir.transform.ValueSetsTransformerImpl.*;

public class ResourceTransformerTest {

	private static final ResourceTransformerImpl rt = new ResourceTransformerImpl();
	private static final ValueSetsTransformerImpl vsti = new ValueSetsTransformerImpl();
	private static FileInputStream fisCCD;
	private static FileInputStream fisCCD2;
	private static FileWriter resultFW;
	private static ContinuityOfCareDocument ccd;
	private static ContinuityOfCareDocument ccd2;
	private static final String resultFilePath = "src/test/resources/output/ResourceTransformerTest.txt";
	private static final String transformationStartMsg = "\n# TRANSFORMATION STARTING..\n";
	private static final String transformationEndMsg = "# END OF TRANSFORMATION.\n";
	private static final String endOfTestMsg = "\n## END OF TEST\n";

	@BeforeClass
	public static void init() {
		CDAUtil.loadPackages();

		// read the input test file
		try {
			fisCCD = new FileInputStream("src/test/resources/C-CDA_R2-1_CCD.xml");
			fisCCD2 = new FileInputStream("src/test/resources/C-CDA_R2-1_CCD2.xml"); // Original does not have authoring
																						// device.
			ccd = (ContinuityOfCareDocument) CDAUtil.load(fisCCD);
			ccd2 = (ContinuityOfCareDocument) CDAUtil.load(fisCCD2);
		} catch (Exception ex) {
			ex.printStackTrace();
		}

		// init the output file writer
		File resultFile = new File(resultFilePath);
		resultFile.getParentFile().mkdirs();
		try {
			resultFW = new FileWriter(resultFile);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	@AfterClass
	public static void finalise() {
		try {
			resultFW.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	@Test
	public void referenceOrgNameStringTest() {
		String name = "Organization Name";
		Organization org = new Organization();
		org.setName(name);
		Reference ref = rt.getReference(org);
		Assert.assertEquals("Organization name becomes reference display", ref.getDisplay(), name);
	}

	@Test
	public void referenceHumanNameTest() {
		String firstName = "Homer";
		String lastName = "Simpson";
		HumanName name = new HumanName();
		name.setFamily(lastName);
		name.addGiven(firstName);
		Patient patient = new Patient();
		patient.addName(name);

		Reference ref = rt.getReference(patient);
		Assert.assertEquals("Organization name becomes reference display", ref.getDisplay(),
				name.getNameAsSingleString());
	}

	@Test
	public void referenceImmunizationTest() {
		CodeableConcept cc = new CodeableConcept();
		String expectedDisplay = "expected display";
		cc.setText(expectedDisplay);

		Immunization immunization = new Immunization();

		immunization.setVaccineCode(cc);

		Reference ref = rt.getReference(immunization);
		Assert.assertEquals("Organization name becomes reference display", ref.getDisplay(), expectedDisplay);
	}

	@Test
	public void referenceDisplayFirstRepTest() {
		Reference ref;
		CodeableConcept cc = new CodeableConcept();
		String expectedDisplay = "expected display";
		Coding coding = new Coding();
		coding.setDisplay(expectedDisplay);
		cc.addCoding(coding);

		PractitionerRole pracRole = new PractitionerRole();
		Procedure procedure = new Procedure();
		pracRole.addCode(cc);
		procedure.setCode(cc);

		ref = rt.getReference(pracRole);
		Assert.assertEquals("Organization name becomes reference display", ref.getDisplay(), expectedDisplay);

		ref = rt.getReference(procedure);
		Assert.assertEquals("Organization name becomes reference display", ref.getDisplay(), expectedDisplay);
	}

	@Test
	public void referenceCodeTest() {
		Reference ref;
		String expectedTestID = "testID/0";
		String expectedDisplay = "expected display";

		CodeableConcept cc = new CodeableConcept();
		cc.setText(expectedDisplay);

		Medication med = new Medication();

		IdType id = new IdType("testID", "0");
		med.setId(id);
		med.setCode(cc);

		ref = rt.getReference(med);

		Assert.assertTrue(ref.getReference().contentEquals(expectedTestID));

		MedicationStatement medStatement = new MedicationStatement();

		medStatement.setId(id);

		ref = rt.getReference(medStatement);

		Assert.assertTrue(ref.getReference().contentEquals(expectedTestID));

		Condition condition = new Condition();

		condition.setId(id);
		condition.setCode(cc);

		ref = rt.getReference(condition);

		Assert.assertTrue(ref.getReference().contentEquals(expectedTestID));

	}

	// Most of the test methods just print the transformed object in JSON form.

	@Test
	public void testAllergyProblemAct2AllergyIntolerance() {
		appendToResultFile("## TEST: AllergyProblemAct2AllergyIntolerance\n");
		// null instance test
		AllergyProblemAct cdaNull = null;
		BundleInfo bundleInfo = new BundleInfo(rt);
		ITransformationContext transformationContext = new TransformationContextImpl();
		Bundle fhirNull = rt.tAllergyProblemAct2AllergyIntolerance(cdaNull, bundleInfo, transformationContext).getBundle();
		Assert.assertNull(fhirNull);

		// instances from file
		for (AllergyProblemAct cdaAPA : ResourceTransformerTest.ccd.getAllergiesSection().getAllergyProblemActs()) {
			appendToResultFile(transformationStartMsg);
			Bundle allergyBundle = rt.tAllergyProblemAct2AllergyIntolerance(cdaAPA, bundleInfo, transformationContext).getBundle();
			appendToResultFile(transformationEndMsg);
			appendToResultFile(allergyBundle);
		}
		appendToResultFile(endOfTestMsg);
	}

	@Test
	public void testAllergyProblemAct2AllergyIntolerance_dataAbsentReason() {
		appendToResultFile("## TEST: AllergyProblemAct2AllergyIntolerance\n");

		// given
		BundleInfo bundleInfo = new BundleInfo(rt);
		ITransformationContext transformationContext = new TransformationContextImpl();
		AllergyProblemAct cdaAPA = ResourceTransformerTest.ccd.getAllergiesSection().getAllergyProblemActs().get(0);
		cdaAPA.getIds().clear();
		II nullII = CDAFactories.init().datatype.createII(NullFlavor.NI);
		cdaAPA.getIds().add(nullII);

		// when
		appendToResultFile(transformationStartMsg);
		Bundle allergyBundle = rt.tAllergyProblemAct2AllergyIntolerance(cdaAPA, bundleInfo, transformationContext).getBundle();
		appendToResultFile(transformationEndMsg);
		appendToResultFile(allergyBundle);

		Optional<Resource> maybeAllergyIntolerance = getFirstResourceOfType(allergyBundle, Enumerations.ResourceType.ALLERGYINTOLERANCE);
		AllergyIntolerance allergyIntolerance = null;
		if (maybeAllergyIntolerance.isPresent()) {
			allergyIntolerance = (AllergyIntolerance) maybeAllergyIntolerance.get();
		}

		// then
		Assert.assertNotNull(allergyIntolerance);
		Identifier identifier = allergyIntolerance.getIdentifierFirstRep();
		Assert.assertTrue(identifier.hasExtension());
		verifyExtension(identifier.getExtensionFirstRep(), "error", "Error");

		appendToResultFile(endOfTestMsg);
	}

	@Test
	public void testAllergyProblemAct2AllergyIntolerance_noKnownAllergies() {
		appendToResultFile("## TEST: AllergyProblemAct2AllergyIntolerance\n");

		// given
		BundleInfo bundleInfo = new BundleInfo(rt);
		ITransformationContext transformationContext = new TransformationContextImpl();
		AllergyProblemAct cdaAPA = ResourceTransformerTest.ccd.getAllergiesSection().getAllergyProblemActs().get(0);
		cdaAPA.getAllergyObservations().get(0).setNegationInd(true);

		// when
		appendToResultFile(transformationStartMsg);
		Bundle allergyBundle = rt.tAllergyProblemAct2AllergyIntolerance(cdaAPA, bundleInfo, transformationContext).getBundle();
		appendToResultFile(transformationEndMsg);
		appendToResultFile(allergyBundle);

		Optional<Resource> maybeAllergyIntolerance = getFirstResourceOfType(allergyBundle, Enumerations.ResourceType.ALLERGYINTOLERANCE);
		AllergyIntolerance allergyIntolerance = null;
		if (maybeAllergyIntolerance.isPresent()) {
			allergyIntolerance = (AllergyIntolerance) maybeAllergyIntolerance.get();
		}

		// then
		Assert.assertNotNull(allergyIntolerance);
		Assert.assertEquals("http://snomed.info/sct", allergyIntolerance.getCode().getCodingFirstRep().getSystem());
		Assert.assertEquals("716186003", allergyIntolerance.getCode().getCodingFirstRep().getCode());
		Assert.assertEquals("No Known Allergy (situation)", allergyIntolerance.getCode().getCodingFirstRep().getDisplay());

		appendToResultFile(endOfTestMsg);
	}

	@Test
	public void testAssignedAuthor2Device() {
		appendToResultFile("## TEST: AssignedAuthor2Device\n");
		// null instance test
		BundleInfo bundleInfo = new BundleInfo(rt);
		ITransformationContext transformationContext = new TransformationContextImpl();
		org.openhealthtools.mdht.uml.cda.AssignedAuthor cdaNull = null;
		Bundle fhirNull = rt.tAssignedAuthor2Device(cdaNull, bundleInfo, transformationContext).getBundle();
		Assert.assertNull(fhirNull);

		// instances from file
		if (ResourceTransformerTest.ccd2.getAuthors() != null) {
			for (org.openhealthtools.mdht.uml.cda.Author author : ResourceTransformerTest.ccd2.getAuthors()) {
				// traversing authors
				if (author != null && author.getAssignedAuthor() != null) {
					appendToResultFile(transformationStartMsg);
					Bundle deviceBundle = rt.tAssignedAuthor2Device(author.getAssignedAuthor(), bundleInfo, transformationContext).getBundle();
					appendToResultFile(transformationEndMsg);
					appendToResultFile(deviceBundle);
				}
			}
		}
		appendToResultFile(endOfTestMsg);
	}

	@Test
	public void testAssignedAuthor2Practitioner() {
		appendToResultFile("## TEST: AssignedAuthor2Practitioner\n");
		// null instance test
		BundleInfo bundleInfo = new BundleInfo(rt);
		ITransformationContext transformationContext = new TransformationContextImpl();
		org.openhealthtools.mdht.uml.cda.AssignedAuthor cdaNull = null;
		Bundle fhirNull = rt.tAssignedAuthor2Practitioner(cdaNull, bundleInfo, transformationContext).getBundle();
		Assert.assertNull(fhirNull);

		// instances from file
		if (ResourceTransformerTest.ccd.getAuthors() != null) {
			for (org.openhealthtools.mdht.uml.cda.Author author : ResourceTransformerTest.ccd.getAuthors()) {
				// traversing authors
				if (author != null && author.getAssignedAuthor() != null) {
					appendToResultFile(transformationStartMsg);
					Bundle practitionerBundle = rt.tAssignedAuthor2Practitioner(author.getAssignedAuthor(), bundleInfo, transformationContext)
							.getBundle();
					appendToResultFile(transformationEndMsg);
					appendToResultFile(practitionerBundle);
				}
			}
		}
		appendToResultFile(endOfTestMsg);
	}

	@Test
	public void testAssignedEntity2Practitioner() {
		appendToResultFile("## TEST: AssignedEntity2Practitioner\n");
		// null instance test
		org.openhealthtools.mdht.uml.cda.AssignedEntity cdaNull = null;
		BundleInfo bundleInfo = new BundleInfo(rt);
		ITransformationContext transformationContext = new TransformationContextImpl();
		Bundle fhirNull = rt.tAssignedEntity2Practitioner(cdaNull, bundleInfo, transformationContext).getBundle();
		Assert.assertNull(fhirNull);

		// instances from file
		if (ResourceTransformerTest.ccd.getProceduresSection() != null
				&& !ResourceTransformerTest.ccd.getProceduresSection().isSetNullFlavor()) {
			if (ResourceTransformerTest.ccd.getProceduresSection().getProcedures() != null
					&& !ResourceTransformerTest.ccd.getProceduresSection().getProcedures().isEmpty()) {
				for (org.openhealthtools.mdht.uml.cda.Procedure procedure : ResourceTransformerTest.ccd
						.getProceduresSection().getProcedures()) {
					// traversing procedures
					if (procedure.getPerformers() != null && !procedure.getPerformers().isEmpty()) {
						for (org.openhealthtools.mdht.uml.cda.Performer2 performer : procedure.getPerformers()) {
							if (performer.getAssignedEntity() != null
									&& !performer.getAssignedEntity().isSetNullFlavor()) {
								appendToResultFile(transformationStartMsg);
								Bundle fhirPractitionerBundle = rt
										.tAssignedEntity2Practitioner(performer.getAssignedEntity(), bundleInfo, transformationContext)
										.getBundle();
								appendToResultFile(transformationEndMsg);
								appendToResultFile(fhirPractitionerBundle);
							}
						}
					}
				}
			}
		}
		appendToResultFile(endOfTestMsg);
	}

	@Test
	public void testClinicalDocument2Composition() {
		appendToResultFile("## TEST: ClinicalDocument2Composition\n");
		// null instance test
		org.openhealthtools.mdht.uml.cda.consol.ContinuityOfCareDocument cdaNull = null;
		ITransformationContext transformationContext = new TransformationContextImpl();
		Bundle fhirNull = rt.tClinicalDocument2Composition(cdaNull, transformationContext).getBundle();
		Assert.assertNull(fhirNull);

		// instance from file
		if (ResourceTransformerTest.ccd != null && !ResourceTransformerTest.ccd.isSetNullFlavor()) {
			appendToResultFile(transformationStartMsg);
			Bundle fhirComp = rt.tClinicalDocument2Composition(ResourceTransformerTest.ccd, transformationContext).getBundle();
			appendToResultFile(transformationEndMsg);
			appendToResultFile(fhirComp);
		}
		appendToResultFile(endOfTestMsg);
	}

	@Test
	public void testEncounterActivity2Encounter() {
		appendToResultFile("## TEST: EncounterActivity2Encounter\n");
		// null instance test
		org.openhealthtools.mdht.uml.cda.consol.EncounterActivities cdaNull = null;
		BundleInfo bundleInfo = new BundleInfo(rt);
		ITransformationContext transformationContext = new TransformationContextImpl();
		Bundle fhirNull = rt.tEncounterActivity2Encounter(cdaNull, bundleInfo, transformationContext).getBundle();
		Assert.assertNull(fhirNull);

		// instances from file
		if (ResourceTransformerTest.ccd.getEncountersSection() != null
				&& !ResourceTransformerTest.ccd.getEncountersSection().isSetNullFlavor()) {
			if (ResourceTransformerTest.ccd.getEncountersSection().getEncounterActivitiess() != null
					&& !ResourceTransformerTest.ccd.getEncountersSection().getEncounterActivitiess().isEmpty()) {
				for (org.openhealthtools.mdht.uml.cda.consol.EncounterActivities encounterActivity : ResourceTransformerTest.ccd
						.getEncountersSection().getEncounterActivitiess()) {
					if (encounterActivity != null && !encounterActivity.isSetNullFlavor()) {
						appendToResultFile(transformationStartMsg);
						Bundle fhirEncounterBundle = rt.tEncounterActivity2Encounter(encounterActivity, bundleInfo, transformationContext)
								.getBundle();
						appendToResultFile(transformationEndMsg);
						appendToResultFile(fhirEncounterBundle);
					}
				}
			}
		}
		appendToResultFile(endOfTestMsg);
	}

	@Test
	public void testEncounterActivity2Encounter_ActivityCodeTranslation() {
		appendToResultFile("## TEST: EncounterActivity2Encounter_ActivityCodeTranslation\n");

		// given
		org.openhealthtools.mdht.uml.cda.consol.EncounterActivities encounterActivities = EncounterActivityGenerator.getDefaultInstance().generate(CDAFactories.init());

		String code = "IMP";
		CD cd = makeTestCD(code);
		// to test code.translation, we need to provide yet another CD object that *is* the translation
		CD translatedCd = makeTestCD(code);

		cd.getTranslations().add(translatedCd);

		encounterActivities.setCode(cd);

		BundleInfo bundleInfo = new BundleInfo(rt);
		ITransformationContext transformationContext = new TransformationContextImpl();

	    // when
		appendToResultFile(transformationStartMsg);
		Bundle bundle = rt.tEncounterActivity2Encounter(encounterActivities, bundleInfo, transformationContext).getBundle();
		appendToResultFile(transformationEndMsg);
		appendToResultFile(bundle);

		Optional<Resource> maybeEncounter = getFirstResourceOfType(bundle, Enumerations.ResourceType.ENCOUNTER);
		Encounter encounter = null;
		if (maybeEncounter.isPresent()) {
			encounter = (Encounter) maybeEncounter.get();
		}

	    // then
		Assert.assertNotNull(encounter);
		Assert.assertFalse(transformationContext.hasErrors());
		Assert.assertEquals(encounter.getClass_().getCode(), V3ActCode.IMP.toCode());

		appendToResultFile(endOfTestMsg);
	}

	@Test
	public void testEncounterActivity2Encounter_ActivityCode_MultipleTranslations() {
		appendToResultFile("## TEST: EncounterActivity2Encounter_ActivityCodeTranslation\n");

		// given
		BundleInfo bundleInfo = new BundleInfo(rt);
		ITransformationContext transformationContext = new TransformationContextImpl();
		CDAFactories factories = CDAFactories.init();
		org.openhealthtools.mdht.uml.cda.consol.EncounterActivities encounterActivities = EncounterActivityGenerator.getDefaultInstance().generate(CDAFactories.init());

		String piercingCode = "879862001";

		String code = "IMP";
		CD cd = makeTestCD(code);
		// to test code.translation, we need to provide yet another CD object that *is* the translation
		CD translatedCd = makeTestCD(code);
		String otherCode = "AMB";
		CD translatedCd2 = makeTestCD(otherCode);

		cd.getTranslations().add(translatedCd);
		cd.getTranslations().add(translatedCd2);
		encounterActivities.setCode(cd);

		org.openhealthtools.mdht.uml.cda.Observation cdaObservation = factories.base.createObservation();
		cdaObservation.getTemplateIds().add(factories.datatype.createII(INDICATION_V2_OID));
		cdaObservation.getValues().add(factories.datatype.createCD(piercingCode, SNOMED_OID, "SNOMED CT", "Body piercing (finding)"));
		EntryRelationship entryRelationship = factories.base.createEntryRelationship();
		entryRelationship.setTypeCode(x_ActRelationshipEntryRelationship.RSON);
		entryRelationship.setObservation(cdaObservation);

		encounterActivities.getEntryRelationships().add(entryRelationship);
		encounterActivities.setEffectiveTime(factories.datatype.createIVL_TS("20220903", "20220907"));

		// when
		appendToResultFile(transformationStartMsg);
		Bundle bundle = rt.tEncounterActivity2Encounter(encounterActivities, bundleInfo, transformationContext).getBundle();
		appendToResultFile(transformationEndMsg);
		appendToResultFile(bundle);

		Optional<Resource> maybeEncounter = getFirstResourceOfType(bundle, Enumerations.ResourceType.ENCOUNTER);
		Encounter encounter = null;
		if (maybeEncounter.isPresent()) {
			encounter = (Encounter) maybeEncounter.get();
		}

		// then
		Assert.assertNotNull(encounter);
		Assert.assertFalse(transformationContext.hasErrors());
		Assert.assertEquals(MessageFormat.format(MULTIPLE_CODES_FOUND, code + ", " + otherCode), transformationContext.getWarnings().get(0).getMessage());
		Assert.assertEquals(V3ActCode.IMP.toCode(), encounter.getClass_().getCode());

		Assert.assertEquals("PART", encounter.getParticipantFirstRep().getTypeFirstRep().getCodingFirstRep().getCode());
		Assert.assertEquals("http://terminology.hl7.org/CodeSystem/v3-ParticipationType", encounter.getParticipantFirstRep().getTypeFirstRep().getCodingFirstRep().getSystem());

		// snomed/clinical-finding/body-piercing
		Assert.assertTrue(encounter.getReasonCode().stream()
				.map(CodeableConcept::getCodingFirstRep)
				.map(coding -> List.of(coding.getCode(), coding.getSystem()))
				.collect(Collectors.toList())
				.contains(List.of(piercingCode, SNOMED_CT_URI)));

		Assert.assertEquals(new GregorianCalendar(2022, Calendar.SEPTEMBER, 3).getTime(), encounter.getParticipantFirstRep().getPeriod().getStart());
		Assert.assertEquals(new GregorianCalendar(2022, Calendar.SEPTEMBER, 7).getTime(), encounter.getParticipantFirstRep().getPeriod().getEnd());

		appendToResultFile(endOfTestMsg);
	}

	@Test
	public void testEncounterActivity2Encounter_ActivityCodeTranslation_ErrorIfBadCode() {
		appendToResultFile("## TEST: EncounterActivity2Encounter_ActivityCodeTranslation\n");

		// given
		org.openhealthtools.mdht.uml.cda.consol.EncounterActivities encounterActivities = EncounterActivityGenerator.getDefaultInstance().generate(CDAFactories.init());

		String badCode = "BS";
		CD cd = makeTestCD(badCode);
		// to test code.translation, we need to provide yet another CD object that *is* the translation
		CD translatedCd = makeTestCD(badCode);

		cd.getTranslations().add(translatedCd);

		encounterActivities.setCode(cd);

		BundleInfo bundleInfo = new BundleInfo(rt);
		ITransformationContext transformationContext = new TransformationContextImpl();

		// when
		appendToResultFile(transformationStartMsg);
		Bundle bundle = rt.tEncounterActivity2Encounter(encounterActivities, bundleInfo, transformationContext).getBundle();
		appendToResultFile(transformationEndMsg);
		appendToResultFile(bundle);

		ITransformationMessage error = transformationContext.getErrors().get(0);

		// then
		Assert.assertTrue(transformationContext.hasErrors());
		Assert.assertEquals(error.getMessage(), MessageFormat.format(NO_VALID_CODE_FOUND, badCode));

		appendToResultFile(endOfTestMsg);
	}

	@Nonnull
	private CD makeTestCD(String code) {
		String codeSystem = "http://acme.org/test";
		String codeSystemName = "ACME Hospital";
		String displayName = "Test";

		CD cd = CDGenerator.getNextInstance().generate(CDAFactories.init());
		cd.setCode(code);
		cd.setCodeSystem(codeSystem);
		cd.setCodeSystemName(codeSystemName);
		cd.setDisplayName(displayName);
		return cd;
	}

	private Optional<Resource> getFirstResourceOfType(Bundle bundle, Enumerations.ResourceType type) {
		return bundle.getEntry().stream()
				.map(BundleEntryComponent::getResource)
				.filter(theResource -> (type.name().equalsIgnoreCase(theResource.getResourceType().toString())))
				.findFirst();
	}

	@Test
	public void testFamilyHistoryOrganizer2FamilyMemberHistory() {
		appendToResultFile("## TEST: FamilyHistoryOrganizer2FamilyMemberHistory\n");
		// null instance test
		org.openhealthtools.mdht.uml.cda.consol.FamilyHistoryOrganizer cdaNull = null;
		ITransformationContext transformationContext = new TransformationContextImpl();
		FamilyMemberHistory fhirNull = rt.tFamilyHistoryOrganizer2FamilyMemberHistory(cdaNull, transformationContext);
		Assert.assertNull(fhirNull);

		// instances from file
		if (ResourceTransformerTest.ccd.getFamilyHistorySection() != null
				&& ResourceTransformerTest.ccd.getFamilyHistorySection().getFamilyHistories() != null) {
			for (org.openhealthtools.mdht.uml.cda.consol.FamilyHistoryOrganizer familyHistoryOrganizer : ResourceTransformerTest.ccd
					.getFamilyHistorySection().getFamilyHistories()) {
				if (familyHistoryOrganizer != null) {
					appendToResultFile(transformationStartMsg);
					FamilyMemberHistory fmHistory = rt
							.tFamilyHistoryOrganizer2FamilyMemberHistory(familyHistoryOrganizer, transformationContext);
					appendToResultFile(transformationEndMsg);
					appendToResultFile(fmHistory);
				}
			}
		}
		appendToResultFile(endOfTestMsg);
	}

	@Test
	public void testFunctionalStatus2Observation() {
		appendToResultFile("## TEST: FunctionalStatus2Observation\n");
		// null instance test
		org.openhealthtools.mdht.uml.cda.Observation cdaNull = null;
		BundleInfo bundleInfo = new BundleInfo(rt);
		ITransformationContext transformationContext = new TransformationContextImpl();
		Bundle fhirNull = rt.tFunctionalStatus2Observation(cdaNull, bundleInfo, transformationContext).getBundle();
		Assert.assertNull(fhirNull);

		// instance from file
		FunctionalStatusSection funcStatSec = ResourceTransformerTest.ccd.getFunctionalStatusSection();

		if (funcStatSec != null && !funcStatSec.isSetNullFlavor()) {
			if (funcStatSec.getOrganizers() != null && !funcStatSec.getOrganizers().isEmpty()) {
				for (Organizer funcStatOrg : funcStatSec.getOrganizers()) {
					if (funcStatOrg != null && !funcStatOrg.isSetNullFlavor()) {
						if (funcStatOrg instanceof FunctionalStatusResultOrganizer) {
							if (((FunctionalStatusResultOrganizer) funcStatOrg).getObservations() != null
									&& !((FunctionalStatusResultOrganizer) funcStatOrg).getObservations().isEmpty()) {
								for (org.openhealthtools.mdht.uml.cda.Observation cdaObs : ((FunctionalStatusResultOrganizer) funcStatOrg)
										.getObservations()) {
									appendToResultFile(transformationStartMsg);
									Bundle fhirObs = rt.tFunctionalStatus2Observation(cdaObs, bundleInfo, transformationContext).getBundle();
									appendToResultFile(transformationEndMsg);
									appendToResultFile(fhirObs);
								}
							}
						}
					}
				}
			}
		}
		appendToResultFile(endOfTestMsg);
	}

	@Test
	public void testGuardian2Contact() {
		appendToResultFile("## TEST: Guardian2Contact\n");
		// null instance test
		org.openhealthtools.mdht.uml.cda.Guardian cdaNull = null;
		ContactComponent fhirNull = rt.tGuardian2Contact(cdaNull, new TransformationContextImpl());
		Assert.assertNull(fhirNull);

		// instances from file
		if (ResourceTransformerTest.ccd.getPatientRoles() != null
				&& !ResourceTransformerTest.ccd.getPatientRoles().isEmpty()) {
			for (org.openhealthtools.mdht.uml.cda.PatientRole patientRole : ResourceTransformerTest.ccd
					.getPatientRoles()) {
				if (patientRole != null && !patientRole.isSetNullFlavor() && patientRole.getPatient() != null
						&& !patientRole.getPatient().isSetNullFlavor()) {
					for (org.openhealthtools.mdht.uml.cda.Guardian guardian : patientRole.getPatient().getGuardians()) {
						if (guardian != null && !guardian.isSetNullFlavor()) {
							appendToResultFile(transformationStartMsg);
							ContactComponent contact = rt.tGuardian2Contact(guardian, new TransformationContextImpl());
							appendToResultFile(transformationEndMsg);
							org.hl7.fhir.r4.model.Patient patient = new Patient().addContact(contact);
							appendToResultFile(patient);
						}
					}
				}
			}
		}
		appendToResultFile(endOfTestMsg);
	}

	@Test
	public void testImmunizationActivity2Immunization() {
		appendToResultFile("## TEST: ImmunizationActivity2Immunization\n");
		// null instance test
		org.openhealthtools.mdht.uml.cda.consol.ImmunizationActivity cdaNull = null;
		BundleInfo bundleInfo = new BundleInfo(rt);
		ITransformationContext transformationContext = new TransformationContextImpl();
		Bundle fhirNull = rt.tImmunizationActivity2Immunization(cdaNull, bundleInfo, transformationContext).getBundle();
		Assert.assertNull(fhirNull);

		// instances from file
		ImmunizationsSectionEntriesOptional immSec = ResourceTransformerTest.ccd
				.getImmunizationsSectionEntriesOptional();

		if (immSec != null && !immSec.isSetNullFlavor()) {
			for (ImmunizationActivity immAct : immSec.getImmunizationActivities()) {
				if (immAct != null && !immAct.isSetNullFlavor()) {
					appendToResultFile(transformationStartMsg);
					Bundle fhirImm = rt.tImmunizationActivity2Immunization(immAct, bundleInfo, transformationContext).getBundle();
					appendToResultFile(transformationEndMsg);
					appendToResultFile(fhirImm);
				}
			}
		}
		appendToResultFile(endOfTestMsg);
	}

	@Test
	public void testImmunizationActivity2Immunization_refusalReasonVersionIsR4() {
		appendToResultFile("## TEST: ImmunizationActivity2Immunization - Refusal Reason version is R4\n");

		// given
		BundleInfo bundleInfo = new BundleInfo(rt);
		ITransformationContext transformationContext = new TransformationContextImpl();

		CDAFactories cdaFactories = CDAFactories.init();

		CD cd = CDGenerator.getNextInstance().generate(cdaFactories);
		String code = "OSTOCK";
		cd.setCode(code);

		ImmunizationActivity cdaImmunizationActivity = ImmunizationActivityGenerator.getDefaultInstance().generate(cdaFactories);
		cdaImmunizationActivity.setNegationInd(true);
		cdaImmunizationActivity.setStatusCode(null);

		ImmunizationRefusalReason refusalReason = CDAFactories.init().consol.createImmunizationRefusalReason();
		refusalReason.setCode(cd);

		EntryRelationship refusalReasonRelationship = cdaFactories.base.createEntryRelationship();
		refusalReasonRelationship.setObservation(refusalReason);

		cdaImmunizationActivity.getEntryRelationships().add(refusalReasonRelationship);

		refusalReasonRelationship.setTypeCode(x_ActRelationshipEntryRelationship.RSON);

	    // when
		appendToResultFile(transformationStartMsg);
		Bundle bundle = rt.tImmunizationActivity2Immunization(cdaImmunizationActivity, bundleInfo, transformationContext).getBundle();
		appendToResultFile(transformationEndMsg);

		Optional<Resource> maybeImmunization = getFirstResourceOfType(bundle, Enumerations.ResourceType.IMMUNIZATION);
		Immunization immunization = null;
		if (maybeImmunization.isPresent()) {
			immunization = (Immunization) maybeImmunization.get();
		}

	    // then
		Assert.assertNotNull(immunization);
		Assert.assertEquals(code, immunization.getStatusReason().getCodingFirstRep().getCode());
		appendToResultFile(endOfTestMsg);
	}

	@Test
	public void testImmunizationActivity2Immunization_extraneousDataAbsentReason() {
		appendToResultFile("## TEST: ImmunizationActivity2Immunization - Refusal Reason version is R4\n");

		// given
		BundleInfo bundleInfo = new BundleInfo(rt);
		ITransformationContext transformationContext = new TransformationContextImpl();
		CDAFactories cdaFactories = CDAFactories.init();

		ImmunizationActivity cdaImmunizationActivity = ImmunizationActivityGenerator.getDefaultInstance().generate(cdaFactories);

		cdaImmunizationActivity.getIds().clear();
		II ii = cdaFactories.datatype.createII();
		ii.setNullFlavor(NullFlavor.UNK);
		cdaImmunizationActivity.getIds().add(ii);

		// when
		appendToResultFile(transformationStartMsg);
		Bundle bundle = rt.tImmunizationActivity2Immunization(cdaImmunizationActivity, bundleInfo, transformationContext).getBundle();
		appendToResultFile(transformationEndMsg);

		Optional<Resource> maybeImmunization = getFirstResourceOfType(bundle, Enumerations.ResourceType.IMMUNIZATION);
		Immunization immunization = null;
		if (maybeImmunization.isPresent()) {
			immunization = (Immunization) maybeImmunization.get();
		}

		// then
		Assert.assertFalse(immunization.getIdentifierFirstRep().hasExtension());
		appendToResultFile(endOfTestMsg);
	}

	@Test
	public void testLanguageCommunication2Communication() {
		appendToResultFile("## TEST: LanguageCommunication2Communication\n");
		// null instance test
		org.openhealthtools.mdht.uml.cda.LanguageCommunication cdaNull = null;
		PatientCommunicationComponent fhirNull = rt.tLanguageCommunication2Communication(cdaNull);
		Assert.assertNull(fhirNull);

		// instances from file
		for (org.openhealthtools.mdht.uml.cda.Patient patient : ResourceTransformerTest.ccd.getPatients()) {
			for (org.openhealthtools.mdht.uml.cda.LanguageCommunication LC : patient.getLanguageCommunications()) {
				appendToResultFile(transformationStartMsg);
				PatientCommunicationComponent fhirCommunication = rt.tLanguageCommunication2Communication(LC);
				appendToResultFile(transformationEndMsg);
				org.hl7.fhir.r4.model.Patient fhirPatient = new org.hl7.fhir.r4.model.Patient();
				fhirPatient.addCommunication(fhirCommunication);
				appendToResultFile(fhirPatient);
			}
		}
		appendToResultFile(endOfTestMsg);
	}

	@Test
	public void testLanguageCommunication2Communication_setsProperCodySystem() {
		appendToResultFile("## TEST: LanguageCommunication2Communication_setsProperCodySystem\n");

	    // given
		CDAFactories cdaFactories = CDAFactories.init();
		LanguageCommunication cdaLanguageCommunication = cdaFactories.base.createLanguageCommunication();
		cdaLanguageCommunication.setLanguageCode(new CSCodeGenerator("EN_CA").generate(cdaFactories));

	    // when
		PatientCommunicationComponent communicationComponent = rt.tLanguageCommunication2Communication(cdaLanguageCommunication);

	    // then
		Assert.assertEquals("urn:ietf:bcp:47", communicationComponent.getLanguage().getCodingFirstRep().getSystem());

		appendToResultFile(endOfTestMsg);
	}

	@Test
	public void testManufacturedProduct2Medication() {
		appendToResultFile("## TEST: ManufacturedProduct2Medication\n");
		// null instance test
		org.openhealthtools.mdht.uml.cda.ManufacturedProduct cdaNull = null;
		BundleInfo bundleInfo = new BundleInfo(rt);
		ITransformationContext transformationContext = new TransformationContextImpl();
		EntryResult nullResult = rt.tManufacturedProduct2Medication(cdaNull, bundleInfo, transformationContext);
		Assert.assertFalse(nullResult.hasResult());

		// instances from file
		ImmunizationsSectionEntriesOptional immSection = ResourceTransformerTest.ccd
				.getImmunizationsSectionEntriesOptional();
		if (immSection != null && !immSection.isSetNullFlavor()) {
			if (immSection.getImmunizationActivities() != null && !immSection.getImmunizationActivities().isEmpty()) {
				for (ImmunizationActivity immAct : immSection.getImmunizationActivities()) {
					if (immAct != null && !immAct.isSetNullFlavor()) {
						if (immAct.getConsumable() != null && !immAct.getConsumable().isSetNullFlavor()) {
							if (immAct.getConsumable().getManufacturedProduct() != null
									&& !immAct.getConsumable().getManufacturedProduct().isSetNullFlavor()) {
								// immAct.immSection.immAct.consumable.manuProd
								appendToResultFile(transformationStartMsg);

								EntryResult fhirMed = rt.tManufacturedProduct2Medication(
										immAct.getConsumable().getManufacturedProduct(), bundleInfo, transformationContext);

								appendToResultFile(transformationEndMsg);
								appendToResultFile(fhirMed.getBundle());
							}
						}
					}
				}
			}
		}
		appendToResultFile(endOfTestMsg);
	}

	@Test
	public void testMedicationActivity2MedicationStatement() {
		BundleInfo bundleInfo = new BundleInfo(rt);
		ITransformationContext transformationContext = new TransformationContextImpl();
		appendToResultFile("## TEST: MedicationActivity2MedicationStatement\n");
		// null instance test
		org.openhealthtools.mdht.uml.cda.consol.MedicationActivity cdaNull = null;
		Bundle fhirNull = rt.tMedicationActivity2MedicationStatement(cdaNull, bundleInfo, transformationContext).getBundle();
		Assert.assertNull(fhirNull);

		// instances from file
		if (ResourceTransformerTest.ccd.getMedicationsSection() != null
				&& !ResourceTransformerTest.ccd.getMedicationsSection().isSetNullFlavor()) {
			if (ResourceTransformerTest.ccd.getMedicationsSection().getMedicationActivities() != null
					&& !ResourceTransformerTest.ccd.getMedicationsSection().getMedicationActivities().isEmpty()) {
				for (MedicationActivity cdaMedAct : ResourceTransformerTest.ccd.getMedicationsSection()
						.getMedicationActivities()) {
					if (cdaMedAct != null && !cdaMedAct.isSetNullFlavor()) {
						appendToResultFile(transformationStartMsg);
						Bundle fhirMedStBundle = rt.tMedicationActivity2MedicationStatement(cdaMedAct, bundleInfo, transformationContext)
								.getBundle();
						appendToResultFile(transformationEndMsg);
						appendToResultFile(fhirMedStBundle);
					}
				}
			}
		}
		appendToResultFile(endOfTestMsg);
	}

	@Test
	public void testMedicationDispense2MedicationDispense() {
		BundleInfo bundleInfo = new BundleInfo(rt);
		ITransformationContext transformationContext = new TransformationContextImpl();
		appendToResultFile("## TEST: MedicationDispense2MedicationDispense\n");
		// null instance test
		org.openhealthtools.mdht.uml.cda.consol.MedicationDispense cdaNull = null;
		Bundle fhirNull = rt.tMedicationDispense2MedicationDispense(cdaNull, bundleInfo, transformationContext).getBundle();
		Assert.assertNull(fhirNull);

		// instances from file
		// medicationsSection.medicationActivities.medicationDispense
		if (ResourceTransformerTest.ccd.getMedicationsSection() != null
				&& !ResourceTransformerTest.ccd.getMedicationsSection().isSetNullFlavor()) {
			org.openhealthtools.mdht.uml.cda.consol.MedicationsSection medSec = ResourceTransformerTest.ccd
					.getMedicationsSection();
			if (medSec.getMedicationActivities() != null && !medSec.getMedicationActivities().isEmpty()) {
				for (MedicationActivity medAct : medSec.getMedicationActivities()) {
					if (medAct != null && !medAct.isSetNullFlavor()) {
						if (medAct.getMedicationDispenses() != null && !medAct.getMedicationDispenses().isEmpty()) {
							for (org.openhealthtools.mdht.uml.cda.consol.MedicationDispense medDisp : medAct
									.getMedicationDispenses()) {
								if (medDisp != null && !medDisp.isSetNullFlavor()) {
									appendToResultFile(transformationStartMsg);
									Bundle fhirMedDispBundle = rt
											.tMedicationDispense2MedicationDispense(medDisp, bundleInfo, transformationContext).getBundle();
									appendToResultFile(transformationEndMsg);
									appendToResultFile(fhirMedDispBundle);
								}
							}
						}
					}
				}
			}
		}
		appendToResultFile(endOfTestMsg);
	}

	@Test
	public void testObservation2Observation() {
		appendToResultFile("## TEST: Observation2Observation\n");
		BundleInfo bundleInfo = new BundleInfo(rt);
		ITransformationContext transformationContext = new TransformationContextImpl();
		// null instance test
		org.openhealthtools.mdht.uml.cda.Observation cdaNull = null;
		Bundle fhirNull = rt.tObservation2Observation(cdaNull, bundleInfo, transformationContext).getBundle();
		Assert.assertNull(fhirNull);

		// instances from file
		if (ResourceTransformerTest.ccd.getSocialHistorySection() != null
				&& !ResourceTransformerTest.ccd.getSocialHistorySection().isSetNullFlavor()) {
			if (ResourceTransformerTest.ccd.getSocialHistorySection().getObservations() != null
					&& !ResourceTransformerTest.ccd.getSocialHistorySection().getObservations().isEmpty()) {
				for (org.openhealthtools.mdht.uml.cda.Observation cdaObs : ResourceTransformerTest.ccd
						.getSocialHistorySection().getObservations()) {
					if (cdaObs != null && !cdaObs.isSetNullFlavor()) {
						appendToResultFile(transformationStartMsg);
						Bundle obsBundle = rt.tObservation2Observation(cdaObs, bundleInfo, transformationContext).getBundle();
						appendToResultFile(transformationEndMsg);
						appendToResultFile(obsBundle);
					}
				}
			}
		}
		appendToResultFile(endOfTestMsg);
	}

	@Test
	public void testOrganization2Organization() {
		appendToResultFile("## TEST: Organization2Organization\n");
		// null instance test
		org.openhealthtools.mdht.uml.cda.Organization cdaNull = null;
		ITransformationContext transformationContext = new TransformationContextImpl();
		IEntryResult result1 = rt.tOrganization2Organization(cdaNull, new BundleInfo(rt), transformationContext);
		Assert.assertFalse(result1.hasResult());

		// instances from file
		for (org.openhealthtools.mdht.uml.cda.PatientRole patRole : ResourceTransformerTest.ccd.getPatientRoles()) {
			org.openhealthtools.mdht.uml.cda.Organization cdaOrg = patRole.getProviderOrganization();
			appendToResultFile(transformationStartMsg);
			IEntryResult result2 = rt.tOrganization2Organization(cdaOrg, new BundleInfo(rt), transformationContext);
			org.hl7.fhir.r4.model.Organization fhirOrg = FHIRUtil.findFirstResource(result2.getBundle(),
					org.hl7.fhir.r4.model.Organization.class);
			appendToResultFile(transformationEndMsg);
			appendToResultFile(fhirOrg);
		}
		appendToResultFile(endOfTestMsg);
	}

	@Test
	public void testPatientRole2Patient() {
		appendToResultFile("## TEST: PatientRole2Patient\n");
		// null instance test
		org.openhealthtools.mdht.uml.cda.PatientRole cdaNull = null;
		ITransformationContext transformationContext = new TransformationContextImpl();
		IEntryResult patientResult1 = rt.tPatientRole2Patient(cdaNull, new BundleInfo(rt), transformationContext);
		Assert.assertFalse(patientResult1.hasResult());

		// instances from file
		for (PatientRole pr : ResourceTransformerTest.ccd.getPatientRoles()) {

			// here we do the transformation by calling the method rt.PatientRole2Patient

			Patient patient = null;

			appendToResultFile(transformationStartMsg);
			IEntryResult patientResult2 = rt.tPatientRole2Patient(pr, new BundleInfo(rt), transformationContext);
			appendToResultFile(transformationEndMsg);
			appendToResultFile(patientResult2.getBundle());

			for (BundleEntryComponent entry : patientResult2.getFullBundle().getEntry()) {
				if (entry.getResource() instanceof Patient) {
					patient = (Patient) entry.getResource();
				}
			}

			// patient.identifier
			int idCount = 0;
			for (II id : pr.getIds()) {
				if (id.getRoot() != null && id.getExtension() != null) {
					// since extension may contain "urn:oid:" or "urn:uuid:", assertion is about
					// containing the value as a piece
					Assert.assertTrue("pr.id.extension #" + idCount + " was not transformed",
							patient.getIdentifier().get(idCount).getValue().contains(id.getExtension()));
					Assert.assertTrue("pr.id.root #" + idCount + " was not transformed",
							patient.getIdentifier().get(idCount).getSystem().contains(id.getRoot()));
				} else if (id.getRoot() != null) {
					Assert.assertTrue("pr.id.root #" + idCount + " was not transformed",
							patient.getIdentifier().get(idCount).getValue().contains(id.getRoot()));
				} else if (id.getExtension() != null) {
					Assert.assertTrue("pr.id.root #" + idCount + " was not transformed",
							patient.getIdentifier().get(idCount).getValue().contains(id.getExtension()));
				}
				// codeSystem method is changed and tested

				idCount++;
			}
			// patient.name
			// Notice that patient.name is fullfilled by the method EN2HumanName.
			int nameCount = 0;
			for (EN pn : pr.getPatient().getNames()) {

				// patient.name.use
				if (pn.getUses() == null || pn.getUses().isEmpty()) {
					Assert.assertNull(patient.getName().get(nameCount).getUse().toCode());
				} else {
					Assert.assertEquals("pr.patient.name[" + nameCount + "]" + ".use was not transformed",
							vsti.tEntityNameUse2NameUse(pn.getUses().get(0)).toString().toLowerCase(),
							patient.getName().get(nameCount).getUse().toCode());
				}

				// patient.name.text
				Assert.assertEquals("pr.patient.name[" + nameCount + "].text was not transformed", pn.getText(),
						patient.getName().get(nameCount).getText());

				// patient.name.family
				for (ENXP family : pn.getFamilies()) {
					if (family == null || family.isSetNullFlavor()) {
						// It can return null or an empty list
						Assert.assertTrue(patient.getName().get(nameCount).getFamily() == null
								|| !patient.getName().get(nameCount)
										.hasFamily()/* patient.getName().get(nameCount).getFamily().size() == 0 */);
					} else {
						Assert.assertEquals("pr.patient.name[" + nameCount + "].family was not transformed",
								family.getText(),
								patient.getName().get(nameCount).getFamily()/* .get(familyCount).getValue() */);
					}
				}

				// patient.name.given
				int givenCount = 0;
				for (ENXP given : pn.getGivens()) {
					if (given == null || given.isSetNullFlavor()) {
						// It can return null or an empty list
						Assert.assertTrue(patient.getName().get(nameCount).getGiven() == null
								|| patient.getName().get(nameCount).getGiven().size() == 0);
					} else {
						Assert.assertEquals("pr.patient.name[" + nameCount + "].given was not transformed",
								given.getText(),
								patient.getName().get(nameCount).getGiven().get(givenCount).getValue());
					}
					givenCount++;
				}

				// patient.name.prefix
				int prefixCount = 0;
				for (ENXP prefix : pn.getPrefixes()) {
					if (prefix == null || prefix.isSetNullFlavor()) {
						// It can return null or an empty list
						Assert.assertTrue(patient.getName().get(nameCount).getPrefix() == null
								|| patient.getName().get(nameCount).getPrefix().size() == 0);
					} else {
						Assert.assertEquals("pr.patient.name[" + nameCount + "].prefix was not transformed",
								prefix.getText(),
								patient.getName().get(nameCount).getPrefix().get(prefixCount).getValue());
					}
					prefixCount++;
				}

				// patient.name.suffix
				int suffixCount = 0;
				for (ENXP suffix : pn.getPrefixes()) {
					if (suffix == null || suffix.isSetNullFlavor()) {
						// It can return null or an empty list
						Assert.assertTrue(patient.getName().get(nameCount).getSuffix() == null
								|| patient.getName().get(nameCount).getSuffix().size() == 0);
					} else {
						Assert.assertEquals("pr.patient.name[" + nameCount + "].suffix was not transformed",
								suffix.getText(),
								patient.getName().get(nameCount).getSuffix().get(suffixCount).getValue());
					}
					suffixCount++;
				}

				// patient.name.period
				if (pn.getValidTime() == null || pn.getValidTime().isSetNullFlavor()) {
					// It can return null or an empty list
					Assert.assertTrue(patient.getName().get(nameCount).getPeriod() == null
							|| patient.getName().get(nameCount).getPeriod().isEmpty());
				}
			}

			// patient.telecom
			// Notice that patient.telecom is fullfilled by the method dtt.TEL2ContactPoint
			if (pr.getTelecoms() == null || pr.getTelecoms().isEmpty()) {
				Assert.assertTrue(patient.getTelecom() == null || patient.getTelecom().isEmpty());
			} else {
				// size check
				Assert.assertTrue(pr.getTelecoms().size() == patient.getTelecom().size());
				// We have already tested the method TEL2ContactPoint. Therefore, null-check and
				// size-check is enough for now.
			}

			// patient.gender
			// vst.AdministrativeGenderCode2AdministrativeGenderEnum is used in this
			// transformation.
			// Following test aims to test that ValueSetTransformer method.
			if (pr.getPatient().getAdministrativeGenderCode() == null
					|| pr.getPatient().getAdministrativeGenderCode().isSetNullFlavor()) {
				Assert.assertTrue(patient.getGender() == null || !patient.getGenderElement().hasValue());
			}

			// patient.birthDate
			// Notice that patient.birthDate is fullfilled by the method dtt.TS2Date
			if (pr.getPatient().getBirthTime() == null || pr.getPatient().getBirthTime().isSetNullFlavor()) {
				Assert.assertTrue(patient.getBirthDate() == null);
			}

			// patient.address
			// Notice that patient.address is fullfilled by the method dtt.AD2Address
			if (pr.getAddrs() == null || pr.getAddrs().isEmpty()) {
				Assert.assertTrue(patient.getAddress() == null || patient.getAddress().isEmpty());
			} else {
				// We have already tested the method AD2Address. Therefore, null-check and
				// size-check is enough for now.
				Assert.assertTrue(pr.getAddrs().size() == patient.getAddress().size());
			}

			// patient.maritalStatus
			// vst.MaritalStatusCode2MaritalStatusCodesEnum is used in this transformation.
			// Following test aims to test that ValueSetTransformer method.
			if (pr.getPatient().getMaritalStatusCode() == null
					|| pr.getPatient().getMaritalStatusCode().isSetNullFlavor()) {
				Assert.assertTrue(patient.getMaritalStatus() == null || patient.getMaritalStatus().isEmpty());
			} else {
				Assert.assertTrue(patient.getMaritalStatus().getCoding().get(0).getCode().toLowerCase().charAt(0) == pr
						.getPatient().getMaritalStatusCode().getCode().toLowerCase().charAt(0));
			}

			// patient.languageCommunication
			if (pr.getPatient().getLanguageCommunications() == null
					|| pr.getPatient().getLanguageCommunications().isEmpty()) {
				Assert.assertTrue(patient.getCommunication() == null || patient.getCommunication().isEmpty());
			} else {
				Assert.assertTrue(
						pr.getPatient().getLanguageCommunications().size() == patient.getCommunication().size());

				int sizeCommunication = pr.getPatient().getLanguageCommunications().size();
				while (sizeCommunication != 0) {

					// language
					if (pr.getPatient().getLanguageCommunications().get(sizeCommunication - 1).getLanguageCode() == null
							|| pr.getPatient().getLanguageCommunications().get(0).getLanguageCode().isSetNullFlavor()) {
						Assert.assertTrue(patient.getCommunication().get(sizeCommunication - 1).getLanguage() == null
								|| patient.getCommunication().get(sizeCommunication - 1).getLanguage().isEmpty());
					} else {
						// We have already tested the method CD2CodeableConcept. Therefore, null-check
						// is enough for now.
					}

					// preference
					if (pr.getPatient().getLanguageCommunications().get(sizeCommunication - 1)
							.getPreferenceInd() == null
							|| pr.getPatient().getLanguageCommunications().get(sizeCommunication - 1).getPreferenceInd()
									.isSetNullFlavor()) {
						// Assert.assertTrue(patient.getCommunication().get(sizeCommunication -
						// 1).getPreferred() == null);
						Assert.assertTrue(!patient.getCommunication().get(sizeCommunication - 1).hasPreferred());
					} else {
						Assert.assertEquals(
								pr.getPatient().getLanguageCommunications().get(sizeCommunication - 1)
										.getPreferenceInd().getValue(),
								patient.getCommunication().get(sizeCommunication - 1).getPreferred());
					}
					sizeCommunication--;
				}

			}

			// providerOrganization
			if (pr.getProviderOrganization() == null || pr.getProviderOrganization().isSetNullFlavor()) {
				Assert.assertTrue(
						patient.getManagingOrganization() == null || patient.getManagingOrganization().isEmpty());
			} else {
				if (pr.getProviderOrganization().getNames() == null) {
					Assert.assertTrue(patient.getManagingOrganization().getDisplay() == null);
				}
			}

			// guardian
			if (pr.getPatient().getGuardians() == null || pr.getPatient().getGuardians().isEmpty()) {
				Assert.assertTrue(patient.getContact() == null || patient.getContact().isEmpty());
			} else {
				// Notice that, inside this mapping, the methods dtt.TEL2ContactPoint and
				// dtt.AD2Address are used.
				// Therefore, null-check and size-check are enough
				Assert.assertTrue(pr.getPatient().getGuardians().size() == patient.getContact().size());
			}

			// extensions
			for (Extension extension : patient.getExtension()) {
				Assert.assertNotNull(extension.getUrl());
				if (extension.getExtension().size() == 0) {
					Assert.assertNotNull(extension.getValue());
				}
				else {
					Assert.assertNotNull(extension.getExtensionByUrl("text").getValue());
				}
			}

			//test for religiousAffiliationCode -> ext-patient-religion
			Extension religionExtension = patient.getExtensionByUrl("http://hl7.org/fhir/StructureDefinition/patient-religion");
			Coding religionValue = ((CodeableConcept)religionExtension.getValue()).getCoding().get(0);
			CE cdaReligiousAffiliationCode = pr.getPatient().getReligiousAffiliationCode();
			testCodingValues(cdaReligiousAffiliationCode, religionValue);

			//test for raceCode -> ext-us-core-race
			CE cdaCDARaceCode = pr.getPatient().getRaceCode();
			Extension raceExtension = patient.getExtensionByUrl("http://hl7.org/fhir/us/core/StructureDefinition/us-core-race");
			Extension raceTextExtension = raceExtension.getExtensionByUrl("text");
			Assert.assertEquals(cdaCDARaceCode.getDisplayName(), raceTextExtension.getValue().toString());
			Extension ombCategoryExtension = raceExtension.getExtensionByUrl("ombCategory");
			Coding ombCategoryValue = (Coding)ombCategoryExtension.getValue();
			testCodingValues(cdaCDARaceCode, ombCategoryValue);

			//test for patient.sdtc:raceCode -> ext-us-core-race.ext-detailed
			EList<CE> cdaSdtRaceCodes = pr.getPatient().getSDTCRaceCodes();
			List<Extension> detailedExtensions = raceExtension.getExtensionsByUrl("detailed");
			Assert.assertTrue(cdaSdtRaceCodes.size() == detailedExtensions.size());
			for (CE sdtRaceCode : cdaSdtRaceCodes) {
				Extension detailedExtension = detailedExtensions.stream().filter(detailed -> sdtRaceCode.getCode().equals(((Coding)detailed.getValue()).getCode())).findAny().orElse(null);
				Assert.assertNotNull(detailedExtension.getValue());
				Coding detailedCoding = (Coding) detailedExtension.getValue();
				testCodingValues(sdtRaceCode, detailedCoding);
			}

			//test for ethnicGroupCode -> ext-us-core-ethnicity
			CE cdaEthnicityCode = pr.getPatient().getEthnicGroupCode();
			Extension ethnicityExtension = patient.getExtensionByUrl("http://hl7.org/fhir/us/core/StructureDefinition/us-core-ethnicity");
			Extension ethnicityTextExtension = ethnicityExtension.getExtensionByUrl("text");
			Assert.assertEquals(cdaEthnicityCode.getDisplayName(), ethnicityTextExtension.getValue().toString());
			ombCategoryExtension = ethnicityExtension.getExtensionByUrl("ombCategory");
			ombCategoryValue = (Coding)ombCategoryExtension.getValue();
			testCodingValues(cdaEthnicityCode, ombCategoryValue);

			//test for birthPlace -> ext-patient-birthPlace
			Place cdaBirthPlace = pr.getPatient().getBirthplace().getPlace();
			Extension birthPlaceExtension = patient.getExtensionByUrl("http://hl7.org/fhir/StructureDefinition/patient-birthPlace");
			Address birthAddress = (Address)birthPlaceExtension.getValue();
			Assert.assertEquals(cdaBirthPlace.getAddr().getStreetAddressLines().get(0).getText(), birthAddress.getLine().get(0).toString());
			Assert.assertEquals(cdaBirthPlace.getAddr().getPostalCodes().get(0).getText(), birthAddress.getPostalCode());
			Assert.assertEquals(cdaBirthPlace.getAddr().getStates().get(0).getText(), birthAddress.getState());
			Assert.assertEquals(cdaBirthPlace.getAddr().getCities().get(0).getText(), birthAddress.getCity());
			Assert.assertEquals(cdaBirthPlace.getAddr().getCountries().get(0).getText(), birthAddress.getCountry());
		}
		appendToResultFile(endOfTestMsg);
	}

	@Test
	public void testPatientRole2Patient_dataAbsentReasons() {
		appendToResultFile("## TEST: PatientRole2Patient\n");

		ITransformationContext transformationContext = new TransformationContextImpl();
		PatientRole pr = ResourceTransformerTest.ccd.getPatientRoles().get(0);
		org.openhealthtools.mdht.uml.cda.Patient prPatient = pr.getPatient();
		prPatient.getMaritalStatusCode().setNullFlavor(NullFlavor.UNK);
		prPatient.getRaceCode().setNullFlavor(NullFlavor.ASKU);
		prPatient.getEthnicGroupCode().setNullFlavor(NullFlavor.UNK);

		Patient patient = null;

		appendToResultFile(transformationStartMsg);
		IEntryResult patientResult2 = rt.tPatientRole2Patient(pr, new BundleInfo(rt), transformationContext);
		appendToResultFile(transformationEndMsg);
		appendToResultFile(patientResult2.getBundle());

		for (BundleEntryComponent entry : patientResult2.getFullBundle().getEntry()) {
			if (entry.getResource() instanceof Patient) {
				patient = (Patient) entry.getResource();
			}
		}

		Assert.assertNotNull(patient);

		Assert.assertFalse(patient.getMaritalStatus().hasExtension());

		Extension race = patient.getExtensionByUrl(FHIR_STRUCTURE_US_CORE_RACE_URI);
		Assert.assertTrue(race.hasExtension());
		Extension raceOmbCategory = race.getExtensionByUrl("ombCategory");
		Assert.assertTrue(raceOmbCategory.hasExtension());
		verifyExtension(raceOmbCategory.getExtensionFirstRep(), "asked-unknown", "Asked But Unknown");

		Extension ethnicity = patient.getExtensionByUrl(FHIR_STRUCTURE_US_CORE_ETHNICITY_URI);
		Assert.assertTrue(ethnicity.hasExtension());
		Extension ethnicityOmbCategory = ethnicity.getExtensionByUrl("ombCategory");
		Assert.assertTrue(ethnicityOmbCategory.hasExtension());
		verifyExtension(ethnicityOmbCategory.getExtensionFirstRep(), "unknown", "Unknown");
	}

	@Test
	public void testPlanOfCareSection2CarePlan() {
		appendToResultFile("## TEST: ProblemConcernAct2Condition\n");

		// given
		BundleInfo bundleInfo = new BundleInfo(rt);
		ITransformationContext transformationContext = new TransformationContextImpl();
		CDAFactories factories = CDAFactories.init();
		IDataTypesTransformer dtt = new DataTypesTransformerImpl();

		StrucDocText strucDocText = factories.base.createStrucDocText();
		String sdtText = "this is almost certainly not what a StrucDocText is supposed to be";
		strucDocText.addText(sdtText);

		Section carePlanSection = factories.base.createSection();
		carePlanSection.setText(strucDocText);

		Component5 treatmentPlan = factories.base.createComponent5();
		treatmentPlan.setSection(carePlanSection);

		org.openhealthtools.mdht.uml.cda.consol.PlanOfCareSection planOfCare = factories.consol.createPlanOfCareSection();
		planOfCare.getComponents().add(treatmentPlan);

		PlanOfCareActivityAct act = factories.consol.createPlanOfCareActivityAct();
		String actRoot = "act.root";
		String actExtension = "act.extension";
		act.getIds().add(factories.datatype.createII(actRoot, actExtension));

		// when
		appendToResultFile(transformationStartMsg);
		Bundle bundle = rt.tPlanOfCareSection2CarePlan(null, strucDocText, Lists.newArrayList(act), bundleInfo, transformationContext).getBundle();
		appendToResultFile(transformationEndMsg);
		Optional<Resource> maybeCarePlan = getFirstResourceOfType(bundle, Enumerations.ResourceType.CAREPLAN);
		CarePlan carePlan = null;
		if (maybeCarePlan.isPresent()) {
			carePlan = (CarePlan) maybeCarePlan.get();
		}

		// then
		Assert.assertNotNull(carePlan);

		Assert.assertEquals(actRoot, carePlan.getIdentifierFirstRep().getSystem());
		Assert.assertEquals(actExtension, carePlan.getIdentifierFirstRep().getValue());

		Assert.assertTrue(dtt.tStrucDocText2Narrative(strucDocText).getDiv()
				.equalsDeep(carePlan.getText().getDiv()));
		Assert.assertEquals(ADDITIONAL.getDisplay(), carePlan.getText().getStatus().getDisplay());
		Assert.assertEquals(ADDITIONAL.getSystem(), carePlan.getText().getStatus().getSystem());
		Assert.assertEquals(ASSESS_PLAN, carePlan.getCategoryFirstRep().getCodingFirstRep().getCode());
		Assert.assertEquals(CAREPLAN_CATEGORY_URI, carePlan.getCategoryFirstRep().getCodingFirstRep().getSystem());

		appendToResultFile(endOfTestMsg);
	}

	private void testCodingValues(CE theCDACEvalue,  Coding theCodeValue) {
		Assert.assertEquals(theCDACEvalue.getDisplayName(), theCodeValue.getDisplay());
		Assert.assertEquals("urn:oid:" + theCDACEvalue.getCodeSystem(), theCodeValue.getSystem());
		Assert.assertEquals(theCDACEvalue.getCode(), theCodeValue.getCode());
	}

	@Test
	public void testProblemConcernAct2Condition() {
		BundleInfo bundleInfo = new BundleInfo(rt);
		ITransformationContext transformationContext = new TransformationContextImpl();
		appendToResultFile("## TEST: ProblemConcernAct2Condition\n");
		// null instance test
		org.openhealthtools.mdht.uml.cda.consol.ProblemConcernAct cdaNull = null;
		Bundle fhirNull = rt.tProblemConcernAct2Condition(cdaNull, bundleInfo, transformationContext).getBundle();
		Assert.assertNull(fhirNull);

		// instances from file
		if (ResourceTransformerTest.ccd.getProblemSection() != null
				&& !ResourceTransformerTest.ccd.getProblemSection().isSetNullFlavor()) {
			if (ResourceTransformerTest.ccd.getProblemSection().getProblemConcerns() != null
					&& !ResourceTransformerTest.ccd.getProblemSection().getProblemConcerns().isEmpty()) {
				for (ProblemConcernAct problemConcernAct : ResourceTransformerTest.ccd.getProblemSection()
						.getProblemConcerns()) {
					if (problemConcernAct != null && !problemConcernAct.isSetNullFlavor()) {
						appendToResultFile(transformationStartMsg);
						Bundle fhirConditionBundle = rt.tProblemConcernAct2Condition(problemConcernAct, bundleInfo, transformationContext)
								.getBundle();
						appendToResultFile(transformationEndMsg);
						appendToResultFile(fhirConditionBundle);
					}
				}
			}
		}
		appendToResultFile(endOfTestMsg);
	}

	@Test
	public void testProblemObservation2Condition() {
		appendToResultFile("## TEST: ProblemObservation2Condition\n");

		// given
		BundleInfo bundleInfo = new BundleInfo(rt);
		ITransformationContext transformationContext = new TransformationContextImpl();
		ProblemObservation cdaProblemObservation = CDAFactories.init().consol.createProblemObservation();

	    // when
		appendToResultFile(transformationStartMsg);
		Bundle bundle = rt.tProblemObservation2Condition(cdaProblemObservation, bundleInfo, transformationContext).getBundle();
		appendToResultFile(transformationEndMsg);

		Optional<Resource> maybeCondition = getFirstResourceOfType(bundle, Enumerations.ResourceType.CONDITION);
		Condition condition = null;
		if (maybeCondition.isPresent()) {
			condition = (Condition) maybeCondition.get();
		}

		// then
		Assert.assertNotNull(condition);
		Assert.assertEquals(CODE_SYSTEM_CONDITION_CATEGORY,
				condition.getCategoryFirstRep().getCodingFirstRep().getSystem());

		ConditionVerStatus confirmed = ConditionVerStatus.CONFIRMED;
		Coding condVerifStatusCoding = condition.getVerificationStatus().getCodingFirstRep();
		Assert.assertEquals(confirmed.toCode(), condVerifStatusCoding.getCode());
		Assert.assertEquals(confirmed.getDisplay(), condVerifStatusCoding.getDisplay());
		Assert.assertEquals(confirmed.getSystem(), condVerifStatusCoding.getSystem());

		appendToResultFile(endOfTestMsg);
	}

	@Test
	public void testHealthConcernAct2Condition() {
		String root = "root";
		String extension = "extension";

		appendToResultFile("## TEST: HealthConcernAct2Condition\n");

		// given
		BundleInfo bundleInfo = new BundleInfo(rt);
		ITransformationContext transformationContext = new TransformationContextImpl();
		bundleInfo.getIdedAnnotations().put("hc_1", "expected text");
		Act cdaHealthConcernAct = CDAFactories.init().base.createAct();
		cdaHealthConcernAct.getIds().add(CDAFactories.init().datatype.createII(root, extension));
		ED text = CDAFactories.init().datatype.createED();
		TEL reference = CDAFactories.init().datatype.createTEL();
		reference.setValue("#hc_1");
		text.setReference(reference);
		cdaHealthConcernAct.setText(text);

		// when
		appendToResultFile(transformationStartMsg);
		Bundle bundle = rt.tHealthConcernAct2Condition(cdaHealthConcernAct, bundleInfo, transformationContext).getBundle();
		appendToResultFile(transformationEndMsg);

		Optional<Resource> maybeCondition = getFirstResourceOfType(bundle, Enumerations.ResourceType.CONDITION);
		Condition condition = null;
		if (maybeCondition.isPresent()) {
			condition = (Condition) maybeCondition.get();
		}

		// then
		Assert.assertNotNull(condition);

		Assert.assertEquals(root, condition.getIdentifierFirstRep().getSystem());
		Assert.assertEquals(extension, condition.getIdentifierFirstRep().getValue());
		Assert.assertEquals("expected text", condition.getCode().getText());

		appendToResultFile(endOfTestMsg);
	}

	@Test
	public void testOrganizer2CareTeam() {
		String root = "root";
		String extension = "extension";

		appendToResultFile("## TEST: Organizer2CareTeam\n");

		// given
		BundleInfo bundleInfo = new BundleInfo(rt);
		ITransformationContext transformationContext = new TransformationContextImpl();
		Organizer cdaCareTeamOrganizer = CDAFactories.init().base.createOrganizer();
		cdaCareTeamOrganizer.getIds().add(CDAFactories.init().datatype.createII(root, extension));

		// when
		appendToResultFile(transformationStartMsg);
		Bundle bundle = rt.tOrganizer2CareTeam(null, cdaCareTeamOrganizer, bundleInfo, transformationContext).getBundle();
		appendToResultFile(transformationEndMsg);

		Optional<Resource> maybeCareTeam = getFirstResourceOfType(bundle, Enumerations.ResourceType.CARETEAM);
		CareTeam careTeam = null;
		if (maybeCareTeam.isPresent()) {
			careTeam = (CareTeam) maybeCareTeam.get();
		}

		// then
		Assert.assertNotNull(careTeam);

		Assert.assertEquals(root, careTeam.getIdentifierFirstRep().getSystem());
		Assert.assertEquals(extension, careTeam.getIdentifierFirstRep().getValue());

		appendToResultFile(endOfTestMsg);
	}

	@Test
	public void testProcedure2Procedure() {
		BundleInfo bundleInfo = new BundleInfo(rt);
		ITransformationContext transformationContext = new TransformationContextImpl();
		appendToResultFile("## TEST: Procedure2Procedure\n");
		// null instance test
		org.openhealthtools.mdht.uml.cda.Procedure cdaNull = null;
		EntryResult entryResultNull = rt.tProcedure2Procedure(cdaNull, bundleInfo, transformationContext);
		Assert.assertNull(entryResultNull.getBundle());

		// instances from file
		if (ResourceTransformerTest.ccd.getProceduresSection() != null
				&& !ResourceTransformerTest.ccd.getProceduresSection().isSetNullFlavor()) {
			if (ResourceTransformerTest.ccd.getProceduresSection().getProcedures() != null
					&& !ResourceTransformerTest.ccd.getProceduresSection().getProcedures().isEmpty()) {
				for (org.openhealthtools.mdht.uml.cda.Procedure cdaProcedure : ResourceTransformerTest.ccd
						.getProceduresSection().getProcedures()) {
					// traversing procedures
					appendToResultFile(transformationStartMsg);
					EntryResult entryResult = rt.tProcedure2Procedure(cdaProcedure, bundleInfo, transformationContext);
					Bundle fhirProcedureBundle = entryResult.getBundle();
					appendToResultFile(transformationEndMsg);
					appendToResultFile(fhirProcedureBundle);
				}
			}
		}

		if (ResourceTransformerTest.ccd.getEncountersSection() != null
				&& !ResourceTransformerTest.ccd.getEncountersSection().isSetNullFlavor()) {
			if (ResourceTransformerTest.ccd.getEncountersSection().getProcedures() != null
					&& !ResourceTransformerTest.ccd.getEncountersSection().getProcedures().isEmpty()) {
				for (org.openhealthtools.mdht.uml.cda.Procedure cdaProcedure : ResourceTransformerTest.ccd
						.getEncountersSection().getProcedures()) {
					// traversing procedures
					appendToResultFile(transformationStartMsg);
					EntryResult entryResult = rt.tProcedure2Procedure(cdaProcedure, bundleInfo, transformationContext);
					Bundle fhirProcedureBundle = entryResult.getBundle();
					appendToResultFile(transformationEndMsg);
					appendToResultFile(fhirProcedureBundle);
				}
			}
		}

		if (ResourceTransformerTest.ccd.getAllSections() != null
				&& !ResourceTransformerTest.ccd.getAllSections().isEmpty()) {
			for (org.openhealthtools.mdht.uml.cda.Section section : ResourceTransformerTest.ccd.getAllSections()) {
				if (section.getProcedures() != null && !section.getProcedures().isEmpty()) {
					for (org.openhealthtools.mdht.uml.cda.Procedure cdaProcedure : section.getProcedures()) {
						// traversing procedures
						appendToResultFile(transformationStartMsg);
						EntryResult entryResult = rt.tProcedure2Procedure(cdaProcedure, bundleInfo, transformationContext);
						Bundle fhirProcedureBundle = entryResult.getBundle();
						appendToResultFile(transformationEndMsg);
						appendToResultFile(fhirProcedureBundle);
					}
				}
			}
		}
		appendToResultFile(endOfTestMsg);
	}

	@Test
	public void testProductInstance2Device() {
		// given
		appendToResultFile("## TEST: ProductInstance2Device\n");
		BundleInfo bundleInfo = new BundleInfo(rt);
		ITransformationContext transformationContext = new TransformationContextImpl();
		String patId = "Patient/0";
		transformationContext.setPatient((Patient) (new Patient()).setId(patId));

		CDAFactories factories = CDAFactories.init();
		ProductInstance productInstance = CDAFactories.init().consol.createProductInstance();

		String root = "acme";
		String idExtension = "test";
		productInstance.getIds().add(new IDGenerator(root, idExtension).generate(factories));

		CE ce = CEGenerator.getNextInstance().generate(factories);
		String playingDeviceCode = "testDevice";
		String system = "http://acme.org/";
		ce.setCode(playingDeviceCode);
		ce.setCodeSystem(system);

		org.openhealthtools.mdht.uml.cda.Device playingDevice = CDAFactories.init().base.createDevice();
		playingDevice.setCode(ce);

		productInstance.setPlayingDevice(playingDevice);

		String scopingEntityExtension = "testEntity";
		Entity scopingEntity = CDAFactories.init().base.createEntity();
		scopingEntity.getIds().add(new IDGenerator(root, scopingEntityExtension).generate(factories));

		productInstance.setScopingEntity(scopingEntity);
		
		// when
		appendToResultFile(transformationStartMsg);
		Bundle bundle = rt.tProductInstance2Device(productInstance, bundleInfo, transformationContext).getBundle();
		appendToResultFile(transformationEndMsg);
		appendToResultFile(bundle);

		Optional<Resource> maybeDevice = getFirstResourceOfType(bundle, Enumerations.ResourceType.DEVICE);
		Device device = null;
		if (maybeDevice.isPresent()) {
			device = (Device) maybeDevice.get();
		}

		// then
		Assert.assertNotNull(device);

		Assert.assertEquals(root, device.getIdentifierFirstRep().getSystem());
		Assert.assertEquals(idExtension, device.getIdentifierFirstRep().getValue());

		Device.DeviceUdiCarrierComponent udiCarrier = device.getUdiCarrierFirstRep();
		Assert.assertEquals(root, udiCarrier.getIssuer());
		Assert.assertEquals(idExtension, udiCarrier.getDeviceIdentifier());
		Assert.assertEquals(playingDeviceCode, device.getType().getCoding().get(0).getCode());
		Assert.assertEquals("urn:oid:" + system, device.getType().getCoding().get(0).getSystem());
		Assert.assertEquals(scopingEntityExtension, udiCarrier.getJurisdiction());

		appendToResultFile(endOfTestMsg);
	}

	@Test
	public void testProcedure2Procedure_additionalDeviceInformation() {
	    // given
		appendToResultFile("## TEST: ProductInstance2Device\n");

		String lotNumber = "1618";
		Date manufactureDate = new GregorianCalendar(2022, Calendar.MARCH, 30).getTime();
		String serialNumber = "314159265";
		Date expirationDate = new GregorianCalendar(2032, Calendar.FEBRUARY, 27).getTime();
		String deviceIdentifier = "(01)00848486001048(11)181015(10)ABC999(21)bi12342222(17)221015";

		BundleInfo bundleInfo = new BundleInfo(rt);
		ITransformationContext transformationContext = new TransformationContextImpl();
		CDAFactories factories = CDAFactories.init();

		ProcedureActivityProcedure pap = factories.consol.createProcedureActivityProcedure();

		// create & add ProductInstance
		{
			org.openhealthtools.mdht.uml.cda.Device baseDevice = factories.base.createDevice();
			baseDevice.setCode(factories.datatype.createCE("2282003",
					"2.16.840.1.113883.6.96",
					"SNOMED CT",
					"Breast Implant"));

			ProductInstance productInstance = factories.consol.createProductInstance();
			productInstance.getIds().add(factories.datatype.createII(FDA_OID, deviceIdentifier));
			productInstance.setPlayingDevice(baseDevice);

			Entity scopingEntity = CDAFactories.init().base.createEntity();
			scopingEntity.getIds().add(new IDGenerator("testRoot", "testExtension").generate(factories));
			productInstance.setScopingEntity(scopingEntity);

			Participant2 participant = factories.base.createParticipant2();
			participant.setTypeCode(ParticipationType.DEV);
			participant.setParticipantRole(productInstance);

			pap.getParticipants().add(participant);
		}

		// create & add udi organizer & its necessary observations
		{
			Organizer udiOrganizer = factories.base.createOrganizer();
			udiOrganizer.getTemplateIds().add(factories.datatype.createII(UDI_ORGANIZER_TEMPLATE_OID, "2019-06-21"));
			udiOrganizer.setClassCode(x_ActClassDocumentEntryOrganizer.CLUSTER);
			udiOrganizer.getIds().add(factories.datatype.createII(FDA_OID, deviceIdentifier));

			// Lot or Batch Number Observation (-> Device.lotNumber)
			CD lotNumberCD = createNCIThesaurusCDWithCodeAndDisplay(factories, "C101672", "Lot or Batch Number");
			createAndAddManufacturingObservationToOrganizer(factories, lotNumberCD, lotNumber, "ED",
					udiOrganizer, LOT_OR_BATCH_NUMBER_TEMPLATE_ID);

			// Manufacturing Date Observation (-> Device.manufactureDate)
			CD manufacturingDateCD = createNCIThesaurusCDWithCodeAndDisplay(factories, "C101669", "Manufacturing Date");
			createAndAddManufacturingObservationToOrganizer(factories, manufacturingDateCD, "20220330", "TS",
					udiOrganizer, MANUFACTURING_DATE_TEMPLATE_ID);

			// Serial Number Observation (-> Device.serialNumber)
			CD serialNumberCD = createNCIThesaurusCDWithCodeAndDisplay(factories, "C101671", "Serial Number");
			createAndAddManufacturingObservationToOrganizer(factories, serialNumberCD, serialNumber, "ED",
					udiOrganizer, SERIAL_NUMBER_TEMPLATE_ID);

			// Expiration Date Observation (-> Device.expirationDate)
			CD expirationDateCD = createNCIThesaurusCDWithCodeAndDisplay(factories, "C101670", "Expiration Date");
			createAndAddManufacturingObservationToOrganizer(factories, expirationDateCD, "20320227", "TS",
					udiOrganizer, EXPIRATION_DATE_TEMPLATE_ID);

			// Distinct Identification Code Observation (-> Device.distinctIdentifier)
			CD distinctIdentifierCD = createNCIThesaurusCDWithCodeAndDisplay(factories, "C113843", "Distinct Identification Code");
			createAndAddManufacturingObservationToOrganizer(factories, distinctIdentifierCD, serialNumber, "ED",
					udiOrganizer, DISTINCT_IDENTIFICATION_CODE_TEMPLATE_ID);

			// Device Identifier Observation (-> Device.udiCarrier.deviceIdentifier)
			CD deviceIdentifierCD = createNCIThesaurusCDWithCodeAndDisplay(factories, "C101722", "Primary DI Number");
			createAndAddManufacturingObservationToOrganizer(factories, deviceIdentifierCD, deviceIdentifier, "II",
					udiOrganizer, DEVICE_IDENTIFIER_TEMPLATE_ID);

			pap.addOrganizer(udiOrganizer);
		}

	    // when
		appendToResultFile(transformationStartMsg);
		Bundle bundle = rt.tProcedure2Procedure(pap, bundleInfo, transformationContext).getBundle();
		appendToResultFile(transformationEndMsg);
		appendToResultFile(bundle);

		Optional<Resource> maybeDevice = getFirstResourceOfType(bundle, Enumerations.ResourceType.DEVICE);
		Device device = null;
		if (maybeDevice.isPresent()) {
			device = (Device) maybeDevice.get();
		}

		// then
		Assert.assertNotNull(device);
		Assert.assertEquals(lotNumber, device.getLotNumber());
		Assert.assertEquals(manufactureDate, device.getManufactureDate());
		Assert.assertEquals(serialNumber, device.getSerialNumber());
		Assert.assertEquals(expirationDate, device.getExpirationDate());
		Assert.assertEquals(serialNumber, device.getDistinctIdentifier());
		Assert.assertEquals(deviceIdentifier, device.getUdiCarrierFirstRep().getDeviceIdentifier());

		appendToResultFile(endOfTestMsg);
	}

	private CD createNCIThesaurusCDWithCodeAndDisplay(CDAFactories factories, String code, String displayName) {
		return factories.datatype.createCD(code,
				UMLS_OID,
				"NCI Thesaurus",
				displayName
		);
	}

	@Test
	public void testResultOrganizer2DiagnosticReport_USCore() {
		appendToResultFile("## TEST: ResultOrganizer2DiagnosticReport_USCore\n");

		// given
		BundleInfo bundleInfo = new BundleInfo(rt);
		ITransformationContext transformationContext = new TransformationContextImpl();
		CDAFactories factories = CDAFactories.init();
		DataTypesTransformerImpl dtt = new DataTypesTransformerImpl();

		String statusCode = "completed";
		CD radiologyCD = new CDGenerator(RADIOLOGY_CODING.getCode(), LOINC_OID, "LOINC", RADIOLOGY_CODING.getDisplay()).generate(factories);
		String subjectCode = "subject";

		org.openhealthtools.mdht.uml.cda.Observation cdaObservation = factories.base.createObservation();
		cdaObservation.setCode(radiologyCD);

		Organizer cdaOrganizer = factories.base.createOrganizer();
		cdaOrganizer.setCode(radiologyCD);
		cdaOrganizer.setStatusCode(new CSCodeGenerator(statusCode).generate(factories));
		cdaOrganizer.addObservation(cdaObservation);

		Author cdaAuthorParticipation = factories.base.createAuthor();
		cdaAuthorParticipation.getTemplateIds().add(factories.datatype.createII("2.16.840.1.113883.10.20.22.4.119"));
		cdaObservation.getAuthors().add(cdaAuthorParticipation);

		ResultOrganizer cdaResultOrganizer = factories.consol.createResultOrganizer();
		cdaResultOrganizer.getTemplateIds().add(factories.datatype.createII(RESULT_ORGANIZER_V3_OID));
		cdaResultOrganizer.setCode(radiologyCD);
		IVL_TS effectiveTime = new IVL_TSPeriodGenerator("20220908", "20220909").generate(factories);
		cdaResultOrganizer.setEffectiveTime(effectiveTime);

		Subject cdaSubject = factories.base.createSubject();
		RelatedSubject relatedSubject = factories.base.createRelatedSubject();
		relatedSubject.setCode(new CEGenerator(subjectCode).generate(factories));
		cdaSubject.setRelatedSubject(relatedSubject);

		ResultObservation resultObservation = factories.consol.createResultObservation();
		String testData = "test data";
		resultObservation.getValues().add(factories.datatype.createED(testData));

		org.openhealthtools.mdht.uml.cda.Encounter cdaEncounter = factories.base.createEncounter();
		II ii = IDGenerator.getNextInstance().generate(factories);
		cdaEncounter.getIds().add(ii);
		resultObservation.addEncounter(cdaEncounter);

		cdaResultOrganizer.addObservation(resultObservation);
		cdaResultOrganizer.addOrganizer(cdaOrganizer);
		cdaResultOrganizer.setSubject(cdaSubject);

		// when
		appendToResultFile(transformationStartMsg);
		Bundle bundle = rt.tResultOrganizer2DiagnosticReport(cdaResultOrganizer, bundleInfo, transformationContext).getBundle();
		appendToResultFile(transformationEndMsg);
		Optional<Resource> maybeDiagReport = getFirstResourceOfType(bundle, Enumerations.ResourceType.DIAGNOSTICREPORT);
		DiagnosticReport diagReport = null;
		if (maybeDiagReport.isPresent()) {
			diagReport = (DiagnosticReport) maybeDiagReport.get();
		}

		// then
		Assert.assertNotNull(diagReport);

		Assert.assertEquals(RADIOLOGY_CODING.getCode(), diagReport.getCategoryFirstRep().getCodingFirstRep().getCode());
		Assert.assertEquals(RADIOLOGY_CODING.getSystem(), diagReport.getCategoryFirstRep().getCodingFirstRep().getSystem());
		Assert.assertEquals(RADIOLOGY_CODING.getDisplay(), diagReport.getCategoryFirstRep().getCodingFirstRep().getDisplay());

		Assert.assertEquals(dtt.tIVL_TS2DateTime(effectiveTime).getValue(), diagReport.getIssued());

		Assert.assertEquals(RADIOLOGY_CODING.getCode(), diagReport.getCode().getCodingFirstRep().getCode());
		Assert.assertEquals(RADIOLOGY_CODING.getSystem(), diagReport.getCode().getCodingFirstRep().getSystem());
		Assert.assertEquals(RADIOLOGY_CODING.getDisplay(), diagReport.getCode().getCodingFirstRep().getDisplay());

		Assert.assertEquals(new GregorianCalendar(2022, Calendar.SEPTEMBER, 8).getTime(), diagReport.getEffectivePeriod().getStart());
		Assert.assertEquals(new GregorianCalendar(2022, Calendar.SEPTEMBER, 9).getTime(), diagReport.getEffectivePeriod().getEnd());

		Assert.assertNotNull(diagReport.getPerformerFirstRep());
		Assert.assertNotNull(diagReport.getSubject());

		Assert.assertFalse(diagReport.getPresentedForm().isEmpty());
		Attachment presentedForm = diagReport.getPresentedFormFirstRep();
		Assert.assertEquals(testData, new String(presentedForm.getData(), StandardCharsets.UTF_8));
		Assert.assertNull(presentedForm.getUrl());
		Assert.assertEquals(PLAINTEXT, presentedForm.getContentType());

		appendToResultFile(endOfTestMsg);
	}

	private void createAndAddManufacturingObservationToOrganizer(CDAFactories factories, CD cd, String value, String type, Organizer organizer, String root) {
		org.openhealthtools.mdht.uml.cda.Observation manufacturingObservation = ObservationGenerator.getDefaultInstance().generate(factories);
		manufacturingObservation.setCode(cd);
		ANY any;
		switch (type) {
			case "ED":
				any = factories.datatype.createED(value);
				break;
			case "TS":
				any = factories.datatype.createTS(value);
				break;
			case "II":
				any = factories.datatype.createII("127.0.0.1", value);
				break;
			default:
				// extend as needed
				any = null;
				break;
		}
		manufacturingObservation.getValues().add(any);
		manufacturingObservation.getTemplateIds().add(factories.datatype.createII(root, CDA_R2_1_EFFECTIVE_DATE));
		Component4 component = factories.base.createComponent4();
		component.setObservation(manufacturingObservation);
		organizer.getComponents().add(component);
	}

	@Test
	public void testResultOrganizer2DiagnosticReport() {
		BundleInfo bundleInfo = new BundleInfo(rt);
		ITransformationContext transformationContext = new TransformationContextImpl();
		appendToResultFile("## TEST: ResultOrganizer2DiagnosticReport\n");
		// null instance test
		ResultOrganizer cdaNull = null;
		Bundle fhirNull = rt.tResultOrganizer2DiagnosticReport(cdaNull, bundleInfo, transformationContext).getBundle();
		Assert.assertNull(fhirNull);

		// instance from file
		ResultsSection resultsSec = ResourceTransformerTest.ccd.getResultsSection();

		if (resultsSec != null && !resultsSec.isSetNullFlavor()) {
			if (resultsSec.getOrganizers() != null && !resultsSec.getOrganizers().isEmpty()) {
				for (org.openhealthtools.mdht.uml.cda.Organizer cdaOrganizer : resultsSec.getOrganizers()) {
					if (cdaOrganizer != null && !cdaOrganizer.isSetNullFlavor()) {
						if (cdaOrganizer instanceof ResultOrganizer) {
							appendToResultFile(transformationStartMsg);
							Bundle fhirDiagReport = rt
									.tResultOrganizer2DiagnosticReport((ResultOrganizer) cdaOrganizer, bundleInfo, transformationContext)
									.getBundle();
							appendToResultFile(transformationEndMsg);
							appendToResultFile(fhirDiagReport);
						}
					}
				}
			}
		}
		appendToResultFile(endOfTestMsg);
	}

	@Test
	public void testSection2Section() {
		appendToResultFile("## TEST: Section2Section\n");
		// null instance test
		org.openhealthtools.mdht.uml.cda.Section cdaNull = null;
		SectionComponent fhirNull = rt.tSection2Section(cdaNull);
		Assert.assertNull(fhirNull);

		// instances from file
		org.openhealthtools.mdht.uml.cda.Section sampleSection = null;

		// assigning sampleSection to one sample section
		if (ResourceTransformerTest.ccd.getEncountersSection() != null
				&& !ResourceTransformerTest.ccd.getEncountersSection().isSetNullFlavor()) {
			if (ResourceTransformerTest.ccd.getEncountersSection().getAllSections() != null
					&& !ResourceTransformerTest.ccd.getEncountersSection().getAllSections().isEmpty()) {
				if (ResourceTransformerTest.ccd.getEncountersSection().getAllSections().get(0) != null
						&& !ResourceTransformerTest.ccd.getEncountersSection().getAllSections().get(0)
								.isSetNullFlavor()) {
					sampleSection = ResourceTransformerTest.ccd.getEncountersSection().getAllSections().get(0);
				}
			}
		}
		if (sampleSection != null) {
			org.hl7.fhir.r4.model.Composition fhirComposition = new org.hl7.fhir.r4.model.Composition();
			appendToResultFile(transformationStartMsg);
			SectionComponent fhirSection = rt.tSection2Section(sampleSection);
			appendToResultFile(transformationEndMsg);
			fhirComposition.addSection(fhirSection);
			appendToResultFile(fhirComposition);
		}
		appendToResultFile(endOfTestMsg);
	}

	@Test
	public void testSocialHistory() {
		BundleInfo bundleInfo = new BundleInfo(rt);
		ITransformationContext transformationContext = new TransformationContextImpl();
		appendToResultFile("## TEST: SocialHistory\n");
		SocialHistorySection socialHistSec = ResourceTransformerTest.ccd.getSocialHistorySection();

		if (socialHistSec != null && !socialHistSec.isSetNullFlavor()) {
			if (socialHistSec.getObservations() != null && !socialHistSec.getObservations().isEmpty()) {
				for (org.openhealthtools.mdht.uml.cda.Observation cdaObs : socialHistSec.getObservations()) {
					if (cdaObs != null && !cdaObs.isSetNullFlavor()) {
						appendToResultFile(transformationStartMsg);
						Bundle fhirObs = rt.tObservation2Observation(cdaObs, bundleInfo, transformationContext).getBundle();
						appendToResultFile(transformationEndMsg);
						appendToResultFile(fhirObs);
					}
				}
			}
		}
		appendToResultFile(endOfTestMsg);
	}

	@Test
	public void testVitalSignObservation2Observation() {
		BundleInfo bundleInfo = new BundleInfo(rt);
		ITransformationContext transformationContext = new TransformationContextImpl();
		appendToResultFile("## TEST: VitalSignObservation2Observation\n");
		// null instance test
		org.openhealthtools.mdht.uml.cda.consol.VitalSignObservation cdaNull = null;
		Bundle fhirNull = rt.tVitalSignObservation2Observation(cdaNull, bundleInfo, transformationContext).getBundle();
		Assert.assertNull(fhirNull);

		// instances from file
		VitalSignsSectionEntriesOptional vitalSignsSec = ResourceTransformerTest.ccd
				.getVitalSignsSectionEntriesOptional();
		if (vitalSignsSec != null && !vitalSignsSec.isSetNullFlavor()) {
			if (vitalSignsSec.getVitalSignsOrganizers() != null && !vitalSignsSec.getVitalSignsOrganizers().isEmpty()) {
				for (VitalSignsOrganizer vitalSignOrganizer : vitalSignsSec.getVitalSignsOrganizers()) {
					if (vitalSignOrganizer != null && !vitalSignOrganizer.isSetNullFlavor()) {
						if (vitalSignOrganizer.getVitalSignObservations() != null
								&& !vitalSignOrganizer.getVitalSignObservations().isEmpty()) {
							for (VitalSignObservation vitalSignObservation : vitalSignOrganizer
									.getVitalSignObservations()) {
								if (vitalSignObservation != null && !vitalSignObservation.isSetNullFlavor()) {
									appendToResultFile(transformationStartMsg);
									Bundle fhirObservation = rt
											.tVitalSignObservation2Observation(vitalSignObservation, bundleInfo, transformationContext)
											.getBundle();
									appendToResultFile(transformationEndMsg);
									appendToResultFile(fhirObservation);
								}
							}
						}
					}
				}
			}
		}
		appendToResultFile(endOfTestMsg);
	}

	@Test
	public void testVitalSignObservation2Observation_statusCodeToCategory() {
		appendToResultFile("## TEST: VitalSignObservation2Observation_statusCodeToCategory\n");

		// given
		ObservationCategory vitalSigns = ObservationCategory.VITALSIGNS;

		BundleInfo bundleInfo = new BundleInfo(rt);
		ITransformationContext transformationContext = new TransformationContextImpl();
		CDAFactories factories = CDAFactories.init();

		VitalSignObservation cdaVitalSignObservation = factories.consol.createVitalSignObservation();
		cdaVitalSignObservation.setStatusCode(factories.datatype.createCS("vital-signs"));

		String units = "kg";
		BigDecimal value = BigDecimal.valueOf(3.14);
		PQ pq = factories.datatype.createPQ();
		pq.setValue(value);
		pq.setUnit(units);
		cdaVitalSignObservation.getValues().add(pq);

		// when
		Bundle bundle = rt.tVitalSignObservation2Observation(cdaVitalSignObservation, bundleInfo, transformationContext).getBundle();

	    // then
		Optional<Resource> maybeObservation = getFirstResourceOfType(bundle, Enumerations.ResourceType.OBSERVATION);
		Observation fhirObservation = null;
		if (maybeObservation.isPresent()) {
			fhirObservation = (Observation) maybeObservation.get();
		}

		// then
		Assert.assertNotNull(fhirObservation);
		Assert.assertEquals(vitalSigns.toCode(), fhirObservation.getCategoryFirstRep().getCodingFirstRep().getCode());
		Assert.assertEquals(vitalSigns.getSystem(), fhirObservation.getCategoryFirstRep().getCodingFirstRep().getSystem());

		Quantity fhirQuantity = (Quantity) fhirObservation.getValue();
		Assert.assertEquals(value, fhirQuantity.getValue());
		Assert.assertEquals("http://unitsofmeasure.org", fhirQuantity.getSystem());
		Assert.assertEquals(units, fhirQuantity.getCode());
		Assert.assertEquals(units, fhirQuantity.getUnit());

		appendToResultFile(endOfTestMsg);
	}

	@Test
	public void testObservation2Observation_ed2ValueString() {
		appendToResultFile("## TEST: Observation2Observation_ed2ValueString\n");

		// given
		BundleInfo bundleInfo = new BundleInfo(rt);
		ITransformationContext transformationContext = new TransformationContextImpl();
		CDAFactories factories = CDAFactories.init();

		org.openhealthtools.mdht.uml.cda.Observation cdaObservation = factories.base.createObservation();

		String edText = "test text";
		ED ed = factories.datatype.createED(edText);
		cdaObservation.getValues().add(ed);

		// when
		Bundle bundle = rt.tObservation2Observation(cdaObservation, bundleInfo, transformationContext).getBundle();

		// then
		Optional<Resource> maybeObservation = getFirstResourceOfType(bundle, Enumerations.ResourceType.OBSERVATION);
		Observation fhirObservation = null;
		if (maybeObservation.isPresent()) {
			fhirObservation = (Observation) maybeObservation.get();
		}

		// then
		Assert.assertNotNull(fhirObservation);
		Assert.assertEquals(edText, fhirObservation.getValueStringType().toString());

		appendToResultFile(endOfTestMsg);
	}

	// this previously would have failed
	@Test
	public void testUsCoreDiagnosticReportCodingsContains() {
		// given
		CodeableConcept cc = new CodeableConcept()
				.addCoding(new Coding(RADIOLOGY_CODING.getSystem(), null, null))
				.addCoding(new Coding(null, PATHOLOGY_CODING.getCode(), null))
				.addCoding(new Coding(null, null, CARDIOLOGY_CODING.getDisplay()));

		// when
		boolean result = rt.usCoreDiagnosticReportCodingsContains(cc);

		// then
		Assert.assertFalse(result);
	}

	private void verifyExtension(Extension theExt, String theCode, String theDisplay) {

		Assert.assertEquals(DATA_ABSENT_REASON_EXTENSION_URI, theExt.getUrl());
		Coding extCode = (Coding)theExt.getValue();
		Assert.assertEquals(DATA_ABSENT_REASON_TERMINOLOGY_URI, extCode.getSystem());
		Assert.assertEquals(theCode, extCode.getCode());
		Assert.assertEquals(theDisplay, extCode.getDisplay());
	}

	private void appendToResultFile(Object param) {
		try {
			if (param instanceof String) {
				resultFW.append((String) param);
			} else if (param instanceof IResource) {
				FHIRUtil.printJSON((IResource) param, resultFW);
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}