package tr.com.srdc.cda2fhir.testutil.generator;

import org.hl7.fhir.r4.model.Bundle;
import org.junit.Assert;
import org.openhealthtools.mdht.uml.cda.EntryRelationship;
import org.openhealthtools.mdht.uml.cda.consol.EncounterDiagnosis;
import org.openhealthtools.mdht.uml.hl7.datatypes.CD;
import org.openhealthtools.mdht.uml.hl7.vocab.x_ActRelationshipEntryRelationship;
import tr.com.srdc.cda2fhir.testutil.CDAFactories;

import java.util.ArrayList;
import java.util.List;

public class EncounterDiagnosisGenerator {
	private CDGenerator codeGenerator;
	private List<ProblemObservationGenerator> problemGenerators = new ArrayList<>();

	public List<ProblemObservationGenerator> getProblemGenerators() {
		return problemGenerators;
	}

	public EncounterDiagnosis generate(CDAFactories factories) {
		EncounterDiagnosis ed = factories.consol.createEncounterDiagnosis();

		if (codeGenerator != null) {
			CD cd = codeGenerator.generate(factories);
			ed.setCode(cd);
		}

		problemGenerators.forEach(pg -> {
			EntryRelationship er = factories.base.createEntryRelationship();
			er.setTypeCode(x_ActRelationshipEntryRelationship.SUBJ);
			er.setObservation(pg.generate(factories));
			ed.getEntryRelationships().add(er);
		});

		return ed;
	}

	public static EncounterDiagnosisGenerator getDefaultInstance() {
		EncounterDiagnosisGenerator ecg = new EncounterDiagnosisGenerator();

		ecg.codeGenerator = CDGenerator.getNextInstance();
		ecg.problemGenerators.add(ProblemObservationGenerator.getEncounterDiagnosisInstance());

		return ecg;
	}

	public void verify(Bundle bundle) throws Exception {
		// NO-OP - the encounter diagnosis gets flattened away during transformation
		Assert.fail("Not supported - please call getProblemGenerators() and validate directly against that collection.");
	}

}
