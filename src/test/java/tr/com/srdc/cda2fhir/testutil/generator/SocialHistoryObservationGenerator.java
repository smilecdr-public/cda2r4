package tr.com.srdc.cda2fhir.testutil.generator;

import com.bazaarvoice.jolt.JsonUtils;
import org.hl7.fhir.r4.model.Bundle;
import org.junit.Assert;
import org.openhealthtools.mdht.uml.cda.Observation;
import org.openhealthtools.mdht.uml.hl7.datatypes.ANY;
import org.openhealthtools.mdht.uml.hl7.datatypes.CD;
import org.openhealthtools.mdht.uml.hl7.datatypes.CS;
import org.openhealthtools.mdht.uml.hl7.datatypes.II;
import org.openhealthtools.mdht.uml.hl7.datatypes.IVL_TS;
import tr.com.srdc.cda2fhir.testutil.BundleUtil;
import tr.com.srdc.cda2fhir.testutil.CDAFactories;
import tr.com.srdc.cda2fhir.testutil.TestSetupException;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class SocialHistoryObservationGenerator {
	private static final Map<String, Object> OBSERVATION_STATUS = JsonUtils
			.filepathToMap("src/test/resources//value-maps/ObservationStatus.json");

	private List<IDGenerator> idGenerators = new ArrayList<>();

	private CDGenerator codeGenerator;

	private CSCodeGenerator statusCodeGenerator;

	private IVL_TSPeriodGenerator effectiveTimeGenerator;

	private List<AnyGenerator> valueGenerators = new ArrayList<>();

	public Observation createForGenerate(CDAFactories factories) {
		return factories.consol.createSocialHistoryObservation();
	}

	public Observation generate(CDAFactories factories) {
		Observation obs = createForGenerate(factories);

		idGenerators.forEach(idGenerator -> {
			II ii = idGenerator.generate(factories);
			obs.getIds().add(ii);
		});

		if (codeGenerator != null) {
			CD code = codeGenerator.generate(factories);
			obs.setCode(code);
		}

		if (statusCodeGenerator != null) {
			CS cs = statusCodeGenerator.generate(factories);
			obs.setStatusCode(cs);
		}

		if (effectiveTimeGenerator != null) {
			IVL_TS ivlTs = effectiveTimeGenerator.generate(factories);
			obs.setEffectiveTime(ivlTs);
		}

		valueGenerators.forEach(vg -> {
			ANY any = vg.generate(factories);
			obs.getValues().add(any);
		});

		return obs;
	}

	public static void fillDefaultInstance(SocialHistoryObservationGenerator obs) {
		obs.idGenerators.add(IDGenerator.getNextInstance());
		obs.codeGenerator = CDGenerator.getNextInstance();
		obs.statusCodeGenerator = new CSCodeGenerator(OBSERVATION_STATUS, "completed");
		obs.statusCodeGenerator.set("active");
		obs.effectiveTimeGenerator = IVL_TSPeriodGenerator.getDefaultInstance();
		obs.valueGenerators.add(new AnyGenerator(CDGenerator.getNextInstance()));
	}

	public static SocialHistoryObservationGenerator getDefaultInstance() {
		SocialHistoryObservationGenerator generator = new SocialHistoryObservationGenerator();
		fillDefaultInstance(generator);
		return generator;
	}

	public void verify(org.hl7.fhir.r4.model.Observation observation) {
		if (idGenerators.isEmpty()) {
			Assert.assertFalse("No observation identifier", observation.hasIdentifier());
		} else {
			for (int index = 0; index < idGenerators.size(); ++index) {
				idGenerators.get(index).verify(observation.getIdentifier().get(index));
			}
		}

		if (codeGenerator == null) {
			Assert.assertFalse("No observation status", observation.hasCode());
		} else {
			codeGenerator.verify(observation.getCode());
		}

		if (statusCodeGenerator == null) {
			Assert.assertFalse("No observation status", observation.hasStatus());
		} else {
			statusCodeGenerator.verify(observation.getStatus().toCode());
		}

		if (effectiveTimeGenerator == null) {
			Assert.assertFalse("No observation effective period", observation.hasEffectivePeriod());
		} else {
			effectiveTimeGenerator.verify(observation.getEffectivePeriod());
		}

		if (valueGenerators.isEmpty()) {
			Assert.assertFalse("No observation value", observation.hasValue());
		} else {
			AnyGenerator ag = valueGenerators.get(valueGenerators.size() - 1);
			if (observation.hasValueCodeableConcept()) {
				ag.verify(observation.getValueCodeableConcept());
			} else if (observation.hasValueQuantity()) {
				ag.verify(observation.getValueQuantity());
			} else if (observation.hasValueStringType()) {
				ag.verify(observation.getValueStringType().getValueAsString());
			} else if (observation.hasValueRange()) {
				ag.verify(observation.getValueRange());
			} else if (observation.hasValueRatio()) {
				ag.verify(observation.getValueRatio());
			} else if (observation.hasValueDateTimeType()) {
				ag.verify(observation.getValueDateTimeType());
			} else if (observation.hasValueBooleanType()) {
				ag.verify(observation.getValueBooleanType().booleanValue());
			} else {
				throw new TestSetupException("Invalid observation value");
			}
		}

	}

	public void verify(Bundle bundle) throws Exception {
		org.hl7.fhir.r4.model.Observation obs = BundleUtil.findOneResource(bundle,
				org.hl7.fhir.r4.model.Observation.class);
		verify(obs);
	}
}
