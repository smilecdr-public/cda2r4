package tr.com.srdc.cda2fhir.testutil.generator;

import org.junit.Assert;
import org.openhealthtools.mdht.uml.cda.Device;
import org.openhealthtools.mdht.uml.cda.Entity;
import org.openhealthtools.mdht.uml.cda.consol.ProductInstance;
import org.openhealthtools.mdht.uml.hl7.datatypes.II;
import tr.com.srdc.cda2fhir.testutil.CDAFactories;

import java.util.ArrayList;
import java.util.List;

import static tr.com.srdc.cda2fhir.transform.ResourceTransformerImpl.FDA_OID;

public class ProductInstanceGenerator {

    private List<IDGenerator> idGenerators = new ArrayList<>();

    private CEGenerator playingDeviceCodeGenerator;

    private IDGenerator scopingEntityIdGenerator;

    public ProductInstance generate(CDAFactories factories) {
        ProductInstance pi = factories.consol.createProductInstance();

        idGenerators.forEach(idGenerator -> {
            II ii = idGenerator.generate(factories);
            pi.getIds().add(ii);
        });

        if (playingDeviceCodeGenerator != null) {
            Device playingDevice = factories.base.createDevice();
            playingDevice.setCode(playingDeviceCodeGenerator.generate(factories));
            pi.setPlayingDevice(playingDevice);
        }

        if (scopingEntityIdGenerator != null) {
            Entity scopingEntity = factories.base.createEntity();
            scopingEntity.getIds().add(scopingEntityIdGenerator.generate(factories));
            pi.setScopingEntity(scopingEntity);
        }

        return pi;
    }

    public static ProductInstanceGenerator getDefaultInstance() {
        ProductInstanceGenerator pig = new ProductInstanceGenerator();

        pig.idGenerators.add(IDGenerator.getNextInstance());
        pig.playingDeviceCodeGenerator = CEGenerator.getNextInstance();
        pig.scopingEntityIdGenerator = IDGenerator.getNextInstance();

        return pig;
    }

    public static ProductInstanceGenerator getFDAInstance() {
        ProductInstanceGenerator pig = new ProductInstanceGenerator();

        pig.idGenerators.add(IDGenerator.getInstanceWithRoot(FDA_OID));
        pig.playingDeviceCodeGenerator = CEGenerator.getNextInstance();
        pig.scopingEntityIdGenerator = IDGenerator.getNextInstance();

        return pig;
    }

    public void verify(org.hl7.fhir.r4.model.Device device) {
        if (!idGenerators.isEmpty()) {
            for (int index = 0; index < idGenerators.size(); ++index) {
                if (FDA_OID.equals(idGenerators.get(index).getSystem())) {
                    Assert.assertEquals("Non-FDA device identifier", idGenerators.get(index).getValue(), device.getUdiCarrier().get(index).getCarrierHRF());
                } else {
                    Assert.assertEquals("Non-FDA device identifier", idGenerators.get(index).getValue(), device.getUdiCarrier().get(index).getDeviceIdentifier());
                }
            }
        } else {
            Assert.assertFalse("No UDI Carrier", device.hasUdiCarrier());
        }

        if (playingDeviceCodeGenerator != null) {
            playingDeviceCodeGenerator.verify(device.getType());
        } else {
            Assert.assertFalse("No type", device.hasType());
        }

        if (scopingEntityIdGenerator != null) {
            for (org.hl7.fhir.r4.model.Device.DeviceUdiCarrierComponent udiCarrier : device.getUdiCarrier()) {
                Assert.assertEquals("Udi Carrier identifier", scopingEntityIdGenerator.getValue(), udiCarrier.getJurisdiction());
            }
        }
    }
}
