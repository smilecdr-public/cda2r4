package tr.com.srdc.cda2fhir.testutil.generator;

import com.bazaarvoice.jolt.JsonUtils;
import org.hl7.fhir.r4.model.Bundle;
import org.hl7.fhir.r4.model.Observation;
import org.hl7.fhir.r4.model.Practitioner;
import org.hl7.fhir.r4.model.PractitionerRole;
import org.hl7.fhir.r4.model.Reference;
import org.junit.Assert;
import org.openhealthtools.mdht.uml.cda.EntryRelationship;
import org.openhealthtools.mdht.uml.cda.Performer2;
import org.openhealthtools.mdht.uml.cda.consol.Indication;
import org.openhealthtools.mdht.uml.cda.consol.ProcedureActivityObservation;
import org.openhealthtools.mdht.uml.hl7.datatypes.CD;
import org.openhealthtools.mdht.uml.hl7.datatypes.CS;
import org.openhealthtools.mdht.uml.hl7.datatypes.II;
import org.openhealthtools.mdht.uml.hl7.datatypes.IVL_TS;
import org.openhealthtools.mdht.uml.hl7.vocab.NullFlavor;
import org.openhealthtools.mdht.uml.hl7.vocab.x_ActRelationshipEntryRelationship;
import tr.com.srdc.cda2fhir.testutil.BundleUtil;
import tr.com.srdc.cda2fhir.testutil.CDAFactories;
import tr.com.srdc.cda2fhir.testutil.TestSetupException;
import tr.com.srdc.cda2fhir.transform.entry.impl.EntryResult;
import tr.com.srdc.cda2fhir.transform.util.IDeferredReference;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class ProcedureActivityObservationGenerator {
	private static final Map<String, Object> OSERVATION_STATUS = JsonUtils
			.filepathToMap("src/test/resources//value-maps/ObservationStatus.json");

	private List<IDGenerator> idGenerators = new ArrayList<>();
	private IVL_TSPeriodGenerator ivlTsGenerator;
	private List<CDGenerator> targetSiteCodeGenerators = new ArrayList<>();

	private String statusCode;
	private String statusCodeNullFlavor;

	private CDGenerator codeGenerator;

	private List<CDGenerator> reasonCodeGenerators = new ArrayList<>();

	private List<PerformerGenerator> performerGenerators = new ArrayList<>();

	private List<MedicationActivityGenerator> medicationActivityGenerators = new ArrayList<>();

	private List<EncounterActivityGenerator> encounterActivityGenerators = new ArrayList<>();

	private List<ReactionObservationGenerator> reactionObservationGenerators = new ArrayList<>();

	public ProcedureActivityObservation generate(CDAFactories factories) {
		ProcedureActivityObservation pao = factories.consol.createProcedureActivityObservation();

		idGenerators.forEach(idGenerator -> {
			II ii = idGenerator.generate(factories);
			pao.getIds().add(ii);
		});

		if (ivlTsGenerator != null) {
			IVL_TS ivlTs = ivlTsGenerator.generate(factories);
			pao.setEffectiveTime(ivlTs);
		}

		targetSiteCodeGenerators.forEach(tcg -> {
			CD cd = tcg.generate(factories);
			pao.getTargetSiteCodes().add(cd);
		});

		if (statusCode != null || statusCodeNullFlavor != null) {
			CS cs = factories.datatype.createCS();
			if (statusCode != null) {
				cs.setCode(statusCode);
			}
			if (statusCodeNullFlavor != null) {
				NullFlavor nf = NullFlavor.get(statusCodeNullFlavor);
				if (nf == null) {
					throw new TestSetupException("Invalid null flavor enumeration.");
				}
				cs.setNullFlavor(nf);
			}
			pao.setStatusCode(cs);
		}

		if (codeGenerator != null) {
			CD cd = codeGenerator.generate(factories);
			pao.setCode(cd);
		}

		reasonCodeGenerators.forEach(rcg -> {
			EntryRelationship er = factories.base.createEntryRelationship();
			pao.getEntryRelationships().add(er);
			er.setTypeCode(x_ActRelationshipEntryRelationship.RSON);
			Indication indication = factories.consol.createIndication();
			er.setObservation(indication);
			CD cd = rcg.generate(factories);
			indication.setCode(cd);
		});

		performerGenerators.forEach(pg -> {
			Performer2 performer = pg.generate(factories);
			pao.getPerformers().add(performer);
		});

		medicationActivityGenerators.forEach(mag -> {
			EntryRelationship er = factories.base.createEntryRelationship();
			pao.getEntryRelationships().add(er);
			er.setTypeCode(x_ActRelationshipEntryRelationship.COMP);
			er.setSubstanceAdministration(mag.generate(factories));
		});

		encounterActivityGenerators.forEach(eag -> {
			EntryRelationship er = factories.base.createEntryRelationship();
			er.setTypeCode(x_ActRelationshipEntryRelationship.COMP);
			er.setInversionInd(true);
			er.setEncounter(eag.generate(factories));

			pao.getEntryRelationships().add(er);
		});

		reactionObservationGenerators.forEach(rog -> {
			EntryRelationship er = factories.base.createEntryRelationship();
			er.setTypeCode(x_ActRelationshipEntryRelationship.COMP);
			er.setObservation(rog.generate(factories));

			pao.getEntryRelationships().add(er);
		});

		return pao;
	}

	public static ProcedureActivityObservationGenerator getDefaultInstance() {
		ProcedureActivityObservationGenerator paog = new ProcedureActivityObservationGenerator();

		paog.idGenerators.add(IDGenerator.getNextInstance());
		paog.ivlTsGenerator = IVL_TSPeriodGenerator.getDefaultInstance();
		paog.targetSiteCodeGenerators.add(CDGenerator.getNextInstance());
		paog.statusCode = "active";
		paog.codeGenerator = CDGenerator.getNextInstance();
		paog.reasonCodeGenerators.add(CDGenerator.getNextInstance());
		paog.performerGenerators.add(PerformerGenerator.getDefaultInstance());
		paog.encounterActivityGenerators.add(EncounterActivityGenerator.getDefaultInstance());

		return paog;
	}

	public void verify(Observation observation) {
		if (!idGenerators.isEmpty()) {
			for (int index = 0; index < idGenerators.size(); ++index) {
				idGenerators.get(index).verify(observation.getIdentifier().get(index));
			}
		} else {
			Assert.assertTrue("No condition identifier", !observation.hasIdentifier());
		}

		if (ivlTsGenerator == null) {
			Assert.assertTrue("Missing procedure performed", !observation.hasEffectivePeriod());
		} else {
			ivlTsGenerator.verify(observation.getEffectivePeriod());
		}

		if (targetSiteCodeGenerators.isEmpty()) {
			Assert.assertTrue("Missing procedure target site", !observation.hasBodySite());
		} else {
			targetSiteCodeGenerators.get(0).verify(observation.getBodySite());
		}

		if (statusCode == null || statusCodeNullFlavor != null) {
			Assert.assertTrue("Missing procedure status", !observation.hasStatus());
		} else {
			String expected = (String) OSERVATION_STATUS.get(statusCode);
			if (expected == null) {
				expected = "unknown";
			}
			Assert.assertEquals("Procedure status", expected, observation.getStatus().toCode());
		}

		if (codeGenerator == null) {
			Assert.assertTrue("Missing procedure code", !observation.hasCode());
		} else {
			codeGenerator.verify(observation.getCode());
		}

		if (performerGenerators.isEmpty()) {
			Assert.assertTrue("Missing procedure performer", !observation.hasPerformer());
		} else {
			Assert.assertEquals("Procedure performer count", performerGenerators.size(),
					observation.getPerformer().size());
		}
	}

	public void verify(Bundle bundle, Observation observation) throws Exception {
		verify(observation);

		if (performerGenerators.isEmpty()) {
			Assert.assertFalse("Missing procedure performer", observation.hasPerformer());
		} else {
			BundleUtil util = new BundleUtil(bundle);

			for (int index = 0; index < performerGenerators.size(); ++index) {
				PerformerGenerator pg = performerGenerators.get(index);
				Reference performerReference = observation.getPerformer().get(index);

				String practitionerId = performerReference.getReference();
				pg.verifyFromPractionerId(bundle, practitionerId);

				Practitioner practitioner = util.getResourceFromReference(practitionerId, Practitioner.class);
				pg.verify(practitioner);

				PractitionerRole role = util.getPractitionerRole(practitionerId);
				pg.verify(role);
			}
		}
	}

	public void verify(Bundle bundle) throws Exception {
		Observation observation = BundleUtil.findOneResource(bundle, Observation.class);
		verify(bundle, observation);
	}

	public void verifyDeferredReferences(EntryResult entryResult) {
		if (!encounterActivityGenerators.isEmpty()) {
			for (int index = 0; index < encounterActivityGenerators.size(); ++index) {
				IDGenerator idGenerator = encounterActivityGenerators.get(index).getIDGenerator(0);
				IDeferredReference deferredReference = entryResult.getDeferredReferences().get(index);
				idGenerator.verify(deferredReference.getIdentifier());
			}
		}
	}

}
