package tr.com.srdc.cda2fhir.testutil.generator;

import com.bazaarvoice.jolt.JsonUtils;
import org.hl7.fhir.r4.model.AdverseEvent;
import org.hl7.fhir.r4.model.Bundle;
import org.hl7.fhir.r4.model.Condition;
import org.junit.Assert;
import org.openhealthtools.mdht.uml.cda.EntryRelationship;
import org.openhealthtools.mdht.uml.cda.Observation;
import org.openhealthtools.mdht.uml.cda.consol.ReactionObservation;

import org.openhealthtools.mdht.uml.cda.consol.SeverityObservation;
import org.openhealthtools.mdht.uml.hl7.datatypes.CE;
import org.openhealthtools.mdht.uml.hl7.datatypes.II;
import org.openhealthtools.mdht.uml.hl7.vocab.ActClassObservation;
import org.openhealthtools.mdht.uml.hl7.vocab.x_ActMoodDocumentObservation;
import org.openhealthtools.mdht.uml.hl7.vocab.x_ActRelationshipEntryRelationship;
import tr.com.srdc.cda2fhir.testutil.BundleUtil;
import tr.com.srdc.cda2fhir.testutil.CDAFactories;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class ReactionObservationGenerator extends ObservationGenerator {
	private static final Map<String, Object> REACTION_OBSERVATION_CODE = JsonUtils
			.filepathToMap("src/test/resources//value-maps/ReactionObservationCode.json");
	private static final Map<String, Object> ADVERSE_EVENT_SEVERITY = JsonUtils
			.filepathToMap("src/test/resources//value-maps/Severity.json");

	private CDCodeGenerator codeGenerator;
	private CECodeGenerator severityGenerator;

	@Override
	public ReactionObservation createForGenerate(CDAFactories factories) {
		return factories.consol.createReactionObservation();
	}

	public static ReactionObservationGenerator getDefaultInstance() {
		ReactionObservationGenerator rog = new ReactionObservationGenerator();
		ObservationGenerator.fillDefaultInstance(rog);

		rog.replaceCodeGenerator(null);
		rog.codeGenerator = new CDCodeGenerator(REACTION_OBSERVATION_CODE, "ASSERTION");
		rog.codeGenerator.set("ASSERTION");
		rog.replaceStatusCodeValue("completed");
		rog.severityGenerator = new CECodeGenerator(ADVERSE_EVENT_SEVERITY);
		rog.severityGenerator.set("24484000");

		return rog;
	}

	@Override
	public Observation generate(CDAFactories factories) {
		Observation observation = super.generate(factories);

		if (codeGenerator != null) {
			observation.setCode(codeGenerator.generate(factories));
		}

		if (severityGenerator != null) {
			EntryRelationship er = factories.base.createEntryRelationship();
			observation.getEntryRelationships().add(er);
			er.setTypeCode(x_ActRelationshipEntryRelationship.SUBJ);
			er.setInversionInd(true);

			SeverityObservation so = factories.consol.createSeverityObservation();
			so.setClassCode(ActClassObservation.OBS);
			so.setMoodCode(x_ActMoodDocumentObservation.EVN);
			II templateId = factories.datatype.createII("2.16.840.1.113883.10.20.22.4.8", "2014-06-09");
			so.getTemplateIds().add(templateId);
			so.setCode(new CDGenerator("SEV", "2.16.840.1.113883.5.4", "HL7ActCode", "Severity").generate(factories));
			so.setStatusCode(new CSCodeGenerator("completed").generate(factories));
			CE severity = severityGenerator.generate(factories);
			so.getValues().add(severity);

			er.setObservation(so);
		}

		return observation;
	}

	public void verify(AdverseEvent adverseEvent) {
		if (!getIdGenerators().isEmpty()) {
			getIdGenerators().get(0).verify(adverseEvent.getIdentifier());
		} else {
			Assert.assertFalse("No adverse event identifier", adverseEvent.hasIdentifier());
		}

		if (codeGenerator == null) {
			Assert.assertFalse("No actuality", adverseEvent.hasActuality());
		} else {
			codeGenerator.verify(adverseEvent.getActuality().toCode());
		}

		if (getEffectiveTimeGenerator() == null) {
			Assert.assertFalse("No observation effective period", adverseEvent.hasDate());
		} else {
			getEffectiveTimeGenerator().verify(adverseEvent.getDateElement());
		}

		if (severityGenerator == null) {
			Assert.assertFalse("No severity code", adverseEvent.hasSeverity());
		} else {
			severityGenerator.verify(adverseEvent.getSeverity());
		}
	}

	public void verify(Bundle bundle, AdverseEvent adverseEvent) throws Exception {
		verify(adverseEvent);

		if (getValueGenerators() != null && !getValueGenerators().isEmpty()) {
			List<Condition> condition = BundleUtil.findAllResources(bundle, Condition.class);
			List<Condition> filteredConditions = condition.stream()
					.filter(c -> adverseEvent.getResultingCondition().stream()
							.anyMatch(rc -> rc.getReference().equals(c.getId())))
					.collect(Collectors.toList());
			Assert.assertEquals("number of conditions doesn't match", getValueGenerators().size(), filteredConditions.size());

			for (int index = 0; index < filteredConditions.size(); ++index) {
				getValueGenerators().get(index).verify(filteredConditions.get(index).getCode());
			}
		}
	}
}
