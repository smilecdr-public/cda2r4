package tr.com.srdc.cda2fhir.testutil.generator;

import org.openhealthtools.mdht.uml.hl7.datatypes.CD;
import tr.com.srdc.cda2fhir.testutil.CDAFactories;

import java.util.Map;

public class CDCodeGenerator extends CodeGenerator<CD> {
	public CDCodeGenerator(Map<String, Object> map, String defaultCode) {
		super(map, defaultCode);
	}

	public CDCodeGenerator(Map<String, Object> map) {
		super(map);
	}

	@Override
	protected CD create(CDAFactories factories) {
		return factories.datatype.createCD();
	}
}
