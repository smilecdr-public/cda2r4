package tr.com.srdc.cda2fhir.testutil.generator;

import com.bazaarvoice.jolt.JsonUtils;
import org.hl7.fhir.r4.model.Bundle;
import org.hl7.fhir.r4.model.Coding;
import org.hl7.fhir.r4.model.MedicationStatement;
import org.hl7.fhir.r4.model.Organization;
import org.hl7.fhir.r4.model.Practitioner;
import org.hl7.fhir.r4.model.PractitionerRole;
import org.hl7.fhir.r4.model.Procedure;
import org.hl7.fhir.r4.model.Procedure.ProcedurePerformerComponent;
import org.junit.Assert;
import org.openhealthtools.mdht.uml.cda.EntryRelationship;
import org.openhealthtools.mdht.uml.cda.Performer2;
import org.openhealthtools.mdht.uml.cda.consol.Indication;
import org.openhealthtools.mdht.uml.cda.consol.ProcedureActivityAct;
import org.openhealthtools.mdht.uml.hl7.datatypes.CD;
import org.openhealthtools.mdht.uml.hl7.datatypes.CS;
import org.openhealthtools.mdht.uml.hl7.datatypes.II;
import org.openhealthtools.mdht.uml.hl7.datatypes.IVL_TS;
import org.openhealthtools.mdht.uml.hl7.vocab.NullFlavor;
import org.openhealthtools.mdht.uml.hl7.vocab.x_ActRelationshipEntryRelationship;
import tr.com.srdc.cda2fhir.testutil.BundleUtil;
import tr.com.srdc.cda2fhir.testutil.CDAFactories;
import tr.com.srdc.cda2fhir.testutil.TestSetupException;
import tr.com.srdc.cda2fhir.transform.entry.impl.EntryResult;
import tr.com.srdc.cda2fhir.transform.util.IDeferredReference;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class ProcedureActivityActGenerator {
	private static final Map<String, Object> PROCEDURE_STATUS = JsonUtils
			.filepathToMap("src/test/resources//value-maps/ProcedureStatus.json");

	private List<IDGenerator> idGenerators = new ArrayList<>();
	private IVL_TSPeriodGenerator ivlTsGenerator;

	private String statusCode;
	private String statusCodeNullFlavor;

	private CDGenerator codeGenerator;

	private List<CDGenerator> reasonCodeGenerators = new ArrayList<>();

	private List<PerformerGenerator> performerGenerators = new ArrayList<>();

	private List<MedicationActivityGenerator> medicationActivityGenerators = new ArrayList<>();

	private List<EncounterActivityGenerator> encounterActivityGenerators = new ArrayList<>();

	public ProcedureActivityAct generate(CDAFactories factories) {
		ProcedureActivityAct paa = factories.consol.createProcedureActivityAct();

		idGenerators.forEach(idGenerator -> {
			II ii = idGenerator.generate(factories);
			paa.getIds().add(ii);
		});

		if (ivlTsGenerator != null) {
			IVL_TS ivlTs = ivlTsGenerator.generate(factories);
			paa.setEffectiveTime(ivlTs);
		}

		if (statusCode != null || statusCodeNullFlavor != null) {
			CS cs = factories.datatype.createCS();
			if (statusCode != null) {
				cs.setCode(statusCode);
			}
			if (statusCodeNullFlavor != null) {
				NullFlavor nf = NullFlavor.get(statusCodeNullFlavor);
				if (nf == null) {
					throw new TestSetupException("Invalid null flavor enumeration.");
				}
				cs.setNullFlavor(nf);
			}
			paa.setStatusCode(cs);
		}

		if (codeGenerator != null) {
			CD cd = codeGenerator.generate(factories);
			paa.setCode(cd);
		}

		reasonCodeGenerators.forEach(rcg -> {
			EntryRelationship er = factories.base.createEntryRelationship();
			paa.getEntryRelationships().add(er);
			er.setTypeCode(x_ActRelationshipEntryRelationship.RSON);
			Indication indication = factories.consol.createIndication();
			er.setObservation(indication);
			CD cd = rcg.generate(factories);
			indication.setCode(cd);
		});

		performerGenerators.forEach(pg -> {
			Performer2 performer = pg.generate(factories);
			paa.getPerformers().add(performer);
		});

		medicationActivityGenerators.forEach(mag -> {
			EntryRelationship er = factories.base.createEntryRelationship();
			paa.getEntryRelationships().add(er);
			er.setTypeCode(x_ActRelationshipEntryRelationship.COMP);
			er.setSubstanceAdministration(mag.generate(factories));
		});

		encounterActivityGenerators.forEach(eag -> {
			EntryRelationship er = factories.base.createEntryRelationship();
			er.setTypeCode(x_ActRelationshipEntryRelationship.COMP);
			er.setInversionInd(true);
			er.setEncounter(eag.generate(factories));

			paa.getEntryRelationships().add(er);
		});

		return paa;
	}

	public static ProcedureActivityActGenerator getDefaultInstance() {
		ProcedureActivityActGenerator paag = new ProcedureActivityActGenerator();

		paag.idGenerators.add(IDGenerator.getNextInstance());
		paag.ivlTsGenerator = IVL_TSPeriodGenerator.getDefaultInstance();
		paag.statusCode = "active";
		paag.codeGenerator = CDGenerator.getNextInstance();
		paag.reasonCodeGenerators.add(CDGenerator.getNextInstance());
		paag.performerGenerators.add(PerformerGenerator.getDefaultInstance());

		return paag;
	}

	public static ProcedureActivityActGenerator getMedicationActivityInstance() {
		ProcedureActivityActGenerator paag = new ProcedureActivityActGenerator();

		paag.idGenerators.add(IDGenerator.getNextInstance());
		paag.ivlTsGenerator = IVL_TSPeriodGenerator.getDefaultInstance();
		paag.statusCode = "active";
		paag.codeGenerator = CDGenerator.getNextInstance();
		paag.reasonCodeGenerators.add(CDGenerator.getNextInstance());
		paag.performerGenerators.add(PerformerGenerator.getDefaultInstance());
		paag.medicationActivityGenerators.add(MedicationActivityGenerator.getDefaultInstance());
		paag.encounterActivityGenerators.add(EncounterActivityGenerator.getDefaultInstance());

		return paag;
	}

	public IDGenerator getIDGenerator(int index) {
		return idGenerators.get(index);
	}

	public void verify(Procedure procedure) {
		if (!idGenerators.isEmpty()) {
			for (int index = 0; index < idGenerators.size(); ++index) {
				idGenerators.get(index).verify(procedure.getIdentifier().get(index));
			}
		} else {
			Assert.assertFalse("No condition identifier", procedure.hasIdentifier());
		}

		if (ivlTsGenerator == null) {
			Assert.assertFalse("Missing procedure performed", procedure.hasPerformedPeriod());
		} else {
			ivlTsGenerator.verify(procedure.getPerformedPeriod());
		}

		if (statusCode == null || statusCodeNullFlavor != null) {
			Assert.assertFalse("Missing procedure status", procedure.hasStatus());
		} else {
			String expected = (String) PROCEDURE_STATUS.get(statusCode);
			if (expected == null) {
				expected = "unknown";
			}
			Assert.assertEquals("Procedure status", expected, procedure.getStatus().toCode());
		}

		if (codeGenerator == null) {
			Assert.assertFalse("Missing procedure code", procedure.hasCode());
		} else {
			codeGenerator.verify(procedure.getCode());
		}

		if (reasonCodeGenerators.isEmpty()) {
			Assert.assertFalse("Missing procedure reason code", procedure.hasReasonCode());
		} else {
			for (int index = 0; index < reasonCodeGenerators.size(); ++index) {
				reasonCodeGenerators.get(index).verify(procedure.getReasonCode().get(index));
			}
		}

		if (performerGenerators.isEmpty()) {
			Assert.assertFalse("Missing procedure performer", procedure.hasPerformer());
		} else {
			Assert.assertEquals("Procedure performer count", performerGenerators.size(),
					procedure.getPerformer().size());
		}
	}

	public void verify(Bundle bundle, Procedure procedure) throws Exception {
		verify(procedure);

		if (performerGenerators.isEmpty()) {
			Assert.assertFalse("Missing procedure performer", procedure.hasPerformer());
		} else {
			BundleUtil util = new BundleUtil(bundle);

			for (int index = 0; index < performerGenerators.size(); ++index) {
				PerformerGenerator pg = performerGenerators.get(index);
				ProcedurePerformerComponent ppc = procedure.getPerformer().get(index);

				String practitionerId = ppc.getActor().getReference();
				pg.verifyFromPractionerId(bundle, practitionerId);

				Practitioner practitioner = util.getResourceFromReference(practitionerId, Practitioner.class);
				pg.verify(practitioner);

				PractitionerRole role = util.getPractitionerRole(practitionerId);
				pg.verify(role);

				Assert.assertTrue("Procedure performer has role", ppc.hasFunction());
				Coding ppcRole = ppc.getFunction().getCoding().get(0);
				Assert.assertEquals("Procedure performer has role", pg.getCodeCode(), ppcRole.getCode());

				if (!role.hasOrganization()) {
					pg.verify((Organization) null);
					Assert.assertFalse("No on behalf organization", ppc.hasOnBehalfOf());
				} else {
					String reference = role.getOrganization().getReference();
					Organization organization = util.getResourceFromReference(reference, Organization.class);
					pg.verify(organization);
					Assert.assertEquals("Procedure on behalf organization", reference,
							ppc.getOnBehalfOf().getReference());
				}

			}
		}
	}

	public void verify(Bundle bundle) throws Exception {
		Procedure procedure = BundleUtil.findOneResource(bundle, Procedure.class);
		verify(bundle, procedure);

		if (!medicationActivityGenerators.isEmpty()) {
			List<MedicationStatement> medicationStatements = BundleUtil.findResources(bundle, MedicationStatement.class, medicationActivityGenerators.size());
			for (int index = 0; index < medicationActivityGenerators.size(); ++index) {
				MedicationStatement medicationStatement = medicationStatements.get(index);
				medicationActivityGenerators.get(index).verify(medicationStatement);
				Assert.assertTrue("medication statement does not refer to procedure",
						medicationStatement.getPartOf().stream()
								.anyMatch(reference -> reference.getReference().equals(procedure.getId())));
			}
		}
	}

	public void verifyDeferredReferences(EntryResult entryResult) {
		if (!encounterActivityGenerators.isEmpty()) {
			for (int index = 0; index < encounterActivityGenerators.size(); ++index) {
				IDGenerator idGenerator = encounterActivityGenerators.get(index).getIDGenerator(0);
				IDeferredReference deferredReference = entryResult.getDeferredReferences().get(index);
				idGenerator.verify(deferredReference.getIdentifier());
			}
		}
	}

}
