package tr.com.srdc.cda2fhir;

import static tr.com.srdc.cda2fhir.transform.DataTypesTransformerImpl.DATA_ABSENT_REASON_EXTENSION_URI;
import static tr.com.srdc.cda2fhir.transform.DataTypesTransformerImpl.DATA_ABSENT_REASON_TERMINOLOGY_URI;

import org.hl7.fhir.r4.model.Bundle;
import org.hl7.fhir.r4.model.Coding;
import org.hl7.fhir.r4.model.Extension;
import org.hl7.fhir.r4.model.Period;
import org.hl7.fhir.r4.model.Procedure;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import org.openhealthtools.mdht.uml.cda.consol.ProcedureActivityAct;
import org.openhealthtools.mdht.uml.cda.util.CDAUtil;
import tr.com.srdc.cda2fhir.testutil.CDAFactories;
import tr.com.srdc.cda2fhir.testutil.generator.ProcedureActivityActGenerator;
import tr.com.srdc.cda2fhir.transform.ResourceTransformerImpl;
import tr.com.srdc.cda2fhir.transform.entry.impl.EntryResult;
import tr.com.srdc.cda2fhir.transform.util.ITransformationContext;
import tr.com.srdc.cda2fhir.transform.util.impl.BundleInfo;
import tr.com.srdc.cda2fhir.transform.util.impl.TransformationContextImpl;
import tr.com.srdc.cda2fhir.util.FHIRUtil;

public class ProcedureActivityActTest {
	private static final ResourceTransformerImpl rt = new ResourceTransformerImpl();

	private static CDAFactories factories;

	@BeforeClass
	public static void init() {
		CDAUtil.loadPackages();
		factories = CDAFactories.init();
	}

	@Test
	public void testDefault() throws Exception {
		ProcedureActivityActGenerator generator = ProcedureActivityActGenerator.getDefaultInstance();

		ProcedureActivityAct paa = generator.generate(factories);

		BundleInfo bundleInfo = new BundleInfo(rt);
		ITransformationContext transformationContext = new TransformationContextImpl();
		EntryResult entryResult = rt.tProcedureActivityAct2Procedure(paa, bundleInfo, transformationContext);
		Bundle bundle = entryResult.getBundle();

		generator.verify(bundle);
	}

	@Test
	public void testMedicationAdministration() throws Exception {
		ProcedureActivityActGenerator generator = ProcedureActivityActGenerator.getMedicationActivityInstance();

		ProcedureActivityAct paa = generator.generate(factories);

		BundleInfo bundleInfo = new BundleInfo(rt);
		ITransformationContext transformationContext = new TransformationContextImpl();
		EntryResult entryResult = rt.tProcedureActivityAct2Procedure(paa, bundleInfo, transformationContext);
		Bundle bundle = entryResult.getBundle();

		generator.verify(bundle);
		generator.verifyDeferredReferences(entryResult);
	}
	
	@Test
	public void testProcedurePerformedPeriodWithDataAbsentReason() throws Exception {
		ProcedureActivityActGenerator generator = ProcedureActivityActGenerator.getDefaultInstance();

		ProcedureActivityAct paa = generator.generate(factories);
		paa.setEffectiveTime(null); // Cleanup effectiveTime - the test
		
		BundleInfo bundleInfo = new BundleInfo(rt);
		ITransformationContext transformationContext = new TransformationContextImpl();
		EntryResult entryResult = rt.tProcedureActivityAct2Procedure(paa, bundleInfo, transformationContext);
		Bundle bundle = entryResult.getBundle();

		Procedure proc = FHIRUtil.findFirstResource(bundle, Procedure.class);
		
		Period period = proc.getPerformedPeriod();
		Extension periodExt = period.getExtension().get(0);
		
		Assert.assertEquals(DATA_ABSENT_REASON_EXTENSION_URI, periodExt.getUrl());
		Coding extCode = (Coding)periodExt.getValue();
		Assert.assertEquals(DATA_ABSENT_REASON_TERMINOLOGY_URI, extCode.getSystem());
		Assert.assertEquals("error", extCode.getCode());
		Assert.assertEquals("Error", extCode.getDisplay());	
	}

}
