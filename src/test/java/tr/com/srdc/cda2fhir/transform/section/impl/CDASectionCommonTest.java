package tr.com.srdc.cda2fhir.transform.section.impl;

import ca.uhn.fhir.context.FhirContext;
import ca.uhn.fhir.util.ClasspathUtil;
import org.hl7.fhir.r4.model.Bundle;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class CDASectionCommonTest {

    private static CDASectionCommon cdaSectionCommon;

    @Test
    public void mergeOxygenObs_shouldDoNothingToEntryCount_whenBundleContainsBloodOximetry() {
        String input = ClasspathUtil.loadResource("bundle-jsons/observation-bundle-bloodOximetry.json");
        Bundle bundle = FhirContext.forR4().newJsonParser().parseResource(Bundle.class, input);

        assertEquals(1, bundle.getEntry().size());
        cdaSectionCommon.mergeOxygenObs(bundle);
        assertEquals(1, bundle.getEntry().size());
    }

    @Test
    public void mergeOxygenObs_shouldDoNothingToEntryCount_whenBundleContainsOxygenConcentration() {
        String input = ClasspathUtil.loadResource("bundle-jsons/observation-bundle-oxygenConcentration.json");
        Bundle bundle = FhirContext.forR4().newJsonParser().parseResource(Bundle.class, input);

        assertEquals(1, bundle.getEntry().size());
        cdaSectionCommon.mergeOxygenObs(bundle);
        assertEquals(1, bundle.getEntry().size());
    }

    @Test
    public void mergeOxygenObs_shouldMergeIntoOne_whenBundleContainsOxygenConcentrationAndBloodOximetry() {
        String input = ClasspathUtil.loadResource("bundle-jsons/observation-bundle-oxygenConcentration-bloodOximetry.json");
        Bundle bundle = FhirContext.forR4().newJsonParser().parseResource(Bundle.class, input);

        assertEquals(2, bundle.getEntry().size());
        cdaSectionCommon.mergeOxygenObs(bundle);
        assertEquals(1, bundle.getEntry().size());
    }

    @Test
    public void mergeOxygenObs_shouldMergeIntoOne_whenBundleContainsOxygenConcentrationAndBloodOximetryAndFlowRate() {
        String input = ClasspathUtil.loadResource("bundle-jsons/observation-bundle-oxygenConcentration-bloodOximetry-flowRate.json");
        Bundle bundle = FhirContext.forR4().newJsonParser().parseResource(Bundle.class, input);

        assertEquals(3, bundle.getEntry().size());
        cdaSectionCommon.mergeOxygenObs(bundle);
        assertEquals(1, bundle.getEntry().size());
    }
}
