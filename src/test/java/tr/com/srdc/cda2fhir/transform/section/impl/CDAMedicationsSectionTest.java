package tr.com.srdc.cda2fhir.transform.section.impl;

import org.hl7.fhir.r4.model.Base;
import org.hl7.fhir.r4.model.MedicationStatement;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import org.openhealthtools.mdht.uml.cda.consol.MedicationsSection;
import org.openhealthtools.mdht.uml.cda.consol.impl.MedicationActivityImpl;
import org.openhealthtools.mdht.uml.cda.util.CDAUtil;
import org.openhealthtools.mdht.uml.hl7.datatypes.II;
import org.openhealthtools.mdht.uml.hl7.vocab.x_DocumentSubstanceMood;
import tr.com.srdc.cda2fhir.testutil.CDAFactories;
import tr.com.srdc.cda2fhir.transform.ResourceTransformerImpl;
import tr.com.srdc.cda2fhir.transform.util.ITransformationContext;
import tr.com.srdc.cda2fhir.transform.util.impl.BundleInfo;
import tr.com.srdc.cda2fhir.transform.util.impl.TransformationContextImpl;

import java.util.List;

public class CDAMedicationsSectionTest {
    private static final ResourceTransformerImpl rt = new ResourceTransformerImpl();

    private static CDAFactories factories;

    @BeforeClass
    public static void init() {
        CDAUtil.loadPackages();
        factories = CDAFactories.init();
    }

    @Test
    public void testMedicationStatus() throws Exception {
        // setup

        // Make an evn medication activity.
        MedicationActivityImpl medActEvn = (MedicationActivityImpl) factories.consol.createMedicationActivity();
        medActEvn.getIds().add(factories.datatype.createII("1.1.1"));
        medActEvn.setMoodCode(x_DocumentSubstanceMood.EVN);

        // Make an INT medication activity.
        MedicationActivityImpl medActInt = (MedicationActivityImpl) factories.consol.createMedicationActivity();
        medActInt.getIds().add(factories.datatype.createII("2.2.2"));
        medActInt.setMoodCode(x_DocumentSubstanceMood.INT);

        // Make a Medication Section and populate it with the Medication Activities
        MedicationsSection section = factories.consol.createMedicationsSection();
        section.addSubstanceAdministration(medActEvn);
        section.addSubstanceAdministration(medActInt);

        BundleInfo bundleInfo = new BundleInfo(rt);
        ITransformationContext transformationContext = new TransformationContextImpl();
        CDAMedicationsSection fixture = new CDAMedicationsSection(section);

        // execute
        SectionResultDynamic result = fixture.transform(bundleInfo, transformationContext);

        // validate
        Assert.assertEquals("number of medication statements", 1, result.getSectionResources().size());
        Assert.assertEquals("transformed medication statement id", "1.1.1",
                ((MedicationStatement) result.getSectionResources().get(0)).getIdentifierFirstRep().getValue());
        Assert.assertTrue("warning message",
                transformationContext.getWarnings().stream()
                        .anyMatch(t -> "INT mood not supported.".equals(t.getMessage())));
    }
}
