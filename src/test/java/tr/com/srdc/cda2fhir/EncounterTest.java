package tr.com.srdc.cda2fhir;

import java.util.Map;

import org.hl7.fhir.r4.model.Bundle;
import org.hl7.fhir.r4.model.CodeableConcept;
import org.hl7.fhir.r4.model.Condition;
import org.hl7.fhir.r4.model.Encounter;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import org.openhealthtools.mdht.uml.cda.consol.EncounterActivities;
import org.openhealthtools.mdht.uml.cda.consol.EncounterDiagnosis;
import org.openhealthtools.mdht.uml.cda.consol.ProblemObservation;
import org.openhealthtools.mdht.uml.cda.util.CDAUtil;
import org.openhealthtools.mdht.uml.hl7.datatypes.CD;
import org.openhealthtools.mdht.uml.hl7.datatypes.ED;
import org.openhealthtools.mdht.uml.hl7.datatypes.TEL;

import com.helger.commons.collection.attr.StringMap;

import tr.com.srdc.cda2fhir.testutil.BundleUtil;
import tr.com.srdc.cda2fhir.testutil.CDAFactories;
import tr.com.srdc.cda2fhir.testutil.generator.EncounterActivityGenerator;
import tr.com.srdc.cda2fhir.transform.ResourceTransformerImpl;
import tr.com.srdc.cda2fhir.transform.util.ITransformationContext;
import tr.com.srdc.cda2fhir.transform.entry.impl.EntryResult;
import tr.com.srdc.cda2fhir.transform.util.impl.BundleInfo;
import tr.com.srdc.cda2fhir.transform.util.impl.TransformationContextImpl;

public class EncounterTest {
	private static final ResourceTransformerImpl rt = new ResourceTransformerImpl();
	private static CDAFactories factories;

	@BeforeClass
	public static void init() {
		CDAUtil.loadPackages();

		factories = CDAFactories.init();
	}

	@Test
	public void testMapping() throws Exception {
		EncounterActivityGenerator encounterGenerator = EncounterActivityGenerator.getDefaultInstance();
		EncounterActivities encounter = encounterGenerator.generate(factories);

		EntryResult result = rt.tEncounterActivity2Encounter(encounter, new BundleInfo(rt), new TransformationContextImpl());

		encounterGenerator.verify(result.getBundle());
	}

	@Test
	public void testEncounterOriginalText() throws Exception {

		// Make an encounter activity.
		EncounterActivities encounterActivities = factories.consol.createEncounterActivities();

		BundleInfo bundleInfo = new BundleInfo(rt);
		String expectedValue = "freetext entry";
		String referenceValue = "fakeid1";
		CD cd = factories.datatype.createCD();
		ED ed = factories.datatype.createED();
		TEL tel = factories.datatype.createTEL();
		tel.setValue("#" + referenceValue);
		ed.setReference(tel);
		cd.setCode("code");
		cd.setCodeSystem("codeSystem");
		cd.setOriginalText(ed);
		Map<String, String> idedAnnotations = new StringMap();
		idedAnnotations.put(referenceValue, expectedValue);
		bundleInfo.mergeIdedAnnotations(idedAnnotations);
		ITransformationContext transformationContext = new TransformationContextImpl();

		encounterActivities.setCode(cd);
		Bundle bundle = rt.tEncounterActivity2Encounter(encounterActivities, bundleInfo, transformationContext).getBundle();
		Encounter fhirEncounter = BundleUtil.findOneResource(bundle, Encounter.class);
		CodeableConcept cc = fhirEncounter.getType().get(0);
		Assert.assertEquals("Encounter Activity Code text value assigned", expectedValue, cc.getText());

	}

	@Test
	public void testEncounterCondition() throws Exception {
		EncounterActivities encounterActivities = factories.consol.createEncounterActivities();
		EncounterDiagnosis encounterDiagnosis = factories.consol.createEncounterDiagnosis();
		ProblemObservation problemObservation = factories.consol.createProblemObservation();

		encounterActivities.addAct(encounterDiagnosis);
		encounterDiagnosis.addObservation(problemObservation);

		BundleInfo bundleInfo = new BundleInfo(rt);
		ITransformationContext transformationContext = new TransformationContextImpl();

		Bundle bundle = rt.tEncounterActivity2Encounter(encounterActivities, bundleInfo, transformationContext).getBundle();

		Encounter fhirEncounter = BundleUtil.findOneResource(bundle, Encounter.class);
		Condition fhirCondition = BundleUtil.findOneResource(bundle, Condition.class);

		Assert.assertEquals(fhirCondition.getId(), fhirEncounter.getDiagnosisFirstRep().getCondition().getReference());
		Assert.assertEquals("http://terminology.hl7.org/CodeSystem/condition-category", fhirCondition.getCategoryFirstRep().getCodingFirstRep().getSystem());
		Assert.assertEquals("encounter-diagnosis", fhirCondition.getCategoryFirstRep().getCodingFirstRep().getCode());
		Assert.assertEquals("Encounter Diagnosis", fhirCondition.getCategoryFirstRep().getCodingFirstRep().getDisplay());
	}

}
