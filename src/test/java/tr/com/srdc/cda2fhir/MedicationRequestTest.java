package tr.com.srdc.cda2fhir;

import java.util.Date;
import java.util.List;

import org.hl7.fhir.r4.model.*;
import org.hl7.fhir.r4.model.MedicationRequest.MedicationRequestIntent;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import org.openhealthtools.mdht.uml.cda.Supply;
import org.openhealthtools.mdht.uml.cda.consol.MedicationActivity;
import org.openhealthtools.mdht.uml.cda.consol.MedicationSupplyOrder;
import org.openhealthtools.mdht.uml.cda.util.CDAUtil;

import tr.com.srdc.cda2fhir.testutil.BundleUtil;
import tr.com.srdc.cda2fhir.testutil.CDAFactories;
import tr.com.srdc.cda2fhir.testutil.ManufacturedProductGenerator;
import tr.com.srdc.cda2fhir.testutil.MedicationSupplyOrderGenerator;
import tr.com.srdc.cda2fhir.testutil.generator.IVL_TSPeriodGenerator;
import tr.com.srdc.cda2fhir.testutil.generator.MedicationActivityGenerator;
import tr.com.srdc.cda2fhir.transform.DataTypesTransformerImpl;
import tr.com.srdc.cda2fhir.transform.ResourceTransformerImpl;
import tr.com.srdc.cda2fhir.transform.ValueSetsTransformerImpl;
import tr.com.srdc.cda2fhir.transform.entry.impl.EntryResult;
import tr.com.srdc.cda2fhir.transform.util.ITransformationContext;
import tr.com.srdc.cda2fhir.transform.util.impl.BundleInfo;
import tr.com.srdc.cda2fhir.transform.util.impl.TransformationContextImpl;

import static tr.com.srdc.cda2fhir.testutil.generator.MedicationActivityGenerator.FREE_TEXT_INSTRUCTIONS;
import static tr.com.srdc.cda2fhir.transform.DataTypesTransformerImpl.DATA_ABSENT_REASON_EXTENSION_URI;
import static tr.com.srdc.cda2fhir.transform.DataTypesTransformerImpl.DATA_ABSENT_REASON_TERMINOLOGY_URI;

public class MedicationRequestTest {

	private static MedicationSupplyOrderGenerator medSupplyOrderGenerator;
	private static CDAFactories factories;
	private static ResourceTransformerImpl rt;
	private static ValueSetsTransformerImpl vst;

	@BeforeClass
	public static void init() {

		CDAUtil.loadPackages();
		factories = CDAFactories.init();
		medSupplyOrderGenerator = new MedicationSupplyOrderGenerator(factories);
		rt = new ResourceTransformerImpl();
		vst = new ValueSetsTransformerImpl();
	}

	@Test
	public void testMedicationSupplyOrder2MedicationRequest() throws Exception {

		BundleInfo bInfo = new BundleInfo(rt);
		ITransformationContext transformationContext = new TransformationContextImpl();
		transformationContext.setPatient((Patient)(new Patient()).setId("Patient/0"));
		MedicationSupplyOrder defaultMedSupplyOrder = medSupplyOrderGenerator.generateDefaultMedicationSupplyOrder();
		EntryResult entryResult = rt.medicationSupplyOrder2MedicationRequest(defaultMedSupplyOrder, bInfo, transformationContext);
		Bundle resultBundle = entryResult.getBundle();
		MedicationRequest medRequest = BundleUtil.findOneResource(resultBundle, MedicationRequest.class);
		Medication medication = BundleUtil.findOneResource(resultBundle, Medication.class);
		Practitioner practitioner = BundleUtil.findOneResource(resultBundle, Practitioner.class);

		Assert.assertEquals("MedicationRequest", medRequest.getResourceType().toString());
		Assert.assertEquals(MedicationSupplyOrderGenerator.DEFAULT_ROOT_ID,
				medRequest.getIdentifierFirstRep().getValue());
		Assert.assertEquals(MedicationSupplyOrderGenerator.DEFAULT_STATUS_CODE, medRequest.getStatus().toCode());
		Assert.assertEquals(MedicationRequestIntent.INSTANCEORDER.toCode(), medRequest.getIntent().toCode());
		Assert.assertEquals(medication.getId(), medRequest.getMedicationReference().getReference());
		Assert.assertEquals("Patient/0", medRequest.getSubject().getReference());
		Assert.assertEquals(1, medRequest.getDispenseRequest().getNumberOfRepeatsAllowed());

		Assert.assertEquals(MedicationSupplyOrderGenerator.DEFAULT_QUANTITY_UNIT,
				medRequest.getDispenseRequest().getQuantity().getUnit());
		Assert.assertEquals(MedicationSupplyOrderGenerator.DEFAULT_QUANTITY_VALUE,
				medRequest.getDispenseRequest().getQuantity().getValue().toString());
		Assert.assertEquals("2019-01-01T00:00:00.000-05:00",
				medRequest.getDispenseRequest().getValidityPeriod().getStartElement().getValueAsString());
		Assert.assertEquals("2019-01-01T12:34:56.000-05:00",
				medRequest.getDispenseRequest().getValidityPeriod().getEndElement().getValueAsString());
		Assert.assertEquals(MedicationSupplyOrderGenerator.DEFAULT_INSTRUCTION, medRequest.getNoteFirstRep().getText());
		Assert.assertEquals(medRequest.getRequester().getReference(), practitioner.getId());

		Assert.assertEquals(ManufacturedProductGenerator.DEFAULT_MANU_MATERIAL_CODE_CODE,
				medication.getCode().getCodingFirstRep().getCode());
		Assert.assertEquals("urn:oid:" + vst.tOid2Url(ManufacturedProductGenerator.DEFAULT_MANU_MATERIAL_CODE_SYSTEM),
				vst.tOid2Url(medication.getCode().getCodingFirstRep().getSystem()));
		Assert.assertEquals(ManufacturedProductGenerator.DEFAULT_MANU_MATERIAL_DISPLAY_NAME,
				medication.getCode().getCodingFirstRep().getDisplay());
		Assert.assertEquals(ManufacturedProductGenerator.DEFAULT_TRANSLATION_CODE,
				medication.getCode().getCoding().get(1).getCode());
		Assert.assertEquals("urn:oid:" + vst.tOid2Url(ManufacturedProductGenerator.DEFAULT_TRANSLATION_CODE_SYSTEM),
				vst.tOid2Url(medication.getCode().getCoding().get(1).getSystem()));
		Assert.assertEquals(ManufacturedProductGenerator.DEFAULT_TRANSLATION_DISPLAY_NAME,
				medication.getCode().getCoding().get(1).getDisplay());

	}

	@Test
	public void testMedicationSupplyOrder2MedicationRequest_dataAbsentReason() throws Exception {
		// given
		BundleInfo bundleInfo = new BundleInfo(rt);
		ITransformationContext transformationContext = new TransformationContextImpl();
		transformationContext.setPatient((Patient) (new Patient()).setId("Patient/0"));

		MedicationSupplyOrder defaultMedSupplyOrder = medSupplyOrderGenerator.generateDefaultMedicationSupplyOrder();
		defaultMedSupplyOrder.getIds().clear();

		// when
		Bundle resultBundle = rt.medicationSupplyOrder2MedicationRequest(defaultMedSupplyOrder, bundleInfo, transformationContext).getBundle();
		MedicationRequest medRequest = BundleUtil.findOneResource(resultBundle, MedicationRequest.class);

		// then
		verifyMedRequestDataAbsentReasons(medRequest);
	}

	private void verifyMedRequestDataAbsentReasons(MedicationRequest medRequest) {
		Assert.assertNotNull(medRequest);
		Assert.assertTrue(medRequest.getIdentifierFirstRep().hasExtension());
		Extension dataAbsentExt = medRequest.getIdentifierFirstRep().getExtensionFirstRep();
		Assert.assertEquals(DATA_ABSENT_REASON_EXTENSION_URI, dataAbsentExt.getUrl());
		Coding dataAbsentCoding = (Coding) dataAbsentExt.getValue();
		Assert.assertEquals(DATA_ABSENT_REASON_TERMINOLOGY_URI, dataAbsentCoding.getSystem());
		Assert.assertEquals("error", dataAbsentCoding.getCode());
		Assert.assertEquals("Error", dataAbsentCoding.getDisplay());
	}

	@Test
	public void testMedicationSupplyOrderEmpty() throws Exception {
		BundleInfo bInfo = new BundleInfo(rt);
		ITransformationContext transformationContext = new TransformationContextImpl();
		MedicationSupplyOrder defaultMedSupplyOrder = medSupplyOrderGenerator.generateDefaultMedicationSupplyOrder();
		Integer nullValue = null;
		defaultMedSupplyOrder.getRepeatNumber().setValue(nullValue);
		EntryResult entryResult = rt.medicationSupplyOrder2MedicationRequest(defaultMedSupplyOrder, bInfo, transformationContext);
		Bundle resultBundle = entryResult.getBundle();
		MedicationRequest medRequest = BundleUtil.findOneResource(resultBundle, MedicationRequest.class);
		Assert.assertTrue(medRequest.getDispenseRequest().getNumberOfRepeatsAllowedElement().isEmpty());
	}

	@Test
	public void testMedicationActivity() throws Exception {
		BundleInfo bInfo = new BundleInfo(rt);
		ITransformationContext transformationContext = new TransformationContextImpl();
		MedicationActivity medActivity = factories.consol.createMedicationActivity();
		Supply supply = medSupplyOrderGenerator.generateDefaultMedicationSupplyOrder();
		medActivity.addSupply(supply);
		EntryResult entryResult = rt.tMedicationActivity2MedicationStatement(medActivity, bInfo, transformationContext);
		List<MedicationRequest> medRequests = BundleUtil.findResources(entryResult.getBundle(), MedicationRequest.class,
				1);
		List<Medication> medications = BundleUtil.findResources(entryResult.getBundle(), Medication.class, 1);
		List<Practitioner> practitioners = BundleUtil.findResources(entryResult.getBundle(), Practitioner.class, 1);
		List<Organization> organizations = BundleUtil.findResources(entryResult.getBundle(), Organization.class, 1);

	}

	@Test
	public void testMedicationActivity2MedicationRequest() throws Exception {
		// given
		BundleInfo bundleInfo = new BundleInfo(rt);
		DataTypesTransformerImpl dtt = new DataTypesTransformerImpl();

		ITransformationContext transformationContext = new TransformationContextImpl();
		transformationContext.setPatient((Patient)(new Patient()).setId("Patient/0"));

		boolean reportedType = false;
		Date authoredOn = dtt.tTS2Date(factories.datatype.createTS("20190101")).getValue();

		MedicationActivity medActivity = MedicationActivityGenerator.getDefaultInstance().generateDefaultMedicationActivity(factories);
		Supply supply = medSupplyOrderGenerator.generateDefaultMedicationSupplyOrder();
		medActivity.addSupply(supply);

		// when
		Bundle bundle = rt.tMedicationActivity2MedicationRequest(medActivity, bundleInfo, transformationContext).getBundle();
		MedicationRequest medRequest = BundleUtil.findOneResource(bundle, MedicationRequest.class);
		Medication medication = BundleUtil.findOneResource(bundle, Medication.class);
		Practitioner practitioner = BundleUtil.findOneResource(bundle, Practitioner.class);

		// then
		Assert.assertNotNull(medRequest);
		Assert.assertEquals("MedicationRequest", medRequest.getResourceType().toString());
		Assert.assertEquals(MedicationSupplyOrderGenerator.DEFAULT_ROOT_ID,
				medRequest.getIdentifierFirstRep().getValue());
		Assert.assertEquals(MedicationSupplyOrderGenerator.DEFAULT_STATUS_CODE, medRequest.getStatus().toCode());
		Assert.assertEquals(MedicationRequestIntent.ORDER.toCode(), medRequest.getIntent().toCode());
		Assert.assertNotNull(medication);
		Assert.assertEquals(medication.getId(), medRequest.getMedicationReference().getReference());
		Assert.assertEquals("Patient/0", medRequest.getSubject().getReference());
		Assert.assertNotNull(practitioner);
		Assert.assertEquals(medRequest.getRequester().getReference(), practitioner.getId());
		Assert.assertEquals(reportedType, medRequest.getReportedBooleanType().booleanValue());
		Assert.assertEquals(authoredOn, medRequest.getAuthoredOn());
		Assert.assertEquals(FREE_TEXT_INSTRUCTIONS, medRequest.getDosageInstructionFirstRep().getText());
		Assert.assertEquals(FREE_TEXT_INSTRUCTIONS, medRequest.getDosageInstructionFirstRep().getPatientInstruction());

		Assert.assertEquals(1, medRequest.getDispenseRequest().getNumberOfRepeatsAllowed());
		Assert.assertEquals(MedicationSupplyOrderGenerator.DEFAULT_QUANTITY_UNIT,
				medRequest.getDispenseRequest().getQuantity().getUnit());
		Assert.assertEquals(MedicationSupplyOrderGenerator.DEFAULT_QUANTITY_VALUE,
				medRequest.getDispenseRequest().getQuantity().getValue().toString());
		Assert.assertEquals("2019-01-01T00:00:00.000-05:00",
				medRequest.getDispenseRequest().getValidityPeriod().getStartElement().getValueAsString());
		Assert.assertEquals("2019-01-01T12:34:56.000-05:00",
				medRequest.getDispenseRequest().getValidityPeriod().getEndElement().getValueAsString());
		Assert.assertEquals(MedicationSupplyOrderGenerator.DEFAULT_INSTRUCTION, medRequest.getNoteFirstRep().getText());
	}

	@Test
	public void testMedicationActivity2MedicationRequest_NullFlavor() throws Exception {

		// given
		BundleInfo bundleInfo = new BundleInfo(rt);

		ITransformationContext transformationContext = new TransformationContextImpl();
		transformationContext.setPatient((Patient)(new Patient()).setId("Patient/0"));
		transformationContext.setEncounter((Encounter) (new Encounter()).setId("Encounter/0"));

		MedicationActivity medActivity = MedicationActivityGenerator.getDefaultInstance().generateDefaultMedicationActivity(factories);
		medActivity.getIds().clear();

		IVL_TSPeriodGenerator nullFlavorPeriod = IVL_TSPeriodGenerator.getNullFlavorInstance();	
		medActivity.getEffectiveTimes().clear();
		medActivity.getEffectiveTimes().add(nullFlavorPeriod.generate(factories));
		
		Supply supply = medSupplyOrderGenerator.generateMedicationSupplyOrderWithNullFlavorProduct();
		medActivity.addSupply(supply);

		// when
		Bundle bundle = rt.tMedicationActivity2MedicationRequest(medActivity, bundleInfo, transformationContext).getBundle();
		
		// then
		MedicationRequest medRequest = BundleUtil.findOneResource(bundle, MedicationRequest.class);

		verifyMedRequestDataAbsentReasons(medRequest);

		// validate start extension
		Assert.assertNotNull(medRequest.getDispenseRequest());
		Assert.assertFalse(medRequest.getDispenseRequest().getValidityPeriod().getStartElement().hasExtension());
	
		// validate end extension
		Assert.assertFalse(medRequest.getDispenseRequest().getValidityPeriod().getEndElement().hasExtension());
		
		// validate medicationCodeableConcept
		Extension medCCExt = medRequest.getMedicationCodeableConcept().getExtensionFirstRep();
		Assert.assertEquals(DATA_ABSENT_REASON_EXTENSION_URI, medCCExt.getUrl());
		Coding medCCExtCode = (Coding)medCCExt.getValue();
		Assert.assertEquals(DATA_ABSENT_REASON_TERMINOLOGY_URI, medCCExtCode.getSystem());
		Assert.assertEquals("not-applicable", medCCExtCode.getCode());
		Assert.assertEquals("Not Applicable", medCCExtCode.getDisplay());
	}
}
