package tr.com.srdc.cda2fhir;

import java.io.FileInputStream;
import java.nio.charset.StandardCharsets;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Set;
import java.util.UUID;
import java.util.stream.Collectors;

import org.eclipse.emf.ecore.xml.type.internal.DataValue;
import org.hamcrest.CoreMatchers;
import org.hamcrest.MatcherAssert;

/*
 * #%L
 * CDA to FHIR Transformer Library
 * %%
 * Copyright (C) 2016 SRDC Yazilim Arastirma ve Gelistirme ve Danismanlik Tic. A.S.
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.hl7.fhir.r4.model.*;
import org.hl7.fhir.r4.model.Bundle.BundleEntryComponent;
import org.hl7.fhir.r4.model.CarePlan;
import org.hl7.fhir.r4.model.CareTeam;
import org.hl7.fhir.r4.model.CareTeam.CareTeamParticipantComponent;
import org.hl7.fhir.r4.model.ClinicalImpression;
import org.hl7.fhir.r4.model.CodeType;
import org.hl7.fhir.r4.model.CodeableConcept;
import org.hl7.fhir.r4.model.Coding;
import org.hl7.fhir.r4.model.Composition;
import org.hl7.fhir.r4.model.Composition.CompositionAttestationMode;
import org.hl7.fhir.r4.model.Composition.SectionComponent;
import org.hl7.fhir.r4.model.Condition;
import org.hl7.fhir.r4.model.DateType;
import org.hl7.fhir.r4.model.Device;
import org.hl7.fhir.r4.model.DiagnosticReport;
import org.hl7.fhir.r4.model.DocumentReference;
import org.hl7.fhir.r4.model.Encounter;
import org.hl7.fhir.r4.model.Extension;
import org.hl7.fhir.r4.model.FamilyMemberHistory;
import org.hl7.fhir.r4.model.Goal;
import org.hl7.fhir.r4.model.Identifier;
import org.hl7.fhir.r4.model.Immunization;
import org.hl7.fhir.r4.model.MedicationStatement;
import org.hl7.fhir.r4.model.Observation;
import org.hl7.fhir.r4.model.Observation.ObservationComponentComponent;
import org.hl7.fhir.r4.model.Provenance.ProvenanceAgentComponent;
import org.hl7.fhir.r4.model.Patient;
import org.hl7.fhir.r4.model.Practitioner;
import org.hl7.fhir.r4.model.Procedure;
import org.hl7.fhir.r4.model.Provenance;
import org.hl7.fhir.r4.model.Quantity;
import org.hl7.fhir.r4.model.Reference;
import org.hl7.fhir.r4.model.Resource;
import org.hl7.fhir.r4.model.ServiceRequest;
import org.hl7.fhir.r4.model.codesystems.ConditionVerStatus;
import org.hl7.fhir.r4.model.codesystems.ProvenanceAgentType;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;
import org.openhealthtools.mdht.uml.cda.consol.ConsolPackage;
import org.openhealthtools.mdht.uml.cda.consol.ContinuityOfCareDocument;
import org.openhealthtools.mdht.uml.cda.util.CDAUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ca.uhn.fhir.context.FhirContext;
import tr.com.srdc.cda2fhir.conf.Config;
import tr.com.srdc.cda2fhir.testutil.BundleUtil;
import tr.com.srdc.cda2fhir.transform.CCDTransformerImpl;
import tr.com.srdc.cda2fhir.transform.ResourceTransformerImpl;
import tr.com.srdc.cda2fhir.transform.section.CDASectionTypeEnum;
import tr.com.srdc.cda2fhir.transform.util.IIdentifierMap;
import tr.com.srdc.cda2fhir.transform.util.ITransformationMessage;
import tr.com.srdc.cda2fhir.transform.util.ITransformationResult;
import tr.com.srdc.cda2fhir.transform.util.IdentifierMapFactory;
import tr.com.srdc.cda2fhir.util.FHIRUtil;

import static tr.com.srdc.cda2fhir.transform.ValueSetsTransformerImpl.SNOMED_CT_URI;
import static tr.com.srdc.cda2fhir.transform.DataTypesTransformerImpl.DATA_ABSENT_REASON_EXTENSION_URI;
import static tr.com.srdc.cda2fhir.transform.DataTypesTransformerImpl.DATA_ABSENT_REASON_TERMINOLOGY_URI;

public class CCDTransformerTest {
	private static final Logger logger = LoggerFactory.getLogger(ResourceTransformerImpl.class);
	private final static List<CDASectionTypeEnum> addlSections = new ArrayList<CDASectionTypeEnum>();

	static {
		addlSections.add(CDASectionTypeEnum.VITAL_SIGNS_SECTION);
		addlSections.add(CDASectionTypeEnum.RESULTS_SECTION);
		addlSections.add(CDASectionTypeEnum.FUNCTIONAL_STATUS_SECTION);
		addlSections.add(CDASectionTypeEnum.FAMILY_HISTORY_SECTION);
		addlSections.add(CDASectionTypeEnum.MEDICAL_EQUIPMENT_SECTION);
		addlSections.add(CDASectionTypeEnum.ENCOUNTERS_SECTION);
		addlSections.add(CDASectionTypeEnum.CONSULTATION_SECTION);
		addlSections.add(CDASectionTypeEnum.GOAL_SECTION);
		addlSections.add(CDASectionTypeEnum.PLAN_OF_CARE_SECTION);
		addlSections.add(CDASectionTypeEnum.HEALTH_CONCERN_SECTION);
		addlSections.add(CDASectionTypeEnum.SOCIAL_HISTORY_SECTION);
		addlSections.add(CDASectionTypeEnum.CARE_TEAM_SECTION);
	}

	@BeforeClass
	public static void init() {
		// Load MDHT CDA packages. Otherwise ContinuityOfCareDocument and similar
		// documents will not be recognised.
		// This has to be called before loading the document; otherwise will have no
		// effect.
		CDAUtil.loadPackages();
	}

	private static List<Reference> getSectionEntriesByCode(Composition composition, String code) {
		for (SectionComponent section : composition.getSection()) {
			String sectionCode = section.getCode().getCoding().get(0).getCode();
			if (code.equals(sectionCode)) {
				return section.getEntry();
			}
		}
		return null;
	}

	private static <T extends Resource> void verifySectionCounts(Bundle bundle, String sectionCode, Class<T> clazz) {
		Composition composition = (Composition) bundle.getEntry().get(0).getResource();
		List<Reference> compositionEntries = getSectionEntriesByCode(composition, sectionCode);
		List<T> resources = FHIRUtil.findResources(bundle, clazz);
		String msg = String.format("Expect only section resources for type %s", clazz.getSimpleName());
		Assert.assertEquals(msg, compositionEntries == null ? 0 : compositionEntries.size(), resources.size());
	}

	private static void verifyNoDuplicatePractitioner(Bundle bundle) {
		IIdentifierMap<String> identifierMap = IdentifierMapFactory.bundleToIds(bundle);
		List<Practitioner> practitioners = FHIRUtil.findResources(bundle, Practitioner.class);
		for (Practitioner practitioner : practitioners) {
			if (!practitioner.hasIdentifier()) {
				logger.info("No practioner identifier");
				continue;
			}
			String id = practitioner.getId();
			for (Identifier identifier : practitioner.getIdentifier()) {
				String idInMap = identifierMap.get(practitioner.fhirType(), identifier);
				Assert.assertNotNull("Practitioner id from map", idInMap);
				Assert.assertEquals("Practitioner id from map", id, idInMap);
			}
		}
	}

	private static ITransformationResult readVerifyFile(String sourceName, List<CDASectionTypeEnum> addlSections, String documentBody) throws Exception {
		logger.info(String.format("Verifying file %s", sourceName));
		FileInputStream fis = new FileInputStream("src/test/resources/" + sourceName);

		ContinuityOfCareDocument cda = (ContinuityOfCareDocument) CDAUtil.loadAs(fis,
				ConsolPackage.eINSTANCE.getContinuityOfCareDocument());
		CCDTransformerImpl ccdTransformer = new CCDTransformerImpl();
		if (addlSections != null) {
			addlSections.stream().forEach(r -> ccdTransformer.addSection(r));
		}
		Config.setGenerateDafProfileMetadata(false);
		Config.setGenerateNarrative(true);
		ITransformationResult result = ccdTransformer.transformDocument(cda, documentBody, null);
		Bundle bundle = result.getBundle();
		Assert.assertNotNull("Expect a bundle after transformation", bundle);
		Assert.assertTrue("Expect some entries", bundle.hasEntry());

		String baseName = sourceName.substring(0, sourceName.length() - 4);
		FHIRUtil.printJSON(bundle, "src/test/resources/output/" + baseName + ".json");

		BundleUtil.verifyIdsUnique(bundle);

		Composition composition = BundleUtil.findOneResource(bundle, Composition.class);
		Assert.assertTrue("Expect composition to be the first resource",
				bundle.getEntry().get(0).getResource() == composition);

		// Nothing should create encounters but Encounters Section
		verifySectionCounts(bundle, "46240-8", Encounter.class);

		verifyNoDuplicatePractitioner(bundle);

		return result;
	}

	private static ITransformationResult readVerifyFile(String sourceName, List<CDASectionTypeEnum> addlSections) throws Exception {
		return readVerifyFile(sourceName, addlSections, null);
	}

	private static List<Reference> getSectionEntries(Composition composition, String title) {
		for (SectionComponent section : composition.getSection()) {
			if (title.equals(section.getTitle())) {
				return section.getEntry();
			}
		}
		return null;
	}

	private static <T extends Resource> void verifySection(Bundle bundle, String title, Class<T> clazz, int count,
														   int referenceCount) throws Exception {
		// TODO Better handling for heterogeneous sections (i.e., Medical Devices section can contain either devices or
		//  procedures, but is not the only section to contain either of those resources.)
		List<T> resources = BundleUtil.findResources(bundle, clazz, count);
		Set<String> ids = resources.stream().map(r -> r.getId()).collect(Collectors.toSet());
		Composition composition = (Composition) bundle.getEntry().get(0).getResource();
		List<Reference> references = getSectionEntries(composition, title);
		List<Reference> referencesOfType = references.stream().filter(r -> r.getReference().startsWith(clazz.getSimpleName())).collect(Collectors.toList());
		Assert.assertNotNull("Expect references in section " + title, references);
		Assert.assertEquals("Expect " + referenceCount + " references in composition", referenceCount,
				referencesOfType.size());
		for (int idx = 0; idx < referenceCount; ++idx) {
			String id = referencesOfType.get(idx).getReference();
			Assert.assertTrue("Expect composition reference to be a resource id", ids.contains(id));
		}
	}

	private static <T extends Resource> void verifySection(Bundle bundle, String title, Class<T> clazz, int count)
			throws Exception {
		verifySection(bundle, title, clazz, count, count);
	}

	// Gold Sample r2.1
	@Test
	public void testSample1() throws Exception {
		readVerifyFile("170.315_b1_toc_gold_sample2_v1.xml", addlSections);
		Bundle bundle = readVerifyFile("170.315_b1_toc_gold_sample2_v1.xml", null).getBundle();

		verifySection(bundle, "ALLERGIES AND ADVERSE REACTIONS", AllergyIntolerance.class, 1);
		verifySection(bundle, "PROBLEMS", Condition.class, 1);
		verifySection(bundle, "MEDICATIONS", MedicationStatement.class, 1);
		verifySection(bundle, "IMMUNIZATIONS", Immunization.class, 1);
		verifySection(bundle, "PROCEDURES", Procedure.class, 1);
		verifySection(bundle, "VITAL SIGNS", Observation.class, 4, 3);
		verifySection(bundle, "SOCIAL HISTORY", Observation.class, 4, 1);


		// Spot checks
		Patient patient = BundleUtil.findOneResource(bundle, Patient.class);
		Assert.assertTrue("Expect an identifier for patient", patient.hasIdentifier());
		Assert.assertEquals("Expect the patient id in the CCDA file", "414122222",
				patient.getIdentifier().get(0).getValue());
	}

	@Test
	public void testSample2() throws Exception {
		Bundle bundle = readVerifyFile("C-CDA_R2-1_CCD.xml", addlSections).getBundle();

		verifySection(bundle, "ALLERGIES AND ADVERSE REACTIONS", AllergyIntolerance.class, 2);
		verifySection(bundle, "PROBLEMS", Condition.class, 6, 4);
		verifySection(bundle, "MEDICATIONS", MedicationStatement.class, 1);
		verifySection(bundle, "MEDICATIONS", MedicationRequest.class, 1);
		verifySection(bundle, "IMMUNIZATIONS", Immunization.class, 5);
		verifySection(bundle, "PROCEDURES", Procedure.class, 3, 2);
		verifySection(bundle, "PROCEDURES", Observation.class, 22, 1);
		verifySection(bundle, "ENCOUNTERS", Encounter.class, 1);
		verifySection(bundle, "VITAL SIGNS", Observation.class, 22, 8);
		verifySection(bundle, "SOCIAL HISTORY", Observation.class, 22, 4);
		verifySection(bundle, "RESULTS", DiagnosticReport.class, 2, 2);
		verifySection(bundle, "FUNCTIONAL STATUS", Observation.class, 22, 2);
		verifySection(bundle, "FAMILY HISTORY", FamilyMemberHistory.class, 1, 1);
		verifySection(bundle, "MEDICAL EQUIPMENT", Device.class, 5, 3);
		verifySection(bundle, "MEDICAL EQUIPMENT", Procedure.class, 3, 1);

		// Spot checks
		BundleUtil util = new BundleUtil(bundle);
		util.spotCheckImmunizationPractitioner("e6f1ba43-c0ed-4b9b-9f12-f435d8ad8f92", "Hippocrates", null,
				"Good Health Clinic");
		util.spotCheckEncounterPractitioner("2a620155-9d11-439e-92b3-5d9815ff4de8", null, "59058001", null);
		util.spotCheckProcedurePractitioner("d68b7e32-7810-4f5b-9cc2-acd54b0fd85d", null, null,
				"Community Health and Hospitals");
		util.spotCheckPractitioner("urn:oid:2.16.840.1.113883.19.5.9999.456", "2981823", null, "1001 Village Avenue");
		util.spotCheckAttesterPractitioner(CompositionAttestationMode.PROFESSIONAL, "Primary", "207QA0505X", null);
		util.spotCheckAttesterPractitioner(CompositionAttestationMode.LEGAL, "Primary", "207QA0505X", null);
		util.spotCheckPractitioner("http://hl7.org/fhir/sid/us-npi", "5555555555", "Primary", "1004 Healthcare Drive");
		util.spotCheckAuthorPractitioner("Primary", "207QA0505X", null);
		util.spotCheckObservationPractitioner("b63a8636-cfff-4461-b018-40ba58ba8b32", null, null, null);
		util.spotCheckMedStatementPractitioner("6c844c75-aa34-411c-b7bd-5e4a9f206e29", "Primary", null, null);
		util.spotCheckObservationPractitioner("ed9589fd-fda0-41f7-a3d0-dc537554f5c2", null, null, null);
		util.spotCheckPregnancyObservationDeliveryDate("5d65cb75-c12a-43a9-a25a-f76976a77af1", "2022-07-21");
		util.spotCheckConditionPractitioner("ab1791b0-5c71-11db-b0de-0800200c9a66", null, null, null);
		util.spotCheckAllergyPractitioner("36e3e930-7b14-11db-9fe1-0800200c9a66", null, null, null);
	}

	@Test
	public void testSample3() throws Exception {
		readVerifyFile("170.315_b1_toc_gold_sample2_v1.xml", addlSections);
		Bundle bundle = readVerifyFile("Vitera_CCDA_SMART_Sample.xml", null).getBundle();

		verifySection(bundle, "Allergies", AllergyIntolerance.class, 5);
		verifySection(bundle, "Problems", Condition.class, 1);
		// All the medication statements in this sample document are INT for some reason, so they get filtered out
		verifySection(bundle, "Medications", MedicationStatement.class, 0);
		verifySection(bundle, "Immunizations", Immunization.class, 1);
		verifySection(bundle, "Procedures and Surgical/Medical History", Procedure.class, 4);
		verifySection(bundle, "Encounters", Encounter.class, 13);
	}

	@Test
	public void testKaraoSample1NoteSection() throws Exception {
		readVerifyFile("kareo_sample_file_1.xml", addlSections);
		Bundle bundle = readVerifyFile("kareo_sample_file_1.xml", null).getBundle();

		DocumentReference df = getDocumentReferenceByCode(bundle, "11488-4");
		Assert.assertNotNull(df);

		//-- id
		Assert.assertNotNull(df.getId());

		//-- Narrative
		Assert.assertEquals("additional", df.getText().getStatus().toCode());
		MatcherAssert.assertThat(df.getText().getDivAsString(), CoreMatchers.containsString("Albert"));

		//-- identifier
		Assert.assertEquals("3939930", df.getIdentifierFirstRep().getValue());

		//-- Status
		Assert.assertEquals("current", df.getStatus().toCode());

		//-- DocStatus
		Assert.assertEquals("final", df.getDocStatus().toCode());

		//-- Type
		Assert.assertEquals(1, df.getType().getCoding().size()); // code 34109-9 is removed
		Assert.assertEquals("11488-4", df.getType().getCoding().get(0).getCode());

		//-- Category
		Assert.assertEquals("http://hl7.org/fhir/us/core/CodeSystem/us-core-documentreference-category", df.getCategoryFirstRep().getCoding().get(0).getSystem());
		Assert.assertEquals("clinical-note", df.getCategoryFirstRep().getCoding().get(0).getCode());

		//-- subject
		Assert.assertEquals("Happy Always Kid", df.getSubject().getDisplay());

		//-- date
		Assert.assertNotNull(df.getDate());

		//-- authors
		List<Reference> authors = df.getAuthor();
		Assert.assertEquals(1, authors.size());
		Reference authorRefId1 = authors.get(0);
		MatcherAssert.assertThat(authorRefId1.getReference(), CoreMatchers.containsString("Practitioner"));

		//-- authenticator
		String authenticatorRefId = df.getAuthenticator().getReference();
		MatcherAssert.assertThat(authenticatorRefId, CoreMatchers.containsString("Practitioner"));

		//-- custodian
		Reference custodian = df.getCustodian();
		MatcherAssert.assertThat(custodian.getReference(), CoreMatchers.containsString("Organization"));

		//-- RelatedTo
		Reference relatedToRef = df.getRelatesToFirstRep().getTarget();
		DocumentReference relatedToDocRef = (DocumentReference)findResource(bundle, relatedToRef);
		Assert.assertNotNull(relatedToDocRef);
		Assert.assertEquals(relatedToDocRef.getId(), relatedToRef.getReference());

		//-- content (attachment)
		Assert.assertNotNull(df.getContentFirstRep().getAttachment().getData());
		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
		Assert.assertEquals("2020-06-22", format.format(df.getContentFirstRep().getAttachment().getCreationElement().getValue()));
		//-- format
		Assert.assertEquals("http://ihe.net/fhir/ValueSet/IHE.FormatCode.codesystem", df.getContentFirstRep().getFormat().getSystem());
		Assert.assertEquals("urn:ihe:iti:xds:2017:mimeTypeSufficient", df.getContentFirstRep().getFormat().getCode());
		//-- mediaType
		Assert.assertEquals("text/plain", df.getContentFirstRep().getAttachment().getContentType());
		//-- data
		Assert.assertNotNull(df.getContentFirstRep().getAttachment().getData());
		//-- url
		MatcherAssert.assertThat(df.getContentFirstRep().getAttachment().getUrl(), CoreMatchers.containsString("DocumentReference"));

		//-- context (encounter reference)
		Encounter encounter = getEncounter(bundle, "2a620155-9d12-439e-92b3-5d9815ff4de8");
		Assert.assertNotNull(encounter);
		String encounterRef = df.getContext().getEncounterFirstRep().getReference();
		Assert.assertEquals(encounter.getId(), encounterRef);

		//-- context.period
		Assert.assertEquals("2020-06-22", df.getContext().getPeriod().getStartElement().getValueAsString());

		// ClinicalImpression mapping result
		String expectedSummary = "The patient was found to have fever and Dr Davis is suspecting Anemia based on the" +
				" patient history. So Dr Davis asked the patient to closely monitor the temperature and blood pressure" +
				" and get admitted to Community Health Hospitals if the fever does not subside within a day.";
		ClinicalImpression clinicalImpression = (ClinicalImpression) bundle
				.getEntry()
				.stream()
				.filter(t -> "ClinicalImpression".equals(t.getResource().getResourceType().name()))
				.collect(Collectors.toList())
				.get(0)
				.getResource();
		Assert.assertNotNull(clinicalImpression);
		Coding coding = clinicalImpression.getCode().getCoding().get(0);
		Assert.assertEquals("51848-0", coding.getCode());
		Assert.assertEquals( "http://loinc.org", coding.getSystem());
		Assert.assertEquals(expectedSummary, clinicalImpression.getSummary());
		Assert.assertEquals(ClinicalImpression.ClinicalImpressionStatus.COMPLETED, clinicalImpression.getStatus());
		Assert.assertEquals("Happy Always Kid", clinicalImpression.getSubject().getDisplay());

		logger.debug("Bundle :\n" + FhirContext.forR4().newJsonParser().setPrettyPrint(true).encodeResourceToString(bundle));
	}


	@Test
	public void testNoteSection_complexStructure() throws Exception {
		readVerifyFile("04_Jimmy_Smokes.xml", addlSections);
		Bundle bundle = readVerifyFile("04_Jimmy_Smokes.xml", null).getBundle();

		DocumentReference df = getDocumentReferenceByCode(bundle, "11506-3");
		Assert.assertNotNull(df);

		Assert.assertNotNull(df.getContentFirstRep().getAttachment().getData());
		byte[] decodedBytes = DataValue.Base64.decode(df.getContentFirstRep().getAttachment().getDataElement().getValueAsString());
		String decodedAttachment = new String(decodedBytes, StandardCharsets.UTF_8);
		MatcherAssert.assertThat(decodedAttachment, CoreMatchers.containsString("Subjective:"));
		MatcherAssert.assertThat(decodedAttachment, CoreMatchers.containsString("Complains people are too loud these days."));
	}

	@Test
	public void testNoteDocumentReferenceType() throws Exception {
		readVerifyFile("Alice_Newman_Smile_Modified_21Oct2022.xml", addlSections);
		Bundle bundle = readVerifyFile("Alice_Newman_Smile_Modified_21Oct2022.xml", null).getBundle();

		// -- 1. Consultation Note
		DocumentReference df = getDocumentReference(bundle, "2440875445019180013940496617303432203104");
		Assert.assertNotNull(df);

		// -- Type
		Assert.assertEquals(1, df.getType().getCoding().size()); // code 34109-9 is removed
		Assert.assertEquals("http://loinc.org", df.getType().getCoding().get(0).getSystem());
		Assert.assertEquals("11488-4", df.getType().getCoding().get(0).getCode());
		Assert.assertEquals("Consultation Note", df.getType().getCoding().get(0).getDisplay());

		// -- 2. Discharge Summary Note
		df = getDocumentReference(bundle, "244087888809180013997104617303432203105");
		Assert.assertNotNull(df);

		// -- Type
		Assert.assertEquals(1, df.getType().getCoding().size()); // code 34109-9 is removed
		Assert.assertEquals("http://loinc.org", df.getType().getCoding().get(0).getSystem());
		Assert.assertEquals("18842-5", df.getType().getCoding().get(0).getCode());
		Assert.assertEquals("Discharge Summary Note", df.getType().getCoding().get(0).getDisplay());

		// -- 3. Progress Note
		df = getDocumentReference(bundle, "244087888809180013940496617303432203103");
		Assert.assertNotNull(df);

		// -- Type
		Assert.assertEquals(1, df.getType().getCoding().size()); // code 34109-9 is removed
		Assert.assertEquals("http://loinc.org", df.getType().getCoding().get(0).getSystem());
		Assert.assertEquals("11506-3", df.getType().getCoding().get(0).getCode());
		Assert.assertEquals("Progress Note", df.getType().getCoding().get(0).getDisplay());

		logger.debug(
				"Bundle :\n" + FhirContext.forR4().newJsonParser().setPrettyPrint(true).encodeResourceToString(bundle));
	}

	@Test
	public void testKaraoSample1GoalSection() throws Exception {
		readVerifyFile("kareo_sample_file_1.xml", addlSections);
		Bundle bundle = readVerifyFile("kareo_sample_file_1.xml", null).getBundle();

		Goal goal = getGoal(bundle, "3700b3b0-fbed-11e2-b778-0800200c9a66");
		Assert.assertNotNull(goal);

		//-- id
		Assert.assertNotNull(goal.getId());

		//-- status
		Assert.assertEquals("active", goal.getLifecycleStatus().toCode());

		//-- subject
		Assert.assertEquals("Happy Always Kid", goal.getSubject().getDisplay());

		//-- priority
		Coding priority = goal.getPriority().getCodingFirstRep();
		Assert.assertEquals("394849002", priority.getCode());
		Assert.assertEquals("High priority", priority.getDisplay());

		//-- description
		Assert.assertEquals("Get rid of intermittent fever that is occurring every few weeks.", goal.getDescription().getText());

		//-- start
		DateType startDate = (DateType)goal.getStart();
		Assert.assertEquals("2020-06-22", startDate.getValueAsString());

		//-- due
		DateType dueDate = (DateType)goal.getTargetFirstRep().getDue();
		Assert.assertEquals("2020-06-25", dueDate.getValueAsString());

		//-- expressedBy
		Reference expressedBy = goal.getExpressedBy();
		Assert.assertNotNull(expressedBy.getReference());

		Practitioner practitioner = (Practitioner)findResource(bundle, expressedBy);
		Assert.assertNotNull(practitioner);

		Assert.assertEquals("urn:oid:2.16.840.1.113883.19.5", practitioner.getIdentifierFirstRep().getSystem());
		Assert.assertEquals("996-756-495", practitioner.getIdentifierFirstRep().getValue());

		logger.debug("Goal :\n" + FhirContext.forR4().newJsonParser().setPrettyPrint(true).encodeResourceToString(goal));
		logger.debug("Practitioner :\n" + FhirContext.forR4().newJsonParser().setPrettyPrint(true).encodeResourceToString(practitioner));
	}


	@Test
	public void testAliceNewmanSamplePlanOfCareSection() throws Exception {
		readVerifyFile("Alice_Newman_Modified_03Aug2022.xml", addlSections);
		Bundle bundle = readVerifyFile("Alice_Newman_Modified_03Aug2022.xml", null).getBundle();

		// -- should be only one CarePlan
		CarePlan carePlan = getCarePlan(bundle);
		Assert.assertNotNull(carePlan);

		//-- title
		Assert.assertEquals("Plan Of Care", carePlan.getTitle());

		//-- description
		MatcherAssert.assertThat(carePlan.getDescription(), CoreMatchers.containsString("Clindamycin"));

		//-- subject
		Assert.assertEquals("ALICE JONES NEWMAN, Alicia Jones", carePlan.getSubject().getDisplay());

		//-- intent/status
		Assert.assertEquals("plan", carePlan.getIntent().toCode());
		Assert.assertEquals("active", carePlan.getStatus().toCode());

		//-- contributor
		Assert.assertEquals("Albert Davis",carePlan.getContributorFirstRep().getDisplay());

		//-- Service Request
		Assert.assertEquals(2, carePlan.getActivity().size());

		//-- ServiceRequest identifier 32104838605178034007802217660702019122
		ServiceRequest svcReq1 = getServiceRequest(bundle, "32104838605178034007802217660702019122");
		Assert.assertNotNull(svcReq1);
		Assert.assertNotNull(svcReq1.getId());
		Assert.assertEquals("http://www.ama-assn.org/go/cpt", svcReq1.getCode().getCodingFirstRep().getSystem());
		Assert.assertEquals("94640", svcReq1.getCode().getCodingFirstRep().getCode());
		MatcherAssert.assertThat(svcReq1.getCode().getCodingFirstRep().getDisplay(), CoreMatchers.startsWith("Pressurized"));
		Assert.assertEquals("plan", svcReq1.getIntent().toCode());
		Assert.assertEquals("active", svcReq1.getStatus().toCode());
		Assert.assertEquals("2017-09-15", svcReq1.getOccurrenceDateTimeType().asStringValue());
		Assert.assertEquals(0, svcReq1.getPerformer().size());
		MatcherAssert.assertThat(svcReq1.getRequester().getReference(), CoreMatchers.containsString("Practitioner"));
		Assert.assertEquals("Albert Davis", svcReq1.getRequester().getDisplay());
		Assert.assertEquals("ALICE JONES NEWMAN, Alicia Jones", svcReq1.getSubject().getDisplay());
		MatcherAssert.assertThat(svcReq1.getText().getDivAsString(), CoreMatchers.containsString("Pressurized"));

		//-- ServiceRequest identifier 212964936054030491176531019101800697877
		ServiceRequest svcReq2 = getServiceRequest(bundle, "212964936054030491176531019101800697877");
		Assert.assertNotNull(svcReq2);
		Assert.assertNotNull(svcReq2.getId());
		Assert.assertEquals("http://www.ama-assn.org/go/cpt", svcReq2.getCode().getCodingFirstRep().getSystem());
		Assert.assertEquals("33207", svcReq2.getCode().getCodingFirstRep().getCode());
		MatcherAssert.assertThat(svcReq2.getCode().getCodingFirstRep().getDisplay(), CoreMatchers.startsWith("Ins new"));
		Assert.assertEquals("plan", svcReq2.getIntent().toCode());
		Assert.assertEquals("active", svcReq2.getStatus().toCode());
		Assert.assertEquals("2017-09-15", svcReq2.getOccurrenceDateTimeType().asStringValue());
		MatcherAssert.assertThat(svcReq2.getPerformerFirstRep().getReference(), CoreMatchers.containsString("Practitioner"));
		Assert.assertEquals("John Smith", svcReq2.getPerformerFirstRep().getDisplay());
		MatcherAssert.assertThat(svcReq2.getRequester().getReference(), CoreMatchers.containsString("Practitioner"));
		Assert.assertEquals("Albert Davis", svcReq2.getRequester().getDisplay());
		Assert.assertEquals("ALICE JONES NEWMAN, Alicia Jones", svcReq2.getSubject().getDisplay());
		MatcherAssert.assertThat(svcReq2.getText().getDivAsString(), CoreMatchers.containsString("sample text not a reference"));

		logger.debug("CarePlan :\n" + FhirContext.forR4().newJsonParser().setPrettyPrint(true).encodeResourceToString(carePlan));
		logger.debug("ServiceRequest 1 :\n" + FhirContext.forR4().newJsonParser().setPrettyPrint(true).encodeResourceToString(svcReq1));
		logger.debug("ServiceRequest 2 :\n" + FhirContext.forR4().newJsonParser().setPrettyPrint(true).encodeResourceToString(svcReq2));
		logger.debug("Bundle :\n" + FhirContext.forR4().newJsonParser().setPrettyPrint(true).encodeResourceToString(bundle));
	}

	@Test
	public void testDiagnosticReportEncounterPopulates() throws Exception {
		Bundle bundle = readVerifyFile("Alice_Newman_Modified_19Oct2022.xml", addlSections).getBundle();

		List<DiagnosticReport> diagnosticReports = BundleUtil.findAllResources(bundle, DiagnosticReport.class);
		DiagnosticReport diagReport = diagnosticReports.stream()
				.filter(dr -> dr.getIdentifierFirstRep().getValue().equals("218261805546354437394639774149949999210"))
				.findFirst().get();
		Encounter encounter = FHIRUtil.findFirstResource(bundle, Encounter.class);

		// ensure this the encounter referenced in the Encounters section
		Assert.assertEquals("279635419247698927653159992848961060804", encounter.getIdentifierFirstRep().getValue());
		Assert.assertEquals("urn:oid:2.16.840.1.113883.3.7220", encounter.getIdentifierFirstRep().getSystem());

		// verify observation
		Assert.assertNotNull(diagReport);
		Assert.assertNotNull(diagReport.getEncounter());
		Assert.assertEquals(new Reference(encounter.getId()).getReference(), diagReport.getEncounter().getReference());
	}

	@Test
	public void testHealthConcernSectionWithPractitionerAsserter() throws Exception {

		readVerifyFile("kareo_sample_file_1.xml", addlSections);
		Bundle bundle = readVerifyFile("kareo_sample_file_1.xml", null).getBundle();

		//-- verify observation
		Reference obsRef = getReference(bundle, "Health Concerns Section", "Observation");
		Assert.assertNotNull(obsRef);
		Observation obs = (Observation)findResource(bundle, obsRef);
		Assert.assertNotNull(obs);

		// -- identifier
		Assert.assertEquals("1eeb1e51-ee1d-1234-11xy-11z11ddb111z", obs.getIdentifierFirstRep().getValue());
		// -- status
		Assert.assertEquals("final", obs.getStatus().toCode());
		// -- code
		Assert.assertEquals("http://loinc.org", obs.getCode().getCodingFirstRep().getSystem());
		Assert.assertEquals("11323-3", obs.getCode().getCodingFirstRep().getCode());
		Assert.assertEquals("Health status", obs.getCode().getCodingFirstRep().getDisplay());
		// -- value
		Assert.assertEquals("http://snomed.info/sct", obs.getValueCodeableConcept().getCodingFirstRep().getSystem());
		Assert.assertEquals("161901003", obs.getValueCodeableConcept().getCodingFirstRep().getCode());
		Assert.assertEquals("Chronically ill", obs.getValueCodeableConcept().getCodingFirstRep().getDisplay());
		// -- subject
		Assert.assertEquals("Happy Always Kid", obs.getSubject().getDisplay());

		//-- verify condition
		Reference condRef = getReference(bundle, "Health Concerns Section", "Condition");
		Assert.assertNotNull(condRef);
		Condition cond = (Condition)findResource(bundle, condRef);
		Assert.assertNotNull(cond);

		//-- clinicalStatus
		Assert.assertEquals("http://terminology.hl7.org/CodeSystem/condition-clinical", cond.getClinicalStatus().getCodingFirstRep().getSystem());
		Assert.assertEquals("resolved", cond.getClinicalStatus().getCodingFirstRep().getCode());
		Assert.assertEquals("Resolved", cond.getClinicalStatus().getCodingFirstRep().getDisplay());
		//-- category
		Assert.assertEquals("http://hl7.org/fhir/us/core/CodeSystem/condition-category", cond.getCategoryFirstRep().getCodingFirstRep().getSystem());
		Assert.assertEquals("health-concern", cond.getCategoryFirstRep().getCodingFirstRep().getCode());
		Assert.assertEquals("Health Concern", cond.getCategoryFirstRep().getCodingFirstRep().getDisplay());
		//-- code - the mapping of the health concern act was changed as part of SMILE-5948
		//          this input file does not reflect the current Tebra input patterns, so this part of the test fails
//		Assert.assertEquals("http://loinc.org", cond.getCode().getCodingFirstRep().getSystem());
//		Assert.assertEquals("75310-3", cond.getCode().getCodingFirstRep().getCode());
//		Assert.assertEquals("Health Concern", cond.getCode().getCodingFirstRep().getDisplay());
		// -- subject
		Assert.assertEquals("Happy Always Kid", cond.getSubject().getDisplay());
		// -- subject
		Assert.assertEquals("2020-06-22", cond.getOnsetDateTimeType().getValueAsString());
		// -- asserter Practitioner
		Reference pracRef = cond.getAsserter();
		Assert.assertNotNull(pracRef);
		Practitioner prac = (Practitioner)findResource(bundle, pracRef);
		Assert.assertNotNull(prac);
		Assert.assertEquals("20cf14fb-b65c-4c8c-a54d-b0cca834c18c", prac.getIdentifierFirstRep().getValue());

		logger.debug("Observation :\n" + FhirContext.forR4().newJsonParser().setPrettyPrint(true).encodeResourceToString(obs));
		logger.debug("Condition :\n" + FhirContext.forR4().newJsonParser().setPrettyPrint(true).encodeResourceToString(cond));
		logger.debug("Bundle :\n" + FhirContext.forR4().newJsonParser().setPrettyPrint(true).encodeResourceToString(bundle));
	}

	@Test
	public void testHealthConcernSectionWithPatientAsserter() throws Exception {

		readVerifyFile("kareo_sample_file_patient_author.xml", addlSections);
		Bundle bundle = readVerifyFile("kareo_sample_file_patient_author.xml", null).getBundle();

		//-- verify observation
		Reference obsRef = getReference(bundle, "Health Concerns Section", "Observation");
		Assert.assertNotNull(obsRef);
		Observation obs = (Observation)findResource(bundle, obsRef);
		Assert.assertNotNull(obs);

		// -- identifier
		Assert.assertEquals("1eeb1e51-ee1d-1234-11xy-11z11ddb111z", obs.getIdentifierFirstRep().getValue());
		// -- status
		Assert.assertEquals("final", obs.getStatus().toCode());
		// -- code
		Assert.assertEquals("http://loinc.org", obs.getCode().getCodingFirstRep().getSystem());
		Assert.assertEquals("11323-3", obs.getCode().getCodingFirstRep().getCode());
		Assert.assertEquals("Health status", obs.getCode().getCodingFirstRep().getDisplay());
		// -- value
		Assert.assertEquals("http://snomed.info/sct", obs.getValueCodeableConcept().getCodingFirstRep().getSystem());
		Assert.assertEquals("161901003", obs.getValueCodeableConcept().getCodingFirstRep().getCode());
		Assert.assertEquals("Chronically ill", obs.getValueCodeableConcept().getCodingFirstRep().getDisplay());
		// -- subject
		Assert.assertEquals("Happy Always Kid", obs.getSubject().getDisplay());

		//-- verify condition
		Reference condRef = getReference(bundle, "Health Concerns Section", "Condition");
		Assert.assertNotNull(condRef);
		Condition cond = (Condition)findResource(bundle, condRef);
		Assert.assertNotNull(cond);

		//-- clinicalStatus
		Assert.assertEquals("http://terminology.hl7.org/CodeSystem/condition-clinical", cond.getClinicalStatus().getCodingFirstRep().getSystem());
		Assert.assertEquals("resolved", cond.getClinicalStatus().getCodingFirstRep().getCode());
		Assert.assertEquals("Resolved", cond.getClinicalStatus().getCodingFirstRep().getDisplay());
		//-- category
		Assert.assertEquals("http://hl7.org/fhir/us/core/CodeSystem/condition-category", cond.getCategoryFirstRep().getCodingFirstRep().getSystem());
		Assert.assertEquals("health-concern", cond.getCategoryFirstRep().getCodingFirstRep().getCode());
		Assert.assertEquals("Health Concern", cond.getCategoryFirstRep().getCodingFirstRep().getDisplay());
		//-- code - the mapping of the health concern act was changed as part of SMILE-5948
		//          this input file does not reflect the current Tebra input patterns, so this part of the test fails
//		Assert.assertEquals("http://loinc.org", cond.getCode().getCodingFirstRep().getSystem());
//		Assert.assertEquals("75310-3", cond.getCode().getCodingFirstRep().getCode());
//		Assert.assertEquals("Health Concern", cond.getCode().getCodingFirstRep().getDisplay());
		// -- subject
		Assert.assertEquals("Happy Always Kid", cond.getSubject().getDisplay());
		// -- onset
		Assert.assertEquals("2020-06-22", cond.getOnsetDateTimeType().getValueAsString());
		// -- verificationStatus
		Assert.assertEquals(ConditionVerStatus.CONFIRMED.toCode(), cond.getVerificationStatus().getCodingFirstRep().getCode());
		Assert.assertEquals(ConditionVerStatus.CONFIRMED.getDisplay(), cond.getVerificationStatus().getCodingFirstRep().getDisplay());
		Assert.assertEquals(ConditionVerStatus.CONFIRMED.getSystem(), cond.getVerificationStatus().getCodingFirstRep().getSystem());
		// -- asserter Patient
		Reference patRef = cond.getAsserter();
		Assert.assertNotNull(patRef);
		Patient pat = (Patient)findResource(bundle, patRef);
		Assert.assertNotNull(pat);
		Assert.assertEquals("111223333", pat.getIdentifierFirstRep().getValue());

		logger.debug("Observation :\n" + FhirContext.forR4().newJsonParser().setPrettyPrint(true).encodeResourceToString(obs));
		logger.debug("Condition :\n" + FhirContext.forR4().newJsonParser().setPrettyPrint(true).encodeResourceToString(cond));
		logger.debug("Bundle :\n" + FhirContext.forR4().newJsonParser().setPrettyPrint(true).encodeResourceToString(bundle));
	}

	@Test
	public void testCareTeamSection() throws Exception {

		readVerifyFile("Alice_Newman_Modified_03Aug2022.xml", addlSections);
		Bundle bundle = readVerifyFile("Alice_Newman_Modified_03Aug2022.xml", null).getBundle();

		//-- CareTeam
		CareTeam careTeam = (CareTeam)FHIRUtil.findFirstResource(bundle, CareTeam.class);
		Assert.assertNotNull(careTeam);

		//-- id
		Assert.assertNotNull(careTeam.getId());

		//-- Narrative
		Assert.assertEquals("additional", careTeam.getText().getStatus().toCode());
		MatcherAssert.assertThat(careTeam.getText().getDivAsString(), CoreMatchers.containsString("Albert"));

		//-- status
		Assert.assertEquals("active", careTeam.getStatus().toCode());

		//-- name
		Assert.assertEquals("Care Team", careTeam.getName());

		// -- subject
		Assert.assertEquals("ALICE JONES NEWMAN, Alicia Jones", careTeam.getSubject().getDisplay());

		// -- period
		Assert.assertEquals("2018-10-08", (new SimpleDateFormat("yyyy-MM-dd")).format(careTeam.getPeriod().getStart()));

		//-- participants
		List<CareTeamParticipantComponent> participants = careTeam.getParticipant();
		Assert.assertEquals(2, participants.size());

		//-- participant1
		CareTeamParticipantComponent participant1 = participants.get(0);
		MatcherAssert.assertThat(participant1.getMember().getReference(), CoreMatchers.startsWith("Practitioner"));
		Assert.assertEquals("Albert Davis", participant1.getMember().getDisplay());
		Assert.assertEquals("2018-10-08", (new SimpleDateFormat("yyyy-MM-dd")).format(participant1.getPeriod().getStart()));

		//-- participant2
		CareTeamParticipantComponent participant2 = participants.get(1);
		MatcherAssert.assertThat(participant2.getMember().getReference(), CoreMatchers.startsWith("Practitioner"));
		Assert.assertEquals("Tracy Davis", participant2.getMember().getDisplay());
		Assert.assertEquals("2018-10-08", (new SimpleDateFormat("yyyy-MM-dd")).format(participant2.getPeriod().getStart()));

		//-- paticipant.role
		Assert.assertEquals("http://nucc.org/provider-taxonomy", participant2.getRoleFirstRep().getCodingFirstRep().getSystem());
		Assert.assertEquals("207Q00000X", participant2.getRoleFirstRep().getCodingFirstRep().getCode());

		logger.debug("CareTeam :\n" + FhirContext.forR4().newJsonParser().setPrettyPrint(true).encodeResourceToString(careTeam));
		logger.debug("Bundle :\n" + FhirContext.forR4().newJsonParser().setPrettyPrint(true).encodeResourceToString(bundle));
	}

	@Test
	public void testBirthSexFemale() throws Exception {

		readVerifyFile("kareo_sample_file_patient_author.xml", addlSections);
		Bundle bundle = readVerifyFile("kareo_sample_file_patient_author.xml", null).getBundle();

		Patient patient = BundleUtil.findOneResource(bundle, Patient.class);
		Assert.assertNotNull(patient);

		List<Extension> extList = patient.getExtension();
		Extension birthSexExt = null;
		for (Extension ext : extList) {
			if ("http://hl7.org/fhir/us/core/StructureDefinition/us-core-birthsex".equals(ext.getUrl())) {
				birthSexExt = ext;
				break;
			}
		}
		Assert.assertNotNull(birthSexExt);
		Assert.assertEquals("F", ((CodeType)birthSexExt.getValue()).getValue());

		logger.debug("Patient :\n" + FhirContext.forR4().newJsonParser().setPrettyPrint(true).encodeResourceToString(patient));
	}

	@Test
	public void testBirthSexUnknown() throws Exception {

		readVerifyFile("Alice_Newman_Modified_03Aug2022.xml", addlSections);
		Bundle bundle = readVerifyFile("Alice_Newman_Modified_03Aug2022.xml", null).getBundle();

		Patient patient = BundleUtil.findOneResource(bundle, Patient.class);
		Assert.assertNotNull(patient);

		List<Extension> extList = patient.getExtension();
		Extension birthSexExt = null;
		for (Extension ext : extList) {
			if ("http://hl7.org/fhir/us/core/StructureDefinition/us-core-birthsex".equals(ext.getUrl())) {
				birthSexExt = ext;
				break;
			}
		}
		Assert.assertNotNull(birthSexExt);
		Assert.assertEquals("UNK", ((CodeType)birthSexExt.getValue()).getValue());

		logger.debug("Patient :\n" + FhirContext.forR4().newJsonParser().setPrettyPrint(true).encodeResourceToString(patient));
	}

	@Test
	public void testDeviceManufacturingInformation() throws Exception {

		readVerifyFile("Alice_Newman_Modified_03Aug2022.xml", addlSections);
		Bundle bundle = readVerifyFile("Alice_Newman_Modified_03Aug2022.xml", null).getBundle();

		Device device = BundleUtil.findOneResource(bundle, Device.class);
		Assert.assertNotNull(device);

		Assert.assertNotNull(device);
		Assert.assertEquals("BLC200461H", device.getSerialNumber());

		logger.debug("Device :\n" + FhirContext.forR4().newJsonParser().setPrettyPrint(true).encodeResourceToString(device));
	}

	@Test
	public void testMedicationActivityRouting() throws Exception {
		readVerifyFile("C-CDA_R2-1_CCD.xml", addlSections);
		Bundle bundle = readVerifyFile("C-CDA_R2-1_CCD.xml", null).getBundle();

		MedicationRequest medicationRequest = BundleUtil.findOneResource(bundle, MedicationRequest.class);

		// not many assertions because the hard-coding to ORDER is sufficient to differentiate the two issues routes
		Assert.assertNotNull(medicationRequest);
		Assert.assertEquals(MedicationRequest.MedicationRequestIntent.ORDER, medicationRequest.getIntent());
		Assert.assertEquals(MedicationRequest.MedicationRequestStatus.ACTIVE, medicationRequest.getStatus());

		logger.debug("MedicationRequest :\n" + FhirContext.forR4().newJsonParser().setPrettyPrint(true).encodeResourceToString(medicationRequest));
	}

	@Test
	public void testEncounterHasReasonCode() throws Exception {

		readVerifyFile("Alice_Newman_Modified_03Aug2022.xml", addlSections);
		Bundle bundle = readVerifyFile("Alice_Newman_Modified_03Aug2022.xml", null).getBundle();

		Encounter encounter = BundleUtil.findOneResource(bundle, Encounter.class);
		Assert.assertNotNull(encounter);

		Assert.assertNotNull(encounter);
		Assert.assertEquals("57676002", encounter.getReasonCodeFirstRep().getCodingFirstRep().getCode());
		Assert.assertEquals(SNOMED_CT_URI, encounter.getReasonCodeFirstRep().getCodingFirstRep().getSystem());
		Assert.assertEquals("Joint pain", encounter.getReasonCodeFirstRep().getCodingFirstRep().getDisplay());

		logger.debug("Encounter :\n" + FhirContext.forR4().newJsonParser().setPrettyPrint(true).encodeResourceToString(encounter));
	}

	@Test
	public void testMedicationRequestHasEncounter() throws Exception {
		addlSections.add(CDASectionTypeEnum.MEDICATIONS_SECTION);
		Bundle bundle = readVerifyFile("Alice_Newman_Modified_03Aug2022.xml", addlSections).getBundle();

		MedicationRequest medicationRequest = FHIRUtil.findFirstResource(bundle, MedicationRequest.class);
		Encounter encounter = FHIRUtil.findFirstResource(bundle, Encounter.class);

		// this is the first and only encounter in the ENCOUNTERS section, so it's the one we should link to
		Assert.assertEquals("279635419247698927653159992848961060804", encounter.getIdentifierFirstRep().getValue());
		Assert.assertEquals("urn:oid:2.16.840.1.113883.3.7220", encounter.getIdentifierFirstRep().getSystem());

		Assert.assertNotNull(medicationRequest);
		Assert.assertNotNull(medicationRequest.getEncounter().getReference());
		Assert.assertEquals(new Reference(encounter.getId()).getReference(), medicationRequest.getEncounter().getReference());
		addlSections.remove(CDASectionTypeEnum.MEDICATIONS_SECTION);
	}

	@Test
	public void testProvenance() throws Exception {
		String documentBody = "<ClinicalDoc>Meowmeowmeowmeow</ClinicalDoc>";
		readVerifyFile("Alice_Newman_Modified_03Aug2022.xml", addlSections, documentBody);
		Bundle bundle = readVerifyFile("Alice_Newman_Modified_03Aug2022.xml", null, documentBody).getBundle();

		Provenance provenance = FHIRUtil.findFirstResource(bundle, Provenance.class);
		Assert.assertNotNull(provenance);

		// -- no device, only author and transmitter
		Assert.assertEquals(2, provenance.getAgent().size());
		ProvenanceAgentComponent authorPac = provenance.getAgent().get(0);

		//-- author.type
		Coding authorType = authorPac.getType().getCodingFirstRep();
		Assert.assertNull(authorType.getId());
		Assert.assertEquals(authorType.getSystem(), ProvenanceAgentType.AUTHOR.getSystem());
		Assert.assertEquals(authorType.getCode(), ProvenanceAgentType.AUTHOR.toCode());
		Assert.assertEquals(authorType.getDisplay(), ProvenanceAgentType.AUTHOR.getDisplay());

		// author.role
		List<CodeableConcept> authorRoleList = authorPac.getRole();
		Assert.assertEquals(0, authorRoleList.size());

		MatcherAssert.assertThat(authorPac.getWho().getReference(),  CoreMatchers.containsString("Practitioner"));
		MatcherAssert.assertThat(authorPac.getOnBehalfOf().getReference(), CoreMatchers.containsString("Organization"));

		//-- transmitter
		ProvenanceAgentComponent transmitterPac = provenance.getAgent().get(1);

		// transmitter.type
		Coding transmitterType = transmitterPac.getType().getCodingFirstRep();
		Assert.assertNull(transmitterType.getId());
		Assert.assertEquals(transmitterType.getSystem(), "http://hl7.org/fhir/us/core/CodeSystem/us-core-provenance-participant-type");
		Assert.assertEquals(transmitterType.getCode(), "transmitter");
		Assert.assertEquals(transmitterType.getDisplay(), "Transmitter");

		MatcherAssert.assertThat(transmitterPac.getWho().getReference(),  CoreMatchers.containsString("Practitioner"));
		MatcherAssert.assertThat(transmitterPac.getOnBehalfOf().getReference(), CoreMatchers.containsString("Organization"));


		logger.debug("Provenance :\n" + FhirContext.forR4().newJsonParser().setPrettyPrint(true).encodeResourceToString(provenance));
		logger.debug("Bundle :\n" + FhirContext.forR4().newJsonParser().setPrettyPrint(true).encodeResourceToString(bundle));
	}

	@Test
	public void testErrorDetection() throws Exception {
		ITransformationResult result = readVerifyFile("C-CDA_R2-1_CCD_with_errors.xml", addlSections);

		// Spot check error messages
		Assert.assertTrue("result has no error messages", result.hasErrors());
		Assert.assertEquals("Wrong number of error messages", 1, result.getErrors().size());
		Assert.assertEquals("Error message severity", ITransformationMessage.Severity.ERROR, result.getErrors().get(0).getSeverity());
		Assert.assertEquals("Error message entry", "Immunization Activity", result.getErrors().get(0).getEntryName());
		Assert.assertEquals("Error message field", "statusCode", result.getErrors().get(0).getFieldName());
		Assert.assertEquals("Error message message", "Code [detelpmoc] could not be parsed.", result.getErrors().get(0).getMessage());

		// The order of the warnings is non-deterministic, so these messages won't necessarily occur at any particular index in the list.
		// We just need to ensure they are in there somewhere.
		Assert.assertTrue(result.hasWarnings());
		Assert.assertEquals("Wrong number of warning messages", 5, result.getWarnings().size());
		result.getWarnings().forEach(warning -> Assert.assertEquals(
				"Warning message severity", ITransformationMessage.Severity.WARNING, warning.getSeverity()));
		Assert.assertTrue("Allergy value cardinality warning is missing",
				result.getWarnings().stream()
						.anyMatch(warning -> {
							return "Allergy - Intolerance Observation".equals(warning.getEntryName()) &&
									"value".equals(warning.getFieldName()) &&
									"Expected maximum cardinality of 1, 2 instances found.".equals(warning.getMessage());
						}));
		Assert.assertTrue("Medication status interpretation warning is missing",
				result.getWarnings().stream()
						.anyMatch(warning -> {
							return "Medication Supply Order".equals(warning.getEntryName()) &&
									"statusCode".equals(warning.getFieldName()) &&
									"No value found.".equals(warning.getMessage());
						}));

	}

	@Test
	public void testAliceNewmanVitalSignSectionForBPandOxygenMerge() throws Exception {

		readVerifyFile("Alice_Newman_Modified_03Aug2022.xml", addlSections);
		Bundle bundle = readVerifyFile("Alice_Newman_Modified_03Aug2022.xml", null).getBundle();

		//-------------------------------------
		//-- Verify Blood Pressure merge
		//-------------------------------------
		List<Resource> bpObsList = getResource(bundle, "VITAL SIGNS", "Observation", "Blood Pressure");
		List<Resource> bpSObsList = getResource(bundle, "VITAL SIGNS", "Observation", "BP Systolic");
		List<Resource> bpDObsList = getResource(bundle, "VITAL SIGNS", "Observation", "BP Diastolic");

		// -- two pairs of BP Diastolic/Systolic
		Assert.assertEquals(2, bpObsList.size());
		Assert.assertEquals(0, bpSObsList.size()); // merged
		Assert.assertEquals(0, bpDObsList.size()); // merged

		//-- NOTE : the order is not guaranteed
		Observation bpObs = (Observation)bpObsList.get(0);
		Observation bpObs1;
		Observation bpObs2;
		if (bpObs.getIdentifierFirstRep().getValue().equals("47214085567808985905881784129564384806")) {
			bpObs1 = bpObs;
			bpObs2 = (Observation)bpObsList.get(1);
		} else {
			bpObs1 = (Observation)bpObsList.get(1);
			bpObs2 = bpObs;
		}

		logger.debug("bpObs1 :\n" + FhirContext.forR4().newJsonParser().setPrettyPrint(true).encodeResourceToString(bpObs1));
		logger.debug("bpObs2 :\n" + FhirContext.forR4().newJsonParser().setPrettyPrint(true).encodeResourceToString(bpObs2));

		// *** First Blood Pressure
		// assert code
		verifyLoincCoding(bpObs1.getCode().getCodingFirstRep(), "85354-9", "Blood Pressure");
		Assert.assertNull(bpObs1.getValue());

		// component 1, code BP Systolic
		ObservationComponentComponent bpObs1Comp1 = bpObs1.getComponent().get(0);
		verifyLoincCoding(bpObs1Comp1.getCode().getCodingFirstRep(), "8480-6", "BP Systolic");
		verifyQuantity(bpObs1Comp1.getValueQuantity(), "mm[Hg]", "145.0", "mm[Hg]");

		// component 2, code BP Diastolic
		ObservationComponentComponent bpObs1Comp2 = bpObs1.getComponent().get(1);
		verifyLoincCoding(bpObs1Comp2.getCode().getCodingFirstRep(), "8462-4", "BP Diastolic");
		verifyQuantity(bpObs1Comp2.getValueQuantity(), "mm[Hg]", "88.0", "mm[Hg]");

		// *** Second Blood Pressure
		// assert code
		verifyLoincCoding(bpObs2.getCode().getCodingFirstRep(), "85354-9", "Blood Pressure");
		Assert.assertNull(bpObs2.getValue());

		// component 1, code BP Systolic
		ObservationComponentComponent bpObs2Comp1 = bpObs2.getComponent().get(0);
		verifyLoincCoding(bpObs2Comp1.getCode().getCodingFirstRep(), "8480-6", "BP Systolic");
		verifyQuantity(bpObs2Comp1.getValueQuantity(), "mm[Hg]", "145.0", "mm[Hg]");

		// component 2, code BP Diastolic
		ObservationComponentComponent bpObs2Comp2 = bpObs2.getComponent().get(1);
		verifyLoincCoding(bpObs2Comp2.getCode().getCodingFirstRep(), "8462-4", "BP Diastolic");
		verifyQuantity(bpObs2Comp2.getValueQuantity(), "mm[Hg]", "88.0", "mm[Hg]");

		//-------------------------------------
		//-- Verify Oxygen Merge
		//-------------------------------------
		List<Resource> oxObsList = getResource(bundle, "VITAL SIGNS", "Observation", "O2 % BldC Oximetry");
		List<Resource> oxIObsList = getResource(bundle, "VITAL SIGNS", "Observation", "Inhaled Oxygen Concentration");
		List<Resource> oxFObssList = getResource(bundle, "VITAL SIGNS", "Observation", "Flow Rate");

		// -- two pairs of Oxygen
		Assert.assertEquals(2, oxObsList.size());
		Assert.assertEquals(0, oxIObsList.size()); // merged
		Assert.assertEquals(0, oxFObssList.size()); // merged

		//-- NOTE : the order is not guaranteed
		Observation oxObs = (Observation)oxObsList.get(0);
		Observation oxObs1;
		Observation oxObs2;
		if (oxObs.getIdentifierFirstRep().getValue().equals("47214085567808985905881784129564384806")) {
			oxObs1 = oxObs;
			oxObs2 = (Observation)oxObsList.get(1);
		} else {
			oxObs1 = (Observation)oxObsList.get(1);
			oxObs2 = oxObs;
		}

		logger.debug("OxObs1 :\n" + FhirContext.forR4().newJsonParser().setPrettyPrint(true).encodeResourceToString(oxObs1));
		logger.debug("OxObs2 :\n" + FhirContext.forR4().newJsonParser().setPrettyPrint(true).encodeResourceToString(oxObs2));

		//*** First Oxygen
		// assert code 1 (no change)
		verifyLoincCoding(oxObs1.getCode().getCoding().get(0), "59408-5", "O2 % BldC Oximetry");
		// assert code 2 (new)
		verifyLoincCoding(oxObs1.getCode().getCoding().get(1), "2708-6", "Oxygen saturation in Arterial blood");
		verifyQuantity(oxObs1.getValueQuantity(), "%", "95.0", "%");

		//-- component 1 Inhaled Oxygen Concentration: no value in the file
		Coding dar = oxObs1.getComponentFirstRep().getDataAbsentReason().getCodingFirstRep();
		Assert.assertEquals(DATA_ABSENT_REASON_TERMINOLOGY_URI, dar.getSystem());
		Assert.assertEquals("error", dar.getCode());
		Assert.assertEquals("Error", dar.getDisplay());

		//-- component 2 Flow Rate
		ObservationComponentComponent oxObs1Comp2 = oxObs1.getComponent().get(1);
		verifyLoincCoding(oxObs1Comp2.getCode().getCodingFirstRep(), "3151-8", "Flow Rate");
		verifyQuantity(oxObs1Comp2.getValueQuantity(), "L/min", "6", "L/min");

		//*** second Oxygen
		// assert code 1 (no change)
		verifyLoincCoding(oxObs2.getCode().getCoding().get(0), "59408-5", "O2 % BldC Oximetry");
		// assert code 2 (new)
		verifyLoincCoding(oxObs2.getCode().getCoding().get(1), "2708-6", "Oxygen saturation in Arterial blood");
		verifyQuantity(oxObs2.getValueQuantity(), "%", "95.0", "%");

		//-- component 1 Inhaled Oxygen Concentration
		ObservationComponentComponent oxObs2Comp1 = oxObs2.getComponent().get(0);
		verifyLoincCoding(oxObs2Comp1.getCode().getCodingFirstRep(), "3150-0", "Inhaled Oxygen Concentration");
		verifyQuantity(oxObs2Comp1.getValueQuantity(), "%", "36.0", "%");

		//-- component 2 Flow Rate
		ObservationComponentComponent oxObs2Comp2 = oxObs2.getComponent().get(1);
		verifyLoincCoding(oxObs2Comp2.getCode().getCodingFirstRep(), "3151-8", "Flow Rate");
		verifyQuantity(oxObs2Comp2.getValueQuantity(), "L/min", "6", "L/min");

		logger.debug("Bundle :\n" + FhirContext.forR4().newJsonParser().setPrettyPrint(true).encodeResourceToString(bundle));
	}

	@Test
	public void testJeremyBatesExample_NullFlavor() throws Exception {

		readVerifyFile("Jeremy_Bates.xml", addlSections);
		Bundle bundle = readVerifyFile("Jeremy_Bates.xml", null).getBundle();

		//-- test Procedure.code
		Procedure proc = FHIRUtil.findFirstResource(bundle, Procedure.class);
		Extension procCodeExt = proc.getCode().getExtensionFirstRep();
		Assert.assertNull(proc.getCode().getCodingFirstRep().getCode());
		verifyExtension(procCodeExt, "error", "Error");

		//-- test Procedure.performedPeriod
		Assert.assertNull(proc.getPerformedPeriod().getStartElement().getValueAsString());
		Assert.assertNull(proc.getPerformedPeriod().getEndElement().getValueAsString());

		//-- test Condition.onsetDateTime (error)
		Reference condRef = getReference(bundle, "PROBLEMS", "Condition");
		Condition cond = (Condition)findResource(bundle, condRef);
		Assert.assertFalse(cond.getOnsetDateTimeType().hasExtension());

		//-- test Condition.abatementDateTime (error)
		Assert.assertFalse(cond.getAbatementDateTimeType().hasExtension());

		//-- test Immunization.identifier (error)
		Immunization imm = FHIRUtil.findFirstResource(bundle, Immunization.class);
		Assert.assertFalse(imm.getIdentifierFirstRep().hasExtension());

		//-- test Immunization.vaccineCode (not-applicable)
		Extension immVccCodeExt = imm.getVaccineCode().getExtensionFirstRep();
		verifyExtension(immVccCodeExt, "not-applicable", "Not Applicable");

		//-- test Immunization.lotNumber (unknown)
		Assert.assertFalse(imm.getLotNumberElement().hasExtension());

		//-- test Location.telecom (unknown)
		Location loc = FHIRUtil.findFirstResource(bundle, Location.class);
		Extension locTelecomExt = loc.getTelecomFirstRep().getExtensionFirstRep();
		verifyExtension(locTelecomExt, "unknown", "Unknown");

		// Note: MedicationRequest is tested on MedicationRequestTest

		// test AllergyIntolerance.code (error)
		AllergyIntolerance ai = FHIRUtil.findFirstResource(bundle, AllergyIntolerance.class);
		Extension aiCodeExt = ai.getCode().getExtensionFirstRep();
		verifyExtension(aiCodeExt, "error", "Error");

		logger.debug("Bundle :\n" + FhirContext.forR4().newJsonParser().setPrettyPrint(true).encodeResourceToString(bundle));
	}

	@Test
	public void testObservationComponentDataAbsentReason() throws Exception {
		readVerifyFile("3715.xml", addlSections);
		Bundle bundle = readVerifyFile("3715.xml", null).getBundle();

		Observation obs = bundle.getEntry().stream()
				.map(BundleEntryComponent::getResource)
				.filter(t -> t.getResourceType().equals(ResourceType.Observation))
				.map(t -> (Observation) t)
				.filter(t -> "8a3179dc-78bf-4600-82e7-df5cfa0a6319".equals(t.getIdentifierFirstRep().getValue()))
				.findFirst().get();

		Assert.assertNotNull(obs);
		ObservationComponentComponent comp = obs.getComponentFirstRep();
		Coding componentCode = comp.getCode().getCodingFirstRep();
		Assert.assertEquals("http://loinc.org", componentCode.getSystem());
		Assert.assertEquals("3150-0", componentCode.getCode());
		Assert.assertEquals("Inhaled Oxygen Concentration", componentCode.getDisplay());

		Coding componentDar = comp.getDataAbsentReason().getCodingFirstRep();
		Assert.assertEquals(DATA_ABSENT_REASON_TERMINOLOGY_URI, componentDar.getSystem());
		Assert.assertEquals("error", componentDar.getCode());
		Assert.assertEquals("Error", componentDar.getDisplay());

	}

	@Ignore
	@Test
	public void testEpicSample1() throws Exception {
		readVerifyFile("Epic/DOC0001.XML", addlSections);
	}

	@Ignore
	@Test
	public void testEpicSample2() throws Exception {
		readVerifyFile("Epic/DOC0001 2.XML", addlSections);
	}

	@Ignore
	@Test
	public void testEpicSample3() throws Exception {
		readVerifyFile("Epic/DOC0001 3.XML", addlSections);
	}

	@Ignore
	@Test
	public void testEpicSample4() throws Exception {
		readVerifyFile("Epic/DOC0001 4.XML", addlSections);
	}

	@Ignore
	@Test
	public void testEpicSample5() throws Exception {
		readVerifyFile("Epic/DOC0001 5.XML", addlSections);
	}

	@Ignore
	@Test
	public void testEpicSample6() throws Exception {
		readVerifyFile("Epic/DOC0001 6.XML", addlSections);
	}

	@Ignore
	@Test
	public void testEpicSample7() throws Exception {
		readVerifyFile("Epic/DOC0001 7.XML", addlSections);
	}

	@Ignore
	@Test
	public void testEpicSample8() throws Exception {
		readVerifyFile("Epic/DOC0001 8.XML", addlSections);
	}

	@Ignore
	@Test
	public void testEpicSample9() throws Exception {
		readVerifyFile("Epic/DOC0001 9.XML", addlSections);
	}

	@Ignore
	@Test
	public void testEpicSample10() throws Exception {
		readVerifyFile("Epic/DOC0001 10.XML", addlSections);
	}

	@Ignore
	@Test
	public void testEpicSample11() throws Exception {
		readVerifyFile("Epic/DOC0001 11.XML", addlSections);
	}

	@Ignore
	@Test
	public void testEpicSample12() throws Exception {
		readVerifyFile("Epic/DOC0001 12.XML", addlSections);
	}

	@Ignore
	@Test
	public void testEpicSample13() throws Exception {
		readVerifyFile("Epic/DOC0001 13.XML", addlSections);
	}

	@Ignore
	@Test
	public void testEpicSample14() throws Exception {
		readVerifyFile("Epic/DOC0001 14.XML", addlSections);
	}

	@Ignore
	@Test
	public void testEpicSample15() throws Exception {
		readVerifyFile("Epic/DOC0001 15.XML", addlSections);
	}

	@Ignore
	@Test
	public void testEpicSample16() throws Exception {
		readVerifyFile("Epic/HannahBanana_EpicCCD.xml", addlSections);
	}

	@Ignore
	@Test
	public void testCernerSample1() throws Exception {
		readVerifyFile("Cerner/Person-RAKIA_TEST_DOC00001 (1).XML", addlSections);
	}

	@Ignore
	@Test
	public void testCernerSample2() throws Exception {
		readVerifyFile("Cerner/Encounter-RAKIA_TEST_DOC00001.XML", addlSections);
	}

	@Test
	public void testCreateTransactionBundle_addPatchEntries_nonUpdatedResource() {
		Bundle inputBundle = new Bundle();

		// Set up a Practitioner that should not get an update patch entry
		Practitioner practitioner = new Practitioner();
		practitioner.addIdentifier().setValue("practitionerId");
		inputBundle.addEntry().setResource(practitioner);

		CCDTransformerImpl ccdTransformer = new CCDTransformerImpl();
		Bundle outputBundle = ccdTransformer.createTransactionBundle(inputBundle, null, false);

		Assert.assertEquals(1, outputBundle.getEntry().size());
	}

	@Test
	public void testCreateTransactionBundle_addPatchEntries_AllergyIntolerance() {
		Bundle inputBundle = new Bundle();

		// Set up an allergy intolerance that should get an update patch entry
		AllergyIntolerance allergyIntolerance = new AllergyIntolerance();
		allergyIntolerance.addIdentifier().setValue("allergyId");
		allergyIntolerance.setClinicalStatus(new CodeableConcept().addCoding(new Coding(
				"http://terminology.hl7.org/CodeSystem/allergyintolerance-clinical", "inactive", "Inactive")));
		inputBundle.addEntry().setResource(allergyIntolerance);

		CCDTransformerImpl ccdTransformer = new CCDTransformerImpl();
		Bundle outputBundle = ccdTransformer.createTransactionBundle(inputBundle, null, false);

		Assert.assertEquals(2, outputBundle.getEntry().size());

		BundleEntryComponent allergyUpdateEntry = outputBundle.getEntry().get(1);
		Parameters allergyUpdateParameters = (Parameters) allergyUpdateEntry.getResource();
		Assert.assertEquals(Bundle.HTTPVerb.PATCH, allergyUpdateEntry.getRequest().getMethod());
		Assert.assertEquals("AllergyIntolerance?identifier=allergyId", allergyUpdateEntry.getRequest().getUrl());

		List<Parameters.ParametersParameterComponent> allergyUpdateParts = allergyUpdateParameters.getParameter().get(0).getPart();
		Assert.assertEquals("AllergyIntolerance.clinicalStatus", allergyUpdateParts.get(1).getValue().castToString(allergyUpdateParts.get(1).getValue()).getValue());
		Assert.assertEquals("inactive", allergyUpdateParts.get(2).getValue().castToCodeableConcept(allergyUpdateParts.get(2).getValue()).getCodingFirstRep().getCode());
	}

	@Test
	public void testCreateTransactionBundle_addPatchEntries_AllergyIntolerance_noId() {
		Bundle inputBundle = new Bundle();

		// If an allergy intolerance that should get an update patch entry does not have an id,
		// we cannot formulate a URL for the patch, so don't create one
		AllergyIntolerance allergyIntolerance = new AllergyIntolerance();
		allergyIntolerance.setClinicalStatus(new CodeableConcept().addCoding(new Coding(
				"http://terminology.hl7.org/CodeSystem/allergyintolerance-clinical", "inactive", "Inactive")));
		inputBundle.addEntry().setResource(allergyIntolerance);

		CCDTransformerImpl ccdTransformer = new CCDTransformerImpl();
		Bundle outputBundle = ccdTransformer.createTransactionBundle(inputBundle, null, false);

		Assert.assertEquals(1, outputBundle.getEntry().size());
	}

	@Test
	public void testCreateTransactionBundle_addPatchEntries_MedicationRequest() {
		Bundle inputBundle = new Bundle();

		// Set up a MedicationRequest that should get an update patch entry
		MedicationRequest medicationRequest = new MedicationRequest();
		medicationRequest.addIdentifier().setValue("medicationRequestId");
		medicationRequest.setStatus(MedicationRequest.MedicationRequestStatus.COMPLETED);
		inputBundle.addEntry().setResource(medicationRequest);

		CCDTransformerImpl ccdTransformer = new CCDTransformerImpl();
		Bundle outputBundle = ccdTransformer.createTransactionBundle(inputBundle, null, false);

		Assert.assertEquals(2, outputBundle.getEntry().size());

		BundleEntryComponent medicationEntry = outputBundle.getEntry().get(1);
		Parameters medicationUpdateParameters = (Parameters) medicationEntry.getResource();
		Assert.assertEquals(Bundle.HTTPVerb.PATCH, medicationEntry.getRequest().getMethod());
		Assert.assertEquals("MedicationRequest?identifier=medicationRequestId", medicationEntry.getRequest().getUrl());

		List<Parameters.ParametersParameterComponent> medicationUpdateParts = medicationUpdateParameters.getParameter().get(0).getPart();
		Assert.assertEquals("MedicationRequest.status", medicationUpdateParts.get(1).getValue().castToString(medicationUpdateParts.get(1).getValue()).getValue());
		Assert.assertEquals("completed", medicationUpdateParts.get(2).getValue().castToCode(medicationUpdateParts.get(2).getValue()).getCode());
	}

	@Test
	public void testCreateTransactionBundle_addPatchEntries_Condition() throws Exception {
		Bundle inputBundle = new Bundle();

		// Set up a Condition that should get an update patch entry
		Condition condition = new Condition();
		condition.addIdentifier().setValue("conditionId");
		condition.setClinicalStatus(new CodeableConcept().addCoding(new Coding(
				"http://terminology.hl7.org/CodeSystem/condition-clinical", "inactive", "Inactive")));
		inputBundle.addEntry().setResource(condition);

		CCDTransformerImpl ccdTransformer = new CCDTransformerImpl();
		Bundle outputBundle = ccdTransformer.createTransactionBundle(inputBundle, null, false);

		Assert.assertEquals(2, outputBundle.getEntry().size());

		BundleEntryComponent conditionUpdateEntry = outputBundle.getEntry().get(1);
		Parameters conditionUpdateParameters = (Parameters) conditionUpdateEntry.getResource();
		Assert.assertEquals(Bundle.HTTPVerb.PATCH, conditionUpdateEntry.getRequest().getMethod());
		Assert.assertEquals("Condition?identifier=conditionId", conditionUpdateEntry.getRequest().getUrl());

		List<Parameters.ParametersParameterComponent> conditionUpdateParts = conditionUpdateParameters.getParameter().get(0).getPart();
		Assert.assertEquals("Condition.clinicalStatus", conditionUpdateParts.get(1).getValue().castToString(conditionUpdateParts.get(1).getValue()).getValue());
		Assert.assertEquals("inactive", conditionUpdateParts.get(2).getValue().castToCodeableConcept(conditionUpdateParts.get(2).getValue()).getCodingFirstRep().getCode());
	}

	@Test
	public void testCreateTransactionBundle_addPatchEntries_PatientAddress() throws Exception {
		Bundle inputBundle = new Bundle();

		// Set up a Patient that should get an update patch entry
		Patient patient = new Patient();
		patient.addIdentifier().setValue("patientId");
		patient.addAddress().addLine("123 Granite Lane").setCity("Bedrock").setPeriod(new Period().setEndElement(new DateTimeType("2023-04-20")));
		patient.addAddress().addLine("937 Shale Street").setCity("Bedrock").setPeriod(new Period().setStartElement(new DateTimeType("2023-04-21")));
		inputBundle.addEntry().setResource(patient);

		CCDTransformerImpl ccdTransformer = new CCDTransformerImpl();
		Bundle outputBundle = ccdTransformer.createTransactionBundle(inputBundle, null, false);

		Assert.assertEquals(2, outputBundle.getEntry().size());

		BundleEntryComponent patientUpdateEntry = outputBundle.getEntry().get(1);
		Parameters conditionUpdateParameters = (Parameters) patientUpdateEntry.getResource();
		Assert.assertEquals(Bundle.HTTPVerb.PATCH, patientUpdateEntry.getRequest().getMethod());
		Assert.assertEquals("Patient?identifier=patientId", patientUpdateEntry.getRequest().getUrl());

		Assert.assertEquals(3, conditionUpdateParameters.getParameter().size());

		List<Parameters.ParametersParameterComponent> patientUpdateParts = conditionUpdateParameters.getParameter().get(0).getPart();
		Assert.assertEquals("replace", patientUpdateParts.get(0).getValue().castToString(patientUpdateParts.get(0).getValue()).getValue());
		Assert.assertEquals("Patient.address.where(line = '123 Granite Lane' and use != 'old').use", patientUpdateParts.get(1).getValue().castToString(patientUpdateParts.get(1).getValue()).getValue());
		Assert.assertEquals(Address.AddressUse.OLD.toCode(), patientUpdateParts.get(2).getValue().castToCode(patientUpdateParts.get(2).getValue()).getValue());

		patientUpdateParts = conditionUpdateParameters.getParameter().get(1).getPart();
		Assert.assertEquals("add", patientUpdateParts.get(0).getValue().castToString(patientUpdateParts.get(0).getValue()).getValue());
		Assert.assertEquals("Patient", patientUpdateParts.get(1).getValue().castToString(patientUpdateParts.get(1).getValue()).getValue());
		Assert.assertEquals("address", patientUpdateParts.get(2).getValue().castToString(patientUpdateParts.get(2).getValue()).getValue());
		Assert.assertEquals("937 Shale Street", patientUpdateParts.get(3).getValue().castToAddress(patientUpdateParts.get(3).getValue()).getLine().get(0).getValue());
	}

	@Test
	public void testCreateTransactionBundle_addPatchEntries_PatientName() throws Exception {
		Bundle inputBundle = new Bundle();

		// Set up a Patient that should get an update patch entry
		Patient patient = new Patient();
		patient.addIdentifier().setValue("patientId");
		patient.addName().setFamily("Flintstone").addGiven("Fred").setUse(HumanName.NameUse.USUAL);
		patient.addName().setFamily("Picopedro").addGiven("Pablo").setUse(HumanName.NameUse.USUAL).setPeriod(new Period().setEndElement(new DateTimeType("2023-04-20")));
		inputBundle.addEntry().setResource(patient);

		CCDTransformerImpl ccdTransformer = new CCDTransformerImpl();
		Bundle outputBundle = ccdTransformer.createTransactionBundle(inputBundle, null, false);

		Assert.assertEquals(2, outputBundle.getEntry().size());

		BundleEntryComponent patientUpdateEntry = outputBundle.getEntry().get(1);
		Parameters conditionUpdateParameters = (Parameters) patientUpdateEntry.getResource();
		Assert.assertEquals(Bundle.HTTPVerb.PATCH, patientUpdateEntry.getRequest().getMethod());
		Assert.assertEquals("Patient?identifier=patientId", patientUpdateEntry.getRequest().getUrl());

		Assert.assertEquals(3, conditionUpdateParameters.getParameter().size());

		List<Parameters.ParametersParameterComponent> patientUpdateParts = conditionUpdateParameters.getParameter().get(0).getPart();
		Assert.assertEquals("add", patientUpdateParts.get(0).getValue().castToString(patientUpdateParts.get(0).getValue()).getValue());
		Assert.assertEquals("Patient", patientUpdateParts.get(1).getValue().castToString(patientUpdateParts.get(1).getValue()).getValue());
		Assert.assertEquals("name", patientUpdateParts.get(2).getValue().castToString(patientUpdateParts.get(2).getValue()).getValue());
		Assert.assertEquals("Flintstone", patientUpdateParts.get(3).getValue().castToHumanName(patientUpdateParts.get(3).getValue()).getFamily());
		Assert.assertEquals("Fred", patientUpdateParts.get(3).getValue().castToHumanName(patientUpdateParts.get(3).getValue()).getGiven().get(0).getValue());
		Assert.assertEquals(HumanName.NameUse.USUAL, patientUpdateParts.get(3).getValue().castToHumanName(patientUpdateParts.get(3).getValue()).getUse());

		patientUpdateParts = conditionUpdateParameters.getParameter().get(1).getPart();
		Assert.assertEquals("replace", patientUpdateParts.get(0).getValue().castToString(patientUpdateParts.get(0).getValue()).getValue());
		Assert.assertEquals("Patient.name.where(family ~ 'Picopedro' and given ~ ('Pablo') and use != 'old').use", patientUpdateParts.get(1).getValue().castToString(patientUpdateParts.get(1).getValue()).getValue());
		Assert.assertEquals(HumanName.NameUse.OLD.toCode(), patientUpdateParts.get(2).getValue().castToCode(patientUpdateParts.get(2).getValue()).getValue());
	}

	@Test
	public void testCreateTransactionBundle_addPatchEntries_PatientRace() throws Exception {
		Bundle inputBundle = new Bundle();

		// Set up a Patient that should get an update patch entry
		Patient patient = new Patient();
		patient.addIdentifier().setValue("patientId");
		patient.addExtension().setUrl("http://hl7.org/fhir/us/core/StructureDefinition/us-core-race").addExtension()
						.setUrl("ombCategory").setValue(new Coding().setSystem("urn:oid:2.16.840.1.113883.6.238").setCode("2106-3"));
		inputBundle.addEntry().setResource(patient);

		CCDTransformerImpl ccdTransformer = new CCDTransformerImpl();
		Bundle outputBundle = ccdTransformer.createTransactionBundle(inputBundle, null, false);

		Assert.assertEquals(2, outputBundle.getEntry().size());

		BundleEntryComponent patientUpdateEntry = outputBundle.getEntry().get(1);
		Parameters conditionUpdateParameters = (Parameters) patientUpdateEntry.getResource();
		Assert.assertEquals(Bundle.HTTPVerb.PATCH, patientUpdateEntry.getRequest().getMethod());
		Assert.assertEquals("Patient?identifier=patientId", patientUpdateEntry.getRequest().getUrl());

		Assert.assertEquals(2, conditionUpdateParameters.getParameter().size());

		List<Parameters.ParametersParameterComponent> patientUpdateParts = conditionUpdateParameters.getParameter().get(0).getPart();
		Assert.assertEquals("delete", patientUpdateParts.get(0).getValue().castToString(patientUpdateParts.get(0).getValue()).getValue());
		Assert.assertEquals("Patient.extension.where(url = 'http://hl7.org/fhir/us/core/StructureDefinition/us-core-race')", patientUpdateParts.get(1).getValue().castToString(patientUpdateParts.get(1).getValue()).getValue());

		patientUpdateParts = conditionUpdateParameters.getParameter().get(1).getPart();
		Assert.assertEquals("add", patientUpdateParts.get(0).getValue().castToString(patientUpdateParts.get(0).getValue()).getValue());
		Assert.assertEquals("Patient", patientUpdateParts.get(1).getValue().castToString(patientUpdateParts.get(1).getValue()).getValue());
		Assert.assertEquals("extension", patientUpdateParts.get(2).getValue().castToString(patientUpdateParts.get(2).getValue()).getValue());
		Assert.assertEquals("2106-3", ((Coding) patientUpdateParts.get(3).getValue().castToExtension(patientUpdateParts.get(3).getValue()).getExtensionFirstRep().getValue()).getCode());
	}

	private void verifyLoincCoding(Coding coding, String value, String display) {
		Assert.assertEquals("http://loinc.org", coding.getSystem());
		Assert.assertEquals(value, coding.getCode());
		Assert.assertEquals(display, coding.getDisplay());
	}

	private void verifyQuantity(Quantity quanity, String code, String value, String unit) {
		Assert.assertEquals("http://unitsofmeasure.org", quanity.getSystem());
		Assert.assertEquals(code, quanity.getCode());
		Assert.assertEquals(value, quanity.getValue().toString());
		Assert.assertEquals(unit, quanity.getUnit());
	}

	private DocumentReference getDocumentReferenceByCode(Bundle theBundle, String code) {

		List<BundleEntryComponent> elist = theBundle.getEntry();
		for (BundleEntryComponent e : elist) {
			if (e.getResource() instanceof DocumentReference) {
				DocumentReference docRef = (DocumentReference) e.getResource();
				List<Coding> codingList = docRef.getType().getCoding();
				for (Coding coding : codingList) {
					if (code.equals(coding.getCode()))
						return docRef;
				}
			}
		}
		return null;
	}

	private Encounter getEncounter(Bundle theBundle, String theEncounterIdentifier) {

		List<BundleEntryComponent> elist = theBundle.getEntry();
		for (BundleEntryComponent e : elist) {
			if (e.getResource() instanceof Encounter) {
				Encounter encounter = (Encounter)e.getResource();
				if (theEncounterIdentifier.equals(encounter.getIdentifierFirstRep().getValue()))
					return encounter;
			}
		}
		return null;
	}

	private DocumentReference getDocumentReference(Bundle theBundle, String theDocumentReferenceIdentifier) {
        
        List<BundleEntryComponent> elist = theBundle.getEntry();
        for (BundleEntryComponent e : elist) {
            if (e.getResource() instanceof DocumentReference) {
                DocumentReference documentReference = (DocumentReference)e.getResource();
                if (theDocumentReferenceIdentifier.equals(documentReference.getIdentifierFirstRep().getValue()))
                    return documentReference;
            }           
        }
        return null;
    }

	private Goal getGoal(Bundle theBundle, String theGoalIdentifier) {

		List<BundleEntryComponent> elist = theBundle.getEntry();
		for (BundleEntryComponent e : elist) {
			if (e.getResource() instanceof Goal) {
				Goal goal = (Goal)e.getResource();
				if (theGoalIdentifier.equals(goal.getIdentifierFirstRep().getValue()))
					return goal;
			}
		}
		return null;
	}

	private ServiceRequest getServiceRequest(Bundle theBundle, String theSvcReqIdentifier) {

		List<BundleEntryComponent> elist = theBundle.getEntry();
		for (BundleEntryComponent e : elist) {
			if (e.getResource() instanceof ServiceRequest) {
				ServiceRequest svcReq = (ServiceRequest)e.getResource();
				if (theSvcReqIdentifier.equals(svcReq.getIdentifierFirstRep().getValue()))
					return svcReq;
			}
		}
		return null;
	}

	private CarePlan getCarePlan(Bundle theBundle) {

		List<BundleEntryComponent> elist = theBundle.getEntry();
		for (BundleEntryComponent e : elist) {
			if (e.getResource() instanceof CarePlan) {
				return (CarePlan)e.getResource();
			}
		}
		return null;
	}

	private Reference getReference(Bundle theBundle, String sectionTitle, String resourceType) {

		Composition composition = (Composition) theBundle.getEntry().get(0).getResource();
		List<Reference> compositionEntries = getSectionEntries(composition, sectionTitle);

		if (compositionEntries != null) {
			for (Reference ref : compositionEntries) {
				if (ref.getReference().contains(resourceType))
					return ref;
			}
		}
		return null;
	}

	public Resource findResource(Bundle theBundle, Reference reference) {

		List<BundleEntryComponent> entries = theBundle.getEntry();
		for (BundleEntryComponent entry : entries) {
			if (entry.getResource().getId().equals(reference.getReference()))
				return entry.getResource();
		}
		return null;
	}

	private List<Resource> getResource(Bundle theBundle, String sectionTitle, String resourceType, String displayName) {

		Composition composition = (Composition) theBundle.getEntry().get(0).getResource();
		List<Reference> compositionEntries = getSectionEntries(composition, sectionTitle);

		List<Resource> resourceList = new ArrayList<>();
		if (compositionEntries != null) {
			for (Reference ref : compositionEntries) {
				if (ref.getReference().contains(resourceType)  && displayName.equals(ref.getDisplay())) {
					resourceList.add(findResource(theBundle, ref));
				}
			}
		}
		return resourceList;
	}

	private void verifyExtension(Extension theExt, String theCode, String theDisplay) {
		Assert.assertEquals(DATA_ABSENT_REASON_EXTENSION_URI, theExt.getUrl());
		Coding extCode = (Coding)theExt.getValue();
		Assert.assertEquals(DATA_ABSENT_REASON_TERMINOLOGY_URI, extCode.getSystem());
		Assert.assertEquals(theCode, extCode.getCode());
		Assert.assertEquals(theDisplay, extCode.getDisplay());
	}
}
