package tr.com.srdc.cda2fhir.transform;

import java.util.Map;

/*
 * #%L
 * CDA to FHIR Transformer Library
 * %%
 * Copyright (C) 2016 SRDC Yazilim Arastirma ve Gelistirme ve Danismanlik Tic. A.S.
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.hl7.fhir.r4.model.Bundle.BundleType;
import org.hl7.fhir.r4.model.Identifier;
import org.openhealthtools.mdht.uml.cda.consol.ContinuityOfCareDocument;
import tr.com.srdc.cda2fhir.transform.util.ITransformationResult;

public interface ICDATransformer {
	/**
	 * Transforms a Clinical Document Architecture (CDA) instance to a Bundle of
	 * corresponding FHIR resources
	 *
	 * @param cda             A ContinuityOfCareDocument (CDA) instance to be
	 *                        transformed
	 * @param documentBody    The base64 decoded body from the original document
	 * @param assemblerDevice An identifier with the name of the device doing the
	 *
	 * @return A {@link ITransformationResult} containing a FHIR Bundle that contains a Composition corresponding to the CCD
	 *         document and all other resources that are referenced within the Composition and any error or warning
	 *         messages detected during processing.
	 */
	ITransformationResult transformDocument(ContinuityOfCareDocument cda, String documentBody, Identifier assemblerDevice);

	/**
	 * Transforms a Clinical Document Architecture (CDA) instance to a Bundle of
	 * corresponding FHIR resources
	 *
	 * @param cda                A ContinuityOfCareDocument (CDA) instance to be
	 *                           transformed
	 * @param bundleType         The type of bundle being transformed, currently
	 *                           only supports transactional bundles
	 * @param resourceProfileMap Map of resources used in DAF assignment, no longer
	 *                           actively supported but left in API.
	 * @param documentBody       The String body from the original document.
	 * @param assemblerDevice    An identifier object for the Device responsible for
	 *                           the transformation.
	 *
	 * @return A {@link ITransformationResult} result containing a FHIR Bundle that contains a Composition corresponding to the CCD
	 *         document and all other resources that are referenced within the Composition and any error or warning
	 *         messages detected during processing.
	 */
	ITransformationResult transformDocument(ContinuityOfCareDocument cda, BundleType bundleType,
											Map<String, String> resourceProfileMap, String documentBody, Identifier assemblerDevice) throws Exception;
}
