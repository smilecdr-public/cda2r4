package tr.com.srdc.cda2fhir.transform.section.impl;

import org.eclipse.emf.common.util.EList;
import org.hl7.fhir.r4.model.Goal;
import org.openhealthtools.mdht.uml.cda.Section;
import org.openhealthtools.mdht.uml.hl7.vocab.ActClassObservation;
import org.openhealthtools.mdht.uml.hl7.vocab.x_ActMoodDocumentObservation;

import tr.com.srdc.cda2fhir.transform.IResourceTransformer;
import tr.com.srdc.cda2fhir.transform.entry.IEntryResult;
import tr.com.srdc.cda2fhir.transform.section.ICDASection;
import tr.com.srdc.cda2fhir.transform.util.IBundleInfo;
import tr.com.srdc.cda2fhir.transform.util.ITransformationContext;
import tr.com.srdc.cda2fhir.transform.util.impl.LocalBundleInfo;

public class CDAGoalSection implements ICDASection {
	
	public static final String GOAL_SECTION_TEMPLATE_ID = "2.16.840.1.113883.10.20.22.2.60";
	public static final String GOAL_ENTRY_OBSERVATION_TEMPLATE_ID = "2.16.840.1.113883.10.20.22.4.121";
	public static final String GOAL_OBSERVATION_PRIORITY_TEMPLATE_ID = "2.16.840.1.113883.10.20.22.4.143";
   	
    private Section mySection;

    public CDAGoalSection(Section theSection) {
        mySection = theSection;
    }

    @Override
    public SectionResultSingular<Goal> transform(IBundleInfo bundleInfo, ITransformationContext theTransformationContext) {
         
    	IResourceTransformer rt = bundleInfo.getResourceTransformer();
        SectionResultSingular<Goal> result = SectionResultSingular.getInstance(Goal.class);
        LocalBundleInfo localBundleInfo = new LocalBundleInfo(bundleInfo);
        
        EList<org.openhealthtools.mdht.uml.cda.Observation> cdaObsList = mySection.getObservations();
        
        for (org.openhealthtools.mdht.uml.cda.Observation cdaObs : cdaObsList) {
            if (cdaObs.getTemplateIds().stream().anyMatch(t -> GOAL_ENTRY_OBSERVATION_TEMPLATE_ID.equals(t.getRoot()))) {
            	// Only care about classCode : OBS
            	ActClassObservation classCode = cdaObs.getClassCode();
            	if (classCode == null || !"OBS".equalsIgnoreCase(classCode.getName()))
            		continue;
            	
            	// Only care about moodeCode : GOL
            	x_ActMoodDocumentObservation moodCode = cdaObs.getMoodCode();
            	if (moodCode == null || !"GOL".equalsIgnoreCase(moodCode.getName()))
            		continue;
            	
            	IEntryResult er = rt.tGoalObservation2Goal(cdaObs, localBundleInfo, theTransformationContext);
                result.updateFrom(er);
                localBundleInfo.updateFrom(er);
            }
        }

        return result;
    }
}
