package tr.com.srdc.cda2fhir.transform.section.impl;

import org.eclipse.emf.common.util.EList;
import org.hl7.fhir.r4.model.Condition;
import org.hl7.fhir.r4.model.Observation;
import org.openhealthtools.mdht.uml.cda.Act;
import org.openhealthtools.mdht.uml.cda.Section;

import tr.com.srdc.cda2fhir.transform.IResourceTransformer;
import tr.com.srdc.cda2fhir.transform.entry.IEntryResult;
import tr.com.srdc.cda2fhir.transform.section.ICDASection;
import tr.com.srdc.cda2fhir.transform.util.IBundleInfo;
import tr.com.srdc.cda2fhir.transform.util.ITransformationContext;
import tr.com.srdc.cda2fhir.transform.util.impl.LocalBundleInfo;

public class CDAHealthConcernSection implements ICDASection {

	public static final String HELATH_CONCERN_SECTION_TEMPLATE_ID = "2.16.840.1.113883.10.20.22.2.58";
	public static final String HELATH_CONCERN_ENTRY_OBSERVATION_TEMPLATE_ID = "2.16.840.1.113883.10.20.22.4.5";
	public static final String HELATH_CONCERN_ENTRY_ACT_TEMPLATE_ID = "2.16.840.1.113883.10.20.22.4.132";

	private Section mySection;

	public CDAHealthConcernSection(Section theSection) {
		mySection = theSection;
	}

	@Override
	public SectionResultDynamic transform(IBundleInfo bundleInfo,ITransformationContext theTransformationContext) {

		IResourceTransformer rt = bundleInfo.getResourceTransformer();
		SectionResultDynamic result = new SectionResultDynamic();
		LocalBundleInfo localBundleInfo = new LocalBundleInfo(bundleInfo);

		//-- Observation->Observation
		EList<org.openhealthtools.mdht.uml.cda.Observation> cdaObsList = mySection.getObservations();
		for (org.openhealthtools.mdht.uml.cda.Observation cdaObs : cdaObsList) {
			if (cdaObs.getTemplateIds().stream().anyMatch(t -> HELATH_CONCERN_ENTRY_OBSERVATION_TEMPLATE_ID.equals(t.getRoot()))) {
				IEntryResult er = rt.tHealthConcernObservation2Observation(cdaObs, localBundleInfo, theTransformationContext);
				result.updateFrom(er, Observation.class);
				localBundleInfo.updateFrom(er);
			}
		}
		
		//-- Act->Condition
	    EList<Act> acts = mySection.getActs();  
        for (Act act : acts) {
            if (act.getTemplateIds().stream().anyMatch(t -> HELATH_CONCERN_ENTRY_ACT_TEMPLATE_ID.equals(t.getRoot()))) {
                IEntryResult er = rt.tHealthConcernAct2Condition(act, localBundleInfo, theTransformationContext);
                result.updateFrom(er, Condition.class);
                localBundleInfo.updateFrom(er);
            }
        }
        
		return result;
	}
}
