package tr.com.srdc.cda2fhir.transform;

/*
 * #%L
 * CDA to FHIR Transformer Library
 * %%
 * Copyright (C) 2016 SRDC Yazilim Arastirma ve Gelistirme ve Danismanlik Tic. A.S.
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import java.io.Serializable;
import java.util.stream.Collectors;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Date;
import java.util.Collections;
import java.util.TimeZone;

import ca.uhn.fhir.util.UrlUtil;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.impl.EStructuralFeatureImpl;
import org.eclipse.emf.ecore.util.BasicFeatureMap;
import org.eclipse.emf.ecore.util.FeatureMap;
import org.hl7.fhir.r4.model.Address;
import org.hl7.fhir.r4.model.Attachment;
import org.hl7.fhir.r4.model.Base64BinaryType;
import org.hl7.fhir.r4.model.BaseDateTimeType;
import org.hl7.fhir.r4.model.BooleanType;
import org.hl7.fhir.r4.model.CodeType;
import org.hl7.fhir.r4.model.CodeableConcept;
import org.hl7.fhir.r4.model.Coding;
import org.hl7.fhir.r4.model.ContactPoint;
import org.hl7.fhir.r4.model.ContactPoint.ContactPointSystem;
import org.hl7.fhir.r4.model.DateTimeType;
import org.hl7.fhir.r4.model.DateType;
import org.hl7.fhir.r4.model.DecimalType;
import org.hl7.fhir.r4.model.Extension;
import org.hl7.fhir.r4.model.HumanName;
import org.hl7.fhir.r4.model.Identifier;
import org.hl7.fhir.r4.model.Identifier.IdentifierUse;
import org.hl7.fhir.r4.model.InstantType;
import org.hl7.fhir.r4.model.IntegerType;
import org.hl7.fhir.r4.model.Narrative;
import org.hl7.fhir.r4.model.Narrative.NarrativeStatus;
import org.hl7.fhir.r4.model.Period;
import org.hl7.fhir.r4.model.Quantity;
import org.hl7.fhir.r4.model.Range;
import org.hl7.fhir.r4.model.Ratio;
import org.hl7.fhir.r4.model.Reference;
import org.hl7.fhir.r4.model.SimpleQuantity;
import org.hl7.fhir.r4.model.StringType;
import org.hl7.fhir.r4.model.Timing;
import org.hl7.fhir.r4.model.Timing.TimingRepeatComponent;
import org.hl7.fhir.r4.model.UriType;
import org.openhealthtools.mdht.uml.cda.StrucDocText;
import org.openhealthtools.mdht.uml.hl7.datatypes.AD;
import org.openhealthtools.mdht.uml.hl7.datatypes.ADXP;
import org.openhealthtools.mdht.uml.hl7.datatypes.ANY;
import org.openhealthtools.mdht.uml.hl7.datatypes.BIN;
import org.openhealthtools.mdht.uml.hl7.datatypes.BL;
import org.openhealthtools.mdht.uml.hl7.datatypes.CD;
import org.openhealthtools.mdht.uml.hl7.datatypes.CV;
import org.openhealthtools.mdht.uml.hl7.datatypes.DatatypesFactory;
import org.openhealthtools.mdht.uml.hl7.datatypes.ED;
import org.openhealthtools.mdht.uml.hl7.datatypes.EN;
import org.openhealthtools.mdht.uml.hl7.datatypes.ENXP;
import org.openhealthtools.mdht.uml.hl7.datatypes.II;
import org.openhealthtools.mdht.uml.hl7.datatypes.INT;
import org.openhealthtools.mdht.uml.hl7.datatypes.IVL_PQ;
import org.openhealthtools.mdht.uml.hl7.datatypes.IVL_TS;
import org.openhealthtools.mdht.uml.hl7.datatypes.IVXB_TS;
import org.openhealthtools.mdht.uml.hl7.datatypes.PIVL_TS;
import org.openhealthtools.mdht.uml.hl7.datatypes.PN;
import org.openhealthtools.mdht.uml.hl7.datatypes.PQ;
import org.openhealthtools.mdht.uml.hl7.datatypes.PQR;
import org.openhealthtools.mdht.uml.hl7.datatypes.REAL;
import org.openhealthtools.mdht.uml.hl7.datatypes.RTO;
import org.openhealthtools.mdht.uml.hl7.datatypes.SC;
import org.openhealthtools.mdht.uml.hl7.datatypes.ST;
import org.openhealthtools.mdht.uml.hl7.datatypes.SXCM_TS;
import org.openhealthtools.mdht.uml.hl7.datatypes.TEL;
import org.openhealthtools.mdht.uml.hl7.datatypes.TS;
import org.openhealthtools.mdht.uml.hl7.datatypes.URL;
import org.openhealthtools.mdht.uml.hl7.vocab.EntityNameUse;
import org.openhealthtools.mdht.uml.hl7.vocab.NullFlavor;
import org.openhealthtools.mdht.uml.hl7.vocab.PostalAddressUse;
import org.openhealthtools.mdht.uml.hl7.vocab.TelecommunicationAddressUse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ca.uhn.fhir.model.api.TemporalPrecisionEnum;
import ca.uhn.fhir.parser.DataFormatException;
import tr.com.srdc.cda2fhir.conf.Config;
import tr.com.srdc.cda2fhir.transform.section.impl.CDASocialHistorySection;
import tr.com.srdc.cda2fhir.transform.util.IBundleInfo;
import tr.com.srdc.cda2fhir.transform.util.ITransformationContext;
import tr.com.srdc.cda2fhir.util.StringUtil;

import static tr.com.srdc.cda2fhir.transform.ResourceTransformerImpl.US_NPI_OID;
import static tr.com.srdc.cda2fhir.transform.ResourceTransformerImpl.US_NPI_URL;
import static tr.com.srdc.cda2fhir.transform.ValueSetsTransformerImpl.UNITS_OF_MEASURE_URL;

public class DataTypesTransformerImpl implements IDataTypesTransformer, Serializable {

	/**
	 *
	 */
	private static final long serialVersionUID = 1L;
	public static final String DATA_ABSENT_REASON_EXTENSION_URI = "http://hl7.org/fhir/StructureDefinition/data-absent-reason";
	public static final String DATA_ABSENT_REASON_TERMINOLOGY_URI = "http://terminology.hl7.org/CodeSystem/data-absent-reason";
	public static final String PLAINTEXT = "text/plain";

	private IValueSetsTransformer vst = new ValueSetsTransformerImpl();

	private final Logger logger = LoggerFactory.getLogger(DataTypesTransformerImpl.class);

	@Override
	public Address AD2Address(AD ad) {
		if (ad == null || ad.isSetNullFlavor()) {
			return null;
		}

		Address address = new Address();

		// use -> use
		if (ad.getUses() != null && !ad.getUses().isEmpty()) {
			// We get the address.type and address.use from the list ad.uses
			for (PostalAddressUse postalAddressUse : ad.getUses()) {
				// If we catch a valid value for type or use, we assign it
				if (postalAddressUse == PostalAddressUse.PHYS || postalAddressUse == PostalAddressUse.PST) {
					address.setType(vst.tPostalAddressUse2AddressType(postalAddressUse));
				} else if (postalAddressUse == PostalAddressUse.H || postalAddressUse == PostalAddressUse.HP
						|| postalAddressUse == PostalAddressUse.WP || postalAddressUse == PostalAddressUse.TMP
						|| postalAddressUse == PostalAddressUse.BAD) {
					address.setUse(vst.tPostalAdressUse2AddressUse(postalAddressUse));
				}
			}
		}

		// text -> text
		if (ad.getText() != null && !ad.getText().isEmpty()) {
			address.setText(ad.getText());
		}

		// streetAddressLine -> line
		if (ad.getStreetAddressLines() != null && !ad.getStreetAddressLines().isEmpty()) {
			for (ADXP adxp : ad.getStreetAddressLines()) {
				if (adxp != null && !adxp.isSetNullFlavor() && adxp.getText() != null) {
					address.addLine(adxp.getText().trim());
				}
			}
		}

		// deliveryAddressLine -> line
		if (ad.getDeliveryAddressLines() != null && !ad.getDeliveryAddressLines().isEmpty()) {
			for (ADXP adxp : ad.getDeliveryAddressLines()) {
				if (adxp != null && !adxp.isSetNullFlavor()) {
					address.addLine(adxp.getText());
				}
			}
		}

		// city -> city
		if (ad.getCities() != null && !ad.getCities().isEmpty()) {
			for (ADXP adxp : ad.getCities()) {
				// Asserting that at most one city information exists
				if (adxp != null && !adxp.isSetNullFlavor()) {
					address.setCity(adxp.getText());
				}
			}
		}

		// county -> district
		if (ad.getCounties() != null && !ad.getCounties().isEmpty()) {
			for (ADXP adxp : ad.getCounties()) {
				// Asserting that at most one county information exists
				if (adxp != null && !adxp.isSetNullFlavor()) {
					address.setDistrict(adxp.getText());
				}
			}

		}

		// country -> country
		if (ad.getCountries() != null && !ad.getCountries().isEmpty()) {
			for (ADXP adxp : ad.getCountries()) {
				if (adxp != null && !adxp.isSetNullFlavor()) {
					address.setCountry(adxp.getText());
				}
			}
		}

		// state -> state
		if (ad.getStates() != null && !ad.getStates().isEmpty()) {
			for (ADXP adxp : ad.getStates()) {
				if (adxp != null && !adxp.isSetNullFlavor()) {
					address.setState(adxp.getText());
				}
			}
		}

		// postalCode -> postalCode
		if (ad.getPostalCodes() != null && !ad.getPostalCodes().isEmpty()) {
			for (ADXP adxp : ad.getPostalCodes()) {
				if (adxp != null && !adxp.isSetNullFlavor() && adxp.getText() != null) {
					address.setPostalCode(adxp.getText().trim());
				}
			}
		}

		// useablePeriods -> period
		Period period = getMaximumUseablePeriod(ad.getUseablePeriods());
		if (period != null) {
			address.setPeriod(period);
		}
		return address;
	}

	private Period getMaximumUseablePeriod(EList<SXCM_TS> theUseablePeriods) {
		if (theUseablePeriods == null || theUseablePeriods.isEmpty()) {
			return null;
		}

		List<Period> periods = new ArrayList<>();
		List<Date> singleDates = new ArrayList<>();
		for (SXCM_TS sxcm_ts : theUseablePeriods) {
			if (sxcm_ts instanceof IVL_TS) {
				periods.add(tIVL_TS2Period((IVL_TS) sxcm_ts));
			} else if (sxcm_ts instanceof PIVL_TS) {
				periods.add(tPIVL_TS2Period((PIVL_TS) sxcm_ts));
			} else {
				// according to the spec, raw use of SXCM_TS types is not allowed anyway, so we warn
				logger.warn("Improper type used in field address.useablePeriod; expected one of [IVL_TS, EIVL_TS, PIVL_TS], but found: SXCM_TS");
				convertAndAddTimestamp(singleDates, sxcm_ts);
			}
		}

		Period period = new Period();

		if (periods.isEmpty()) {
			timestamp2PeriodBounds(singleDates, period);
		} else {
			// earliest date -> Period.start
			Date earliestDate = getEarliestDate(periods, singleDates);
			period.setStart(earliestDate);

			// latest date -> Period.end
			Date latestDate = getLatestDate(periods, singleDates);

			// If we already used a timestamp for the earliest date, and it's the only one, then we make this an open
			// interval. This also covers when the latestDate ends up ealier than the earliestDate
			if (earliestDate != null && singleDates.contains(earliestDate) && singleDates.size() == 1) {
				if (latestDate != null && (earliestDate.after(latestDate) || earliestDate.equals(latestDate))) {
					latestDate = null;
				}
			}
			period.setEnd(latestDate);
		}

		if (period.getStart() == null) {
			period.getStartElement().addExtension(tNullFlavor2DataAbsentReasonExtension(findNullFlavor(theUseablePeriods)));
		}

		return period;
	}

	private NullFlavor findNullFlavor(EList<SXCM_TS> theUseablePeriods) {
		List<NullFlavor> collect = theUseablePeriods.stream()
				.filter(SXCM_TS::isSetNullFlavor)
				.map(SXCM_TS::getNullFlavor)
				.collect(Collectors.toList());

		if (!collect.isEmpty()) {
			return collect.get(0);
		} else {
			return NullFlavor.UNK;
		}
	}

	private void timestamp2PeriodBounds(List<Date> singleDates, Period period) {
		singleDates.removeAll(Collections.singletonList(null));
		Collections.sort(singleDates);
		if (!singleDates.isEmpty()) {
			if (1 == singleDates.size()) {
				period.setStart(singleDates.get(0));
			} else {
				period.setStart(singleDates.get(0));
				period.setEnd(singleDates.get(singleDates.size() - 1));
			}
		}
	}

	private void convertAndAddTimestamp(List<Date> singleDates, SXCM_TS sxcm_ts) {
		if (sxcm_ts == null) {
			singleDates.add(null);
		} else {
			DateTimeType dateTimeType = tString2DateTime(sxcm_ts.getValue());
			if (dateTimeType == null) {
				singleDates.add(null);
			} else {
				singleDates.add(dateTimeType.getValue());
			}
		}
	}

	private Date getEarliestDate(List<Period> periods, List<Date> singleDates) {
		List<Date> dates = periods.stream()
				.map(Period::getStart)
				.collect(Collectors.toList());
		dates.addAll(singleDates);
		dates.removeAll(Collections.singletonList(null));
		Collections.sort(dates);

		if (dates.isEmpty()) {
			return null;
		} else {
			return dates.get(0);
		}
	}

	private Date getLatestDate(List<Period> periods, List<Date> singleDates) {
		List<Date> dates = periods.stream()
				.map(Period::getEnd)
				.collect(Collectors.toList());

		if (dates.contains(null)) {
			return null;
		}
		dates.addAll(singleDates);

		dates.removeAll(Collections.singletonList(null));
		Collections.sort(dates);

		if (dates.isEmpty()) {
			return null;
		} else {
			return dates.get(dates.size()-1);
		}
	}

	@Override
	public Period tPIVL_TS2Period(PIVL_TS thePivl_ts) {
		Period period = new Period();

		IVXB_TS low = thePivl_ts.getPhase().getLow();
		if (low != null && !low.isSetNullFlavor()){
			period.setStart(tTS2Date(low).getValue());
		}

		return period;
	}

	@Override
	public Base64BinaryType tBIN2Base64Binary(BIN bin) {
		if (bin == null || bin.isSetNullFlavor()) {
			return null;
		}
		if (bin.getRepresentation().getLiteral() != null) {
			// TODO: It doesn't seem convenient. There should be a way to get the value of
			// BIN.
			Base64BinaryType base64BinaryDt = new Base64BinaryType();
			base64BinaryDt.setValue(bin.getRepresentation().getLiteral().getBytes());
			return base64BinaryDt;
		} else {
			return null;
		}

	}

	@Override
	public BooleanType tBL2Boolean(BL bl) {
		return (bl == null || bl.isSetNullFlavor()) ? null : new BooleanType(bl.getValue());
	}

	@Override
	public String tED2Annotation(ED ed, Map<String, String> idedAnnotations) {

		if (ed != null) {

			if (idedAnnotations != null) {
				// Try to pull the reference.
				TEL tel = ed.getReference();
				if (tel != null) {
					String value = tel.getValue();
					if (value != null) {
						if (value.charAt(0) == '#') {
							String key = value.substring(1);
							return idedAnnotations.get(key);
						} else {
							// If the reference is populated as just plain text, use that instead
							return value;
						}
					}

				}
			}

			// If not fall back on text value.
			String originalText = ed.getText().trim();
			if (!originalText.equals("")) {
				return originalText;
			}

		}
		return null;
	}

	@Override
	public CodeableConcept tCD2CodeableConcept(CD cd) {
		return tCD2CodeableConcept(cd, null);
	}

	@Override
	public CodeableConcept tCD2CodeableConcept(CD cd, Map<String, String> idedAnnotations) {
		CodeableConcept myCodeableConcept = tCD2CodeableConceptExcludingTranslations(cd, idedAnnotations);

		if (myCodeableConcept == null) {
			return null;
		}

		// translation
		if (cd.getTranslations() != null && !cd.getTranslations().isEmpty()) {
			for (CD myCd : cd.getTranslations()) {
				Coding codingDt = tCD2Coding(myCd);
				if (codingDt != null && !codingDt.isEmpty()) {
					myCodeableConcept.addCoding(codingDt);
				}
			}
		}

		return myCodeableConcept;
	}

	@Override
	public CodeableConcept tCD2CodeableConceptExcludingTranslations(CD cd) {
		return tCD2CodeableConceptExcludingTranslations(cd, null);
	}

	@Override
	public CodeableConcept tCD2CodeableConceptExcludingTranslations(CD cd, Map<String, String> idedAnnotations) {
		if (cd == null) {
			return null;
		}

		CodeableConcept myCodeableConcept = null;
		if (!cd.isSetNullFlavor()) {
			// .
			Coding codingDt = tCD2Coding(cd);
			if (codingDt != null && !codingDt.isEmpty()) {
				myCodeableConcept = new CodeableConcept();
				myCodeableConcept.addCoding(codingDt);
			}
		}

		String annotation = tED2Annotation(cd.getOriginalText(), idedAnnotations);
		if (annotation != null) {
			if (myCodeableConcept == null) {
				myCodeableConcept = new CodeableConcept();
			}
			myCodeableConcept.setText(annotation);
		}

		return myCodeableConcept;
	}

    @Override
    public CodeableConcept tCDTranslation2CodeableConcept(EList<CD> translations) {

       if (translations == null || translations.isEmpty())
            return null;

        CodeableConcept codeableConcept = null;
        for (CD cd : translations) {
            Coding coding = tCD2Coding(cd);
            if (coding != null && !coding.isEmpty()) {
                if (codeableConcept == null)
                    codeableConcept = new CodeableConcept();
                codeableConcept.addCoding(coding);
            }
        }

        return codeableConcept;
    }
    
	@Override
	public Identifier tCD2Identifier(CD cd) {
		if (cd == null) {
			return null;
		}

		Identifier myIdentifier = new Identifier();

		boolean isEmpty = true;
		if (!cd.isSetNullFlavor()) {

			// codeSystem -> system
			if (cd.getCodeSystem() != null && !cd.getCodeSystem().isEmpty()) {
				myIdentifier.setSystem(vst.tOid2Url(cd.getCodeSystem()));
				isEmpty = false;
			}

			// code -> code
			if (cd.getCode() != null && !cd.getCode().isEmpty()) {
				myIdentifier.setValue(cd.getCode());
				isEmpty = false;
			}
		}

		return isEmpty ? null : myIdentifier;
	}

	@Override
	public CodeableConcept tSC2CodeableConcept(SC sc) {
		if (sc == null) {
			return null;
		}

		CodeableConcept myCodeableConcept = null;

		if (!sc.isSetNullFlavor()) {
			// .
			Coding codingDt = new Coding();
			boolean isEmpty = true;

			// codeSystem -> system
			if (sc.getCodeSystem() != null && !sc.getCodeSystem().isEmpty()) {
				codingDt.setSystem(vst.tOid2Url(sc.getCodeSystem()));
				isEmpty = false;
			}

			// code -> code
			if (sc.getCode() != null && !sc.getCode().isEmpty()) {
				codingDt.setCode(sc.getCode());
				isEmpty = false;
			}

			// codeSystemVersion -> version
			if (sc.getCodeSystemVersion() != null && !sc.getCodeSystemVersion().isEmpty()) {
				codingDt.setVersion(sc.getCodeSystemVersion());
				isEmpty = false;
			}

			// displayName -> display
			if (sc.getDisplayName() != null && !sc.getDisplayName().isEmpty()) {
				codingDt.setDisplay(sc.getDisplayName());
				isEmpty = false;
			}

			if (!isEmpty) {
				myCodeableConcept = new CodeableConcept();
				myCodeableConcept.addCoding(codingDt);
			}
		}

		return myCodeableConcept;
	}

	@Override
	public Coding tCV2Coding(CV cv) {
		if (cv == null || cv.isSetNullFlavor()) {
			return null;
		}

		Coding codingDt = new Coding();

		// codeSystem -> system
		if (cv.getCodeSystem() != null && !cv.getCodeSystem().isEmpty()) {
			codingDt.setSystem(cv.getCodeSystem());
		}

		// codeSystemVersion -> version
		if (cv.getCodeSystemVersion() != null && !cv.getCodeSystemVersion().isEmpty()) {
			codingDt.setVersion(cv.getCodeSystemVersion());
		}

		// code -> code
		if (cv.getCode() != null && !cv.getCode().isEmpty()) {
			codingDt.setCode(cv.getCode());
		}

		// displayName -> display
		if (cv.getDisplayName() != null && !cv.getDisplayName().isEmpty()) {
			codingDt.setDisplay(cv.getDisplayName());
		}
		return codingDt;
	}

	@Override
	public Attachment tED2Attachment(ED ed) {
		if (ed == null || ed.isSetNullFlavor()) {
			return null;
		}

		Attachment attachmentDt = new Attachment();

		// mediaType -> contentType
		if (ed.isSetMediaType() && ed.getMediaType() != null && !ed.getMediaType().isEmpty()) {
			attachmentDt.setContentType(ed.getMediaType());
		} else {
			attachmentDt.setContentType(PLAINTEXT);
		}

		// language -> language
		if (ed.getLanguage() != null && !ed.getLanguage().isEmpty()) {
			attachmentDt.setLanguage(ed.getLanguage());
		}

		// text.bytes -> data OR reference.value -> url
		if (ed.getText() != null && !ed.getText().isEmpty()) {
			if (ed.getText().getBytes() != null) {
				attachmentDt.setData(ed.getText().getBytes());
			}
		} else if (ed.getReference() != null && !ed.getReference().isSetNullFlavor()) {
			String referenceValue = ed.getReference().getValue();
			if (referenceValue != null && !referenceValue.isEmpty()) {
				if (UrlUtil.isValid(referenceValue)) {
					attachmentDt.setUrl(referenceValue);
				}
			}
		}

		// integrityCheck -> hash
		if (ed.getIntegrityCheck() != null) {
			attachmentDt.setHash(ed.getIntegrityCheck());
		}

		return attachmentDt;
	}

	@Override
	public HumanName tEN2HumanName(EN en, ITransformationContext theTransformationContext) {
		if (en == null || en.isSetNullFlavor()) {
			return null;
		}

		HumanName myHumanName = new HumanName();

		// text -> text
		if (en.getText() != null && !en.getText().isEmpty()) {
			myHumanName.setText(en.getText());
		}

		// use -> use
		if (en.getUses() != null && !en.getUses().isEmpty()) {
			for (EntityNameUse entityNameUse : en.getUses()) {
				if (entityNameUse != null) {
					myHumanName.setUse(vst.tEntityNameUse2NameUse(entityNameUse));
				}
			}
		}

		// family -> family
		// TODO: FHIR DSTU2 supported multiple family names but STU3 only supports
		// one. Figure out how to handle this. For now, error out if there's multiple
		// family names from source
		if (en.getFamilies() != null && !en.getFamilies().isEmpty()) {
			boolean alreadySet = false;
			for (ENXP family : en.getFamilies()) {
				if (alreadySet) {
					theTransformationContext.addError("Patient Name", "family", "Multiple family names found.");
					break;
				}
				myHumanName.setFamily(family.getText());
				alreadySet = true;
			}
		}

		// given -> given
		if (en.getGivens() != null && !en.getGivens().isEmpty()) {
			for (ENXP given : en.getGivens()) {
				myHumanName.addGiven(given.getText());
			}
		}

		// prefix -> prefix
		if (en.getPrefixes() != null && !en.getPrefixes().isEmpty()) {
			for (ENXP prefix : en.getPrefixes()) {
				myHumanName.addPrefix(prefix.getText());
			}
		}

		// suffix -> suffix
		if (en.getSuffixes() != null && !en.getSuffixes().isEmpty()) {
			for (ENXP suffix : en.getSuffixes()) {
				myHumanName.addSuffix(suffix.getText());
			}
		}

		// validTime -> period
		if (en.getValidTime() != null && !en.getValidTime().isSetNullFlavor()) {
			myHumanName.setPeriod(tIVL_TS2Period(en.getValidTime()));
		}

		return myHumanName;

	}

	@Override
	public HumanName tPN2HumanName(PN pn, ITransformationContext theTransformationContext) {
		if (pn == null || pn.isSetNullFlavor()) {
			return null;
		}

		HumanName myHumanName = new HumanName();

		// text -> text
		if (pn.getText() != null && !pn.getText().isEmpty()) {
			myHumanName.setText(pn.getText());
		}

		// use -> use
		if (pn.getUses() != null) {
			for (EntityNameUse entityNameUse : pn.getUses()) {
				if (entityNameUse != null) {
					myHumanName.setUse(vst.tEntityNameUse2NameUse(entityNameUse));
				}
			}
		}

		// family -> family
		// TODO: FHIR DSTU2 supported multiple family names but STU3 only supports
		// one. Figure out how to handle this. For now, error out if there's multiple
		// family names from source
		if (pn.getFamilies() != null && !pn.getFamilies().isEmpty()) {
			boolean alreadySet = false;
			for (ENXP family : pn.getFamilies()) {
				if (alreadySet) {
					theTransformationContext.addError("Participant Name", "family", "Multiple family names found.");
					break;
				}
				myHumanName.setFamily(family.getText());
				alreadySet = true;
			}
		}

		// given -> given
		if (pn.getGivens() != null && !pn.getGivens().isEmpty()) {
			for (ENXP given : pn.getGivens()) {
				myHumanName.addGiven(given.getText());
			}
		}

		// prefix -> prefix
		if (pn.getPrefixes() != null && !pn.getPrefixes().isEmpty()) {
			for (ENXP prefix : pn.getPrefixes()) {
				myHumanName.addPrefix(prefix.getText());
			}
		}

		// suffix -> suffix
		if (pn.getSuffixes() != null && !pn.getSuffixes().isEmpty()) {
			for (ENXP suffix : pn.getSuffixes()) {
				myHumanName.addSuffix(suffix.getText());
			}
		}

		// validTime -> period
		if (pn.getValidTime() != null && !pn.getValidTime().isSetNullFlavor()) {
			myHumanName.setPeriod(tIVL_TS2Period(pn.getValidTime()));
		}

		return myHumanName;

	}
	@Override
	public Identifier tII2Identifier(II ii) {
		if (ii == null || ii.isSetNullFlavor()) {
			return null;
		}

		Identifier identifierDt = new Identifier();

		// default to official use
		IdentifierUse use = Config.DEFAULT_IDENTIFIER_USE;
		identifierDt.setUse(use);

		// if both root and extension are present, then
		// root -> system
		// extension -> value
		if (ii.getRoot() != null && !ii.getRoot().isEmpty() && ii.getExtension() != null
				&& !ii.getExtension().isEmpty()) {
			if (StringUtil.isOID(ii.getRoot())) {
				// root is oid
				if (US_NPI_OID.equals(ii.getRoot())) {
					identifierDt.setSystem(US_NPI_URL);
				} else {
					identifierDt.setSystem("urn:oid:" + ii.getRoot());
				}
			}
			else if (StringUtil.isUUID(ii.getRoot())) {
				// root is uuid
				identifierDt.setSystem("urn:uuid:" + ii.getRoot());
			} else {
				identifierDt.setSystem(ii.getRoot());
			}

			identifierDt.setValue(ii.getExtension());
		} else if (ii.getRoot() != null && !ii.getRoot().isEmpty()) {
			// else if only the root is present, then
			// root -> value
			// JV 21/09/2022: this is weird but consistent. Whether it changes will be whether clients need it to change
			if (US_NPI_OID.equals(ii.getRoot())) {
				identifierDt.setValue(US_NPI_URL);
			} else {
				identifierDt.setValue(ii.getRoot());
			}
		} else if (ii.getExtension() != null && !ii.getExtension().isEmpty()) {
			// this is not very likely but, if there is only the extension, then
			// extension -> value
			identifierDt.setValue(ii.getExtension());
		}

		if (ii.getAssigningAuthorityName() != null) {
			Reference ref = new Reference();
			ref.setDisplay(ii.getAssigningAuthorityName());
			identifierDt.setAssigner(ref);
		}

		return identifierDt;

	}

	@Override
	public IntegerType tINT2Integer(INT myInt) {
		return (myInt == null || myInt.isSetNullFlavor() || myInt.getValue() == null) ? null
				: new IntegerType(myInt.getValue().toString());
	}

	@Override
	public Range tIVL_PQ2Range(IVL_PQ ivlpq, boolean useUnitForCode) {
		if (ivlpq == null || ivlpq.isSetNullFlavor()) {
			return null;
		}

		Range rangeDt = new Range();

		// low -> low
		if (ivlpq.getLow() != null && !ivlpq.getLow().isSetNullFlavor()) {
		rangeDt.setLow(tPQ2SimpleQuantity(ivlpq.getLow(), useUnitForCode));

		}

		// high -> high
		if (ivlpq.getHigh() != null && !ivlpq.getHigh().isSetNullFlavor()) {
			rangeDt.setHigh(tPQ2SimpleQuantity(ivlpq.getHigh(), useUnitForCode));
		}

		// low is null, high is null and the value is carrying the low value
		// value -> low
		if (ivlpq.getLow() == null && ivlpq.getHigh() == null && ivlpq.getValue() != null) {
			rangeDt.setLow(tPQ2SimpleQuantity(ivlpq, useUnitForCode));
		}

		return rangeDt;
	}

	@Override
	public Period tIVL_TS2Period(IVL_TS ivlts) {
		return tIVL_TS2Period(ivlts, false);
	}

	@Override
	public Period tIVL_TS2Period(IVL_TS ivlts, boolean supportDataAbsentReason) {

		if (ivlts == null || ivlts.isSetNullFlavor()) {
			return null;
		}

		Period period = new Period();

		// low -> start
		IVXB_TS low = ivlts.getLow();
		if (low != null) {
			if (low.isSetNullFlavor()) {
				if (supportDataAbsentReason)
					period.getStartElement().addExtension(tNullFlavor2DataAbsentReasonExtension(low.getNullFlavor()));
			} else {
				String date = low.getValue();
				if (Config.addedZerosToDateTime()) {
					period.setStartElement(tString2DateTime(date + "00"));
				} else {
					period.setStartElement(tString2DateTime(date));
				}
			}
		}

		// high -> end
		IVXB_TS high = ivlts.getHigh();
		if (high != null) {
			if (high.isSetNullFlavor()) {
				if (supportDataAbsentReason)
					period.getEndElement().addExtension(tNullFlavor2DataAbsentReasonExtension(high.getNullFlavor()));
			} else {
				String date = high.getValue();
				if (Config.addedZerosToDateTime()) {
					period.setEndElement(tString2DateTime(date + "00"));
				} else {
					period.setEndElement(tString2DateTime(date));
				}
			}
		}

		// low is null, high is null and the value is carrying the low value
		// value -> low
		if (low == null && high == null && ivlts.getValue() != null
				&& !ivlts.getValue().equals("")) {
			if (Config.addedZerosToDateTime()) {
				period.setStartElement(tString2DateTime(ivlts.getValue() + "00"));
			} else {
				period.setStartElement(tString2DateTime(ivlts.getValue()));
			}
		}

		return period;
	}

	@Override
	public DateTimeType tIVL_TS2DateTime(IVL_TS ivlts) {
		if (ivlts == null || ivlts.isSetNullFlavor()) {
			return null;
		}

		Period period = tIVL_TS2Period(ivlts);

		if (period.hasStartElement()) {
			return period.getStartElement();
		} else {
			return period.getEndElement();
		}
	}

	@Override
	public Timing tPIVL_TS2Timing(PIVL_TS pivlts) {
		// http://wiki.hl7.org/images/c/ca/Medication_Frequencies_in_CDA.pdf
		// http://www.cdapro.com/know/24997
		if (pivlts == null || pivlts.isSetNullFlavor()) {
			return null;
		}

		Timing timing = new Timing();

		// period -> period
		if (pivlts.getPeriod() != null && !pivlts.getPeriod().isSetNullFlavor()) {
			TimingRepeatComponent repeat = new TimingRepeatComponent();
			timing.setRepeat(repeat);
			// period.value -> repeat.period
			if (pivlts.getPeriod().getValue() != null) {
				repeat.setPeriod(pivlts.getPeriod().getValue());
			}
			// period.unit -> repeat.periodUnits
			if (pivlts.getPeriod().getUnit() != null) {
				repeat.setPeriodUnit(vst.tPeriodUnit2UnitsOfTime(pivlts.getPeriod().getUnit()));
			}

			// phase -> repeat.bounds
			if (pivlts.getPhase() != null && !pivlts.getPhase().isSetNullFlavor()) {
				repeat.setBounds(tIVL_TS2Period(pivlts.getPhase()));
			}
		}
		return timing;
	}

	@Override
	public Quantity tPQ2Quantity(PQ pq, boolean useUnitForCode) {
		if (pq == null || pq.isSetNullFlavor()) {
			return null;
		}

		Quantity quantityDt = new Quantity();

		// value -> value
		if (pq.getValue() != null) {
			quantityDt.setValue(pq.getValue());
		}

		// unit -> unit
		if (pq.getUnit() != null && !pq.getUnit().isEmpty()) {
			quantityDt.setUnit(pq.getUnit());
		}

		if (useUnitForCode) {
			quantityDt.setCode(pq.getUnit())
					.setSystem(UNITS_OF_MEASURE_URL);
		} else {
			tPQTranslation2QuantitySystemCode(pq, quantityDt);
		}

		return quantityDt;
	}

	private void tPQTranslation2QuantitySystemCode(PQ pq, Quantity quantityDt) {
		// translation -> system & code
		if (pq.getTranslations() != null && !pq.getTranslations().isEmpty()) {
			for (PQR pqr : pq.getTranslations()) {
				if (pqr != null && !pqr.isSetNullFlavor()) {
					// codeSystem -> system
					if (pqr.getCodeSystem() != null && !pqr.getCodeSystem().isEmpty()) {
						quantityDt.setSystem(vst.tOid2Url(pqr.getCodeSystem()));
					}

					// code -> code
					if (pqr.getCode() != null && !pqr.getCode().isEmpty()) {
						quantityDt.setCode(pqr.getCode());
					}
				}
			}
		}
	}

	@Override
	public SimpleQuantity tPQ2SimpleQuantity(PQ pq, boolean useUnitForCode) {
		Quantity quantity = tPQ2Quantity(pq, useUnitForCode);
		SimpleQuantity simpleQuantity = quantity.castToSimpleQuantity(quantity);
		simpleQuantity.setSystem(vst.tOid2Url("2.16.840.1.113883.1.11.12839"));

		return simpleQuantity;
	}

	@Override
	public DecimalType tREAL2DecimalType(REAL real) {
		return (real == null || real.isSetNullFlavor() || real.getValue() == null) ? null
				: new DecimalType(real.getValue());
	}

	@Override
	public Quantity tREAL2Quantity(REAL real) {
		if (real == null || real.isSetNullFlavor() || real.getValue() == null) {
			return null;
		} else {
			Quantity quantity = new Quantity();
			quantity.setValue(real.getValue());
			return quantity;
		}
	}

	@Override
	public Ratio tRTO2Ratio(RTO rto) {
		if (rto == null || rto.isSetNullFlavor()) {
			return null;
		}

		Ratio myRatio = new Ratio();

		// numerator -> numerator
		if (rto.getNumerator() != null && !rto.getNumerator().isSetNullFlavor()) {
			Quantity quantity = new Quantity();
			REAL numerator = (REAL) rto.getNumerator();
			if (numerator.getValue() != null) {
				quantity.setValue(numerator.getValue().doubleValue());
				myRatio.setNumerator(quantity);
			}
		}

		// denominator -> denominator
		if (!rto.getDenominator().isSetNullFlavor()) {
			Quantity quantity = new Quantity();
			REAL denominator = (REAL) rto.getDenominator();
			if (denominator.getValue() != null) {
				quantity.setValue(denominator.getValue().doubleValue());
				myRatio.setDenominator(quantity);
			}
		}
		return myRatio;
	}

	@Override
	public StringType tST2String(ST st) {
		return (st == null || st.isSetNullFlavor() || st.getText() == null) ? null : new StringType(st.getText());
	}

	@Override
	public DateTimeType tString2DateTime(String date) {
		TS ts = DatatypesFactory.eINSTANCE.createTS();
		ts.setValue(date);
		return tTS2DateTime(ts);
	}

	@Override
	public Narrative tStrucDocText2Narrative(StrucDocText sdt) {
		if (sdt != null) {
			Narrative narrative = new Narrative();
			String narrativeDivString = tStrucDocText2String(sdt);

			try {
				narrative.setDivAsString(narrativeDivString);
			} catch (DataFormatException e) {
				return null;
			}
			narrative.setStatus(NarrativeStatus.ADDITIONAL);
			return narrative;
		}
		return null;
	}

	@Override
	public Narrative tED2Narrative(ED theText, IBundleInfo theBundleInfo) {

		if (theText == null || theText.isSetNullFlavor())
			return null;

		String textStr = null;

		// -- text.reference
		TEL reference = theText.getReference();
		if (reference != null && !reference.isSetNullFlavor()) {
			String refValue = theText.getReference().getValue();
			if (refValue != null && refValue.startsWith("#")) {
				String ref = refValue.substring(1);
				// Get the text based on the reference
				textStr = theBundleInfo.getIdedAnnotations().get(ref);
			}
		} else {
			if (theText.getText() != null) {
				textStr = theText.getText();
			}
		}

		if (textStr != null && !textStr.isBlank()) {
			Narrative narrative = new Narrative();
			narrative.setDivAsString(textStr);
			narrative.setStatus(NarrativeStatus.ADDITIONAL);
			return narrative;
		} else {
			return null;
		}
	}
	
	@Override
	public ContactPoint tTEL2ContactPoint(TEL tel) {
		if (tel == null || tel.isSetNullFlavor()) {
			return null;
		}

		ContactPoint contactPointDt = new ContactPoint();

		// value and system -> value
		if (tel.getValue() != null && !tel.getValue().isEmpty()) {
			String value = tel.getValue();
			String[] systemType = value.split(":");

			// for the values in form tel:+1(555)555-1000
			if (systemType.length > 1) {
				ContactPointSystem contactPointSystem = vst.tTelValue2ContactPointSystem(systemType[0]);
				// system
				if (contactPointSystem != null) {
					contactPointDt.setSystem(contactPointSystem);
				} else {
					contactPointDt.setSystem(Config.DEFAULT_CONTACT_POINT_SYSTEM);
				}
				// value
				contactPointDt.setValue(systemType[1]);
			}
			// for the values in form +1(555)555-5000
			else if (systemType.length == 1) {
				contactPointDt.setValue(systemType[0]);
				// configurable default system value
				contactPointDt.setSystem(Config.DEFAULT_CONTACT_POINT_SYSTEM);
			}
		}

		// useablePeriods -> period
		if (tel.getUseablePeriods() != null && !tel.getUseablePeriods().isEmpty()) {
			Period period = new Period();
			int sxcmCounter = 0;
			for (SXCM_TS sxcmts : tel.getUseablePeriods()) {
				if (sxcmts != null && !sxcmts.isSetNullFlavor()) {
					// useablePeriods[0] -> period.start
					// useablePeriods[1] -> period.end
					if (sxcmCounter == 0) {
						if (sxcmts.getValue() != null && !sxcmts.getValue().isEmpty()) {
							period.setStartElement(tString2DateTime(sxcmts.getValue()));
						}
					} else if (sxcmCounter == 1) {
						if (sxcmts.getValue() != null && !sxcmts.getValue().isEmpty()) {
							period.setEndElement(tString2DateTime(sxcmts.getValue()));
						}
					}
					sxcmCounter++;
				}
			}
			contactPointDt.setPeriod(period);
		}

		// use -> use
		if (tel.getUses() != null && !tel.getUses().isEmpty()) {
			for (TelecommunicationAddressUse telAddressUse : tel.getUses()) {
				if (telAddressUse != null) {
					contactPointDt.setUse(vst.tTelecommunicationAddressUse2ContactPointUse(telAddressUse));
				}
			}
		}

		return contactPointDt;
	}

	@Override
	public DateType tTS2Date(TS ts) {
		DateType date = (DateType) tTS2BaseDateTime(ts, DateType.class);
		if (date == null) {
			return null;
		}

		// TimeZone is NOT permitted
		if (date.getTimeZone() != null) {
			date.setTimeZone(null);
		}

		// precision should be YEAR, MONTH or DAY. otherwise, set it to DAY
		if (date.getPrecision() != TemporalPrecisionEnum.YEAR && date.getPrecision() != TemporalPrecisionEnum.MONTH
				&& date.getPrecision() != TemporalPrecisionEnum.DAY) {
			date.setPrecision(TemporalPrecisionEnum.DAY);
		}

		return date;
	}

	@Override
	public DateTimeType tTS2DateTime(TS ts) {
		DateTimeType dateTime = (DateTimeType) tTS2BaseDateTime(ts, DateTimeType.class);

		if (dateTime == null) {
			return null;
		}

		// if the precision is not YEAR or MONTH, TimeZone SHALL be populated
		if (dateTime.getPrecision() != TemporalPrecisionEnum.YEAR
				&& dateTime.getPrecision() != TemporalPrecisionEnum.MONTH) {
			if (dateTime.getTimeZone() == null) {
				dateTime.setTimeZone(TimeZone.getDefault());
			}
		}

		// if the precision is MINUTE, seconds SHALL be populated
		if (dateTime.getPrecision() == TemporalPrecisionEnum.MINUTE) {
			dateTime.setPrecision(TemporalPrecisionEnum.SECOND);
			dateTime.setSecond(0);
		}

		return dateTime;
	}

	@Override
	public InstantType tTS2Instant(TS ts) {
		InstantType instant = (InstantType) tTS2BaseDateTime(ts, InstantType.class);
		if (instant == null) {
			return null;
		}

		// if the precision is not SECOND or MILLI, convert its precision to SECOND
		if (instant.getPrecision() != TemporalPrecisionEnum.SECOND
				&& instant.getPrecision() != TemporalPrecisionEnum.MILLI) {
			instant.setPrecision(TemporalPrecisionEnum.SECOND);
		}

		// if it doesn't include a timezone, add the local timezone
		if (instant.getTimeZone() == null) {
			instant.setTimeZone(TimeZone.getDefault());
		}
		return instant;
	}

	@Override
	public UriType tURL2Uri(URL url) {
		return (url == null || url.isSetNullFlavor() || url.getValue() == null) ? null : new UriType(url.getValue());
	}

	// Helper Methods
	/**
	 * Extracts the attributes of an HTML element This method is the helper for the
	 * method getTags, which is already a helper for tStrucDocText2String.
	 *
	 * @param entry A EStructuralFeatureImpl.ContainmentUpdatingFeatureMapEntry
	 *              instance
	 * @return A Java String list containing the attributes of an HTML element in
	 *         form: attributeName="attributeValue". Each element corresponds to
	 *         distinct attributes for the same tag
	 */
	private List<String> getAttributesHelperForTStructDocText2String(
			EStructuralFeatureImpl.ContainmentUpdatingFeatureMapEntry entry) {
		if (entry == null) {
			return null;
		}

		List<String> attributeList = new ArrayList<String>();
		if (entry.getValue() instanceof org.eclipse.emf.ecore.xml.type.impl.AnyTypeImpl) {
			for (FeatureMap.Entry attribute : ((org.eclipse.emf.ecore.xml.type.impl.AnyTypeImpl) entry.getValue())
					.getAnyAttribute()) {
				String name = attribute.getEStructuralFeature().getName();
				String value = attribute.getValue().toString();
				if (name != null && !name.isEmpty()) {
					String attributeToAdd = "";
					// we may have attributes which doesn't have any value
					attributeToAdd = attributeToAdd + name;
					if (value != null && !value.isEmpty()) {
						attributeToAdd = attributeToAdd + "=\"" + value + "\"";
					}
					attributeList.add(attributeToAdd);
				}
			}
		}
		return attributeList;
	}

	/**
	 * Extracts the tags and the attributes of an HTML element. Also, this method
	 * transforms the CDA formatted tags to HTML formatted tags. This method is the
	 * helper for the method tStrucDocText2String.
	 *
	 * @param entry A EStructuralFeatureImpl.ContainmentUpdatingFeatureMapEntry
	 *              instance
	 * @return A Java String list containing the start tag and end tag of an HTML
	 *         element in form: &lt;tagName attribute="attributeValue"&gt;. While
	 *         first element of the list correspons to the start tag, second element
	 *         of the list corresponds to the end tag.
	 */
	private List<String> getTagsHelperForTStructDocText2String(
			org.eclipse.emf.ecore.impl.EStructuralFeatureImpl.ContainmentUpdatingFeatureMapEntry entry) {
		if (entry == null) {
			return null;
		}

		String startTag = "";
		String endTag = "";
		String tagName = entry.getEStructuralFeature().getName();

		if (tagName == null || tagName.equals("")) {
			return null;
		}

		List<String> attributeList = getAttributesHelperForTStructDocText2String(entry);
		List<String> tagList = new ArrayList<String>();

		String attributeToRemove = null;

		// removing id attribute from the attributeList
		for (String attribute : attributeList) {
			if (attribute.toLowerCase().startsWith("id=\"", 0)) {
				attributeToRemove = attribute;
			}
		}

		if (attributeToRemove != null) {
			attributeList.remove(attributeToRemove);
		}

		// removing styleCode attribute from the attributeList
		for (String attribute : attributeList) {
			if (attribute.toLowerCase().startsWith("stylecode=\"", 0)) {
				attributeToRemove = attribute;
			}
		}
		if (attributeToRemove != null) {
			attributeList.remove(attributeToRemove);
		}

		// case tag.equals("list"). we need to transform it to "ul" or "ol"
		if (tagName.equals("list")) {
			// first, think of the situtation no attribute exists about ordered/unordered
			tagName = "ul";
			attributeToRemove = null;
			for (String attribute : attributeList) {
				// if the attribute is listType, make the transformation
				if (attribute.toLowerCase().contains("listtype")) {
					// notice that the string "unordered" also contains "ordered"
					// therefore, it is vital to check "unordered" firstly.
					// if "unordered" is not contained by the attribute, then we may check for
					// "ordered"
					if (attribute.toLowerCase().contains("unordered")) {
						tagName = "ul";
					} else if (attribute.toLowerCase().contains("ordered")) {
						tagName = "ol";
					}
					attributeToRemove = attribute;
				}
			}
			// if we found the "listType" attribute, we assigned it to attributeToRemove
			// from now on, we have nothing to do with this attribute. let's remove it from
			// the list.
			if (attributeToRemove != null) {
				attributeList.remove(attributeToRemove);
			}
		} else {
			switch (tagName.toLowerCase()) {
			case "paragraph":
				tagName = "p";
				break;
			case "content":
				tagName = "span";
				break;
			case "item":
				tagName = "li";
				break;
			case "linkhtml":
				tagName = "a";
				break;
			case "renderMultimedia":
				tagName = "img";
				break;
			case "list":
				tagName = "ul";
				break;
			default: // do nothing. let the tagName be as it is
			}
		}

		// now, it is time to prepare our tag by using tagName and attributes
		startTag = "<" + tagName;
		// adding attributes to the start tag
		for (String attribute : attributeList) {
			startTag += " " + attribute;
		}
		// closing the start tag
		startTag += ">";
		endTag = "</" + tagName + ">";

		// 1st element of the returning list: startTag
		tagList.add(startTag);
		// 2nd element of the returning list: endTag
		tagList.add(endTag);

		return tagList;
	}

	/**
	 * Transforms A CDA StructDocText instance to a Java String containing the
	 * transformed text. Since the method is a recursive one and handles with
	 * different types of object, parameter is taken as Object. However, parameters
	 * of type StructDocText should be given by the caller.
	 *
	 * @param param A CDA StructDocText instance
	 * @return A Java String containing the transformed text
	 */
	@Override
	public String tStrucDocText2String(Object param) {
		if (param instanceof org.openhealthtools.mdht.uml.cda.StrucDocText) {
			org.openhealthtools.mdht.uml.cda.StrucDocText paramStrucDocText = (org.openhealthtools.mdht.uml.cda.StrucDocText) param;
			return "<div>" + tStrucDocText2String(paramStrucDocText.getMixed()) + "</div>";
		} else if (param instanceof BasicFeatureMap) {
			String returnValue = "";
			for (Object object : (BasicFeatureMap) param) {
				String pieceOfReturn = tStrucDocText2String(object);
				if (pieceOfReturn != null && !pieceOfReturn.isEmpty()) {
					returnValue = returnValue + pieceOfReturn;
				}
			}
			return returnValue;
		} else if (param instanceof EStructuralFeatureImpl.SimpleFeatureMapEntry) {
			String elementBody = ((EStructuralFeatureImpl.SimpleFeatureMapEntry) param).getValue().toString();
			// deletion of unnecessary content (\n, \t)
			elementBody = elementBody.replaceAll("\n", "").replaceAll("\t", "");

			// replacement of special characters
			elementBody = elementBody.replaceAll("<", "&lt;").replaceAll(">", "&gt;").replaceAll("&", "&amp;");
			// if there was a well-formed char sequence "&amp;", after replacement it will
			// transform to &amp;amp;
			// the following line of code will remove these type of typos
			elementBody = elementBody.replaceAll("&amp;amp;", "&amp;");

			String typeName = ((EStructuralFeatureImpl.SimpleFeatureMapEntry) param).getEStructuralFeature().getName();
			typeName = typeName.toLowerCase();
			if (typeName.equals("comment")) {
				return "<!-- " + elementBody + " -->";
			} else if (typeName.equals("text")) {
				return elementBody;
			} else {
				logger.warn(
						"Unknown element type was found while transforming a StrucDocText instance to Narrative. Returning the value of the element");
				return elementBody;
			}
		} else if (param instanceof EStructuralFeatureImpl.ContainmentUpdatingFeatureMapEntry) {
			EStructuralFeatureImpl.ContainmentUpdatingFeatureMapEntry entry = (EStructuralFeatureImpl.ContainmentUpdatingFeatureMapEntry) param;
			List<String> tagList = getTagsHelperForTStructDocText2String(entry);
			return tagList.get(0) + tStrucDocText2String(entry.getValue()) + tagList.get(1);
		} else if (param instanceof org.eclipse.emf.ecore.xml.type.impl.AnyTypeImpl) {
			// since the name and the attributes are taken already, we just send the mixed
			// of anyTypeImpl
			return tStrucDocText2String(((org.eclipse.emf.ecore.xml.type.impl.AnyTypeImpl) param).getMixed());
		} else {
			logger.warn("Parameter for the method tStrucDocText2String is unknown. Returning null", param.getClass());
			return null;
		}
	}

	/**
	 * Transforms a CDA TS instance or a string including the date information in
	 * CDA format to a FHIR BaseDateTimeType primitive datatype instance. Since
	 * BaseDateTimeType is an abstract class, the second parameter of this method
	 * (Class&lt;?&gt; classOfReturningObject) determines the class that initiates
	 * the BaseDateTimeType object the method is to return.
	 *
	 * @param tsObject               A CDA TS instance or a Java String including
	 *                               the date information in CDA format
	 * @param classOfReturningObject A FHIR class that determines the initiater for
	 *                               the BaseDateTimeType object the method is to
	 *                               return. DateType.class, DateTimeType.class or
	 *                               InstantType.class are expected.
	 * @return A FHIR BaseDateTimeType primitive datatype instance
	 */
	private BaseDateTimeType tTS2BaseDateTime(Object tsObject, Class<?> classOfReturningObject) {
		if (tsObject == null) {
			return null;
		}

		String dateString;
		// checking the type of tsObject, assigning dateString accordingly
		if (tsObject instanceof TS) {
			// null-flavor check
			if (((TS) tsObject).isSetNullFlavor() || ((TS) tsObject).getValue() == null) {
				return null;
			} else {
				dateString = ((TS) tsObject).getValue();
			}
		} else if (tsObject instanceof String) {
			dateString = (String) tsObject;
		} else {
			// unexpected situtation
			// 1st parameter of this method should be either an instanceof TS or String
			return null;
		}

		BaseDateTimeType date;
		GregorianCalendar calendar = new GregorianCalendar(1, Calendar.JANUARY, 1, 0, 0, 0);
		// initializing date
		date = initializeDate(classOfReturningObject, calendar);

		/*
		 * Possible date forms YYYY: year YYYYMM: year month YYYYMMDD: year month day
		 * YYYYMMDDHHMM: year month day hour minute YYYYMMDDHHMMSS.S: year month day
		 * hour minute second YYYYMMDDHHMM+TIZO: year month day hour minute timezone
		 */

		TemporalPrecisionEnum precision = null;
		TimeZone timeZone = null;

		// getting the timezone
		// once got the timezone, crop the timezone part from the string
		if (dateString.contains("+")) {
			timeZone = TimeZone.getTimeZone("GMT" + dateString.substring(dateString.indexOf('+')));
			dateString = dateString.substring(0, dateString.indexOf('+'));
		} else if (dateString.contains("-")) {
			timeZone = TimeZone.getTimeZone("GMT" + dateString.substring(dateString.indexOf('-')));
			dateString = dateString.substring(0, dateString.indexOf('-'));
		}

		// determining precision
		switch (dateString.length()) {
		case 4: // yyyy
			precision = TemporalPrecisionEnum.YEAR;
			break;
		case 6: // yyyymm
			precision = TemporalPrecisionEnum.MONTH;
			break;
		case 8: // yyyymmdd
			precision = TemporalPrecisionEnum.DAY;
			break;
		case 12: // yyyymmddhhmm
			precision = TemporalPrecisionEnum.MINUTE;
			break;
		case 14: // yyyymmddhhmmss
			precision = TemporalPrecisionEnum.SECOND;
			break;
		case 16: // yyyymmddhhmmss.s
		case 17: // yyyymmddhhmmss.ss
		case 18: // yyyymmddhhmmss.sss
		case 19: // yyyymmddhhmmss.ssss
			precision = TemporalPrecisionEnum.MILLI;
			break;
		default:
			precision = null;
		}

		// given string may include up to four digits of fractions of a second
		// therefore, there may be cases where the length of the string is 17,18 or 19
		// and the precision is MILLI.
		// for those of cases where the length causes conflicts, let's check if dot(.)
		// exists in the string

		// setting precision
		if (precision != null) {
			date.setPrecision(precision);
		} else {
			// incorrect format
			return null;
		}

		// if timeZone is present, setting it
		if (timeZone != null && needsTimeZone(precision)) {
			/*
			 * The time zone could roll us back BCE, so we offset the day to avoid this. Adjusting the day doesn't
			 * matter since we are about to overwrite the day/month/year data using values we've already parsed anyway.
			 */

			date.setDay(2);
			date.setPrecision(precision);
			date.setTimeZone(timeZone);
		}

		if (precision == TemporalPrecisionEnum.MILLI) {
			// get the integer starting from the dot(.) char 'till the end of the string as
			// the millis
			int millis = Integer.parseInt(dateString.substring(dateString.indexOf('.') + 1));
			// if millis is given as .4 , it corresponds to 400 millis.
			// therefore, we need a conversion.
			if (millis > 0 && millis < 1000) {
				while (millis * 10 < 1000) {
					millis *= 10;
				}
			} else if (millis >= 1000) {
				// unexpected situtation
				millis = 999;
			} else {
				// unexpected situtation
				millis = 0;
			}

			// setting millis
			date.setMillis(millis);

			// setting second, minute, hour, day, month, year..
			date.setSecond(Integer.parseInt(dateString.substring(12, 14)));
			date.setMinute(Integer.parseInt(dateString.substring(10, 12)));
			date.setHour(Integer.parseInt(dateString.substring(8, 10)));
			date.setDay(Integer.parseInt(dateString.substring(6, 8)));
			date.setMonth(Integer.parseInt(dateString.substring(4, 6)) - 1);
			date.setYear(Integer.parseInt(dateString.substring(0, 4)));

		} else {
			// since there are strange situations where the index changes upon the
			// precision, we set every value in its precision block
			switch (precision) {
			case SECOND:
				date.setSecond(Integer.parseInt(dateString.substring(12, 14)));
			case MINUTE:
				date.setMinute(Integer.parseInt(dateString.substring(10, 12)));
				date.setHour(Integer.parseInt(dateString.substring(8, 10)));
				date.setDay(Integer.parseInt(dateString.substring(6, 8)));
				date.setMonth(Integer.parseInt(dateString.substring(4, 6)) - 1);
				date.setYear(Integer.parseInt(dateString.substring(0, 4)));
				break;
			case DAY:
				date.setDay(Integer.parseInt(dateString.substring(6, 8)));
				date.setMonth(Integer.parseInt(dateString.substring(4, 6)) - 1);
				date.setYear(Integer.parseInt(dateString.substring(0, 4)));
				break;
			case MONTH:
				date.setMonth(Integer.parseInt(dateString.substring(4, 6)) - 1);
				date.setYear(Integer.parseInt(dateString.substring(0, 4)));
				break;
			case YEAR:
				date.setYear(Integer.parseInt(dateString.substring(0, 4)));
				break;
			default:
				date = null;
			}
		}
		return date;
	}

	private boolean needsTimeZone(TemporalPrecisionEnum precision) {
		HashSet<TemporalPrecisionEnum> precisionsNeedingTimezone = new HashSet<>(List.of(
				TemporalPrecisionEnum.MINUTE,
				TemporalPrecisionEnum.SECOND,
				TemporalPrecisionEnum.MILLI));
		return precisionsNeedingTimezone.contains(precision);
	}

	private BaseDateTimeType initializeDate(Class<?> classOfReturningObject, GregorianCalendar calendar) {
		BaseDateTimeType date;
		if (classOfReturningObject == DateType.class) {
			date = new DateType(calendar);
		} else if (classOfReturningObject == DateTimeType.class) {
			date = new DateTimeType(calendar);
		} else if (classOfReturningObject == InstantType.class) {
			date = new InstantType(calendar);
		} else {
			// unexpected situation
			// caller of this method must have a need of DateType, DateTimeType or
			// InstantType
			// otherwise, the returning object will be of type DateType
			date = new DateType(calendar);
		}
		return date;
	}

	@Override
	public StringType tValues2String(EList<ANY> values) {

		// NOTE : MAY contain zero or one [0..1] value (CONF:1098-32743).
		if (values == null)
			return null;

		for (ANY value : values) {
			if (value == null || value.isSetNullFlavor())
				continue;
			if (!(value instanceof ST))
				continue;
			ST valueSt = (ST) value;
			return tST2String(valueSt);
		}
		return null;
	}
	
	@Override
	public Coding tCD2Coding(CD theCd) {
		Coding codingDt = null;
		if (theCd == null) {
			return null;
		}
		if (!theCd.isSetNullFlavor()) {
			codingDt = new Coding();
			// codeSystem -> system
			if (theCd.getCodeSystem() != null && !theCd.getCodeSystem().isEmpty()) {
				codingDt.setSystem(vst.tOid2Url(theCd.getCodeSystem()));
			}

			// code -> code
			if (theCd.getCode() != null && !theCd.getCode().isEmpty()) {
				codingDt.setCode(theCd.getCode());
			}

			// codeSystemVersion -> version
			if (theCd.getCodeSystemVersion() != null && !theCd.getCodeSystemVersion().isEmpty()) {
				codingDt.setVersion(theCd.getCodeSystemVersion());
			}

			// displayName -> display
			if (theCd.getDisplayName() != null && !theCd.getDisplayName().isEmpty()) {
				codingDt.setDisplay(theCd.getDisplayName());
			}
		}
		return codingDt;
	}
	
	@Override
	public Extension tObservationBirthSexValue2BirthSexExtension(ANY theValue) {

		Extension birthSexExt = null;
		if (theValue == null)
			return birthSexExt;

		if (!(theValue instanceof CD))
			return birthSexExt;

		CD valueCd = (CD) theValue;
		String codeSystem = valueCd.getCodeSystem();
		if (!CDASocialHistorySection.BIRTH_SEX_CODE_SYSTEM_ID.equals(codeSystem))
			return birthSexExt;

		if (theValue.isSetNullFlavor()) {
			birthSexExt = new Extension();
			birthSexExt.setUrl("http://hl7.org/fhir/us/core/StructureDefinition/us-core-birthsex");
			CodeType codeType = new CodeType();
			birthSexExt.setValue(codeType);
			codeType.setValue("UNK");
			return birthSexExt;
		}

		String code = valueCd.getCode();
		if (code == null) {
			return birthSexExt;
		}

		birthSexExt = new Extension();
		birthSexExt.setUrl("http://hl7.org/fhir/us/core/StructureDefinition/us-core-birthsex");
		CodeType codeType = new CodeType();
		birthSexExt.setValue(codeType);
		switch (code.toUpperCase()) {
		case "M":
			codeType.setValue("M");
			break;
		case "F":
			codeType.setValue("F");
			break;
		default:
			codeType.setValue("UNK");
		}
		
		return birthSexExt;
	}

	@Override
	public Extension tNullFlavor2DataAbsentReasonExtension(NullFlavor theCdaNullFlavor) {

		String code = null;
		String display = null;

		switch (theCdaNullFlavor) {
		case NI:
		//case NAVU:
		case NP:
			//NI, NAVU, NP -> error
			code = "error";
			display = "Error";
			break;
		case UNK:
		//case DER:
		//case QS:
		case TRC:
			// DER, UNK, QS, TRC -> unknown
			code = "unknown";
			display = "Unknown";
			break;
		case ASKU:
			// ASKU -> asked-unknown
			code = "asked-unknown";
			display = "Asked But Unknown";
			break;
		case NAV:
			// NAV -> temp-unknown
			code = "temp-unknown";
			display = "Temporarily Unknown";
			break;
		case NASK:
		    // NASK -> not-asked
			code = "not-asked";
			display = "Not Asked";
			break;
		case MSK:
			// MSK -> masked
			code = "masked";
			display = "Masked";
			break;
		case NA:
			// NA-> not-applicable
			code = "not-applicable";
			display = "Not Applicable";
			break;
		//case INT:
		case OTH:
		//case UNC:
		    // INV, OTH, UNC -> not-permitted
			code = "not-permitted";
			display = "Not Permitted";
			break;
		case NINF:
			// NINF -> negative-infinity
			code = "negative-infinity";
			display = "Negative Infinity (NINF)";
			break;
		case PINF:
			// PINF -> positive-infinity
			code = "positive-infinity";
			display = "Positive Infinity (PINF)";
			break;
		default:
			return null;
		}

		Extension ext = new Extension();
		ext.setUrl(DATA_ABSENT_REASON_EXTENSION_URI);
		Coding dataAbsentReasonCode = new Coding();
		dataAbsentReasonCode.setSystem(DATA_ABSENT_REASON_TERMINOLOGY_URI);
		dataAbsentReasonCode.setCode(code);
		dataAbsentReasonCode.setDisplay(display);

		ext.setValue(dataAbsentReasonCode);

		return ext;
	}
}
