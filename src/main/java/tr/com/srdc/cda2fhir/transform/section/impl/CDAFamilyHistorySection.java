package tr.com.srdc.cda2fhir.transform.section.impl;

import org.hl7.fhir.r4.model.Bundle;
import org.hl7.fhir.r4.model.FamilyMemberHistory;
import org.openhealthtools.mdht.uml.cda.consol.FamilyHistoryOrganizer;
import org.openhealthtools.mdht.uml.cda.consol.FamilyHistorySection;

import tr.com.srdc.cda2fhir.transform.IResourceTransformer;
import tr.com.srdc.cda2fhir.transform.section.ICDASection;
import tr.com.srdc.cda2fhir.transform.util.IBundleInfo;
import tr.com.srdc.cda2fhir.transform.util.ITransformationContext;

public class CDAFamilyHistorySection implements ICDASection {
	private FamilyHistorySection section;

	@SuppressWarnings("unused")
	private CDAFamilyHistorySection() {
	};

	public CDAFamilyHistorySection(FamilyHistorySection section) {
		this.section = section;
	}

	@Override
	public SectionResultSingular<FamilyMemberHistory> transform(IBundleInfo bundleInfo, ITransformationContext theTransformationContext) {
		IResourceTransformer rt = bundleInfo.getResourceTransformer();
		Bundle result = new Bundle();
		for (FamilyHistoryOrganizer org : section.getFamilyHistories()) {
			FamilyMemberHistory fmh = rt.tFamilyHistoryOrganizer2FamilyMemberHistory(org, theTransformationContext);
			result.addEntry().setResource(fmh);
		}
		return SectionResultSingular.getInstance(result, FamilyMemberHistory.class);
	}
}
