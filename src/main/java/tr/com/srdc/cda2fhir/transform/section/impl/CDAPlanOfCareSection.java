package tr.com.srdc.cda2fhir.transform.section.impl;

import org.eclipse.emf.common.util.EList;
import org.hl7.fhir.r4.model.CarePlan;
import org.openhealthtools.mdht.uml.cda.StrucDocText;
import org.openhealthtools.mdht.uml.cda.consol.PlanOfCareActivityAct;
import org.openhealthtools.mdht.uml.cda.consol.PlanOfCareSection;
import org.openhealthtools.mdht.uml.hl7.datatypes.ST;

import tr.com.srdc.cda2fhir.transform.IResourceTransformer;
import tr.com.srdc.cda2fhir.transform.entry.IEntryResult;
import tr.com.srdc.cda2fhir.transform.section.ICDASection;
import tr.com.srdc.cda2fhir.transform.util.IBundleInfo;
import tr.com.srdc.cda2fhir.transform.util.ITransformationContext;
import tr.com.srdc.cda2fhir.transform.util.impl.LocalBundleInfo;

public class CDAPlanOfCareSection implements ICDASection {
	
	public static final String PLAN_OF_CARE_SECTION_TEMPLATE_ID = "2.16.840.1.113883.10.20.22.2.10";
	
    private PlanOfCareSection mySection;

    public CDAPlanOfCareSection(PlanOfCareSection theSection) {
        mySection = theSection;
    }

    @Override
    public SectionResultSingular<CarePlan> transform(IBundleInfo bundleInfo, ITransformationContext theTransformationContext) {
		IResourceTransformer rt = bundleInfo.getResourceTransformer();
		SectionResultSingular<CarePlan> result = SectionResultSingular.getInstance(CarePlan.class);
		LocalBundleInfo localBundleInfo = new LocalBundleInfo(bundleInfo);

		ST title = mySection.getTitle();
		EList<PlanOfCareActivityAct> acts = mySection.getPlanOfCareActivityActs();
		StrucDocText strucDocText = mySection.getText();

		IEntryResult er = rt.tPlanOfCareSection2CarePlan(title, strucDocText, acts, localBundleInfo, theTransformationContext);
		result.updateFrom(er);
		localBundleInfo.updateFrom(er);

		return result;
    }
}
