package tr.com.srdc.cda2fhir.transform.section;

import tr.com.srdc.cda2fhir.transform.util.IBundleInfo;
import tr.com.srdc.cda2fhir.transform.util.ITransformationContext;

public interface ICDASection {
	/**
	 * Transforms all the entries of a section into the corresponding FHIR resources.
	 *
	 * @param bundleInfo               A BundleInfo object which acts as a context for the current
	 *                                 transformation.
	 * @param theTransformationContext the local context of the transformation
	 * @return a result object wrapping a bundle that contains all the resources derived from
	 *         the contents of the section
	 */
	ISectionResult transform(IBundleInfo bundleInfo, ITransformationContext theTransformationContext);
}
