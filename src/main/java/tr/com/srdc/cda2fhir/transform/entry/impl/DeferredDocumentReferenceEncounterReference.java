package tr.com.srdc.cda2fhir.transform.entry.impl;

import org.hl7.fhir.r4.model.DocumentReference;
import org.hl7.fhir.r4.model.Identifier;
import org.hl7.fhir.r4.model.Reference;
import org.hl7.fhir.r4.model.Resource;

import tr.com.srdc.cda2fhir.transform.util.IDeferredReference;

public class DeferredDocumentReferenceEncounterReference implements IDeferredReference {
	private DocumentReference documentReference;
	private Identifier identifier;

	public DeferredDocumentReferenceEncounterReference(DocumentReference documentReference, Identifier identifier) {
		this.documentReference = documentReference;
		this.identifier = identifier;
	}

	@Override
	public String getFhirType() {
		return "Encounter";
	}

	@Override
	public Identifier getIdentifier() {
		return identifier;
	}

	@Override
	public Resource getResource() {
		return documentReference;
	}

	@Override
	public void resolve(Reference reference) {
		documentReference.getContext().addEncounter(reference);
	}
}
