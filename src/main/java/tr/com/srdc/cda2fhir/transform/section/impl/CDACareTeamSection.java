package tr.com.srdc.cda2fhir.transform.section.impl;

import java.text.MessageFormat;

import org.eclipse.emf.common.util.EList;
import org.hl7.fhir.r4.model.CareTeam;
import org.openhealthtools.mdht.uml.cda.Entry;
import org.openhealthtools.mdht.uml.cda.Organizer;
import org.openhealthtools.mdht.uml.cda.Section;
import org.openhealthtools.mdht.uml.cda.StrucDocText;

import tr.com.srdc.cda2fhir.transform.IResourceTransformer;
import tr.com.srdc.cda2fhir.transform.ResourceTransformerImpl;
import tr.com.srdc.cda2fhir.transform.entry.IEntryResult;
import tr.com.srdc.cda2fhir.transform.section.ICDASection;
import tr.com.srdc.cda2fhir.transform.util.IBundleInfo;
import tr.com.srdc.cda2fhir.transform.util.ITransformationContext;
import tr.com.srdc.cda2fhir.transform.util.impl.LocalBundleInfo;

public class CDACareTeamSection implements ICDASection {
	
	public static final String CARE_TEAM_SECTION_TEMPLATE_ID = "2.16.840.1.113883.10.20.22.2.500";
	public static final String CARE_TEAM_PERFORMER_ACT_TEMPLATE_ID = "2.16.840.1.113883.10.20.22.4.500.1";
	
    private Section mySection;

    public CDACareTeamSection(Section theSection) {
        mySection = theSection;
    }

    @Override
    public SectionResultSingular<CareTeam> transform(IBundleInfo bundleInfo, ITransformationContext theTransformationContext) {
        
    	IResourceTransformer rt = bundleInfo.getResourceTransformer();
        SectionResultSingular<CareTeam> result = SectionResultSingular.getInstance(CareTeam.class);
        LocalBundleInfo localBundleInfo = new LocalBundleInfo(bundleInfo);
        
        EList<Entry> entries = mySection.getEntries();
        StrucDocText strucDocText = mySection.getText();
        
        int count = 0;
		for (Entry entry : entries) {
			count++;
			Organizer organizer = entry.getOrganizer();
			IEntryResult er = rt.tOrganizer2CareTeam(strucDocText, organizer, localBundleInfo, theTransformationContext);
			result.updateFrom(er);
			localBundleInfo.updateFrom(er);
			break;
		}
		
		if (count == 0) {
			theTransformationContext.addWarning("CareTeam Entry", "Organizer", ResourceTransformerImpl.MIN_CARDINALITY_MESSAGE);
		} else if (count > 1){
			theTransformationContext.addWarning("CareTeam Entry", "Organizer", MessageFormat.format(ResourceTransformerImpl.MAX_CARDINALITY_MESSAGE, count));
		}
		
        return result;
    }
}
