package tr.com.srdc.cda2fhir.transform.util.impl;

import org.hl7.fhir.r4.model.Bundle;
import tr.com.srdc.cda2fhir.transform.util.ITransformationContext;
import tr.com.srdc.cda2fhir.transform.util.ITransformationMessage;
import tr.com.srdc.cda2fhir.transform.util.ITransformationResult;

import java.util.Collections;
import java.util.List;

public class TransformationResultImpl implements ITransformationResult {

    private final Bundle myBundle;
    private final List<ITransformationMessage> myErrors;
    private final List<ITransformationMessage> myWarnings;

    public TransformationResultImpl(Bundle theBundle, ITransformationContext theContext) {
        this(theBundle, theContext.getErrors(), theContext.getWarnings());
    }

    public TransformationResultImpl(Bundle theBundle, List<ITransformationMessage> theErrors, List<ITransformationMessage> theWarnings) {
        this.myBundle = theBundle;
        this.myErrors = Collections.unmodifiableList(theErrors);
        this.myWarnings = Collections.unmodifiableList(theWarnings);
    }

    @Override
    public Bundle getBundle() {
        return myBundle;
    }

    @Override
    public boolean hasErrors() {
        return !myErrors.isEmpty();
    }

    @Override
    public boolean hasWarnings() {
        return !myWarnings.isEmpty();
    }

    @Override
    public List<ITransformationMessage> getErrors() {
        return myErrors;
    }

    @Override
    public List<ITransformationMessage> getWarnings() {
        return myWarnings;
    }
}
