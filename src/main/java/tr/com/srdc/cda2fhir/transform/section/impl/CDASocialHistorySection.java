package tr.com.srdc.cda2fhir.transform.section.impl;

import org.hl7.fhir.r4.model.Observation;
import org.openhealthtools.mdht.uml.cda.consol.SocialHistorySection;

import tr.com.srdc.cda2fhir.transform.IResourceTransformer;
import tr.com.srdc.cda2fhir.transform.entry.IEntryResult;
import tr.com.srdc.cda2fhir.transform.section.ICDASection;
import tr.com.srdc.cda2fhir.transform.util.IBundleInfo;
import tr.com.srdc.cda2fhir.transform.util.ITransformationContext;
import tr.com.srdc.cda2fhir.transform.util.impl.LocalBundleInfo;

public class CDASocialHistorySection implements ICDASection {
	
	public static final String SOCIAL_HISTORY_OBSERVATION_BIRTH_SEX_TEMPLATE_ID = "2.16.840.1.113883.10.20.22.4.200";
	public static final String BIRTH_SEX_CODE_SYSTEM_ID = "2.16.840.1.113883.5.1";
	
	private SocialHistorySection section;

	@SuppressWarnings("unused")
	private CDASocialHistorySection() {
	}

	public CDASocialHistorySection(SocialHistorySection section) {
		this.section = section;
	}

	@Override
	public SectionResultSingular<Observation> transform(IBundleInfo bundleInfo, ITransformationContext theTransformationContext) {
		LocalBundleInfo localBundleInfo = new LocalBundleInfo(bundleInfo);
		IResourceTransformer rt = bundleInfo.getResourceTransformer();
		SectionResultSingular<Observation> result = SectionResultSingular.getInstance(Observation.class);
		for (org.openhealthtools.mdht.uml.cda.Observation obs : section.getObservations()) {
			IEntryResult er = rt.tSocialHistoryObservation2Observation(obs, localBundleInfo, theTransformationContext);
			result.updateFrom(er);
			localBundleInfo.updateFrom(er);
		}
		return result;
	}
}
