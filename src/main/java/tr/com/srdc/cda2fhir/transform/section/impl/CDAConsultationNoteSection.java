package tr.com.srdc.cda2fhir.transform.section.impl;

import org.eclipse.emf.common.util.EList;
import org.hl7.fhir.r4.model.DocumentReference;
import org.openhealthtools.mdht.uml.cda.Act;
import org.openhealthtools.mdht.uml.cda.Section;
import org.openhealthtools.mdht.uml.cda.StrucDocText;

import tr.com.srdc.cda2fhir.transform.IResourceTransformer;
import tr.com.srdc.cda2fhir.transform.entry.IEntryResult;
import tr.com.srdc.cda2fhir.transform.section.ICDASection;
import tr.com.srdc.cda2fhir.transform.util.IBundleInfo;
import tr.com.srdc.cda2fhir.transform.util.ITransformationContext;
import tr.com.srdc.cda2fhir.transform.util.impl.LocalBundleInfo;

public class CDAConsultationNoteSection implements ICDASection {
	
	public static final String CONSULTATION_NOTE_SECTION_TEMPLATE_ID = "2.16.840.1.113883.10.20.22.2.65";
	public static final String CONSULTATION_NOTE_ACTIVITY_TEMPLATE_ID = "2.16.840.1.113883.10.20.22.4.202";
	
    private Section mySection;

    public CDAConsultationNoteSection(Section theSection) {
        mySection = theSection;
    }

    @Override
    public SectionResultSingular<DocumentReference> transform(IBundleInfo bundleInfo, ITransformationContext theTransformationContext) {
        
    	IResourceTransformer rt = bundleInfo.getResourceTransformer();
        SectionResultSingular<DocumentReference> result = SectionResultSingular.getInstance(DocumentReference.class);
        LocalBundleInfo localBundleInfo = new LocalBundleInfo(bundleInfo);
        
        EList<Act> acts = mySection.getActs();
        StrucDocText strucDocText = mySection.getText();
        
        for (Act act : acts) {
            if (act.getTemplateIds().stream().anyMatch(t -> CONSULTATION_NOTE_ACTIVITY_TEMPLATE_ID.equals(t.getRoot()))) {
                IEntryResult er = rt.tAct2DocumentReference(strucDocText, act, localBundleInfo, theTransformationContext);
                result.updateFrom(er);
                localBundleInfo.updateFrom(er);
            }
        }
        return result;
    }
}
