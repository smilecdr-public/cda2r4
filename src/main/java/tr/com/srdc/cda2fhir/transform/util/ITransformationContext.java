package tr.com.srdc.cda2fhir.transform.util;

import java.util.List;

import org.hl7.fhir.r4.model.Encounter;
import org.hl7.fhir.r4.model.Patient;
import org.hl7.fhir.r4.model.Reference;

/**
 * Tracks the context relevant to a single invocation of the CDA transformer, for thread safety.
 */
public interface ITransformationContext {
    /**
     * Indicates whether the current request is for the creation of a transaction bundle. Note that, currently, other
     * bundle types are not supported.
     *
     * @return true if the request is for a transaction bundle, otherwise false.
     */
    boolean isTransactionBundle();

    /**
     * Returns a reference indicating the patient who is the subject of the document being converted.
     *
     * @return a reference to a Patient resource.
     */
    Reference getPatientRef();

    /**
     * Returns the patient resource who is the subject of the document being converted.
     *
     * @return a Patient resource.
     */
    Patient getPatient();

    /**
     * Store the patient resource who is the subject of the document being converted.
     *
     * @param thePatient a Patient resource.
     */
    void setPatient(Patient thePatient);

    /**
     * Gets a reference to the Encounter resource corresponding to the Encounter Activity found in the Encounters
     * section.
     *
     * @return a reference to an Encounter resource
     */
    Reference getEncounterRef();

    /**
     * Gets an Encounter resource. The resource will correspond to the Encounter Activity found in the Encounters
     * section. There should only be at most one of each, so this is unambiguous.
     *
     * @return an Encounter resource
     */
    Encounter getEncounter();

    /**
     * Store the encounter Eesource corresponding to the Encounter Activity found in the Encounters section.
     *
     * @param theEncounter an Encounter resource
     */
    void setEncounter(Encounter theEncounter);
    
    /**
     * Obtain a unique identifier for a newly-created resource. Currently, only UUID-based identifiers are returned.
     *
     * @return a unique UUID for use as an identifier.
     */
    String getUniqueId();

    /**
     * Adds an error message to the current operation context.
     *
     * @param theEntryName the name of the CDA entry where the message arose.
     * @param theFieldName the name of the field in the CDA entry most relevant to the message.
     * @param theMessage a detailed message describing the problem that was encountered.
     */
    void addError(String theEntryName, String theFieldName, String theMessage);

    /**
     * Adds a warning message to the current operation context.
     *
     * @param theEntryName the name of the CDA entry where the message arose.
     * @param theFieldName the name of the field in the CDA entry most relevant to the message.
     * @param theMessage a detailed message describing the problem that was encountered.
     */
    void addWarning(String theEntryName, String theFieldName, String theMessage);

    /**
     * Indicates whether any messages with a severity of ERROR have been encountered in the current operation context.
     *
     * @return true if the context contains any error messages, otherwise false.
     */
    boolean hasErrors();

    /**
     * Indicates whether any messages with a severity of WARNING have been encountered in the current operation context.
     *
     * @return true if the context contains any warning messages, otherwise false.
     */
    boolean hasWarnings();

    /**
     * Gets the list of error messages from the operation context.
     *
     * @return the list of error messages.
     */
    List<ITransformationMessage> getErrors();

    /**
     * Gets the list of warning messages from the operation context.
     *
     * @return the list of warning messages.
     */
    List<ITransformationMessage> getWarnings();

}
