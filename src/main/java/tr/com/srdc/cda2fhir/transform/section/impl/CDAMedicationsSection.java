package tr.com.srdc.cda2fhir.transform.section.impl;

import org.hl7.fhir.r4.model.MedicationRequest;
import org.hl7.fhir.r4.model.MedicationStatement;
import org.openhealthtools.mdht.uml.cda.consol.MedicationActivity;
import org.openhealthtools.mdht.uml.cda.consol.MedicationsSection;

import org.openhealthtools.mdht.uml.hl7.datatypes.II;
import org.openhealthtools.mdht.uml.hl7.vocab.ActClassSupply;
import org.openhealthtools.mdht.uml.hl7.vocab.x_DocumentSubstanceMood;
import tr.com.srdc.cda2fhir.transform.IResourceTransformer;
import tr.com.srdc.cda2fhir.transform.entry.IEntryResult;
import tr.com.srdc.cda2fhir.transform.section.ICDASection;
import tr.com.srdc.cda2fhir.transform.util.IBundleInfo;
import tr.com.srdc.cda2fhir.transform.util.ITransformationContext;
import tr.com.srdc.cda2fhir.transform.util.impl.LocalBundleInfo;

import java.text.MessageFormat;

import static tr.com.srdc.cda2fhir.transform.ResourceTransformerImpl.MEDICATION_SUPPLY_ORDER_OID;

public class CDAMedicationsSection implements ICDASection {
	private MedicationsSection section;

	@SuppressWarnings("unused")
	private CDAMedicationsSection() {
	};

	public CDAMedicationsSection(MedicationsSection section) {
		this.section = section;
	}

	@Override
	public SectionResultDynamic transform(IBundleInfo bundleInfo, ITransformationContext theTransformationContext) {
		IResourceTransformer rt = bundleInfo.getResourceTransformer();
		SectionResultDynamic result = new SectionResultDynamic();
		LocalBundleInfo localBundleInfo = new LocalBundleInfo(bundleInfo);

		for (MedicationActivity act : section.getMedicationActivities()) {
			if (x_DocumentSubstanceMood.EVN.equals(act.getMoodCode())) {
				if (needsUSCoreMedicationRequestIG(act)) {
					IEntryResult er = rt.tMedicationActivity2MedicationRequest(act, localBundleInfo, theTransformationContext);
					result.updateFrom(er, MedicationRequest.class);
					localBundleInfo.updateFrom(er);
				} else {
					IEntryResult er = rt.tMedicationActivity2MedicationStatement(act, localBundleInfo, theTransformationContext);
					result.updateFrom(er, MedicationStatement.class);
					localBundleInfo.updateFrom(er);
				}
			} else {
				theTransformationContext.addWarning("Medication Statement", "moodCode",
						MessageFormat.format("{0} mood not supported.", act.getMoodCode()));
			}
		}
		return result;

	}

	private boolean needsUSCoreMedicationRequestIG(MedicationActivity theAct) {
		return theAct.getSupplies().stream()
					.anyMatch(supply -> supply.getTemplateIds().stream()
							.map(II::getRoot)
							.anyMatch(MEDICATION_SUPPLY_ORDER_OID::equals)
						&& ActClassSupply.SPLY.equals(supply.getClassCode())
						&& x_DocumentSubstanceMood.INT.equals(supply.getMoodCode()));
	}

}
