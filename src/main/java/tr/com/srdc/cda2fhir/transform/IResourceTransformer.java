package tr.com.srdc.cda2fhir.transform;

/*
 * #%L
 * CDA to FHIR Transformer Library
 * %%
 * Copyright (C) 2016 SRDC Yazilim Arastirma ve Gelistirme ve Danismanlik Tic. A.S.
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import java.util.List;

import org.hl7.fhir.r4.model.Age;
import org.hl7.fhir.r4.model.Bundle;
import org.hl7.fhir.r4.model.CarePlan;
import org.hl7.fhir.r4.model.Composition.SectionComponent;
import org.hl7.fhir.r4.model.FamilyMemberHistory;
import org.hl7.fhir.r4.model.Group;
import org.hl7.fhir.r4.model.Identifier;
import org.hl7.fhir.r4.model.Observation.ObservationReferenceRangeComponent;
import org.hl7.fhir.r4.model.Patient.PatientCommunicationComponent;
import org.hl7.fhir.r4.model.Practitioner;
import org.hl7.fhir.r4.model.PractitionerRole;
import org.hl7.fhir.r4.model.Reference;
import org.hl7.fhir.r4.model.Resource;
import org.hl7.fhir.r4.model.ServiceRequest;
import org.hl7.fhir.r4.model.Substance;
import org.openhealthtools.mdht.uml.cda.Act;
import org.openhealthtools.mdht.uml.cda.AssignedAuthor;
import org.openhealthtools.mdht.uml.cda.AssignedEntity;
import org.openhealthtools.mdht.uml.cda.Author;
import org.openhealthtools.mdht.uml.cda.ClinicalDocument;
import org.openhealthtools.mdht.uml.cda.Consumable;
import org.openhealthtools.mdht.uml.cda.CustodianOrganization;
import org.openhealthtools.mdht.uml.cda.Entity;
import org.openhealthtools.mdht.uml.cda.Guardian;
import org.openhealthtools.mdht.uml.cda.LanguageCommunication;
import org.openhealthtools.mdht.uml.cda.ManufacturedProduct;
import org.openhealthtools.mdht.uml.cda.Observation;
import org.openhealthtools.mdht.uml.cda.Organization;
import org.openhealthtools.mdht.uml.cda.Organizer;
import org.openhealthtools.mdht.uml.cda.Participant2;
import org.openhealthtools.mdht.uml.cda.ParticipantRole;
import org.openhealthtools.mdht.uml.cda.PatientRole;
import org.openhealthtools.mdht.uml.cda.Performer2;
import org.openhealthtools.mdht.uml.cda.Procedure;
import org.openhealthtools.mdht.uml.cda.Product;
import org.openhealthtools.mdht.uml.cda.Section;
import org.openhealthtools.mdht.uml.cda.StrucDocText;
import org.openhealthtools.mdht.uml.cda.Supply;
import org.openhealthtools.mdht.uml.cda.consol.AgeObservation;
import org.openhealthtools.mdht.uml.cda.consol.AllergyProblemAct;
import org.openhealthtools.mdht.uml.cda.consol.EncounterActivities;
import org.openhealthtools.mdht.uml.cda.consol.FamilyHistoryOrganizer;
import org.openhealthtools.mdht.uml.cda.consol.ImmunizationActivity;
import org.openhealthtools.mdht.uml.cda.consol.Indication;
import org.openhealthtools.mdht.uml.cda.consol.MedicationActivity;
import org.openhealthtools.mdht.uml.cda.consol.MedicationDispense;
import org.openhealthtools.mdht.uml.cda.consol.MedicationInformation;
import org.openhealthtools.mdht.uml.cda.consol.MedicationSupplyOrder;
import org.openhealthtools.mdht.uml.cda.consol.PlanOfCareActivityAct;
import org.openhealthtools.mdht.uml.cda.consol.ProblemConcernAct;
import org.openhealthtools.mdht.uml.cda.consol.ProblemObservation;
import org.openhealthtools.mdht.uml.cda.consol.ProcedureActivityAct;
import org.openhealthtools.mdht.uml.cda.consol.ProcedureActivityObservation;
import org.openhealthtools.mdht.uml.cda.consol.ProductInstance;
import org.openhealthtools.mdht.uml.cda.consol.ReactionObservation;
import org.openhealthtools.mdht.uml.cda.consol.ResultObservation;
import org.openhealthtools.mdht.uml.cda.consol.ResultOrganizer;
import org.openhealthtools.mdht.uml.cda.consol.ServiceDeliveryLocation;
import org.openhealthtools.mdht.uml.cda.consol.VitalSignObservation;
import org.openhealthtools.mdht.uml.hl7.datatypes.CD;
import org.openhealthtools.mdht.uml.hl7.datatypes.ST;

import tr.com.srdc.cda2fhir.transform.entry.IEntityResult;
import tr.com.srdc.cda2fhir.transform.entry.IEntryResult;
import tr.com.srdc.cda2fhir.transform.entry.impl.EntryResult;
import tr.com.srdc.cda2fhir.transform.util.IBundleInfo;
import tr.com.srdc.cda2fhir.transform.util.ITransformationContext;

public interface IResourceTransformer {

	/**
	 * Transforms a CDA AgeObservation instance to a FHIR Age composite datatype
	 * instance
	 *
	 * @param cdaAgeObservation A CDA AgeObservation instance
	 * @param theTransformationContext
     * @return A FHIR Age composite datatype instance
	 */
	Age tAgeObservation2Age(AgeObservation cdaAgeObservation, ITransformationContext theTransformationContext);

	/**
	 * Transforms a CDA AllergyProblemAct instance to a FHIR AllergyIntolerance
	 * resource.
	 *
	 * @param cdaAllergyProblemAct     A CDA AllergyProblemAct instance
	 * @param bundleInfo               A BundleInfo object which acts as a context for
	 *                                 the current transformation bundle.
	 * @param theTransformationContext the local context of the transformation
     * @return An {@link IEntryResult} object which contains FHIR Bundle that with the
	 *         Resource as the first entry, as well as other referenced resources
	 *         and maps.
	 */
	IEntryResult tAllergyProblemAct2AllergyIntolerance(AllergyProblemAct cdaAllergyProblemAct, IBundleInfo bundleInfo, ITransformationContext theTransformationContext);

	/**
	 * Transforms a CDA AssignedAuthor instance to a FHIR Practitioner resource.
	 *
	 * @param cdaAssignedAuthor        A CDA AssignedAuthor instance
	 * @param bundleInfo               A BundleInfo object which acts as a context for the
	 *                                 current transformation bundle.
	 * @param theTransformationContext the local context of the transformation
	 * @return An {@link IEntityResult} object that might include FHIR Bundle and/or a reference to
	 *         a Practitioner
	 */
	IEntityResult tAssignedAuthor2Practitioner(AssignedAuthor cdaAssignedAuthor, IBundleInfo bundleInfo, ITransformationContext theTransformationContext);

	/**
	 * Transforms a CDA AssignedAuthor instance to a FHIR Device resource.
	 *
	 * @param cdaAssignedAuthor        A CDA AssignedAuthor instance
	 * @param bundleInfo               A BundleInfo object which acts as a context for the
	 *                                 current transformation bundle.
	 * @param theTransformationContext the local context of the transformation
     * @return An {@link IEntityResult} object which contains FHIR Bundle that with the
	 *         Resource as the first entry, as well as other referenced resources
	 *         and maps.
	 */
	IEntityResult tAssignedAuthor2Device(AssignedAuthor cdaAssignedAuthor, IBundleInfo bundleInfo, ITransformationContext theTransformationContext);

	/**
	 * Transforms a CDA AssignedEntity instance to a FHIR Practitioner resource.
	 *
	 * @param cdaAssignedEntity        A CDA AssignedEntity instance
	 * @param bundleInfo               A BundleInfo object which acts as a context for the
	 *                                 current transformation bundle.
	 * @param theTransformationContext the local context of the transformation
	 * @return An {@link IEntityResult} object which contains FHIR Bundle that with the
	 *         Resource as the first entry, as well as other referenced resources
	 *         and maps.
	 */
	IEntityResult tAssignedEntity2Practitioner(AssignedEntity cdaAssignedEntity, IBundleInfo bundleInfo, ITransformationContext theTransformationContext);

	/**
	 * Transforms a CDA Author instance to a FHIR Practitioner resource.
	 *
	 * @param cdaAuthor                A CDA Author instance
	 * @param bundleInfo               A BundleInfo object which acts as a context for the current
	 *                                 transformation bundle.
	 * @param theTransformationContext the local context of the transformation
	 * @return An {@link IEntityResult} object which contains FHIR Bundle that with the
	 *         Resource as the first entry, as well as other referenced resources
	 *         and maps.
	 */
	IEntityResult tAuthor2Practitioner(Author cdaAuthor, IBundleInfo bundleInfo, ITransformationContext theTransformationContext);

	/**
	 * Transforms a CDA CD instance to a FHIR Substance resource.
	 *
	 * @param cdaSubstanceCode         A CDA CD instance
	 * @param theTransformationContext the local context of the transformation
	 * @return A FHIR Substance resource
	 */
	Substance tCD2Substance(CD cdaSubstanceCode, ITransformationContext theTransformationContext);

	/**
	 * Transforms a CDA ClicinalDocument instance to a FHIR Composition resource.
	 *
	 * @param cdaClinicalDocument      A CDA ClicinalDocument instance
	 * @param theTransformationContext the local context of the transformation
	 * @return An {@link IEntryResult} object which contains FHIR Bundle that with the
	 *         Resource as the first entry, as well as other referenced resources
	 *         and maps.
	 */
	IEntryResult tClinicalDocument2Composition(ClinicalDocument cdaClinicalDocument, ITransformationContext theTransformationContext);

	/**
	 * Transforms a CDA ClicinalDocument instance to a FHIR Bundle resource.
	 *
	 * @param cdaClinicalDocument      A CDA ClicinalDocument instance
	 * @param includeComposition       Include composition resource or not
	 * @param theTransformationContext the local context of the transformation
	 * @return An {@link IEntryResult} object which contains FHIR Bundle that with the
	 *         Resource as the first entry, as well as other referenced resources
	 *         and maps.
	 */

	IEntryResult tClinicalDocument2Bundle(ClinicalDocument cdaClinicalDocument, boolean includeComposition, ITransformationContext theTransformationContext);

	/**
	 * Transforms a CDA CustodianOrganization instance to a FHIR Organization
	 * resource.
	 *
	 * @param cdaCustodianOrganization A CDA CustodianOrganization instance
	 * @param bundleInfo               A BundleInfo object which acts as a context
	 *                                 for the current transformation bundle.
	 * @param theTransformationContext the local context of the transformation
	 * @return An {@link IEntryResult} object which contains FHIR Bundle that with the
	 *         Resource as the first entry, as well as other referenced resources
	 *         and maps.
	 */
	IEntryResult tCustodianOrganization2Organization(
			CustodianOrganization cdaCustodianOrganization, IBundleInfo bundleInfo, ITransformationContext theTransformationContext);

	/**
	 * Transforms a CDA EncounterActivity instance to a FHIR Encounter resource.
	 *
	 * @param cdaEncounterActivity     A CDA EncounterActivity instance
	 * @param bundleInfo               A BundleInfo object which acts as a context for
	 *                                 the current transformation bundle.
	 * @param theTransformationContext the local context of the transformation
	 * @return An {@link IEntryResult} object which contains FHIR Bundle that with the
	 *         Resource as the first entry, as well as other referenced resources
	 *         and maps.
	 */
	IEntryResult tEncounterActivity2Encounter(
			EncounterActivities cdaEncounterActivity, IBundleInfo bundleInfo, ITransformationContext theTransformationContext);

	/**
	 * Transforms a CDA Entity instance to a FHIR Group resource.
	 *
	 * @param cdaEntity A CDA Entity instance
	 * @return A FHIR Group resource
	 */
	Group tEntity2Group(Entity cdaEntity);

	/**
	 * Transforms a CDA FamilyHistoryOrganizer instance to a FHIR
	 * FamilyMemberHistory resource.
	 *
	 * @param cdaFamilyHistoryOrganizer A CDA FamilyHistoryOrganizer instance
	 * @param theTransformationContext  the local context of the transformation
	 * @return A FHIR FamilyMemberHistory resource
	 */
	FamilyMemberHistory tFamilyHistoryOrganizer2FamilyMemberHistory(FamilyHistoryOrganizer cdaFamilyHistoryOrganizer, ITransformationContext theTransformationContext);

	/**
	 * Transforms a CDA Observation instance that is included in Functional Status
	 * Section to a FHIR Observation resource.
	 *
	 * @param cdaObservation           A CDA Observation instance that is included in
	 *                                 Functional Status Section
	 * @param bundleInfo               A BundleInfo object which acts as a context for the
	 *                                 current transformation bundle.
	 * @param theTransformationContext the local context of the transformation
     * @return An {@link IEntryResult} object which contains FHIR Bundle that with the
	 *         Resource as the first entry, as well as other referenced resources
	 *         and maps.
	 */
	IEntryResult tFunctionalStatus2Observation(Observation cdaObservation,
                                               IBundleInfo bundleInfo, ITransformationContext theTransformationContext);

	/**
	 * Transforms a CDA Guardian instance to a FHIR Patient.Contact resource.
	 *
	 * @param cdaGuardian A CDA Guardian instance
	 * @param theTransformationContext
     * @return A FHIR Patient.Contact resource
	 */
	org.hl7.fhir.r4.model.Patient.ContactComponent tGuardian2Contact(
            Guardian cdaGuardian, ITransformationContext theTransformationContext);

	/**
	 * Transforms a CDA ImmunizationActivity instance to a FHIR Immunization
	 * resource.
	 *
	 * @param cdaImmunizationActivity  A CDA ImmunizationActivity instance
	 * @param theTransformationContext the local context of the transformation
	 * @return An {@link IEntryResult} object which contains FHIR Bundle that with the
	 *         Resource as the first entry, as well as other referenced resources
	 *         and maps.
	 */
	IEntryResult tImmunizationActivity2Immunization(ImmunizationActivity cdaImmunizationActivity,
													IBundleInfo bundleInfo, ITransformationContext theTransformationContext);

	/**
	 * Transforms a CDA LanguageCommunication instance to a FHIR Communication
	 * resource.
	 *
	 * @param cdaLanguageCommunication A CDA LanguageCommunication instance
	 * @return A FHIR Communication resource
	 */
	PatientCommunicationComponent tLanguageCommunication2Communication(LanguageCommunication cdaLanguageCommunication);

	/**
	 * Transforms a CDA ManufacturedProduct instance to a FHIR Medication resource.
	 *
	 * @param cdaManufacturedProduct   A CDA ManufacturedProduct instance
	 * @param theTransformationContext the local context of the transformation
	 * @return An {@link IEntryResult} object which contains FHIR Bundle that with the
	 *         Resource as the first entry, as well as other referenced resources
	 *         and maps.
	 */
	IEntryResult tManufacturedProduct2Medication(ManufacturedProduct cdaManufacturedProduct, IBundleInfo bundleInfo, ITransformationContext theTransformationContext);

	/**
	 * Transforms a CDA MedicationActivity instance to a FHIR MedicationStatement
	 * resource.
	 *
	 * @param cdaMedicationActivity    A CDA MedicationActivity instance
	 * @param bundleInfo               A BundleInfo object which acts as a context for
	 *                                 the current transformation bundle.
	 * @param theTransformationContext the local context of the transformation
	 * @return An {@link IEntryResult} object which contains FHIR Bundle that with the
	 *         Resource as the first entry, as well as other referenced resources
	 *         and maps.
	 */
	IEntryResult tMedicationActivity2MedicationStatement(MedicationActivity cdaMedicationActivity,
														 IBundleInfo bundleInfo, ITransformationContext theTransformationContext);

	/**
	 * Transforms a CDA MedicationDispense instance to a FHIR MedicationDispense
	 * resource.
	 *
	 * @param cdaMedicationDispense    A CDA MedicationDispense instance
	 * @param bundleInfo               A BundleInfo object which acts as a context for
	 *                                 the current transformation bundle.
	 * @param theTransformationContext the local context of the transformation
     * @return An {@link IEntryResult} object which contains FHIR Bundle that with the
	 *         Resource as the first entry, as well as other referenced resources
	 *         and maps.
	 */
	IEntryResult tMedicationDispense2MedicationDispense(
            MedicationDispense cdaMedicationDispense, IBundleInfo bundleInfo, ITransformationContext theTransformationContext);

	/**
	 * Transforms a CDA MedicationInformation instance to a FHIR Medication
	 * resource.
	 *
	 * @param cdaMedicationInformation A CDA MedicationInformation instance
	 * @param theTransformationContext the local context of the transformation
	 * @return An {@link IEntryResult} object which contains FHIR Bundle that with the
	 *         Resource as the first entry, as well as other referenced resources
	 *         and maps.
	 */
	IEntryResult tMedicationInformation2Medication(MedicationInformation cdaMedicationInformation,
												   IBundleInfo bundleInfo, ITransformationContext theTransformationContext);

	/**
	 * Transforms a CDA Observation instance to a FHIR Observation resource.
	 *
	 * @param cdaObservation           A CDA Observation instance
	 * @param theTransformationContext the local context of the transformation
     * @return An {@link IEntryResult} object which contains FHIR Bundle that with the
	 *         Resource as the first entry, as well as other referenced resources
	 *         and maps.
	 */
	IEntryResult tObservation2Observation(Observation cdaObservation,
                                          IBundleInfo bundleInfo, ITransformationContext theTransformationContext);

	/**
	 * Transforms a CDA Organization instance to a FHIR Organization resource.
	 *
	 * @param cdaOrganization          A CDA Organization instance
	 * @param bundleInfo               A BundleInfo object which acts as a context for the
	 *                                 current transformation bundle.
	 * @param theTransformationContext the local context of the transformation
	 * @return An {@link IEntryResult} object which contains FHIR Bundle that with the
	 *         Resource as the first entry, as well as other referenced resources
	 *         and maps.
	 *
	 */
	IEntryResult tOrganization2Organization(Organization cdaOrganization,
											IBundleInfo bundleInfo, ITransformationContext theTransformationContext);

	/**
	 * Transforms a CDA ParticipantRole instance to a FHIR Location resource.
	 *
	 * @param cdaParticipantRole       A CDA ParticipantRole instance
	 * @param theBundleInfo            A BundleInfo object which acts as a context
	 * @param theTransformationContext the local context of the transformation
	 * @return An {@link IEntryResult} object which contains FHIR Bundle that with the
	 *         Resource as the first entry, as well as other referenced resources
	 *         and maps.
	 */
	IEntryResult tParticipantRole2Location(ParticipantRole cdaParticipantRole, IBundleInfo theBundleInfo, ITransformationContext theTransformationContext);

	/**
	 * Transforms a CDA PatientRole instance to a FHIR Patient resource.
	 *
	 * @param cdaPatientRole           A CDA PatientRole instance * @param bundleInfo A
	 *                                 BundleInfo object which acts as a context for the
	 *                                 current transformation bundle.
	 * @param theTransformationContext the local context of the transformation
	 * @return An {@link IEntryResult} object which contains FHIR Bundle that with the
	 *         Resource as the first entry, as well as other referenced resources
	 *         and maps.
	 */
	IEntryResult tPatientRole2Patient(PatientRole cdaPatientRole, IBundleInfo bundleInfo, ITransformationContext theTransformationContext);

	/**
	 * Transforms a CDA Performer2 instance to a FHIR Practitioner resource.
	 *
	 * @param cdaPerformer2            A CDA Performer2 instance
	 * @param bundleInfo               A BundleInfo object which acts as a context for the
	 *                                 current transformation bundle.
	 * @param theTransformationContext the local context of the transformation
	 * @return An {@link IEntityResult} object which contains FHIR Bundle that with the
	 *         Resource as the first entry, as well as other referenced resources.
	 */
	IEntityResult tPerformer22Practitioner(Performer2 cdaPerformer2, IBundleInfo bundleInfo, ITransformationContext theTransformationContext);

	/**
	 * Transforms a CDA ProblemConcernAct instance to FHIR Condition resource(s). A
	 * ProblemConcernAct might include several Problem Observations, and each
	 * Problem Observation corresponds to a FHIR Condition. Therefore, the returning
	 * Bundle can contain several FHIR Conditions.
	 *
	 * @param cdaProblemConcernAct     A CDA ProblemConcernAct instance
	 * @param bundleInfo               A BundleInfo object which acts as a context for
	 *                                 the current transformation bundle.
	 * @param theTransformationContext the local context of the transformation
	 * @return An {@link IEntryResult} object which contains FHIR Bundle that with the
	 *         Resource as the first entry, as well as other referenced resources.
	 */
	IEntryResult tProblemConcernAct2Condition(ProblemConcernAct cdaProblemConcernAct, IBundleInfo bundleInfo, ITransformationContext theTransformationContext);

	/**
	 * Transforms a CDA ProblemObservation instance to FHIR Condition resource.
	 *
	 * @param cdaProbObs               A CDA ProblemObservation instance
	 * @param bundleInfo               A BundleInfo object which acts as a context for the current
	 *                                 transformation bundle.
	 * @param theTransformationContext the local context of the transformation
	 * @return An {@link IEntryResult} object which contains FHIR Bundle that with the
	 *         Condition as the first entry, as well as other referenced resources
	 *         such as Encounter, Practitioner
	 */
	IEntryResult tProblemObservation2Condition(ProblemObservation cdaProbObs, IBundleInfo bundleInfo, ITransformationContext theTransformationContext);

	/**
	 * Transforms a CDA Procedure instance to a FHIR Procedure and supporting
	 * resources.
	 *
	 * @param cdaProcedure             A CDA Procedure instance
	 * @param bundleInfo               A BundleInfo object which acts as a context for
	 * 	                               the current transformation bundle.
	 * @param theTransformationContext the local context of the transformation
	 * @return An {@link IEntryResult} object which contains FHIR Bundle that with the
	 *         Resource as the first entry, as well as other referenced resources.
	 */
	IEntryResult tProcedure2Procedure(Procedure cdaProcedure, IBundleInfo bundleInfo, ITransformationContext theTransformationContext);

	/**
	 * Transforms a CDA Procedure Activity Act instance to a FHIR Procedure and supporting
	 * resources.
	 *
	 * @param cdaProcedure             A CDA Procedure Activity Act instance
	 * @param bundleInfo               A BundleInfo object which acts as a context for
	 * 	                               the current transformation bundle.
	 * @param theTransformationContext the local context of the transformation
	 * @return An {@link IEntryResult} object which contains FHIR Bundle that with the
	 *         Resource as the first entry, as well as other referenced resources.
	 */
	IEntryResult tProcedureActivityAct2Procedure(ProcedureActivityAct cdaProcedure, IBundleInfo bundleInfo, ITransformationContext theTransformationContext);

	/**
	 * Transforms a CDA Procedure Activity Observation instance to a FHIR Observation resource.
	 *
	 * @param cdaObservation           A CDA Procedure Activity Observation instance
	 * @param theTransformationContext the local context of the transformation
	 * @return An {@link IEntryResult} object which contains FHIR Bundle that with the
	 *         Resource as the first entry, as well as other referenced resources
	 *         and maps.
	 */
	IEntryResult tProcedureActivityObservation2Observation(ProcedureActivityObservation cdaObservation,
														   IBundleInfo bundleInfo, ITransformationContext theTransformationContext);

	/**
	 * Transforms a CDA ReactionObservation instance to a FHIR Observation resource.
	 *
	 * @param cdaReactionObservation   A CDA ReactionObservation instance
	 * @param bundleInfo               A BundleInfo object which acts as a context for
	 *                                 the current transformation bundle.
	 * @param theTransformationContext the local context of the transformation
	 * @return An {@link IEntryResult} object which contains FHIR Bundle that with the
	 *         Resource as the first entry, as well as other referenced resources
	 *         and maps.
	 */
	IEntryResult tReactionObservation2Observation(ReactionObservation cdaReactionObservation, IBundleInfo bundleInfo, ITransformationContext theTransformationContext);

	/**
	 * Transforms a CDA ReactionObservation instance to a FHIR Observation resource.
	 *
	 * @param cdaReactionObservation   A CDA ReactionObservation instance
	 * @param bundleInfo               A BundleInfo object which acts as a context for
	 *                                 the current transformation bundle.
	 * @param theTransformationContext the local context of the transformation
     * @return An {@link IEntryResult} object which contains FHIR Bundle that with the
	 *         Resource as the first entry, as well as other referenced resources
	 *         and maps.
	 */
	IEntryResult tReactionObservation2AdverseEvent(ReactionObservation cdaReactionObservation, IBundleInfo bundleInfo, ITransformationContext theTransformationContext);

	/**
	 * Transforms a CDA ReferenceRange instance to a FHIR
	 * ObservationReferenceRangeComponent resource.
	 *
	 * @param cdaReferenceRange A CDA ReferenceRange instance
	 * @return A FHIR ObservationReferenceRangeComponent resource
	 */
	ObservationReferenceRangeComponent tReferenceRange2ReferenceRange(
			org.openhealthtools.mdht.uml.cda.ReferenceRange cdaReferenceRange);

	/**
	 * Transforms a CDA ResultObservation instance to a FHIR Observation resource.
	 *
	 * @param cdaResultObservation     A CDA ResultObservation instance
	 * @param theTransformationContext the local context of the transformation
	 * @return An {@link IEntryResult} object which contains FHIR Bundle that with the
	 *         Resource as the first entry, as well as other referenced resources
	 *         and maps.
	 */
	IEntryResult tResultObservation2Observation(ResultObservation cdaResultObservation, IBundleInfo bundleInfo, ITransformationContext theTransformationContext);

	/**
	 * Transforms a CDA ResultOrganizer instance to a FHIR DiagnosticReport
	 * resource.
	 *
	 * @param cdaResultOrganizer       A CDA ResultOrganizer instance
	 * @param bundleInfo               A BundleInfo object which acts as a context for the
	 *                                 current transformation bundle.
	 * @param theTransformationContext the local context of the transformation
	 * @return An {@link IEntryResult} object which contains FHIR Bundle that with the
	 *         Resource as the first entry, as well as other referenced resources
	 *         and maps.
	 */
	IEntryResult tResultOrganizer2DiagnosticReport(ResultOrganizer cdaResultOrganizer, IBundleInfo bundleInfo, ITransformationContext theTransformationContext);

	/**
	 * Transforms a CDA Section instance to a FHIR SectionComponent resource.
	 *
	 * @param cdaSection A CDA Section instance
	 * @return A FHIR SectionComponent resource
	 */
	SectionComponent tSection2Section(Section cdaSection);

	/**
	 * Transforms a CDA ServiceDeliveryLocation instance to a FHIR Location
	 * resource.
	 *
	 * @param cdaSDLOC                 A CDA ServiceDeliveryLocation instance
	 * @param theBundleInfo            A BundleInfo object which acts as a context
	 * @param theTransformationContext the local context of the transformation
	 * @return An {@link IEntryResult} object which contains FHIR Bundle that with the
	 * Resource as the first entry, as well as other referenced resources
	 * and maps.
	 */
	IEntryResult tServiceDeliveryLocation2Location(ServiceDeliveryLocation cdaSDLOC, IBundleInfo theBundleInfo, ITransformationContext theTransformationContext);

	/**
	 * Transforms a CDA Observation instance to a FHIR Observation resource. Handles the special cases that occur in
	 * the context of the Social History section
	 *
	 * @param cdaObservation           A CDA Observation instance
	 * @param bundleInfo               A BundleInfo object which acts as a context
	 *                                 for the current transformation bundle.
	 * @param theTransformationContext the local context of the transformation
	 * @return An {@link IEntryResult} object which contains FHIR Bundle that with the
	 *         Resource as the first entry, as well as other referenced resources
	 *         and maps.
	 */
	IEntryResult tSocialHistoryObservation2Observation(org.openhealthtools.mdht.uml.cda.Observation cdaObservation,
										  IBundleInfo bundleInfo, ITransformationContext theTransformationContext);

	/**
	 * Transforms a CDA Supply instance to a FHIR Device resource.
	 *
	 * @param cdaSupply                A CDA Supply instance
	 * @param theTransformationContext the local context of the transformation
	 * @return A FHIR Device resource
	 */
	org.hl7.fhir.r4.model.Device tSupply2Device(Supply cdaSupply, ITransformationContext theTransformationContext);

	/**
	 * Transforms a CDA VitalSignObservation to a FHIR Observation resource.
	 *
	 * @param cdaVitalSignObservation  A CDA VitalSignObservation instance
	 * @param bundleInfo               A BundleInfo object which acts as a context
	 *                                 for the current transformation bundle.
	 * @param theTransformationContext the local context of the transformation
	 * @return An {@link IEntryResult} object containing a FHIR Bundle with the Observation
	 *         as the first entry, which can also include other referenced resources
	 *         such as Encounter, Practitioner
	 */
	IEntryResult tVitalSignObservation2Observation(VitalSignObservation cdaVitalSignObservation,
												   IBundleInfo bundleInfo, ITransformationContext theTransformationContext);

	/**
	 *
	 * @param resource A fhir resource for which a reference is needed
	 * @return a reference to the fhir resource parameter
	 */
	Reference getReference(Resource resource);

	/**
	 *
	 * Transforms a CDA MedicationSupplyOrder to a FHIR MedicationRequest
	 * resource.**
	 *
	 * @param supply                   A CDA MedicationSupplyOrder instance*
	 *
	 * @param theTransformationContext the local context of the transformation
	 * @return An {@link IEntryResult} object which contains FHIR Bundle that with the
	 *         Resource as the first entry, as well as other referenced resources
	 *         and maps.
	 **/

	IEntryResult medicationSupplyOrder2MedicationRequest(MedicationSupplyOrder supply, IBundleInfo bundleInfo, ITransformationContext theTransformationContext);

	/**
	 * Provides a provenance file to store the targeted references.
	 *
	 * @param bundle                   The built bundle, needed to parse for references.
	 * @param encodedBody              A string with the encoded document body.
	 * @param assemblerDevice          An Identifier of the original assembling device.
	 * @param theTransformationContext the local context of the transformation
	 * @return Bundle, updated with a provenance object, and entries for binary and
	 *         device.
	 */
	Bundle tProvenance(Bundle bundle, String encodedBody, Identifier assemblerDevice, ITransformationContext theTransformationContext);

	/**
	 * Creates a FHIR Condition object of category problem-list-item from a CDA
	 * Indication object.
	 *
	 * @param cdaIndication            a CDA Indication object
	 * @param bundleInfo               A BundleInfo object which acts as a context for the
	 *                                 current transformation.
	 * @param theTransformationContext the local context of the transformation
	 * @return An {@link IEntryResult} object which contains FHIR Bundle that with the
	 *         Resource as the first entry, as well as other referenced resources
	 *         and maps.
	 */
	IEntryResult tIndication2ConditionProblemListItem(Indication cdaIndication, IBundleInfo bundleInfo, ITransformationContext theTransformationContext);

	/**
	 * Creates a FHIR Condition object of category encounter-diagnosis from a CDA
	 * Indication object.
	 *
	 * @param cdaIndication            a CDA Indication object
	 *
	 * @param theTransformationContext the local context of the transformation
     * @return An {@link IEntryResult} object which contains FHIR Bundle that with the
	 *         Resource as the first entry, as well as other referenced resources
	 *         and maps.
	 */
	IEntryResult tIndication2ConditionEncounter(Indication cdaIndication, IBundleInfo bundleInfo, ITransformationContext theTransformationContext);

	/**
	 * Turns a CDA manufactured product object into a FHIR medication.
	 *
	 * @param cdaProduct               CDA Product object.
	 * @param bundleInfo               A BundleInfo object which acts as a context for the current
	 *                                 transformation.
	 * @param theTransformationContext the local context of the transformation
	 * @return An {@link IEntryResult} that contains a FHIR Bundle with the Medication,
	 *         which can also include other referenced resources such as
	 *         Practitioner.
	 */
	IEntryResult tManufacturedProduct2Medication(Product cdaProduct, IBundleInfo bundleInfo, ITransformationContext theTransformationContext);

	/**
	 * Turns a CDA manufactured product object into a FHIR medication.
	 *
	 * @param cdaConsumable            CDA Manufactured Product object.
	 * @param bundleInfo               A BundleInfo object which acts as a context for the current
	 *                                 transformation.
	 * @param theTransformationContext the local context of the transformation
	 * @return An {@link IEntryResult} that contains a FHIR Bundle with the Medication,
	 *         which can also include other referenced resources such as
	 *         Practitioner.
	 */
	IEntryResult tManufacturedProduct2Medication(Consumable cdaConsumable, IBundleInfo bundleInfo, ITransformationContext theTransformationContext);

	/**
	 * Turns a CDA Nutritional Status observation into a FHIR Observation
	 * @param cdaObservation           CDA Nutritional Status Observation object
	 * @param bundleInfo               A BundleInfo object which acts as a context for the current
	 *                                 transformation.
	 * @param theTransformationContext the local context of the transformation
	 * @return An {@link IEntryResult} that contains a FHIR Bundle with the Observation,
	 * 	       which can also include other referenced resources such as
	 *         Practitioner.
	 */
	IEntryResult tNutritionalStatusObservation2Observation(Observation cdaObservation, IBundleInfo bundleInfo, ITransformationContext theTransformationContext);

	/**
	 * Transforms a CDA participant of the note section to a FHIR PractitionerRole resource
	 *
	 * @param theCdaParticipant        the participant of the note section
	 * @param theTransformationContext the local context of the transformation
	 * @return A FHIR PractitionerRole resource
	 */
	PractitionerRole tParticipant2PractitionerRole(Participant2 theCdaParticipant, ITransformationContext theTransformationContext);
	
	/**
	 * Transforms a CDA participant of the note section to a FHIR Practitioner resource with name of the Practitioner
	 *
	 * @param theCdaParticipant        the participant of the note section
	 * @param theTransformationContext the local context of the transformation
	 * @return A FHIR Practitioner resource
	 */
	Practitioner tParticipant2Practitioner(Participant2 theCdaParticipant, ITransformationContext theTransformationContext);

	/**
	 * Turns a CDA Consultation note section into a FHIR DocumentReference resource
	 * @param theStrucDocText          the 'text' part of the note section
	 * @param theAct                   the 'act' part of the note section
	 * @param theBundleInfo            a BundleInfo object which acts as a context for the current
	 *                                 transformation.
	 * @param theTransformationContext the local context of the transformation
	 * @return An {@link IEntryResult} that contains a FHIR Bundle with DocumentReference resource
	 */
	IEntryResult tAct2DocumentReference(StrucDocText theStrucDocText, Act theAct, IBundleInfo theBundleInfo, ITransformationContext theTransformationContext);

	/**
	 * Turns a CDA Assessment section into a FHIR ClinicalImpression resource
	 *
	 * @param theSection               the Assessment section itself
	 * @param theTransformationContext the local context of the transformation
	 * @return An {@link IEntryResult} that contains a FHIR Bundle with the Observation,
	 * which can also include other referenced resources such as
	 * DocumentReference
	 */
	IEntryResult tAssessment2ClinicalImpression(Section theSection, ITransformationContext theTransformationContext);

	/**
	 * Turns a CDA Goal Observation section into a FHIR Goal resource
	 * 
	 * @param theCdaObs                the CDA Observation
	 * @param theBundleInfo            a BundleInfo object which acts as a context for the current
	 *                                 transformation.
	 * @param theTransformationContext the local context of the transformation
	 * @return An {@link IEntryResult} that contains a FHIR Bundle with Goal resource
	 */
	IEntryResult tGoalObservation2Goal(org.openhealthtools.mdht.uml.cda.Observation theCdaObs, IBundleInfo theBundleInfo, ITransformationContext theTransformationContext);

	/**
	 * Turns a CDA Plan of Care section into a FHIR CarePlan and ServiceRequest resources
	 * 
	 * @param theTitle                    the 'title' of CDA Plan of Care section 
	 * @param theStrucDocText             the 'text' of  CDA Plan of Care section 
	 * @param theActs                     the 'act' of CDA Plan of Care section 
	 * @param theBundleInfo               a BundleInfo object which acts as a context for the current transformation.
	 * @param theTransformationContext    the local context of the transformation
	 * @return An {@link IEntryResult} that contains a FHIR Bundle with CarePlan resource
	 */
	IEntryResult tPlanOfCareSection2CarePlan(ST theTitle, StrucDocText theStrucDocText, List<PlanOfCareActivityAct> theActs, IBundleInfo theBundleInfo, ITransformationContext theTransformationContext);
	
	/**
	 * Transforms a CDA acts of CDA Plan of Care section to a FHIR ServiceRequest resource
	 * 
	 * @param theAct                      the 'act' of CDA Plan of Care section
	 * @param thefhirCarePlan             the FHIR PlanCare resource
	 * @param theResult                   An {@link IEntryResult} that contains a FHIR Bundle with CarePlan resource
	 * @param theBundleInfo               a BundleInfo object which acts as a context for the current transformation.
	 * @param theTransformationContext    the local context of the transformation
	 * @return                            The result of transformation from Act to ServiceRequest
	 */
	ServiceRequest tPlanOfCareActivityAct2ServiceRequest(PlanOfCareActivityAct theAct, CarePlan thefhirCarePlan, EntryResult theResult, IBundleInfo theBundleInfo, ITransformationContext theTransformationContext);


	/**
	 * Transforms a CDA ProductInstance entry to a FHIR Device resource
	 *
	 * @param theProductInstance       A ProductInstance entry from a CDA document
	 * @param theBundleInfo            A BundleInfo object which acts as a context for the current transformation
	 * @param theTransformationContext The local context of the transformation
	 * @return The result of the transformation from ProductInstance to Device
	 */
	IEntryResult tProductInstance2Device(ProductInstance theProductInstance, IBundleInfo theBundleInfo, ITransformationContext theTransformationContext);

	/**
	 * Transforms a CDA Observation of CDA Health Concern section to a FHIR Observation resource
	 * 
	 * @param theCdaObs                   the CDA Observation
	 * @param theBundleInfo               a BundleInfo object which acts as a context for the current transformation.
	 * @param theTransformationContext    the local context of the transformation
	 * @return                            The result of transformation from CDA Observation to FHIR Observation
	 */
	IEntryResult tHealthConcernObservation2Observation(org.openhealthtools.mdht.uml.cda.Observation theCdaObs, IBundleInfo theBundleInfo, ITransformationContext theTransformationContext);
		
	/**
	 * Transforms a CDA ACT of CDA Health Concern section to a FHIR Condition resource
	 * 
	 * @param theAct                      the 'act' of CDA Health Concern section 
	 * @param theBundleInfo               a BundleInfo object which acts as a context for the current transformation.
	 * @param theTransformationContext    the local context of the transformation
	 * @return                            The result of transformation from CDA Observation to FHIR Observation
	 */
	IEntryResult tHealthConcernAct2Condition(Act theAct, IBundleInfo theBundleInfo, ITransformationContext theTransformationContext);

	IEntryResult tOrganizer2CareTeam(StrucDocText theStrucDocText, Organizer theOrganizer, IBundleInfo theBundleInfo, ITransformationContext theTransformationContext);

	/**
	 * Transforms a CDA Medication Activity to a FHIR MedicationRequest resource
	 *
	 * @param theMedActivity 			 The Medication Activity of a Medications section
	 * @param theBundleInfo				 A BundleInfo object which acts as the context for the current transformation
	 * @param theTransformationContext	 The local context of the transformation
	 * @return							 The result of the transformation from CDA Medication Activity to FHIR MedicationRequest
	 */
	IEntryResult tMedicationActivity2MedicationRequest(MedicationActivity theMedActivity, IBundleInfo theBundleInfo, ITransformationContext theTransformationContext);
}
