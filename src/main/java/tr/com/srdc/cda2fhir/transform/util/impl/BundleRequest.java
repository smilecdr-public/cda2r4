package tr.com.srdc.cda2fhir.transform.util.impl;

import java.util.List;
import java.util.ResourceBundle;

import ca.uhn.fhir.util.UrlUtil;
import org.hl7.fhir.r4.model.Base;
import org.hl7.fhir.r4.model.Bundle.BundleEntryComponent;
import org.hl7.fhir.r4.model.Bundle.BundleEntryRequestComponent;
import org.hl7.fhir.r4.model.Bundle.HTTPVerb;
import org.hl7.fhir.r4.model.CodeableConcept;
import org.hl7.fhir.r4.model.Coding;
import org.hl7.fhir.r4.model.Identifier;
import org.hl7.fhir.r4.model.PractitionerRole;
import org.hl7.fhir.r4.model.Property;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import tr.com.srdc.cda2fhir.conf.Config;

public class BundleRequest {

	private final static Logger logger = LoggerFactory.getLogger(BundleRequest.class);

	/**
	 * Reads the configuration file, and parses out any overriding OIDs.
	 *
	 * @param resourceType - the resource name of the OID
	 * @return Array of OID strings for use.
	 */
	private static String[] getDefinedOIDs(String resourceType) {

		String[] splitOIDs = null;
		try {
			final ResourceBundle rb = ResourceBundle.getBundle("config");
			String newOIDs = rb.getString(resourceType);
			splitOIDs = newOIDs.split(",");
		} catch (Exception e) {
			// do not log exception.
		}
		return splitOIDs;
	}

	/**
	 * Takes a bundle entry and generates the ifNotExists String.
	 *
	 * @param bundleEntry
	 * @return String for application on request.
	 */
	public static String generateIfNoneExist(BundleEntryComponent bundleEntry) {

		String ifNotExistString = "";

		// all ifNoneExist strings attempt to start with an identifier.
		Property identifierObject = bundleEntry.getResource().getNamedProperty("identifier");

		// find any overriding OIDs.
		String[] subsetOIDArray = getDefinedOIDs(bundleEntry.getResource().getResourceType().name());

		// Medication and PractitionerRole have special handling below
		// AdverseEvent does not support the "identifier" search parameter in R4
		if (bundleEntry.getResource().getResourceType().name() != "Medication"
				&& bundleEntry.getResource().getResourceType().name() != "PractitionerRole"
				&& bundleEntry.getResource().getResourceType().name() != "AdverseEvent") {
			if (identifierObject != null) {
				List<Base> identifiers = identifierObject.getValues();
				if (identifiers != null) {
					for (Base identifier : identifiers) {

						Identifier currentId = (Identifier) identifier;

						if (currentId.getValue() != null) {
							if (subsetOIDArray != null) {
								// override OID selection(s).
								for (String OID : subsetOIDArray) {
									if (currentId.getSystem().equals(OID)) {
										if (ifNotExistString != "") {
											ifNotExistString = ifNotExistString + ",";
										} else {
											ifNotExistString = "identifier=";
										}
										ifNotExistString = ifNotExistString + currentId.getSystem() + "|"
												+ fullyEscapeValue(currentId.getValue());
									}
								}
							} else {
								if (ifNotExistString != "") {
									ifNotExistString = ifNotExistString + ",";
								} else {
									ifNotExistString = "identifier=";
								}
								if (currentId.getSystem() != null) {
									ifNotExistString = ifNotExistString + currentId.getSystem() + "|"
											+ fullyEscapeValue(currentId.getValue());
								} else {
									ifNotExistString = ifNotExistString + currentId.getValue();
								}
							}
						}
					}
				}
			}
		}

		// if we can't pull an identifier, try other logic.
		if (ifNotExistString == "") {
			// if it's a medication, check by aggregate encodings.
			if (bundleEntry.getResource().getResourceType().name() == "Medication") {
				Property medicationCode = bundleEntry.getResource().getChildByName("code");
				if (medicationCode != null) {
					List<Base> conceptList = medicationCode.getValues();
					for (Base concept : conceptList) {
						CodeableConcept currentConcept = (CodeableConcept) concept;
						List<Coding> currentCoding = currentConcept.getCoding();
						for (Coding coding : currentCoding) {

							if (Config.MEDICATION_CODE_SYSTEM != null) {
								if (coding.getCode() != null) {
									if (coding.getSystem() == Config.MEDICATION_CODE_SYSTEM) {
										// add or for multiple parameters
										if (ifNotExistString != "") {
											ifNotExistString = ifNotExistString + "&";
										} else {
											ifNotExistString = "code=";
										}
										ifNotExistString = ifNotExistString + coding.getSystem() + "|"
												+ coding.getCode();
									}
								}
							} else {
								if (coding.getCode() != null) {
									// add or for multiple parameters
									if (ifNotExistString != "") {
										ifNotExistString = ifNotExistString + "&";
									} else {
										ifNotExistString = "code=";
									}
									ifNotExistString = ifNotExistString + coding.getSystem() + "|" + coding.getCode();
								}
							}
						}
					}
				}
			}
			if (bundleEntry.getResource().getResourceType().name().equals("Organization")) {
				Property orgNames = bundleEntry.getResource().getChildByName("name");
				if (orgNames != null) {
					List<Base> nameList = orgNames.getValues();
					for(Base entry: nameList) {
						String name = entry.primitiveValue();
						if(name != null && !name.isEmpty()) {
							if (!ifNotExistString.equals("")) {
								ifNotExistString = ifNotExistString + ",";
							} else {
								// we need to explicitly include the resource type here, because the use of a qualifier
								// suppresses hapi-fhir's ability to do it for us
								ifNotExistString = "Organization?name:exact=";
							}
							ifNotExistString = ifNotExistString + fullyEscapeValue(name);
						}
						
					}
				}
			}
			

			// if it's a practitioner role, de-duplicate by reference ids.
			if (bundleEntry.getResource().getResourceType().name() == "PractitionerRole") {
				PractitionerRole practitionerRole = (PractitionerRole) bundleEntry.getResource();
				// TODO: Three problems in the original code
				//   1) The call to getIdentifierFirstRep() is guaranteed never to return null, although it may return
				//      an empty object. Easy enough to fix.
				//   2) If there are identifiers available for the practitioner and the organization, the code produces
				//      these chained match queries, which SmileCDR does not currently know how to consume. Maybe we
				//      should, though, but it's a non-trivial fix
				//   3) What about the case where only one of practitioner or organization has an id?
				if (practitionerRole.getPractitionerTarget().hasIdentifier() && practitionerRole.getOrganizationTarget().hasIdentifier()) {
					Identifier practitionerIdentifier = practitionerRole.getPractitionerTarget().getIdentifierFirstRep();
					Identifier organizationIdentifier = practitionerRole.getOrganizationTarget().getIdentifierFirstRep();
					ifNotExistString = "practitioner.identifier=" + practitionerIdentifier.getSystem() + "|"
							+ fullyEscapeValue(practitionerIdentifier.getValue());
					ifNotExistString = ifNotExistString + "&" + "organization.identifier="
							+ organizationIdentifier.getSystem() + "|" + fullyEscapeValue(organizationIdentifier.getValue());
				}
			}

		}

		return ifNotExistString;

	}

	private static String fullyEscapeValue(String theValue) {
		// First, escape the characters '$' ',' and '|' which have special meaning in a FHIR search string
		String fhirSearchEscapedValue = theValue.replaceAll("([$,|])", "\\\\$1");

		// Then URL-escape the resulting string to handle any other special characters
		return UrlUtil.escapeUrlParam(fhirSearchEscapedValue);
	}

	/**
	 * Adds request field to the entry, method is POST, url is resource type.
	 *
	 * @param entry Entry which request field to be added.
	 */
	public static void addRequestToEntry(BundleEntryComponent entry) {

		BundleEntryRequestComponent request = new BundleEntryRequestComponent();

		String ifNoneExistString = generateIfNoneExist(entry).replace(" ", "%20");
		if (ifNoneExistString != null) {
			request.setIfNoneExist(ifNoneExistString);
		}

		request.setMethod(HTTPVerb.POST);
		// request.setUrl(entry.getResource().getResourceName());
		request.setUrl(entry.getResource().getResourceType().name());
		entry.setRequest(request);
	}

}
