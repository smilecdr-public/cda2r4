package tr.com.srdc.cda2fhir.transform.util;

public interface ITransformationMessage {
    /**
     * An enumeration defining the severity of the message.
     */
    enum Severity { ERROR, WARNING }

    /**
     * The severity of the message.
     *
     * @return the severity of the message
     */
    Severity getSeverity();

    /**
     * The name of the CDA entry where the message arose.
     *
     * @return the name of a CDA entry
     */
    String getEntryName();

    /**
     * The name of the field in the CDA entry most relevant to the message.
     *
     * @return the name of a CDA field
     */
    String getFieldName();

    /**
     * A detailed message describing the problem that was encountered.
     *
     * @return the message
     */
    String getMessage();
}
