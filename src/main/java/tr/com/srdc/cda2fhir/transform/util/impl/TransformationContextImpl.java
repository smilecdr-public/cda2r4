package tr.com.srdc.cda2fhir.transform.util.impl;

import com.google.common.collect.ImmutableList;
import org.hl7.fhir.r4.model.Bundle;
import org.hl7.fhir.r4.model.Encounter;
import org.hl7.fhir.r4.model.Patient;
import org.hl7.fhir.r4.model.Reference;
import tr.com.srdc.cda2fhir.transform.util.ITransformationContext;
import tr.com.srdc.cda2fhir.transform.util.ITransformationMessage;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class TransformationContextImpl implements ITransformationContext {

    private final Bundle.BundleType myBundleType;

    private final List<ITransformationMessage> myErrors = new ArrayList<>();
    private final List<ITransformationMessage> myWarnings = new ArrayList<>();

    private Patient myPatient;

    private Encounter myEncounter;

    public TransformationContextImpl() {
        this.myBundleType = null;
    }

    public TransformationContextImpl(Bundle.BundleType theBundleType) {
        this.myBundleType = theBundleType;
    }

    @Override
    public boolean isTransactionBundle() {
        return Bundle.BundleType.TRANSACTION.equals(this.myBundleType);
    }

    @Override
    public Reference getPatientRef() {
    	if (myPatient != null) {
    		Reference patientRef = new Reference(myPatient.getId());
    		patientRef.setDisplay(ReferenceInfo.getDisplay(myPatient));
    		return patientRef;
    	}
    	return null;
    }

    @Override
    public Patient getPatient() {
        return myPatient;
    }

    @Override
    public void setPatient(Patient thePatient) {
        this.myPatient = thePatient;
    }

    @Override
    public Reference getEncounterRef() {
        if (myEncounter != null) {
            Reference encounterRef = new Reference(myEncounter.getId());
            encounterRef.setDisplay(ReferenceInfo.getDisplay(myEncounter));
            return encounterRef;
        }
        return null;
    }

    @Override
    public Encounter getEncounter() {
        return myEncounter;
    }

    @Override
    public void setEncounter(Encounter theEncounter) {
        myEncounter = theEncounter;
    }

    @Override
    public String getUniqueId() {
        return UUID.randomUUID().toString();
    }

    @Override
    public void addError(String theEntryName, String theFieldName, String theMessage) {
        myErrors.add(new TransformationMessageImpl(ITransformationMessage.Severity.ERROR, theEntryName, theFieldName, theMessage));
    }

    @Override
    public void addWarning(String theEntryName, String theFieldName, String theMessage) {
        myWarnings.add(new TransformationMessageImpl(ITransformationMessage.Severity.WARNING, theEntryName, theFieldName, theMessage));
    }

    @Override
    public boolean hasErrors() {
        return !myErrors.isEmpty();
    }

    @Override
    public boolean hasWarnings() {
        return !myWarnings.isEmpty();
    }

    @Override
    public List<ITransformationMessage> getErrors() {
        return ImmutableList.copyOf(myErrors);
    }

    @Override
    public List<ITransformationMessage> getWarnings() {
        return ImmutableList.copyOf(myWarnings);
    }

}
