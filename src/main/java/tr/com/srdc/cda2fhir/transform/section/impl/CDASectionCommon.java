package tr.com.srdc.cda2fhir.transform.section.impl;

import org.eclipse.emf.common.util.EList;
import org.hl7.fhir.r4.model.Bundle;
import org.hl7.fhir.r4.model.CodeableConcept;
import org.hl7.fhir.r4.model.Coding;
import org.hl7.fhir.r4.model.Encounter;
import org.hl7.fhir.r4.model.Immunization;
import org.hl7.fhir.r4.model.Observation;
import org.hl7.fhir.r4.model.Type;
import org.openhealthtools.mdht.uml.cda.consol.EncounterActivities;
import org.openhealthtools.mdht.uml.cda.consol.ImmunizationActivity;
import org.openhealthtools.mdht.uml.cda.consol.VitalSignObservation;
import org.openhealthtools.mdht.uml.cda.consol.VitalSignsOrganizer;
import tr.com.srdc.cda2fhir.transform.IResourceTransformer;
import tr.com.srdc.cda2fhir.transform.ValueSetsTransformerImpl;
import tr.com.srdc.cda2fhir.transform.entry.IEntryResult;
import tr.com.srdc.cda2fhir.transform.util.IBundleInfo;
import tr.com.srdc.cda2fhir.transform.util.ITransformationContext;
import tr.com.srdc.cda2fhir.transform.util.impl.LocalBundleInfo;
import tr.com.srdc.cda2fhir.util.FHIRUtil;

import java.util.List;

public class CDASectionCommon {
	
	public static SectionResultSingular<Immunization> transformImmunizationActivityList(
			EList<ImmunizationActivity> actList, IBundleInfo bundleInfo, ITransformationContext theTransformationContext) {
		IResourceTransformer rt = bundleInfo.getResourceTransformer();
		SectionResultSingular<Immunization> result = SectionResultSingular.getInstance(Immunization.class);
		LocalBundleInfo localBundleInfo = new LocalBundleInfo(bundleInfo);
		for (ImmunizationActivity act : actList) {
			IEntryResult er = rt.tImmunizationActivity2Immunization(act, localBundleInfo, theTransformationContext);
			result.updateFrom(er);
			localBundleInfo.updateFrom(er);
		}
		return result;
	}

	public static SectionResultSingular<Observation> transformVitalSignsOrganizerList(
			EList<VitalSignsOrganizer> orgList, IBundleInfo bundleInfo, ITransformationContext theTransformationContext) {
				
		IResourceTransformer rt = bundleInfo.getResourceTransformer();
		SectionResultSingular<Observation> result = SectionResultSingular.getInstance(Observation.class);
		LocalBundleInfo localBundleInfo = new LocalBundleInfo(bundleInfo);
		for (VitalSignsOrganizer org : orgList) {
			for (VitalSignObservation obs : org.getVitalSignObservations()) {
				IEntryResult er = rt.tVitalSignObservation2Observation(obs, localBundleInfo, theTransformationContext);
				result.updateFrom(er);
				localBundleInfo.updateFrom(er);
			}
		}
		
		mergeBloodPressureObs(result.getBundle());
		mergeOxygenObs(result.getBundle());
		
		return result;
	}

	public static SectionResultSingular<Encounter> transformEncounterActivitiesList(
			EList<EncounterActivities> encounterList, IBundleInfo bundleInfo, ITransformationContext theTransformationContext) {
		IResourceTransformer rt = bundleInfo.getResourceTransformer();
		SectionResultSingular<Encounter> result = SectionResultSingular.getInstance(Encounter.class);
		LocalBundleInfo localBundleInfo = new LocalBundleInfo(bundleInfo);
		for (EncounterActivities act : encounterList) {
			IEntryResult er = rt.tEncounterActivity2Encounter(act, localBundleInfo, theTransformationContext);
			result.updateFrom(er);
			localBundleInfo.updateFrom(er);
		}
		return result;
	}
	
	/**
	 * Merge two blood pressure Observations 8480-6(systolic) and 8462-4 (diastolic), into one
	 * Both 8480-6 and 8462-4 must have the same Resource.identifier
	 * 
	 * @param theObsBundle, theBundle with Blood Pressure Observation
	 */
	private static void mergeBloodPressureObs(Bundle theObsBundle) {
		
		if (theObsBundle == null)
			return;
		
		//-- NOTE: Within one "Vital Sign Section" it could have more than one pair of BP systolic and BP diastolic.
		// They are a pair only if the identifier is same.
		
		List<Observation> systolicObsList = FHIRUtil.findObservationsByLoincCode(theObsBundle, "8480-6");
		List<Observation> diastolicObsList = FHIRUtil.findObservationsByLoincCode(theObsBundle, "8462-4");
				
		for (Observation systolicObs : systolicObsList) {

			// -- value from Systolic
			CodeableConcept systolicCode = systolicObs.getCode();
			Type systolicValue = systolicObs.getValue();

			for (Observation diastolicObs : diastolicObsList) {
				
				// Merge only the Resource.identifier is the same
				if (!FHIRUtil.isSameResource(systolicObs.getIdentifier(), diastolicObs.getIdentifier()))
					continue;

				CodeableConcept diastolicCode = diastolicObs.getCode();
				Type diastolicValue = diastolicObs.getValue();

				// -- replace the code
				CodeableConcept bpCode = new CodeableConcept();
				bpCode.addCoding((new Coding()).setSystem(ValueSetsTransformerImpl.LOINC_URL).setCode("85354-9")
						.setDisplay("Blood Pressure"));
				systolicObs.setCode(bpCode);

				// -- remove the value
				systolicObs.setValue(null);

				// -- move the original code value to the components
				if (systolicObs.hasDataAbsentReason()) {
					systolicObs.addComponent().setCode(systolicCode).setDataAbsentReason(systolicObs.getDataAbsentReason());
				} else {
					systolicObs.addComponent().setCode(systolicCode).setValue(systolicValue);
				}

				if (diastolicObs.hasDataAbsentReason()) {
					systolicObs.addComponent().setCode(diastolicCode).setDataAbsentReason(diastolicObs.getDataAbsentReason());
				} else {
					systolicObs.addComponent().setCode(diastolicCode).setValue(diastolicValue);
				}

				// -- remove diastolicObs entry in the bundle
				FHIRUtil.removeBundleEntryById(theObsBundle, diastolicObs.getId());
			}
		}
	}
	
	/**
	 * 1. Added the code/value of Observation 3150-0/3151-8 into Observation 59408-5 component
	 * 2. Remove Observation 3150-0 and 3151-8
	 * 3. Loinc code 2708-6 to Observation 59408-5
	 * 
	 * NOTE: Those Observations must have same Resource.identifier
	 * 
	 * @param theObsBundle theBundle with Observation 59408-5
	 */
	static void mergeOxygenObs(Bundle theObsBundle) {

		if (theObsBundle == null)
			return;

		List<Observation> oximetryObsList = FHIRUtil.findObservationsByLoincCode(theObsBundle, "59408-5");
		List<Observation> inhaledOxygenObsList = FHIRUtil.findObservationsByLoincCode(theObsBundle, "3150-0");
		List<Observation> flowRateOxygenObsList = FHIRUtil.findObservationsByLoincCode(theObsBundle, "3151-8");

		// Merge only the Resource.identifier is the same
		for (Observation oximetryObs : oximetryObsList) {

			// -- looking for inhaledOxygenObs
			Observation inhaledOxygenObs = null;
			for (Observation obs : inhaledOxygenObsList) {
				if (FHIRUtil.isSameResource(obs.getIdentifier(), oximetryObs.getIdentifier()))
					inhaledOxygenObs = obs;
			}

			// -- looking for flowRateOxygenObs
			Observation flowRateOxygenObs = null;
			for (Observation obs : flowRateOxygenObsList) {
				if (FHIRUtil.isSameResource(obs.getIdentifier(), oximetryObs.getIdentifier()))
					flowRateOxygenObs = obs;
			}

			// -- Couldn't find the Obs to merge
			boolean haveInhaledOxygenObsOrFlowRateOxygenObs = (inhaledOxygenObs != null || flowRateOxygenObs != null);
			if (!haveInhaledOxygenObsOrFlowRateOxygenObs)
				continue;

			// -- save original code/value
			if (inhaledOxygenObs != null) {
				CodeableConcept inhaledOxygenCode = inhaledOxygenObs.getCode();
				Type inhaledOxygenValue = inhaledOxygenObs.getValue();

				if (inhaledOxygenObs.hasDataAbsentReason()) {
					oximetryObs.addComponent().setCode(inhaledOxygenCode).setDataAbsentReason(inhaledOxygenObs.getDataAbsentReason());
				} else {
					oximetryObs.addComponent().setCode(inhaledOxygenCode).setValue(inhaledOxygenValue);
				}

				// -- remove inhaledOxygenObs entry in the bundle
				FHIRUtil.removeBundleEntryById(theObsBundle, inhaledOxygenObs.getId());
			}

			if (flowRateOxygenObs != null) {
				CodeableConcept flowRateOxygenCode = flowRateOxygenObs.getCode();
				Type flowRateOxygenValue = flowRateOxygenObs.getValue();

				if (flowRateOxygenObs.hasDataAbsentReason()) {
					oximetryObs.addComponent().setCode(flowRateOxygenCode).setDataAbsentReason(flowRateOxygenObs.getDataAbsentReason());
				} else {
					oximetryObs.addComponent().setCode(flowRateOxygenCode).setValue(flowRateOxygenValue);
				}

				// -- remove flowRateOxygenObs entry in the bundle
				FHIRUtil.removeBundleEntryById(theObsBundle, flowRateOxygenObs.getId());
			}

			// -- Add Loinc code 2708-6 to oximetryObs.code
			oximetryObs.getCode().addCoding((new Coding()).setSystem(ValueSetsTransformerImpl.LOINC_URL)
					.setCode("2708-6").setDisplay("Oxygen saturation in Arterial blood"));
		}
	}
}
