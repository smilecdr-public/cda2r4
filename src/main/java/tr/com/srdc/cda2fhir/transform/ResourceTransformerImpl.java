package tr.com.srdc.cda2fhir.transform;

import com.google.common.collect.Sets;
import org.apache.commons.lang3.StringUtils;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.xml.type.internal.DataValue.Base64;
import org.hl7.fhir.exceptions.FHIRException;
import org.hl7.fhir.r4.model.AdverseEvent;
import org.hl7.fhir.r4.model.Age;
import org.hl7.fhir.r4.model.AllergyIntolerance;
import org.hl7.fhir.r4.model.AllergyIntolerance.AllergyIntoleranceCriticality;
import org.hl7.fhir.r4.model.AllergyIntolerance.AllergyIntoleranceReactionComponent;
import org.hl7.fhir.r4.model.Annotation;
import org.hl7.fhir.r4.model.Attachment;
import org.hl7.fhir.r4.model.Base64BinaryType;
import org.hl7.fhir.r4.model.BooleanType;
import org.hl7.fhir.r4.model.Bundle;
import org.hl7.fhir.r4.model.Bundle.BundleEntryComponent;
import org.hl7.fhir.r4.model.CarePlan;
import org.hl7.fhir.r4.model.CarePlan.CarePlanIntent;
import org.hl7.fhir.r4.model.CarePlan.CarePlanStatus;
import org.hl7.fhir.r4.model.CareTeam;
import org.hl7.fhir.r4.model.CareTeam.CareTeamParticipantComponent;
import org.hl7.fhir.r4.model.ClinicalImpression;
import org.hl7.fhir.r4.model.CodeType;
import org.hl7.fhir.r4.model.CodeableConcept;
import org.hl7.fhir.r4.model.Coding;
import org.hl7.fhir.r4.model.Composition;
import org.hl7.fhir.r4.model.Composition.CompositionAttestationMode;
import org.hl7.fhir.r4.model.Composition.CompositionAttesterComponent;
import org.hl7.fhir.r4.model.Composition.CompositionEventComponent;
import org.hl7.fhir.r4.model.Composition.DocumentConfidentiality;
import org.hl7.fhir.r4.model.Composition.SectionComponent;
import org.hl7.fhir.r4.model.Composition.SectionMode;
import org.hl7.fhir.r4.model.Condition;
import org.hl7.fhir.r4.model.ContactPoint;
import org.hl7.fhir.r4.model.DateTimeType;
import org.hl7.fhir.r4.model.DateType;
import org.hl7.fhir.r4.model.Device;
import org.hl7.fhir.r4.model.Device.DeviceVersionComponent;
import org.hl7.fhir.r4.model.Device.FHIRDeviceStatus;
import org.hl7.fhir.r4.model.DiagnosticReport;
import org.hl7.fhir.r4.model.DocumentReference;
import org.hl7.fhir.r4.model.DocumentReference.DocumentReferenceContentComponent;
import org.hl7.fhir.r4.model.DocumentReference.DocumentReferenceContextComponent;
import org.hl7.fhir.r4.model.DomainResource;
import org.hl7.fhir.r4.model.Dosage;
import org.hl7.fhir.r4.model.Dosage.DosageDoseAndRateComponent;
import org.hl7.fhir.r4.model.Encounter;
import org.hl7.fhir.r4.model.Encounter.EncounterParticipantComponent;
import org.hl7.fhir.r4.model.Enumerations.AdministrativeGender;
import org.hl7.fhir.r4.model.Enumerations.DocumentReferenceStatus;
import org.hl7.fhir.r4.model.Extension;
import org.hl7.fhir.r4.model.FamilyMemberHistory;
import org.hl7.fhir.r4.model.FamilyMemberHistory.FamilyMemberHistoryConditionComponent;
import org.hl7.fhir.r4.model.Goal;
import org.hl7.fhir.r4.model.Goal.GoalLifecycleStatus;
import org.hl7.fhir.r4.model.Group;
import org.hl7.fhir.r4.model.Group.GroupType;
import org.hl7.fhir.r4.model.HumanName;
import org.hl7.fhir.r4.model.IdType;
import org.hl7.fhir.r4.model.Identifier;
import org.hl7.fhir.r4.model.Immunization;
import org.hl7.fhir.r4.model.Immunization.ImmunizationPerformerComponent;
import org.hl7.fhir.r4.model.Immunization.ImmunizationReactionComponent;
import org.hl7.fhir.r4.model.Immunization.ImmunizationStatus;
import org.hl7.fhir.r4.model.Location;
import org.hl7.fhir.r4.model.Medication;
import org.hl7.fhir.r4.model.MedicationRequest;
import org.hl7.fhir.r4.model.MedicationRequest.MedicationRequestDispenseRequestComponent;
import org.hl7.fhir.r4.model.MedicationRequest.MedicationRequestIntent;
import org.hl7.fhir.r4.model.MedicationStatement;
import org.hl7.fhir.r4.model.MedicationStatement.MedicationStatementStatus;
import org.hl7.fhir.r4.model.Narrative;
import org.hl7.fhir.r4.model.Narrative.NarrativeStatus;
import org.hl7.fhir.r4.model.Observation.ObservationReferenceRangeComponent;
import org.hl7.fhir.r4.model.Organization;
import org.hl7.fhir.r4.model.Patient;
import org.hl7.fhir.r4.model.Patient.ContactComponent;
import org.hl7.fhir.r4.model.Patient.PatientCommunicationComponent;
import org.hl7.fhir.r4.model.Period;
import org.hl7.fhir.r4.model.Practitioner;
import org.hl7.fhir.r4.model.PractitionerRole;
import org.hl7.fhir.r4.model.Procedure.ProcedurePerformerComponent;
import org.hl7.fhir.r4.model.Procedure.ProcedureStatus;
import org.hl7.fhir.r4.model.Provenance;
import org.hl7.fhir.r4.model.Provenance.ProvenanceAgentComponent;
import org.hl7.fhir.r4.model.Provenance.ProvenanceEntityComponent;
import org.hl7.fhir.r4.model.Provenance.ProvenanceEntityRole;
import org.hl7.fhir.r4.model.Quantity;
import org.hl7.fhir.r4.model.Reference;
import org.hl7.fhir.r4.model.Resource;
import org.hl7.fhir.r4.model.ServiceRequest;
import org.hl7.fhir.r4.model.ServiceRequest.ServiceRequestIntent;
import org.hl7.fhir.r4.model.ServiceRequest.ServiceRequestStatus;
import org.hl7.fhir.r4.model.SimpleQuantity;
import org.hl7.fhir.r4.model.StringType;
import org.hl7.fhir.r4.model.Substance;
import org.hl7.fhir.r4.model.Timing;
import org.hl7.fhir.r4.model.Type;
import org.hl7.fhir.r4.model.codesystems.ConditionVerStatus;
import org.hl7.fhir.r4.model.codesystems.ObservationCategory;
import org.hl7.fhir.r4.model.codesystems.ProvenanceAgentRole;
import org.hl7.fhir.r4.model.codesystems.ProvenanceAgentType;
import org.openhealthtools.mdht.uml.cda.Act;
import org.openhealthtools.mdht.uml.cda.AssignedAuthor;
import org.openhealthtools.mdht.uml.cda.AssignedEntity;
import org.openhealthtools.mdht.uml.cda.Author;
import org.openhealthtools.mdht.uml.cda.ClinicalDocument;
import org.openhealthtools.mdht.uml.cda.Component4;
import org.openhealthtools.mdht.uml.cda.Consumable;
import org.openhealthtools.mdht.uml.cda.CustodianOrganization;
import org.openhealthtools.mdht.uml.cda.DocumentationOf;
import org.openhealthtools.mdht.uml.cda.Entity;
import org.openhealthtools.mdht.uml.cda.EntryRelationship;
import org.openhealthtools.mdht.uml.cda.ExternalDocument;
import org.openhealthtools.mdht.uml.cda.Guardian;
import org.openhealthtools.mdht.uml.cda.LanguageCommunication;
import org.openhealthtools.mdht.uml.cda.ManufacturedProduct;
import org.openhealthtools.mdht.uml.cda.Material;
import org.openhealthtools.mdht.uml.cda.Observation;
import org.openhealthtools.mdht.uml.cda.Organizer;
import org.openhealthtools.mdht.uml.cda.Participant2;
import org.openhealthtools.mdht.uml.cda.ParticipantRole;
import org.openhealthtools.mdht.uml.cda.PatientRole;
import org.openhealthtools.mdht.uml.cda.Performer1;
import org.openhealthtools.mdht.uml.cda.Performer2;
import org.openhealthtools.mdht.uml.cda.PlayingEntity;
import org.openhealthtools.mdht.uml.cda.Procedure;
import org.openhealthtools.mdht.uml.cda.Product;
import org.openhealthtools.mdht.uml.cda.RecordTarget;
import org.openhealthtools.mdht.uml.cda.Section;
import org.openhealthtools.mdht.uml.cda.ServiceEvent;
import org.openhealthtools.mdht.uml.cda.StrucDocText;
import org.openhealthtools.mdht.uml.cda.SubstanceAdministration;
import org.openhealthtools.mdht.uml.cda.Supply;
import org.openhealthtools.mdht.uml.cda.consol.AgeObservation;
import org.openhealthtools.mdht.uml.cda.consol.AllergyObservation;
import org.openhealthtools.mdht.uml.cda.consol.AllergyProblemAct;
import org.openhealthtools.mdht.uml.cda.consol.AllergyStatusObservation;
import org.openhealthtools.mdht.uml.cda.consol.CaregiverCharacteristics;
import org.openhealthtools.mdht.uml.cda.consol.CommentActivity;
import org.openhealthtools.mdht.uml.cda.consol.EncounterActivities;
import org.openhealthtools.mdht.uml.cda.consol.EncounterDiagnosis;
import org.openhealthtools.mdht.uml.cda.consol.EstimatedDateOfDelivery;
import org.openhealthtools.mdht.uml.cda.consol.FamilyHistoryOrganizer;
import org.openhealthtools.mdht.uml.cda.consol.ImmunizationActivity;
import org.openhealthtools.mdht.uml.cda.consol.Indication;
import org.openhealthtools.mdht.uml.cda.consol.Instructions;
import org.openhealthtools.mdht.uml.cda.consol.MedicationActivity;
import org.openhealthtools.mdht.uml.cda.consol.MedicationDispense;
import org.openhealthtools.mdht.uml.cda.consol.MedicationInformation;
import org.openhealthtools.mdht.uml.cda.consol.MedicationSupplyOrder;
import org.openhealthtools.mdht.uml.cda.consol.NonMedicinalSupplyActivity;
import org.openhealthtools.mdht.uml.cda.consol.PlanOfCareActivityAct;
import org.openhealthtools.mdht.uml.cda.consol.PregnancyObservation;
import org.openhealthtools.mdht.uml.cda.consol.ProblemConcernAct;
import org.openhealthtools.mdht.uml.cda.consol.ProblemObservation;
import org.openhealthtools.mdht.uml.cda.consol.ProcedureActivityAct;
import org.openhealthtools.mdht.uml.cda.consol.ProcedureActivityObservation;
import org.openhealthtools.mdht.uml.cda.consol.ProcedureActivityProcedure;
import org.openhealthtools.mdht.uml.cda.consol.ProductInstance;
import org.openhealthtools.mdht.uml.cda.consol.ReactionObservation;
import org.openhealthtools.mdht.uml.cda.consol.ResultObservation;
import org.openhealthtools.mdht.uml.cda.consol.ResultOrganizer;
import org.openhealthtools.mdht.uml.cda.consol.ServiceDeliveryLocation;
import org.openhealthtools.mdht.uml.cda.consol.SeverityObservation;
import org.openhealthtools.mdht.uml.cda.consol.VitalSignObservation;
import org.openhealthtools.mdht.uml.hl7.datatypes.AD;
import org.openhealthtools.mdht.uml.hl7.datatypes.ANY;
import org.openhealthtools.mdht.uml.hl7.datatypes.BL;
import org.openhealthtools.mdht.uml.hl7.datatypes.CD;
import org.openhealthtools.mdht.uml.hl7.datatypes.CE;
import org.openhealthtools.mdht.uml.hl7.datatypes.CS;
import org.openhealthtools.mdht.uml.hl7.datatypes.ED;
import org.openhealthtools.mdht.uml.hl7.datatypes.EN;
import org.openhealthtools.mdht.uml.hl7.datatypes.II;
import org.openhealthtools.mdht.uml.hl7.datatypes.IVL_PQ;
import org.openhealthtools.mdht.uml.hl7.datatypes.IVL_TS;
import org.openhealthtools.mdht.uml.hl7.datatypes.IVXB_TS;
import org.openhealthtools.mdht.uml.hl7.datatypes.ON;
import org.openhealthtools.mdht.uml.hl7.datatypes.PIVL_TS;
import org.openhealthtools.mdht.uml.hl7.datatypes.PN;
import org.openhealthtools.mdht.uml.hl7.datatypes.PQ;
import org.openhealthtools.mdht.uml.hl7.datatypes.REAL;
import org.openhealthtools.mdht.uml.hl7.datatypes.RTO;
import org.openhealthtools.mdht.uml.hl7.datatypes.ST;
import org.openhealthtools.mdht.uml.hl7.datatypes.SXCM_TS;
import org.openhealthtools.mdht.uml.hl7.datatypes.TEL;
import org.openhealthtools.mdht.uml.hl7.datatypes.TS;
import org.openhealthtools.mdht.uml.hl7.vocab.ActClass;
import org.openhealthtools.mdht.uml.hl7.vocab.ActClassObservation;
import org.openhealthtools.mdht.uml.hl7.vocab.EntityDeterminer;
import org.openhealthtools.mdht.uml.hl7.vocab.NullFlavor;
import org.openhealthtools.mdht.uml.hl7.vocab.ParticipationType;
import org.openhealthtools.mdht.uml.hl7.vocab.RoleClassRoot;
import org.openhealthtools.mdht.uml.hl7.vocab.x_ActMoodDocumentObservation;
import org.openhealthtools.mdht.uml.hl7.vocab.x_ActRelationshipEntryRelationship;
import org.openhealthtools.mdht.uml.hl7.vocab.x_DocumentSubstanceMood;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import tr.com.srdc.cda2fhir.conf.Config;
import tr.com.srdc.cda2fhir.transform.entry.IEntityInfo;
import tr.com.srdc.cda2fhir.transform.entry.IEntryResult;
import tr.com.srdc.cda2fhir.transform.entry.IMedicationsInformation;
import tr.com.srdc.cda2fhir.transform.entry.impl.DeferredDiagnosticReportEncounterReference;
import tr.com.srdc.cda2fhir.transform.entry.impl.DeferredDocumentReferenceEncounterReference;
import tr.com.srdc.cda2fhir.transform.entry.impl.DeferredObservationEncounterReference;
import tr.com.srdc.cda2fhir.transform.entry.impl.DeferredProcedureEncounterReference;
import tr.com.srdc.cda2fhir.transform.entry.impl.EntityInfo;
import tr.com.srdc.cda2fhir.transform.entry.impl.EntityResult;
import tr.com.srdc.cda2fhir.transform.entry.impl.EntryResult;
import tr.com.srdc.cda2fhir.transform.entry.impl.MedicationsInformation;
import tr.com.srdc.cda2fhir.transform.section.impl.CDACareTeamSection;
import tr.com.srdc.cda2fhir.transform.section.impl.CDAGoalSection;
import tr.com.srdc.cda2fhir.transform.section.impl.CDASocialHistorySection;
import tr.com.srdc.cda2fhir.transform.util.IBundleInfo;
import tr.com.srdc.cda2fhir.transform.util.IDeferredReference;
import tr.com.srdc.cda2fhir.transform.util.ITransformationContext;
import tr.com.srdc.cda2fhir.transform.util.impl.BundleInfo;
import tr.com.srdc.cda2fhir.transform.util.impl.LocalBundleInfo;
import tr.com.srdc.cda2fhir.transform.util.impl.ReferenceInfo;
import tr.com.srdc.cda2fhir.util.Constants;
import tr.com.srdc.cda2fhir.util.FHIRUtil;

import java.io.Serializable;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;

import static tr.com.srdc.cda2fhir.transform.ValueSetsTransformerImpl.LOINC_URL;
import static tr.com.srdc.cda2fhir.transform.ValueSetsTransformerImpl.SNOMED_CT_URI;
import static tr.com.srdc.cda2fhir.transform.ValueSetsTransformerImpl.UNITS_OF_MEASURE_URL;

public class ResourceTransformerImpl implements IResourceTransformer, Serializable {

	/**
	 *
	 */
	private static final long serialVersionUID = 1L;

	public static final String AGE_OBSERVATION_ENTRY = "Age Observation";
	public static final String ALLERGY_CONCERN_ACT_ENTRY = "Allergy Concern Act";
	public static final String ALLERGY_INTOLERANCE_OBSERVATION_ENTRY = "Allergy - Intolerance Observation";
	public static final String ALLERGY_STATUS_OBSERVATION_ENTRY = "Allergy Status Observation";
	public static final String CRITICALITY_OBSERVATION_ENTRY = "Criticality Observation";
	public static final String ENCOUNTER_ACTIVITY_ENTRY = "Encounter Activity";
	public static final String FAMILY_HISTORY_OBSERVATION_ENTRY = "Family History Observation";
	public static final String FAMILY_HISTORY_ORGANIZER_ENTRY = "Family History Organizer";
	public static final String FUNCTIONAL_STATUS_OBSERVATION_ENTRY = "Functional Status Observation";
	public static final String HEADER_ENTRY = "Header";
	public static final String IMMUNIZATION_ACTIVITY_ENTRY = "Immunization Activity";
	public static final String IMMUNIZATION_MEDICATION_INFORMATION_ENTRY = "Immunization Medication Information";
	public static final String INDICATION_ENTRY = "Indication";
	public static final String MEDICATION_ACTIVITY_ENTRY = "Medication Activity";
	public static final String MEDICATION_DISPENSE_ENTRY = "Medication Dispense";
	public static final String MEDICATION_SUPPLY_ORDER_ENTRY = "Medication Supply Order";
	public static final String OBSERVATION_ENTRY = "Observation";
	public static final String PROBLEM_CONCERN_ACT_ENTRY = "Problem Concern Act";
	public static final String PROBLEM_OBSERVATION_ENTRY = "Problem Observation";
	public static final String PROCEDURE_ENTRY = "Procedure";
	public static final String PROCEDURE_ACTIVITY_ACT_ENTRY = "Procedure Activity Act";
	public static final String PROCEDURE_ACTIVITY_OBSERVATION_ENTRY = "Procedure Activity Observation";
	private static final String PRODUCT_INSTANCE_ENTRY = "Product Instance";
	public static final String REACTION_OBSERVATION_ENTRY = "Reaction Observation";
	public static final String RESULT_ORGANIZER_ENTRY = "Result Organizer";
	public static final String SEVERITY_OBSERVATION_ENTRY = "Severity Observation";
	public static final String CONSULTATION_NOTE_ENTRY = "Note Document Reference";
	public static final String GOAL_OBSERVATION_ENTRY = "Goal Observation";
	public static final String ASSESSMENT_SECTION = "Assessment Section";
	public static final String HEALTH_CONCERN_OBSERVATION_ENTRY = "Health Concern Observation";
	public static final String HEALTH_CONCERN_ACT_STATUS_ENTRY = "Health Concern Act Status";
	public static final String HEALTH_CONCERN_ACT_AUTHOR_ENTRY = "Health Concern Act Author";
	private static final String RESULTS_ORGANIZER_OBSERVATION_ENTRY = "Results Organizer Observation";
	public static final String CARE_TEAM_ORGANIZER_ENTRY = "Care Team Organizer";
	public static final String CARE_PLAN_SECTION = "Care Plan Section";


	public static final String AUTHOR_FIELD = "author";
	public static final String CODE_FIELD = "code";
	public static final String COMPONENT_FIELD = "component";
	public static final String COMPONENT_OBSERVATION_FIELD = "component.observation";
	public static final String CONFIDENTIALITY_CODE_FIELD = "confidentialityCode";
	public static final String CONSUMABLE_FIELD = "consumable";
	public static final String CONSUMABLE_PRODUCT_FIELD = "consumable.manufacturedProduct";
	public static final String DOSE_QUANTITY_FIELD = "doseQuantity";
	public static final String EFFECTIVE_TIME_FIELD = "effectiveTime";
	public static final String ENTRY_RELATIONSHIP_TYPE_FIELD = "entryRelationship@typeCode";
	public static final String IDENTIFIER_FIELD = "identifier";
	public static final String MANUFACTURED_MATERIAL_FIELD = "manufacturedMaterial";
	public static final String MANUFACTURED_MATERIAL_CODE_FIELD = "manufacturedMaterial.code";
	public static final String OBSERVATION_VALUE_FIELD = "observation.value";
	public static final String PARTICIPANT_FIELD = "participant";
	public static final String PARTICIPANT_LA_TYPE_FIELD = "participant@typeCode=LA";
	public static final String PARTICIPANT_CODE_FIELD = "participant.participantRole.playingEntity.code";
	public static final String RELATED_SUBJECT_CODE_FIELD = "subject.relatedSubject.code";
	public static final String RELATED_SUBJECT_GENDER_FIELD = "subject.relatedSubject.subject.administrativeGenderCode";
	private static final String SCOPING_ENTITY_ID_FIELD = "scopingEntity.id";
	public static final String STATUS_CODE_FIELD = "statusCode";
	public static final String SUBJECT_FIELD = "subject";
	public static final String SUMMARY_FIELD = "summary";
	public static final String TITLE_FIELD = "title";
	public static final String VALUE_FIELD = "value";
	public static final String PRIORITY_PREFERENCE_FIELD = "Priority Preference";
	public static final String VALUE_NULL_FLAVOR_FIELD = "value.nullFlavor";

	public static final String ENCOUNTER_RELATIONSHIP = "entryRelationship[@typeCode='COMP'].encounter";
	public static final String SUBJECT_OBSERVATION_RELATIONSHIP = "entryRelationship[@typeCode='SUBJ'].observation";
	public static final String UDI_ORGANIZER_RELATIONSHIP = "entryRelationship[@typeCode='COMP'].organizer";

	public static final String MAX_CARDINALITY_MESSAGE = "Expected maximum cardinality of 1, {0} instances found.";
	public static final String MIN_CARDINALITY_MESSAGE = "Expected minimum cardinality of 1, no instances found.";
	public static final String MISSING_CODE_WITH_DEFAULT_MESSAGE = "No value found, defaulting to {0}.";
	public static final String MISSING_ENTRIES_MESSAGE = "No entries found.";
	public static final String MISSING_VALUE_MESSAGE = "No value found.";
	public static final String NO_IDENTIFIERS_MESSAGE = "No identifiers found.";
	public static final String OBSERVATION_ENCOUNTER_CARDINALITY_MESSAGE = "The CDA procedure refers to {0} encounters, but FHIR Observations can only refer to 1. Using the first entry.";
	public static final String PROCEDURE_ENCOUNTER_CARDINALITY_MESSAGE = "The CDA procedure refers to {0} encounters, but FHIR Procedures can only refer to 1. Using the first entry.";
	public static final String UNPARSEABLE_CODE_MESSAGE = "Code [{0}] could not be parsed.";
	public static final String UNPARSEABLE_CODE_WITH_DEFAULT_MESSAGE = "Code [{0}] could not be parsed, defaulting to {1}.";
	private static final String UNSUPPORTED_TYPE_MESSAGE = "Data type [{0}] is not supported for this field.";
	public static final String NO_VALID_CODE_FOUND = "No recognized code found for use in target field. Found codes were: [{0}].";
	public static final String NO_CODE_FOUND = "Missing required element: Encounter Activity.code.translation.";
	public static final String MULTIPLE_CODES_FOUND = "Multiple recognized codes found for target field, choosing first one. Found codes were: [{0}].";
	public static final String FHIR_STRUCTURE_PATIENT_RELIGION_URI = "http://hl7.org/fhir/StructureDefinition/patient-religion";
	public static final String FHIR_STRUCTURE_US_CORE_RACE_URI = "http://hl7.org/fhir/us/core/StructureDefinition/us-core-race";
	public static final String FHIR_STRUCTURE_US_CORE_ETHNICITY_URI = "http://hl7.org/fhir/us/core/StructureDefinition/us-core-ethnicity";
	public static final String FHIR_STRUCTURE_PATIENT_BIRTH_PLACE_URI = "http://hl7.org/fhir/StructureDefinition/patient-birthPlace";
	public static final String FDA_OID = "2.16.840.1.113883.3.3719";
	public static final String CODE_SYSTEM_CONDITION_CATEGORY = "http://terminology.hl7.org/CodeSystem/condition-category";
	public static final String CODE_SYSTEM_US_CORE_CONDITION_CATEGORY = "http://hl7.org/fhir/us/core/CodeSystem/condition-category";
	public static final String SNOMED_OID = "2.16.840.1.113883.6.96";
	public static final String SNOMED_URL = "http://snomed.info/sct";
	public static final String SNOMED_UNITS_VALUE = "246514001";
	public static final String SNOMED_NO_KNOWN_ALLERGIES_CODE = "716186003";
	public static final String SNOMED_NO_KNOWN_ALLERGIES_DISPLAY = "No Known Allergy (situation)";
	public static final String US_NPI_OID = "2.16.840.1.113883.4.6";
	public static final String US_NPI_URL = "http://hl7.org/fhir/sid/us-npi";
	public static final String CAREPLAN_CATEGORY_URI = "http://hl7.org/fhir/us/core/CodeSystem/careplan-category";
	public static final String ASSESS_PLAN = "assess-plan";
	public static final String RESULT_ORGANIZER_V3_OID = "2.16.840.1.113883.10.20.22.2.95";
	public static final Coding RADIOLOGY_CODING = new Coding(LOINC_URL, "LP29684-5", "Radiology");
	public static final Coding CARDIOLOGY_CODING = new Coding(LOINC_URL, "LP29708-2", "Cardiology");
	public static final Coding PATHOLOGY_CODING = new Coding(LOINC_URL, "LP7839-6", "Pathology");
	public static final Coding NO_KNOWN_ALLERGIES_CODING = new Coding(SNOMED_URL, SNOMED_NO_KNOWN_ALLERGIES_CODE, SNOMED_NO_KNOWN_ALLERGIES_DISPLAY);
	public static final List<Coding> US_CORE_DIAGNOSTIC_REPORT_CODINGS = List.of(RADIOLOGY_CODING, CARDIOLOGY_CODING, PATHOLOGY_CODING);

	public static final String DIAGNOSTIC_CATEGORY_URI = "http://terminology.hl7.org/CodeSystem/v2-0074";
	public static final String OBSERVATION_CATEGORY_URI = "http://terminology.hl7.org/CodeSystem/observation-category";
	public static final String ENCOUNTER_ACTIVITY_V3_OID = "2.16.840.1.113883.10.20.22.4.49";

	public static final String DOCUMENT_REFERENCE_CATEGORY_URI = "http://hl7.org/fhir/us/core/CodeSystem/us-core-documentreference-category";
	public static final String INDICATION_V2_OID = "2.16.840.1.113883.10.20.22.4.19";
	public static final String MEDICATION_FREE_TEXT_SIG_TEMPLATE_OID = "2.16.840.1.113883.10.20.22.4.147";
	public static final String MEDICATION_SUPPLY_ORDER_OID = "2.16.840.1.113883.10.20.22.4.17";
	public static final String MEDICATION_REQUEST_HARD_CODED_MESSAGE = "MedicationRequest.reported is currently hard-coded to false.";
	public static final String POSSIBLE_DUPLICATION_MESSAGE = "No identifier was found for the Care Plan. A duplicate record may occur in the repository.";

	private IDataTypesTransformer dtt;
	private IValueSetsTransformer vst;

	private final Logger logger = LoggerFactory.getLogger(ResourceTransformerImpl.class);

	public ResourceTransformerImpl() {
		dtt = new DataTypesTransformerImpl();
		vst = new ValueSetsTransformerImpl();
	}

	private List<Identifier> tIIs2Identifiers(EList<II> iis) {
		if (!iis.isEmpty()) {
			List<Identifier> result = new ArrayList<Identifier>();
			for (II ii : iis) {
				if (ii != null && !ii.isSetNullFlavor()) {
					Identifier identifier = dtt.tII2Identifier(ii);
					result.add(identifier);
				}
			}
			return result;
		}
		return null;
	}

	@Override
	public Reference getReference(Resource resource) {
		Reference reference = new Reference(resource.getId());
		String referenceString = ReferenceInfo.getDisplay(resource);
		if (referenceString != null) {
			reference.setDisplay(referenceString);
		}
		reference.setResource(resource);
		return reference;
	}

	@Override
	public Age tAgeObservation2Age(AgeObservation cdaAgeObservation, ITransformationContext theTransformationContext) {
		if (cdaAgeObservation == null || cdaAgeObservation.isSetNullFlavor()) {
			return null;
		}

		Age fhirAge = new Age();

		// value-> age
		if (!cdaAgeObservation.getValues().isEmpty()) {
			if (cdaAgeObservation.getValues().size() > 1) {
				theTransformationContext.addWarning(AGE_OBSERVATION_ENTRY, VALUE_FIELD, MessageFormat.format(MAX_CARDINALITY_MESSAGE, cdaAgeObservation.getValues().size()));
			}
			for (ANY value : cdaAgeObservation.getValues()) {
				if (value != null && !value.isSetNullFlavor()) {
					if (value instanceof PQ) {
						if (((PQ) value).getValue() != null) {
							// value.value -> value
							fhirAge.setValue(((PQ) value).getValue());
							// value.unit -> unit
							fhirAge.setUnit(((PQ) value).getUnit());
							fhirAge.setSystem(UNITS_OF_MEASURE_URL);
						}
					}
				}
			}
		} else {
			theTransformationContext.addWarning(AGE_OBSERVATION_ENTRY, VALUE_FIELD, MIN_CARDINALITY_MESSAGE);
		}

		return fhirAge;
	}

	@Override
	public EntryResult tAllergyProblemAct2AllergyIntolerance(AllergyProblemAct cdaAllergyProbAct,
															 IBundleInfo bundleInfo, ITransformationContext theTransformationContext) {
		EntryResult result = new EntryResult();

		if (cdaAllergyProbAct == null || cdaAllergyProbAct.isSetNullFlavor()) {
			return result;
		}

		AllergyIntolerance fhirAllergyIntolerance = new AllergyIntolerance();
		result.addResource(fhirAllergyIntolerance);

		// resource id
		IdType resourceId = new IdType("AllergyIntolerance", theTransformationContext.getUniqueId());
		fhirAllergyIntolerance.setId(resourceId);

		// meta.profile
		if (Config.isGenerateDafProfileMetadata()) {
			fhirAllergyIntolerance.getMeta().addProfile(Constants.PROFILE_DAF_ALLERGY_INTOLERANCE);
		}

		// id -> identifier
		if (cdaAllergyProbAct.getIds() != null && !cdaAllergyProbAct.getIds().isEmpty()) {
			for (II ii : cdaAllergyProbAct.getIds()) {
				if (!ii.isSetNullFlavor()) {
					fhirAllergyIntolerance.addIdentifier(dtt.tII2Identifier(ii));
				} else {
					addIdentifierDataAbsentReasonExtension(fhirAllergyIntolerance, NullFlavor.NI);
					theTransformationContext.addWarning(ALLERGY_CONCERN_ACT_ENTRY, IDENTIFIER_FIELD, NO_IDENTIFIERS_MESSAGE);
				}
			}
		} else {
			addIdentifierDataAbsentReasonExtension(fhirAllergyIntolerance, NullFlavor.NI);
			theTransformationContext.addWarning(ALLERGY_CONCERN_ACT_ENTRY, IDENTIFIER_FIELD, NO_IDENTIFIERS_MESSAGE);
		}

		// patient
		fhirAllergyIntolerance.setPatient(theTransformationContext.getPatientRef());

		// statusCode -> verificationStatus
		if (cdaAllergyProbAct.getStatusCode() != null && !cdaAllergyProbAct.getStatusCode().isSetNullFlavor()) {
			if (cdaAllergyProbAct.getStatusCode().getCode() != null
					&& !cdaAllergyProbAct.getStatusCode().getCode().isEmpty()) {
				CodeableConcept allergyIntoleranceStatusEnum = vst
						.tStatusCode2AllergyIntoleranceVerificationStatus(cdaAllergyProbAct.getStatusCode().getCode());
				if (allergyIntoleranceStatusEnum != null) {
					fhirAllergyIntolerance.setVerificationStatus(allergyIntoleranceStatusEnum);
				} else {
					theTransformationContext.addWarning(ALLERGY_CONCERN_ACT_ENTRY, STATUS_CODE_FIELD, MessageFormat.format(UNPARSEABLE_CODE_WITH_DEFAULT_MESSAGE, cdaAllergyProbAct.getStatusCode().getCode(), "'unconfirmed'"));
					fhirAllergyIntolerance.setVerificationStatus(Config.DEFAULT_ALLERGY_VERIFICATION_STATUS);
				}
			} else {
				theTransformationContext.addWarning(ALLERGY_CONCERN_ACT_ENTRY, STATUS_CODE_FIELD, MessageFormat.format(MISSING_CODE_WITH_DEFAULT_MESSAGE, "'unconfirmed'"));
				fhirAllergyIntolerance.setVerificationStatus(Config.DEFAULT_ALLERGY_VERIFICATION_STATUS);
			}
		} else {
			theTransformationContext.addWarning(ALLERGY_CONCERN_ACT_ENTRY, STATUS_CODE_FIELD, MessageFormat.format(MISSING_CODE_WITH_DEFAULT_MESSAGE, "'unconfirmed'"));
			fhirAllergyIntolerance.setVerificationStatus(Config.DEFAULT_ALLERGY_VERIFICATION_STATUS);
		}

		// effectiveTime -> asserted
		if (cdaAllergyProbAct.getEffectiveTime() != null && !cdaAllergyProbAct.getEffectiveTime().isSetNullFlavor()) {

			// low(if not exists, value) -> asserted
			if (cdaAllergyProbAct.getEffectiveTime().getLow() != null
					&& !cdaAllergyProbAct.getEffectiveTime().getLow().isSetNullFlavor()) {
				fhirAllergyIntolerance.setRecordedDateElement(dtt.tTS2DateTime(cdaAllergyProbAct.getEffectiveTime().getLow()));
				//						.setAsserterTarget(dtt.tTS2DateTime(cdaAllergyProbAct.getEffectiveTime().getLow()));
			} else if (cdaAllergyProbAct.getEffectiveTime().getValue() != null
					&& !cdaAllergyProbAct.getEffectiveTime().getValue().isEmpty()) {
				fhirAllergyIntolerance
						.setRecordedDateElement(dtt.tString2DateTime(cdaAllergyProbAct.getEffectiveTime().getValue()));
			}
		} else {
			theTransformationContext.addWarning(ALLERGY_CONCERN_ACT_ENTRY, EFFECTIVE_TIME_FIELD, MISSING_VALUE_MESSAGE);
		}

		// getting allergyObservation
		if (cdaAllergyProbAct.getAllergyObservations() != null
				&& !cdaAllergyProbAct.getAllergyObservations().isEmpty()) {
			for (AllergyObservation cdaAllergyObs : cdaAllergyProbAct.getAllergyObservations()) {
				// TODO: Separate this out into its own method to allow reuse in other sections
				if (cdaAllergyObs != null && !cdaAllergyObs.isSetNullFlavor()) {

					// allergyObservation.author -> AllergyIntolerance.recorder
					if (cdaAllergyObs.getAuthors() != null && !cdaAllergyObs.getAuthors().isEmpty()) {
						for (Author author : cdaAllergyObs.getAuthors()) {
							if (author != null && !author.isSetNullFlavor()) {
								EntityResult entityResult = tAuthor2Practitioner(author, bundleInfo, theTransformationContext);
								result.updateFrom(entityResult);
								if (entityResult.hasPractitioner()) {
									Reference reference = getReference(entityResult.getPractitioner());
									fhirAllergyIntolerance.setRecorder(reference);
								}
							}
						}
					}

					if (cdaAllergyObs.getNegationInd() != null && cdaAllergyObs.getNegationInd()) {
						// Tebra-specific mapping
						// allergyObservation.negationInd -> code (no known allergies)
						fhirAllergyIntolerance.getCode().addCoding(NO_KNOWN_ALLERGIES_CODING);
					} else if (cdaAllergyObs.getParticipants() != null && !cdaAllergyObs.getParticipants().isEmpty()) {
						// allergyObservation.participant.participantRole.playingEntity.code -> code
						if (cdaAllergyObs.getParticipants().size() > 1) {
							theTransformationContext.addWarning(ALLERGY_INTOLERANCE_OBSERVATION_ENTRY, PARTICIPANT_FIELD, MessageFormat.format(MAX_CARDINALITY_MESSAGE, cdaAllergyObs.getParticipants().size()));
						}
						for (Participant2 participant : cdaAllergyObs.getParticipants()) {
							if (participant != null && !participant.isSetNullFlavor()) {
								if (participant.getParticipantRole() != null
										&& !participant.getParticipantRole().isSetNullFlavor()) {
									if (participant.getParticipantRole().getPlayingEntity() != null
											&& !participant.getParticipantRole().getPlayingEntity().isSetNullFlavor()) {

										CE playingCode = participant.getParticipantRole().getPlayingEntity().getCode();
										if (playingCode != null) {
											if (playingCode.isSetNullFlavor()) {
												fhirAllergyIntolerance.getCode().addExtension(dtt.tNullFlavor2DataAbsentReasonExtension(playingCode.getNullFlavor()));
											} else {
												fhirAllergyIntolerance.setCode(dtt.tCD2CodeableConcept(playingCode));
											}
										} else {
											theTransformationContext.addWarning(ALLERGY_INTOLERANCE_OBSERVATION_ENTRY, PARTICIPANT_CODE_FIELD, MISSING_VALUE_MESSAGE);
										}

										if (participant.getParticipantRole().getPlayingEntity().getNames() != null) {
											List<PN> names = participant.getParticipantRole().getPlayingEntity()
													.getNames();
											if (!names.isEmpty()) {
												if (fhirAllergyIntolerance.getCode() == null) {
													fhirAllergyIntolerance.setCode(new CodeableConcept());
												}
												PN name = names.get(0);
												fhirAllergyIntolerance.getCode().setText(name.getText());
											}
										}

									}
								}
							}
						}
					}

					// allergyObservation.value[@xsi:type='CD'] -> category
					if (cdaAllergyObs.getValues() != null && !cdaAllergyObs.getValues().isEmpty()) {
						if (cdaAllergyObs.getValues().size() > 1) {
							theTransformationContext.addWarning(ALLERGY_INTOLERANCE_OBSERVATION_ENTRY, VALUE_FIELD, MessageFormat.format(MAX_CARDINALITY_MESSAGE, cdaAllergyObs.getValues().size()));
						}
						for (ANY value : cdaAllergyObs.getValues()) {
							if (value != null && !value.isSetNullFlavor()) {
								if (value instanceof CD) {
									// the FHIR concepts of category and type are merged into a single value set in CDA
									// try to parse the code both ways to find out what it really is
									String cdaValueCode = ((CD) value).getCode();
									AllergyIntolerance.AllergyIntoleranceCategory allergyIntoleranceCategory =
											vst.tAllergyCategoryCode2AllergyIntoleranceCategory(cdaValueCode);
									if (allergyIntoleranceCategory != null) {
										fhirAllergyIntolerance.addCategory(allergyIntoleranceCategory);
									}

									AllergyIntolerance.AllergyIntoleranceType allergyIntoleranceType =
											vst.tAllergyCategoryCode2AllergyIntoleranceType(cdaValueCode);
									if (allergyIntoleranceType != null) {
										fhirAllergyIntolerance.setType(allergyIntoleranceType);
									}
								}
							}
						}
					} else {
						theTransformationContext.addWarning(ALLERGY_INTOLERANCE_OBSERVATION_ENTRY, VALUE_FIELD, MIN_CARDINALITY_MESSAGE);
					}

					// effectiveTime -> onset
					if (cdaAllergyObs.getEffectiveTime() != null
							&& !cdaAllergyObs.getEffectiveTime().isSetNullFlavor()) {

						// low(if not exists, value) -> onset
						if (cdaAllergyObs.getEffectiveTime().getLow() != null
								&& !cdaAllergyObs.getEffectiveTime().getLow().isSetNullFlavor()) {
							fhirAllergyIntolerance
									.setOnset(dtt.tTS2DateTime(cdaAllergyObs.getEffectiveTime().getLow()));
						} else if (cdaAllergyObs.getEffectiveTime().getValue() != null
								&& !cdaAllergyObs.getEffectiveTime().getValue().isEmpty()) {
							fhirAllergyIntolerance
									.setOnset(dtt.tString2DateTime(cdaAllergyObs.getEffectiveTime().getValue()));
						}
					} else {
						theTransformationContext.addWarning(ALLERGY_INTOLERANCE_OBSERVATION_ENTRY, EFFECTIVE_TIME_FIELD, MISSING_VALUE_MESSAGE);
					}

					// searching for reaction observation
					if (cdaAllergyObs.getEntryRelationships() != null
							&& !cdaAllergyObs.getEntryRelationships().isEmpty()) {
						for (EntryRelationship entryRelShip : cdaAllergyObs.getEntryRelationships()) {
							if (entryRelShip != null && !entryRelShip.isSetNullFlavor()) {
								if (entryRelShip.getObservation() != null && !entryRelShip.isSetNullFlavor()) {
									Observation observation = entryRelShip.getObservation();

									// status observation -> clinical status
									if (observation instanceof AllergyStatusObservation) {
										if (observation.getValues().isEmpty()) {
											theTransformationContext.addWarning(ALLERGY_STATUS_OBSERVATION_ENTRY, VALUE_FIELD,
													MIN_CARDINALITY_MESSAGE);
										} else if (observation.getValues().size() > 1) {
											theTransformationContext.addWarning(ALLERGY_STATUS_OBSERVATION_ENTRY, VALUE_FIELD,
													MessageFormat.format(MAX_CARDINALITY_MESSAGE, observation.getValues().size()));
										}
										observation.getValues().stream()
												.filter(value -> value instanceof CE)
												.map(value -> (CE) value)
												.map(CD::getCode)
												.forEach(code -> {
															CodeableConcept status = vst.tProblemStatus2AllergyIntoleranceClinicalStatus(code);
															if (status != null) {
																fhirAllergyIntolerance.setClinicalStatus(status);
															} else {
																theTransformationContext.addWarning(ALLERGY_STATUS_OBSERVATION_ENTRY, VALUE_FIELD,
																		MessageFormat.format(UNPARSEABLE_CODE_MESSAGE, code));
															}
														}
												);
									}

									// reaction observation
									if (entryRelShip.getObservation() instanceof ReactionObservation) {

										ReactionObservation cdaReactionObs = (ReactionObservation) entryRelShip
												.getObservation();
										AllergyIntoleranceReactionComponent fhirReaction = fhirAllergyIntolerance
												.addReaction();

										// reactionObservation/value[@xsi:type='CD'] -> reaction.manifestation
										if (cdaReactionObs.getValues() != null
												&& !cdaReactionObs.getValues().isEmpty()) {
											if (cdaReactionObs.getValues().size() > 1) {
												theTransformationContext.addWarning(REACTION_OBSERVATION_ENTRY, VALUE_FIELD,
														MessageFormat.format(MAX_CARDINALITY_MESSAGE, cdaReactionObs.getValues().size()));
											}
											for (ANY value : cdaReactionObs.getValues()) {
												if (value instanceof CD) {
													fhirReaction.addManifestation(dtt.tCD2CodeableConcept((CD) value,
															bundleInfo.getIdedAnnotations()));
												}
											}
										} else {
											theTransformationContext.addWarning(REACTION_OBSERVATION_ENTRY, VALUE_FIELD,
													MIN_CARDINALITY_MESSAGE);
										}

										// reactionObservation/low -> reaction.onset
										if (cdaReactionObs.getEffectiveTime() != null
												&& !cdaReactionObs.getEffectiveTime().isSetNullFlavor()) {
											if (cdaReactionObs.getEffectiveTime().getLow() != null
													&& !cdaReactionObs.getEffectiveTime().getLow().isSetNullFlavor()) {
												fhirReaction.setOnsetElement(dtt.tString2DateTime(
														cdaReactionObs.getEffectiveTime().getLow().getValue()));
											}
										}

										// severityObservation/value[@xsi:type='CD'].code -> severity
										if (cdaReactionObs.getSeverityObservation() != null
												&& !cdaReactionObs.getSeverityObservation().isSetNullFlavor()) {
											SeverityObservation cdaSeverityObs = cdaReactionObs
													.getSeverityObservation();
											if (cdaSeverityObs.getValues() != null
													&& !cdaSeverityObs.getValues().isEmpty()) {
												if (cdaSeverityObs.getValues().size() > 1) {
													theTransformationContext.addWarning(SEVERITY_OBSERVATION_ENTRY, VALUE_FIELD,
															MessageFormat.format(MAX_CARDINALITY_MESSAGE, cdaSeverityObs.getValues().size()));
												}
												for (ANY value : cdaSeverityObs.getValues()) {
													if (value != null && !value.isSetNullFlavor()) {
														if (value instanceof CD) {
															String cdaSeverityCode = ((CD) value).getCode();
															AllergyIntolerance.AllergyIntoleranceSeverity allergyIntoleranceSeverity =
																	vst.tSeverityCode2AllergyIntoleranceSeverity(cdaSeverityCode);
															if (allergyIntoleranceSeverity != null) {
																fhirReaction.setSeverity(allergyIntoleranceSeverity);
															} else {
																theTransformationContext.addWarning(SEVERITY_OBSERVATION_ENTRY, VALUE_FIELD,
																		MessageFormat.format(UNPARSEABLE_CODE_MESSAGE, cdaSeverityCode));
															}
														}
													}
												}
											} else {
												theTransformationContext.addWarning(SEVERITY_OBSERVATION_ENTRY, VALUE_FIELD,
														MIN_CARDINALITY_MESSAGE);
											}
										}
									}

									// criticality observation. found by checking the templateId
									// entryRelationship.observation[templateId/@root='2.16.840.1.113883.10.20.22.4.145'].value[CD].code
									// -> criticality
									if (entryRelShip.getObservation().getTemplateIds() != null
											&& !entryRelShip.getObservation().getTemplateIds().isEmpty()) {
										for (II templateId : entryRelShip.getObservation().getTemplateIds()) {
											if (templateId.getRoot() != null && templateId.getRoot()
													.equals("2.16.840.1.113883.10.20.22.4.145")) {
												org.openhealthtools.mdht.uml.cda.Observation cdaCriticalityObservation = entryRelShip
														.getObservation();
												if (cdaCriticalityObservation.getValues().isEmpty()) {
													theTransformationContext.addWarning(CRITICALITY_OBSERVATION_ENTRY, VALUE_FIELD,
															MIN_CARDINALITY_MESSAGE);
												} else if (cdaCriticalityObservation.getValues().size() > 1) {
													theTransformationContext.addWarning(CRITICALITY_OBSERVATION_ENTRY, VALUE_FIELD,
															MessageFormat.format(MAX_CARDINALITY_MESSAGE, cdaCriticalityObservation.getValues().size()));
												}
												for (ANY value : cdaCriticalityObservation.getValues()) {
													if (value != null && !value.isSetNullFlavor()) {
														if (value instanceof CD) {
															String cdaCriticalityObservationValue = ((CD) value).getCode();
															AllergyIntoleranceCriticality allergyIntoleranceCriticalityEnum =
																	vst.tCriticalityObservationValue2AllergyIntoleranceCriticality(
																			cdaCriticalityObservationValue);
															if (allergyIntoleranceCriticalityEnum != null) {
																fhirAllergyIntolerance.setCriticality(allergyIntoleranceCriticalityEnum);
															} else {
																theTransformationContext.addWarning(CRITICALITY_OBSERVATION_ENTRY, VALUE_FIELD,
																		MessageFormat.format(UNPARSEABLE_CODE_MESSAGE, cdaCriticalityObservationValue));
															}
														}
													}
												}
												// since we already found the desired templateId, we may break the
												// searching for templateId to avoid containing duplicate observations
												break;
											}
										}
									}
								}
							}
						}
					}
				}
			}
		} else {
			theTransformationContext.addWarning(ALLERGY_CONCERN_ACT_ENTRY, SUBJECT_OBSERVATION_RELATIONSHIP, MISSING_ENTRIES_MESSAGE);
		}

		// For Tebra, rather than set the clinical status to a default value if it cannot be determined from the
		// Allergy Status Observation, we will fall back to setting it based on the statusCode. Note that statusCode
		// is also being used to set the validationCode above. We'll just have to live with that for now.
		// The mappings are different in the bidirectional redesign anyway.
		if (!fhirAllergyIntolerance.hasClinicalStatus()) {
			if (cdaAllergyProbAct.getStatusCode() != null && !cdaAllergyProbAct.getStatusCode().isSetNullFlavor()) {
				if (cdaAllergyProbAct.getStatusCode().getCode() != null
						&& !cdaAllergyProbAct.getStatusCode().getCode().isEmpty()) {
					CodeableConcept allergyIntoleranceStatusEnum = vst
							.tStatusCode2AllergyIntoleranceClinicalStatus(cdaAllergyProbAct.getStatusCode().getCode());
					if (allergyIntoleranceStatusEnum != null) {
						fhirAllergyIntolerance.setClinicalStatus(allergyIntoleranceStatusEnum);
					}
				}
			}
		}

		return result;
	}

	private void addIdentifierDataAbsentReasonExtension(DomainResource theDomainResource, NullFlavor theNullFlavor) {
		try {
			Identifier identifier = (Identifier) theDomainResource.addChild(IDENTIFIER_FIELD);
			identifier.addExtension(dtt.tNullFlavor2DataAbsentReasonExtension(theNullFlavor));
		} catch (ClassCastException | FHIRException e) {
			logger.warn(MessageFormat.format("Could not add element [{0}]", IDENTIFIER_FIELD));
		}
	}

	@Override
	public EntityResult tAssignedAuthor2Device(AssignedAuthor cdaAssignedAuthor, IBundleInfo bundleInfo, ITransformationContext theTransformationContext) {
		if (cdaAssignedAuthor == null || cdaAssignedAuthor.isSetNullFlavor()) {
			return new EntityResult();
		}

		List<II> ids = null;
		if (cdaAssignedAuthor.getIds() != null && !cdaAssignedAuthor.getIds().isEmpty()) {
			for (II ii : cdaAssignedAuthor.getIds()) {
				if (ii != null && !ii.isSetNullFlavor()) {
					if (ids == null) {
						ids = new ArrayList<II>();
					}
					ids.add(ii);
				}
			}
		}

		if (ids != null) {
			IEntityInfo existingInfo = bundleInfo.findEntityResult(ids);
			if (existingInfo != null) {
				return new EntityResult(existingInfo);
			}
		}

		EntityInfo info = new EntityInfo();
		Device fhirDevice = new Device();

		// id -> identifier
		if (ids != null) {
			for (II id : ids) {
				fhirDevice.addIdentifier(dtt.tII2Identifier(id));
			}
		}
		EntityResult result = new EntityResult(info, ids);
		// resource id
		IdType resourceDeviceId = new IdType("Device", theTransformationContext.getUniqueId());
		fhirDevice.setId(resourceDeviceId);

		// All things with the Device.
		if (cdaAssignedAuthor.getAssignedAuthoringDevice() != null
				&& !cdaAssignedAuthor.getAssignedAuthoringDevice().isSetNullFlavor()) {
			DeviceVersionComponent fhirDeviceVersion = fhirDevice.addVersion();
			fhirDevice.setManufacturer(cdaAssignedAuthor.getAssignedAuthoringDevice().getManufacturerModelName().getText());
			fhirDeviceVersion.setValue(cdaAssignedAuthor.getAssignedAuthoringDevice().getSoftwareName().getText());

			if(cdaAssignedAuthor.getAssignedAuthoringDevice().getTypeId() != null) {
				fhirDeviceVersion.setComponent(dtt.tII2Identifier(cdaAssignedAuthor.getAssignedAuthoringDevice().getTypeId()));
			}

			if(cdaAssignedAuthor.getAssignedAuthoringDevice().getSoftwareName().getCodeSystemVersion() != null) {
				fhirDeviceVersion.setType(dtt.tSC2CodeableConcept(cdaAssignedAuthor.getAssignedAuthoringDevice().getSoftwareName()));
			}

			if (cdaAssignedAuthor.getAssignedAuthoringDevice().getCode() != null) {
				fhirDevice.setType(dtt.tCD2CodeableConcept(cdaAssignedAuthor.getAssignedAuthoringDevice().getCode()));
			} else {
				Coding cd1 = new Coding();
				cd1.setCode(cdaAssignedAuthor.getAssignedAuthoringDevice().getManufacturerModelName().getCode());
				if (cdaAssignedAuthor.getAssignedAuthoringDevice().getManufacturerModelName()
						.getDisplayName() == null) {
					cd1.setDisplay(cdaAssignedAuthor.getAssignedAuthoringDevice().getManufacturerModelName().getText());
				} else {
					cd1.setDisplay(
							cdaAssignedAuthor.getAssignedAuthoringDevice().getManufacturerModelName().getDisplayName());
				}
				cd1.setSystem(
						cdaAssignedAuthor.getAssignedAuthoringDevice().getManufacturerModelName().getCodeSystem());
				fhirDevice.setType(new CodeableConcept().addCoding(cd1));
			}

		}

		info.setDevice(fhirDevice);

		// representedOrganization -> EntityInfo.organization
		if (cdaAssignedAuthor.getRepresentedOrganization() != null
				&& !cdaAssignedAuthor.getRepresentedOrganization().isSetNullFlavor()) {
			IEntryResult orgResult = tOrganization2Organization(cdaAssignedAuthor.getRepresentedOrganization(),
					bundleInfo, theTransformationContext);

			org.hl7.fhir.r4.model.Organization org = (Organization) orgResult
					.findResourceResult(org.hl7.fhir.r4.model.Organization.class);

			if (orgResult.hasResult()) {
				info.setOrgIsNew(true);
			}

			result.updateFrom(orgResult);

			if (org != null) {
				info.setOrganization(org);
				fhirDevice.setOwner(new Reference(org.getId()));
			}

		}

		return result;
	}

	@Override
	public EntityResult tAssignedAuthor2Practitioner(AssignedAuthor cdaAssignedAuthor, IBundleInfo bundleInfo, ITransformationContext theTransformationContext) {

		if (cdaAssignedAuthor == null || cdaAssignedAuthor.isSetNullFlavor()) {
			return new EntityResult();
		}

		List<II> ids = null;
		if (cdaAssignedAuthor.getIds() != null && !cdaAssignedAuthor.getIds().isEmpty()) {
			for (II ii : cdaAssignedAuthor.getIds()) {
				if (ii != null && !ii.isSetNullFlavor()) {
					if (ids == null) {
						ids = new ArrayList<II>();
					}
					ids.add(ii);
				}
			}
		}

		if (ids != null) {
			IEntityInfo existingInfo = bundleInfo.findEntityResult(ids);
			if (existingInfo != null) {
				return new EntityResult(existingInfo);
			}
		}

		EntityInfo info = new EntityInfo();

		EntityResult result = new EntityResult(info, ids);

		Practitioner fhirPractitioner = new Practitioner();
		info.setPractitioner(fhirPractitioner);

		// resource id
		IdType resourceId = new IdType("Practitioner", theTransformationContext.getUniqueId());
		fhirPractitioner.setId(resourceId);

		// id -> identifier
		if (ids != null) {
			for (II id : ids) {
				fhirPractitioner.addIdentifier(dtt.tII2Identifier(id));
			}
		}

		// meta.profile
		if (Config.isGenerateDafProfileMetadata()) {
			fhirPractitioner.getMeta().addProfile(Constants.PROFILE_DAF_PRACTITIONER);
		}

		// assignedPerson.name -> name
		if (cdaAssignedAuthor.getAssignedPerson() != null && !cdaAssignedAuthor.getAssignedPerson().isSetNullFlavor()) {
			if (cdaAssignedAuthor.getAssignedPerson().getNames() != null
					&& !cdaAssignedAuthor.getAssignedPerson().getNames().isEmpty()) {
				for (PN pn : cdaAssignedAuthor.getAssignedPerson().getNames()) {
					if (pn != null && !pn.isSetNullFlavor()) {
						fhirPractitioner.addName(dtt.tEN2HumanName(pn, theTransformationContext));
					}
				}
			}
		}

		// addr -> address
		if (cdaAssignedAuthor.getAddrs() != null && !cdaAssignedAuthor.getAddrs().isEmpty()) {
			for (AD ad : cdaAssignedAuthor.getAddrs()) {
				if (ad != null && !ad.isSetNullFlavor()) {
					fhirPractitioner.addAddress(dtt.AD2Address(ad));
				}
			}
		}

		// telecom -> telecom
		if (cdaAssignedAuthor.getTelecoms() != null && !cdaAssignedAuthor.getTelecoms().isEmpty()) {
			for (TEL tel : cdaAssignedAuthor.getTelecoms()) {
				if (tel != null && !tel.isSetNullFlavor()) {
					fhirPractitioner.addTelecom(dtt.tTEL2ContactPoint(tel));
				}
			}
		}

		// Adding a practitionerRole
		// Practitioner.PractitionerRole fhirPractitionerRole =
		// fhirPractitioner.addPractitionerRole();
		PractitionerRole fhirPractitionerRole = new PractitionerRole();
		fhirPractitionerRole.setId(new IdType("PractitionerRole", theTransformationContext.getUniqueId()));

		// code -> practitionerRole.role
		if (cdaAssignedAuthor.getCode() != null && !cdaAssignedAuthor.isSetNullFlavor()) {
			// fhirPractitionerRole.setRole(dtt.tCD2CodeableConcept(cdaAssignedAuthor.getCode()));
			fhirPractitionerRole.addCode(dtt.tCD2CodeableConcept(cdaAssignedAuthor.getCode()));
		}

		// representedOrganization -> practitionerRole.managingOrganization
		if (cdaAssignedAuthor.getRepresentedOrganization() != null
				&& !cdaAssignedAuthor.getRepresentedOrganization().isSetNullFlavor()) {

			IEntryResult orgResult = tOrganization2Organization(cdaAssignedAuthor.getRepresentedOrganization(),
					bundleInfo, theTransformationContext);

			org.hl7.fhir.r4.model.Organization fhirOrganization = (Organization) orgResult
					.findResourceResult(org.hl7.fhir.r4.model.Organization.class);

			if (orgResult.hasResult()) {
				info.setOrgIsNew(true);
			}
			result.updateFrom(orgResult);

			if (fhirOrganization != null) {
				fhirPractitionerRole.setOrganization(getReference(fhirOrganization));
				// allows us to resolve the identifiers when making the ifNoneExist parameters.
				fhirPractitionerRole.setOrganizationTarget(fhirOrganization);
				info.setOrganization(fhirOrganization);
				fhirPractitionerRole.setPractitioner(getReference(fhirPractitioner));
				info.setPractitionerRole(fhirPractitionerRole);
			}

		}

		return result;
	}

	@Override
	public EntityResult tAssignedEntity2Practitioner(AssignedEntity cdaAssignedEntity, IBundleInfo bundleInfo, ITransformationContext theTransformationContext) {
		if (cdaAssignedEntity == null || cdaAssignedEntity.isSetNullFlavor()) {
			return new EntityResult();
		}

		List<II> ids = null;
		if (cdaAssignedEntity.getIds() != null && !cdaAssignedEntity.getIds().isEmpty()) {
			for (II ii : cdaAssignedEntity.getIds()) {
				if (ii != null && !ii.isSetNullFlavor()) {
					if (ids == null) {
						ids = new ArrayList<II>();
					}
					ids.add(ii);
				}
			}
		}

		if (ids != null) {
			IEntityInfo existingInfo = bundleInfo.findEntityResult(ids);
			if (existingInfo != null) {
				return new EntityResult(existingInfo);
			}
		}

		EntityInfo info = new EntityInfo();

		EntityResult result = new EntityResult(info, ids);

		Practitioner fhirPractitioner = new Practitioner();
		info.setPractitioner(fhirPractitioner);

		// resource id
		IdType resourceId = new IdType("Practitioner", theTransformationContext.getUniqueId());
		fhirPractitioner.setId(resourceId);

		// meta.profile
		if (Config.isGenerateDafProfileMetadata()) {
			fhirPractitioner.getMeta().addProfile(Constants.PROFILE_DAF_PRACTITIONER);
		}

		// id -> identifier
		if (ids != null) {
			for (II id : ids) {
				fhirPractitioner.addIdentifier(dtt.tII2Identifier(id));
			}
		}

		// assignedPerson.name -> name
		if (cdaAssignedEntity.getAssignedPerson() != null && !cdaAssignedEntity.getAssignedPerson().isSetNullFlavor()) {
			for (PN pn : cdaAssignedEntity.getAssignedPerson().getNames()) {
				if (pn != null && !pn.isSetNullFlavor()) {
					fhirPractitioner.addName(dtt.tEN2HumanName(pn, theTransformationContext));
				}
			}
		}

		// addr -> address
		if (cdaAssignedEntity.getAddrs() != null && !cdaAssignedEntity.getAddrs().isEmpty()) {
			for (AD ad : cdaAssignedEntity.getAddrs()) {
				if (ad != null && !ad.isSetNullFlavor()) {
					fhirPractitioner.addAddress(dtt.AD2Address(ad));
				}
			}
		}

		// telecom -> telecom
		if (cdaAssignedEntity.getTelecoms() != null && !cdaAssignedEntity.getTelecoms().isEmpty()) {
			for (TEL tel : cdaAssignedEntity.getTelecoms()) {
				if (tel != null && !tel.isSetNullFlavor()) {
					fhirPractitioner.addTelecom(dtt.tTEL2ContactPoint(tel));
				}
			}
		}

		// Practitioner.PractitionerRole fhirPractitionerRole =
		// fhirPractitioner.addPractitionerRole();
		PractitionerRole fhirPractitionerRole = new PractitionerRole();
		fhirPractitionerRole.setId(new IdType("PractitionerRole", theTransformationContext.getUniqueId()));
		fhirPractitionerRole.setPractitioner(getReference(fhirPractitioner));

		// code -> practitionerRole.role
		if (cdaAssignedEntity.getCode() != null && !cdaAssignedEntity.isSetNullFlavor()) {
			// Add PractitionerRole resource even if there is no  RepresentedOrganizations
			CodeableConcept code = dtt.tCD2CodeableConcept(cdaAssignedEntity.getCode());
			if (code != null) {
				fhirPractitionerRole.addCode(code);
			}
		}

		// representedOrganization -> practitionerRole.organization
		// NOTE: we skipped multiple instances of represented organization; we just omit
		// apart from the first
		if (!cdaAssignedEntity.getRepresentedOrganizations().isEmpty()) {
			if (cdaAssignedEntity.getRepresentedOrganizations().get(0) != null
					&& !cdaAssignedEntity.getRepresentedOrganizations().get(0).isSetNullFlavor()) {

				IEntryResult orgResult = tOrganization2Organization(
						cdaAssignedEntity.getRepresentedOrganizations().get(0), bundleInfo, theTransformationContext);

				org.hl7.fhir.r4.model.Organization fhirOrganization = (Organization) orgResult
						.findResourceResult(org.hl7.fhir.r4.model.Organization.class);

				if (orgResult.hasResult()) {
					info.setOrgIsNew(true);
				}

				result.updateFrom(orgResult);

				if (fhirOrganization != null) {
					fhirPractitionerRole.setOrganization(getReference(fhirOrganization));
					info.setOrganization(fhirOrganization);
				}
			}
		}

		// NOTE: only add the PractitionerRole to the bundle, if there is Code or Organization
		if (fhirPractitionerRole.hasCode() || fhirPractitionerRole.hasOrganization())
			info.setPractitionerRole(fhirPractitionerRole);

		return result;
	}

	@Override
	public EntityResult tAuthor2Practitioner(Author cdaAuthor,
											 IBundleInfo bundleInfo, ITransformationContext theTransformationContext) {
		if (cdaAuthor == null || cdaAuthor.isSetNullFlavor()) {
			return new EntityResult();
		}

		if (cdaAuthor.getAssignedAuthor() == null || cdaAuthor.getAssignedAuthor().isSetNullFlavor()) {
			return new EntityResult();
		}

		return tAssignedAuthor2Practitioner(cdaAuthor.getAssignedAuthor(), bundleInfo, theTransformationContext);
	}

	@Override
	public Substance tCD2Substance(CD cdaSubstanceCode, ITransformationContext theTransformationContext) {
		if (cdaSubstanceCode == null || cdaSubstanceCode.isSetNullFlavor()) {
			return null;
		}

		Substance fhirSubstance = new Substance();

		// resource id
		fhirSubstance.setId(new IdType("Substance", theTransformationContext.getUniqueId()));

		// meta.profile
		if (Config.isGenerateDafProfileMetadata()) {
			fhirSubstance.getMeta().addProfile(Constants.PROFILE_DAF_SUBSTANCE);
		}

		// code -> code
		fhirSubstance.setCode(dtt.tCD2CodeableConcept(cdaSubstanceCode));

		return fhirSubstance;
	}

	@Override
	public EntryResult tClinicalDocument2Composition(ClinicalDocument cdaClinicalDocument, ITransformationContext theTransformationContext) {
		return tClinicalDocument2Bundle(cdaClinicalDocument, true, theTransformationContext);
	}

	@Override
	public EntryResult tClinicalDocument2Bundle(ClinicalDocument cdaClinicalDocument, boolean includeComposition, ITransformationContext theTransformationContext) {
		EntryResult result = new EntryResult();

		if (cdaClinicalDocument == null || cdaClinicalDocument.isSetNullFlavor()) {
			return result;
		}

		// create and init the global bundle and the composition resources
		Composition fhirComp = includeComposition ? new Composition() : null;

		if (fhirComp != null) {
			fhirComp.setId(new IdType("Composition", theTransformationContext.getUniqueId()));
			result.addResource(fhirComp);

			CodeableConcept classConcept = new CodeableConcept();
			Coding classCoding = new Coding("http://hl7.org/fhir/ValueSet/doc-classcodes", "LP173421-7", "Note");
			classConcept.addCoding(classCoding);
			fhirComp.addCategory(classConcept);

			// id -> identifier
			if (cdaClinicalDocument.getId() != null && !cdaClinicalDocument.getId().isSetNullFlavor()) {
				fhirComp.setIdentifier(dtt.tII2Identifier(cdaClinicalDocument.getId()));
			}

			// status
			fhirComp.setStatus(Config.DEFAULT_COMPOSITION_STATUS);

			// effectiveTime -> date
			if (cdaClinicalDocument.getEffectiveTime() != null
					&& !cdaClinicalDocument.getEffectiveTime().isSetNullFlavor()) {
				fhirComp.setDateElement(dtt.tTS2DateTime(cdaClinicalDocument.getEffectiveTime()));
			} else {
				theTransformationContext.addWarning(HEADER_ENTRY, EFFECTIVE_TIME_FIELD, MISSING_VALUE_MESSAGE);
			}

			// code -> type
			if (cdaClinicalDocument.getCode() != null && !cdaClinicalDocument.getCode().isSetNullFlavor()) {
				fhirComp.setType(dtt.tCD2CodeableConcept(cdaClinicalDocument.getCode()));
			} else {
				theTransformationContext.addWarning(HEADER_ENTRY, CODE_FIELD, MISSING_VALUE_MESSAGE);
			}

			// title.text -> title
			if (cdaClinicalDocument.getTitle() != null && !cdaClinicalDocument.getTitle().isSetNullFlavor()) {
				if (cdaClinicalDocument.getTitle().getText() != null
						&& !cdaClinicalDocument.getTitle().getText().isEmpty()) {
					fhirComp.setTitle(cdaClinicalDocument.getTitle().getText());
				}
			} else {
				theTransformationContext.addWarning(HEADER_ENTRY, TITLE_FIELD, MISSING_VALUE_MESSAGE);
			}

			// confidentialityCode -> confidentiality
			if (cdaClinicalDocument.getConfidentialityCode() != null
					&& !cdaClinicalDocument.getConfidentialityCode().isSetNullFlavor()) {
				if (cdaClinicalDocument.getConfidentialityCode().getCode() != null
						&& !cdaClinicalDocument.getConfidentialityCode().getCode().isEmpty()) {
					// fhirComp.setConfidentiality(cdaClinicalDocument.getConfidentialityCode().getCode());
					try {
						fhirComp.setConfidentiality(DocumentConfidentiality
								.fromCode(cdaClinicalDocument.getConfidentialityCode().getCode()));
					} catch (FHIRException e) {
						theTransformationContext.addError(HEADER_ENTRY, CONFIDENTIALITY_CODE_FIELD,
								MessageFormat.format(UNPARSEABLE_CODE_MESSAGE, cdaClinicalDocument.getConfidentialityCode().getCode()));
					}
				}
			} else {
				theTransformationContext.addWarning(HEADER_ENTRY, CONFIDENTIALITY_CODE_FIELD, MISSING_VALUE_MESSAGE);
			}
		}

		BundleInfo bundleInfo = new BundleInfo(this);

		EList<RecordTarget> recordTargets = cdaClinicalDocument.getRecordTargets();
		if (recordTargets != null && !recordTargets.isEmpty()) { // Support empty for testing purposes. We might need a
			// flag here not to include patient in the bundle as
			// well in future
			// transform the patient data and assign it to Composition.subject.
			// patient might refer to additional resources such as organization; hence the
			// method returns a bundle.
			IEntryResult patientResult = tPatientRole2Patient(
					cdaClinicalDocument.getRecordTargets().get(0).getPatientRole(), bundleInfo, theTransformationContext);

			result.updateFrom(patientResult);
			bundleInfo.updateFrom(patientResult);

			Patient fhirPatient = (Patient) patientResult.findResourceResult(Patient.class);

			if (fhirPatient != null) {
				fhirComp.setSubject(getReference(fhirPatient));
			}

		}

		// author.assignedAuthor -> author
		if (cdaClinicalDocument.getAuthors() != null && !cdaClinicalDocument.getAuthors().isEmpty()) {
			for (Author author : cdaClinicalDocument.getAuthors()) {
				// Asserting that at most one author exists
				if (author != null && !author.isSetNullFlavor()) {
					if (author.getAssignedAuthor() != null && !author.getAssignedAuthor().isSetNullFlavor()) {
						if (author.getAssignedAuthor().getAssignedPerson() != null
								&& !author.getAssignedAuthor().getAssignedPerson().isSetNullFlavor()) {
							EntityResult entityResult = tAuthor2Practitioner(author, bundleInfo, theTransformationContext);
							result.updateFrom(entityResult);
							bundleInfo.updateFrom(entityResult);
							if (fhirComp != null && entityResult.hasPractitioner()) {
								fhirComp.addAuthor().setReference(entityResult.getPractitionerId());
								String referenceString = ReferenceInfo.getDisplay(entityResult.getPractitioner());
								if (referenceString != null) {
									fhirComp.getAuthor().get(0).setDisplay(referenceString);
								}
							}
						} else if (author.getAssignedAuthor().getAssignedAuthoringDevice() != null
								&& author.getAssignedAuthor().getRepresentedOrganization() != null
								&& !author.getAssignedAuthor().getRepresentedOrganization().isSetNullFlavor()
								&& !author.getAssignedAuthor().getAssignedAuthoringDevice().isSetNullFlavor()) {
							EntityResult entityResult = tAssignedAuthor2Device(author.getAssignedAuthor(), bundleInfo, theTransformationContext);
							result.updateFrom(entityResult);
							bundleInfo.updateFrom(entityResult);
							result.addResource(entityResult.getDevice()); // Device added separately because updateFrom
							// ignores it.
							if (fhirComp != null && entityResult.hasDevice() && entityResult.hasOrganization()) {
								fhirComp.getAuthor().add(new Reference(entityResult.getDeviceId()));
							}
						}
					}
				}
			}
		} else {
			theTransformationContext.addWarning(HEADER_ENTRY, AUTHOR_FIELD, MIN_CARDINALITY_MESSAGE);
		}

		// legalAuthenticator -> attester[mode = legal]
		if (cdaClinicalDocument.getLegalAuthenticator() != null
				&& !cdaClinicalDocument.getLegalAuthenticator().isSetNullFlavor()) {
			CompositionAttesterComponent attester = fhirComp != null ? fhirComp.addAttester() : null;
			if (attester != null) {
				attester.setMode(CompositionAttestationMode.LEGAL);
				attester.setTimeElement(dtt.tTS2DateTime(cdaClinicalDocument.getLegalAuthenticator().getTime()));
			}
			EntityResult entityResult = tAssignedEntity2Practitioner(
					cdaClinicalDocument.getLegalAuthenticator().getAssignedEntity(), bundleInfo, theTransformationContext);
			result.updateFrom(entityResult);
			bundleInfo.updateFrom(entityResult);
			Reference reference = getReference(entityResult.getPractitioner());
			if (attester != null && reference != null) {
				attester.setParty(reference);
			}
		}

		// authenticator -> attester[mode = professional]
		for (org.openhealthtools.mdht.uml.cda.Authenticator authenticator : cdaClinicalDocument.getAuthenticators()) {
			if (!authenticator.isSetNullFlavor()) {
				CompositionAttesterComponent attester = fhirComp != null ? fhirComp.addAttester() : null;
				if (attester != null) {
					attester.setMode(CompositionAttestationMode.PROFESSIONAL);
					attester.setTimeElement(dtt.tTS2DateTime(authenticator.getTime()));
				}
				EntityResult entityResult = tAssignedEntity2Practitioner(authenticator.getAssignedEntity(), bundleInfo, theTransformationContext);
				result.updateFrom(entityResult);
				bundleInfo.updateFrom(entityResult);
				Reference reference = getReference(entityResult.getPractitioner());
				if (attester != null && reference != null) {
					attester.setParty(reference);
				}
			}
		}

		for (DocumentationOf docOf : cdaClinicalDocument.getDocumentationOfs()) {
			if (docOf.getServiceEvent() != null && fhirComp != null) {
				ServiceEvent cdaServiceEvent = docOf.getServiceEvent();
				CompositionEventComponent event = new CompositionEventComponent();

				fhirComp.addEvent(event);
				// documentationOf.serviceEvent.effectiveTime => event.period
				if (cdaServiceEvent.getEffectiveTime() != null) {
					Period period = dtt.tIVL_TS2Period(cdaServiceEvent.getEffectiveTime());
					event.setPeriod(period);
				}

				// documentationOf.serviceEvent.assignedEntity -> composition.event.detail
				if (cdaServiceEvent.getPerformers() != null && !cdaServiceEvent.getPerformers().isEmpty()) {
					for (Performer1 performer : cdaServiceEvent.getPerformers()) {
						if (performer.getAssignedEntity() != null) {
							EntityResult entityResult = tPerformer12Practitioner(performer, bundleInfo, theTransformationContext);
							if (entityResult.hasPractitioner()) {

								result.updateFrom(entityResult);
								Reference reference = getReference(entityResult.getPractitioner());
								event.addDetail(reference);
							}
						}

					}
				}
			}
		}
		// custodian.assignedCustodian.representedCustodianOrganization -> custodian
		if (cdaClinicalDocument.getCustodian() != null && !cdaClinicalDocument.getCustodian().isSetNullFlavor()) {
			if (cdaClinicalDocument.getCustodian().getAssignedCustodian() != null
					&& !cdaClinicalDocument.getCustodian().getAssignedCustodian().isSetNullFlavor()) {
				if (cdaClinicalDocument.getCustodian().getAssignedCustodian()
						.getRepresentedCustodianOrganization() != null
						&& !cdaClinicalDocument.getCustodian().getAssignedCustodian()
						.getRepresentedCustodianOrganization().isSetNullFlavor()) {
					IEntryResult orgResult = tCustodianOrganization2Organization(cdaClinicalDocument.getCustodian()
							.getAssignedCustodian().getRepresentedCustodianOrganization(), bundleInfo, theTransformationContext);
					result.updateFrom(orgResult);
					bundleInfo.updateFrom(orgResult);

					if (fhirComp != null) {
						org.hl7.fhir.r4.model.Organization fhirOrganization = (Organization) orgResult
								.findResourceResult(org.hl7.fhir.r4.model.Organization.class);

						if (fhirOrganization != null) {
							fhirComp.setCustodian(getReference(fhirOrganization));
						}
					}

				}
			}
		}

		return result;
	}

	@Override
	public IEntryResult tCustodianOrganization2Organization(
			CustodianOrganization cdaOrganization, IBundleInfo bundleInfo, ITransformationContext theTransformationContext) {
		EntryResult result = new EntryResult();

		if (cdaOrganization == null || cdaOrganization.isSetNullFlavor()) {
			return result;
		}

		org.hl7.fhir.r4.model.Organization fhirOrganization = new org.hl7.fhir.r4.model.Organization();

		// resource id
		IdType resourceId = new IdType("Organization", theTransformationContext.getUniqueId());
		fhirOrganization.setId(resourceId);

		// meta.profile
		if (Config.isGenerateDafProfileMetadata()) {
			fhirOrganization.getMeta().addProfile(Constants.PROFILE_DAF_ORGANIZATION);
		}

		// id -> identifier
		if (cdaOrganization.getIds() != null && !cdaOrganization.getIds().isEmpty()) {

			org.hl7.fhir.r4.model.Organization previousOrganization = (Organization) bundleInfo
					.findResourceResult(cdaOrganization.getIds(), org.hl7.fhir.r4.model.Organization.class);

			if (previousOrganization != null) {
				result.addExistingResource(previousOrganization);
				return result;
			} else {
				result.putIIResource(cdaOrganization.getIds(), org.hl7.fhir.r4.model.Organization.class,
						fhirOrganization);

			}

			for (II ii : cdaOrganization.getIds()) {
				if (ii != null && !ii.isSetNullFlavor()) {
					fhirOrganization.addIdentifier(dtt.tII2Identifier(ii));
				}
			}
		}
		result.addResource(fhirOrganization);

		// name.text -> name
		if (cdaOrganization.getName() != null && !cdaOrganization.getName().isSetNullFlavor()) {
			fhirOrganization.setName(cdaOrganization.getName().getText());
		}

		// telecom -> telecom
		if (cdaOrganization.getTelecoms() != null && !cdaOrganization.getTelecoms().isEmpty()) {
			for (TEL tel : cdaOrganization.getTelecoms()) {
				if (tel != null && !tel.isSetNullFlavor()) {
					fhirOrganization.addTelecom(dtt.tTEL2ContactPoint(tel));
				}
			}
		}

		// addr -> address
		if (cdaOrganization.getAddrs() != null && !cdaOrganization.getAddrs().isEmpty()) {
			for (AD ad : cdaOrganization.getAddrs()) {
				if (ad != null && !ad.isSetNullFlavor()) {
					fhirOrganization.addAddress(dtt.AD2Address(ad));
				}
			}
		}

		return result;
	}

	@Override
	public EntryResult tEncounterActivity2Encounter(
			EncounterActivities cdaEncounterActivity, IBundleInfo bundleInfo, ITransformationContext theTransformationContext) {
		EntryResult result = new EntryResult();

		if (cdaEncounterActivity == null || cdaEncounterActivity.isSetNullFlavor()) {
			return result;
		}

		Encounter fhirEncounter = new Encounter();
		result.addResource(fhirEncounter);

		// NOTE: hospitalization.period not found. However, daf requires it being mapped

		// resource id
		IdType resourceId = new IdType("Encounter", theTransformationContext.getUniqueId());
		fhirEncounter.setId(resourceId);

		// meta.profile
		if (Config.isGenerateDafProfileMetadata()) {
			fhirEncounter.getMeta().addProfile(Constants.PROFILE_DAF_ENCOUNTER);
		}

		// subject
		fhirEncounter.setSubject(theTransformationContext.getPatientRef());

		// id -> identifier
		if (cdaEncounterActivity.getIds() != null && !cdaEncounterActivity.getIds().isEmpty()) {
			for (II id : cdaEncounterActivity.getIds()) {
				if (id != null && !id.isSetNullFlavor()) {
					fhirEncounter.addIdentifier(dtt.tII2Identifier(id));
				}
			}
		} else {
			theTransformationContext.addWarning(ENCOUNTER_ACTIVITY_ENTRY, IDENTIFIER_FIELD, MIN_CARDINALITY_MESSAGE);
		}

		// statusCode -> status
		if (cdaEncounterActivity.getStatusCode() != null && !cdaEncounterActivity.getStatusCode().isSetNullFlavor()) {
			String cdaStatusCode = cdaEncounterActivity.getStatusCode().getCode();
			Encounter.EncounterStatus fhirStatusCode = vst.tStatusCode2EncounterStatusEnum(cdaStatusCode);
			if (fhirStatusCode != null) {
				fhirEncounter.setStatus(fhirStatusCode);
			} else {
				theTransformationContext.addWarning(ENCOUNTER_ACTIVITY_ENTRY, STATUS_CODE_FIELD,
						MessageFormat.format(UNPARSEABLE_CODE_WITH_DEFAULT_MESSAGE, cdaStatusCode, Config.DEFAULT_ENCOUNTER_STATUS));
				fhirEncounter.setStatus(Config.DEFAULT_ENCOUNTER_STATUS);
			}
		} else {
			theTransformationContext.addWarning(ENCOUNTER_ACTIVITY_ENTRY, STATUS_CODE_FIELD,
					MessageFormat.format(MISSING_CODE_WITH_DEFAULT_MESSAGE, Config.DEFAULT_ENCOUNTER_STATUS));
			fhirEncounter.setStatus(Config.DEFAULT_ENCOUNTER_STATUS);
		}

		// code -> type
		if (cdaEncounterActivity.getCode() != null) {
			fhirEncounter
					.addType(dtt.tCD2CodeableConcept(cdaEncounterActivity.getCode(), bundleInfo.getIdedAnnotations()));
		} else {
			theTransformationContext.addWarning(ENCOUNTER_ACTIVITY_ENTRY, CODE_FIELD, MISSING_VALUE_MESSAGE);
		}

		// code.translation -> classElement
		List<String> encounterClasses = new ArrayList<>();
		List<String> badEncounterClasses = new ArrayList<>();
		if (cdaEncounterActivity.getCode() != null && !cdaEncounterActivity.getCode().isSetNullFlavor()) {
			if (cdaEncounterActivity.getCode().getTranslations() != null
					&& !cdaEncounterActivity.getCode().getTranslations().isEmpty()) {
				for (CD cd : cdaEncounterActivity.getCode().getTranslations()) {
					if (cd != null && !cd.isSetNullFlavor()) {
						Coding encounterClass = vst.tEncounterCode2EncounterCode(cd.getCode());
						if (encounterClass != null) {
							encounterClasses.add(cd.getCode());
						} else {
							badEncounterClasses.add(cd.getCode());
						}
					}
				}
				if (!encounterClasses.isEmpty()) {
					fhirEncounter.setClass_(vst.tEncounterCode2EncounterCode(encounterClasses.get(0)));
					if (encounterClasses.size() > 1) {
						String codes = String.join(", ", encounterClasses);
						theTransformationContext.addWarning(ENCOUNTER_ACTIVITY_ENTRY, CODE_FIELD, MessageFormat.format(MULTIPLE_CODES_FOUND, codes));
					}
				} else if (!badEncounterClasses.isEmpty()) {
					String badCodes = String.join(", ", badEncounterClasses);
					theTransformationContext.addError(ENCOUNTER_ACTIVITY_ENTRY, CODE_FIELD, MessageFormat.format(NO_VALID_CODE_FOUND, badCodes));
				} else {
					theTransformationContext.addError(ENCOUNTER_ACTIVITY_ENTRY, CODE_FIELD, NO_CODE_FOUND);
				}
			}
		}

		// priorityCode -> priority
		if (cdaEncounterActivity.getPriorityCode() != null
				&& !cdaEncounterActivity.getPriorityCode().isSetNullFlavor()) {
			fhirEncounter.setPriority(dtt.tCD2CodeableConcept(cdaEncounterActivity.getPriorityCode()));
		}

		// performer -> participant.individual
		LocalBundleInfo localBundleInfo = new LocalBundleInfo(bundleInfo);
		if (cdaEncounterActivity.getPerformers() != null && !cdaEncounterActivity.getPerformers().isEmpty()) {
			for (Performer2 cdaPerformer : cdaEncounterActivity.getPerformers()) {
				if (cdaPerformer != null && !cdaPerformer.isSetNullFlavor()) {
					EntityResult entityResult = tPerformer22Practitioner(cdaPerformer, localBundleInfo, theTransformationContext);
					result.updateFrom(entityResult);
					localBundleInfo.updateFrom(result);
					if (entityResult.hasPractitioner()) {
						EncounterParticipantComponent fhirParticipant = new EncounterParticipantComponent();
						// default encounter participant type code
						fhirParticipant.addType().addCoding(Config.DEFAULT_ENCOUNTER_PARTICIPANT_TYPE_CODE);
						fhirParticipant.setIndividual(getReference(entityResult.getPractitioner()));

						// effectiveTime -> period
						if (cdaEncounterActivity.getEffectiveTime() != null
								&& !cdaEncounterActivity.getEffectiveTime().isSetNullFlavor()) {
							fhirParticipant.setPeriod(dtt.tIVL_TS2Period(cdaEncounterActivity.getEffectiveTime()));
						} else {
							theTransformationContext.addWarning(ENCOUNTER_ACTIVITY_ENTRY, EFFECTIVE_TIME_FIELD, MISSING_VALUE_MESSAGE);
						}

						fhirEncounter.addParticipant(fhirParticipant);
					}
				}
			}
		}

		// effectiveTime -> period
		if (cdaEncounterActivity.getEffectiveTime() != null
				&& !cdaEncounterActivity.getEffectiveTime().isSetNullFlavor()) {
			fhirEncounter.setPeriod(dtt.tIVL_TS2Period(cdaEncounterActivity.getEffectiveTime()));
		} else {
			theTransformationContext.addWarning(ENCOUNTER_ACTIVITY_ENTRY, EFFECTIVE_TIME_FIELD, MISSING_VALUE_MESSAGE);
		}

		// indication -> reasonReference
		for (Indication cdaIndication : cdaEncounterActivity.getIndications()) {
			if (!cdaIndication.isSetNullFlavor()) {
				IEntryResult condResult = tIndication2ConditionEncounter(cdaIndication, bundleInfo, theTransformationContext);

				result.updateFrom(condResult);

				Condition fhirCondition = (Condition) condResult.findResourceResult(Condition.class);

				if (fhirCondition != null) {
					fhirEncounter.addReasonReference(getReference(fhirCondition));
				}

			}
		}

		// encounter diagnosis -> diagnosis.condition
		for (EncounterDiagnosis cdaDiagnosis : cdaEncounterActivity.getEncounterDiagnosiss()) {
			if (!cdaDiagnosis.isSetNullFlavor()) {
				for (ProblemObservation cdaProblem : cdaDiagnosis.getProblemObservations()) {
					if (!cdaProblem.isSetNullFlavor()) {
						IEntryResult conditionResult = tProblemObservation2Condition(cdaProblem, bundleInfo, theTransformationContext);

						result.updateFrom(conditionResult);

						Condition fhirCondition = (Condition) conditionResult.findResourceResult(Condition.class);

						if (fhirCondition != null) {
							fhirCondition.getCategory().stream()
									.flatMap(t -> t.getCoding().stream())
									.filter(t -> StringUtils.equals(CODE_SYSTEM_CONDITION_CATEGORY, t.getSystem()))
									.filter(t -> StringUtils.equals("problem-list-item", t.getCode()))
									.forEach(t -> {
										t.setCode("encounter-diagnosis");
										t.setDisplay("Encounter Diagnosis");
									});
							fhirEncounter.addDiagnosis().setCondition(getReference(fhirCondition));
						}
					}
				}
			}
		}

		// participant[@typeCode='LOC'].participantRole[SDLOC] -> location
		if (cdaEncounterActivity.getParticipants() != null && !cdaEncounterActivity.getParticipants().isEmpty()) {
			for (Participant2 cdaParticipant : cdaEncounterActivity
					.getParticipants()) {
				if (cdaParticipant != null && !cdaParticipant.isSetNullFlavor()) {

					// checking if the participant is location
					if (cdaParticipant.getTypeCode() == ParticipationType.LOC) {
						if (cdaParticipant.getParticipantRole() != null
								&& !cdaParticipant.getParticipantRole().isSetNullFlavor()) {
							if (cdaParticipant.getParticipantRole().getClassCode() != null
									&& cdaParticipant.getParticipantRole().getClassCode() == RoleClassRoot.SDLOC) {
								// We first make the mapping to a resource.location
								// then, we create a resource.encounter.location
								// then, we add the resource.location to resource.encounter.location

								// usage of ParticipantRole2Location
								IEntryResult locationEntryResult = tParticipantRole2Location(
										cdaParticipant.getParticipantRole(), bundleInfo, theTransformationContext);
								result.updateFrom(locationEntryResult);
								Location fhirLocation = (Location) locationEntryResult.findResourceResult(Location.class);
								if (fhirLocation != null) {
									fhirEncounter.addLocation().setLocation(getReference(fhirLocation));
								}
							}
						}
					}
				}
			}
		}

		// entryRelationship[@typeCode="RSON"]/observation -> reasonCode
		for (EntryRelationship entryRelationship : cdaEncounterActivity.getEntryRelationships()) {
			if (entryRelationship.getTypeCode() != null
					&& x_ActRelationshipEntryRelationship.RSON == entryRelationship.getTypeCode()) {
				Observation observation = entryRelationship.getObservation();
				if (observation != null && !observation.isSetNullFlavor()) {
					if (isIndicationV2(observation)) {
						observation.getValues().stream()
								.filter(CD.class::isInstance)
								.map(CD.class::cast)
								.filter(cd -> SNOMED_OID.equals(cd.getCodeSystem()))
								.forEach(cd -> fhirEncounter.addReasonCode(
										new CodeableConcept(dtt.tCD2Coding(cd).setSystem(SNOMED_CT_URI))));
					}
				}
			}
		}

		theTransformationContext.setEncounter(fhirEncounter);

		return result;
	}

	private boolean isIndicationV2(Observation observation) {
		return observation.getTemplateIds().stream().map(II::getRoot).anyMatch(INDICATION_V2_OID::equals);
	}

	@Override
	public Group tEntity2Group(Entity cdaEntity) {
		// never used
		if (cdaEntity == null || cdaEntity.isSetNullFlavor()) {
			return null;
		} else if (cdaEntity.getDeterminerCode() != org.openhealthtools.mdht.uml.hl7.vocab.EntityDeterminer.KIND) {
			return null;
		}

		Group fhirGroup = new Group();

		// id -> identifier
		if (cdaEntity.getIds() != null && !cdaEntity.getIds().isEmpty()) {
			for (II id : cdaEntity.getIds()) {
				if (id != null && !id.isSetNullFlavor()) {
					if (id.getDisplayable() == true) {
						// unique
						fhirGroup.addIdentifier(dtt.tII2Identifier(id));
					}
				}
			}
		}

		// classCode -> type
		if (cdaEntity.getClassCode() != null) {
			GroupType groupTypeEnum = vst.tEntityClassRoot2GroupType(cdaEntity.getClassCode());
			if (groupTypeEnum != null) {
				fhirGroup.setType(groupTypeEnum);
			}

		}

		// deteminerCode -> actual
		if (cdaEntity.isSetDeterminerCode() && cdaEntity.getDeterminerCode() != null) {
			if (cdaEntity.getDeterminerCode() == EntityDeterminer.KIND) {
				fhirGroup.setActual(false);
			} else {
				fhirGroup.setActual(true);
			}
		}

		// code -> code
		if (cdaEntity.getCode() != null && !cdaEntity.getCode().isSetNullFlavor()) {
			fhirGroup.setCode(dtt.tCD2CodeableConcept(cdaEntity.getCode()));
		}

		return fhirGroup;
	}

	@Override
	public FamilyMemberHistory tFamilyHistoryOrganizer2FamilyMemberHistory(FamilyHistoryOrganizer cdaFHO, ITransformationContext theTransformationContext) {
		if (cdaFHO == null || cdaFHO.isSetNullFlavor()) {
			return null;
		}

		FamilyMemberHistory fhirFMH = new FamilyMemberHistory();

		// resource id
		IdType resourceId = new IdType("FamilyMemberHistory", theTransformationContext.getUniqueId());
		fhirFMH.setId(resourceId);

		// meta.profile
		if (Config.isGenerateDafProfileMetadata()) {
			fhirFMH.getMeta().addProfile(Constants.PROFILE_DAF_FAMILY_MEMBER_HISTORY);
		}

		// patient
		fhirFMH.setPatient(theTransformationContext.getPatientRef());

		// id -> identifier
		if (cdaFHO.getIds().isEmpty()) {
			theTransformationContext.addWarning(FAMILY_HISTORY_ORGANIZER_ENTRY, IDENTIFIER_FIELD, MIN_CARDINALITY_MESSAGE);
		}
		for (II id : cdaFHO.getIds()) {
			if (id != null && !id.isSetNullFlavor()) {
				fhirFMH.addIdentifier(dtt.tII2Identifier(id));
			}
		}

		// statusCode -> status
		if (cdaFHO.getStatusCode() != null && !cdaFHO.getStatusCode().isSetNullFlavor()) {
			String cdaStatusCode = cdaFHO.getStatusCode().getCode();
			FamilyMemberHistory.FamilyHistoryStatus fhirStatucCode =
					vst.tFamilyHistoryOrganizerStatusCode2FamilyHistoryStatus(cdaStatusCode);
			if (fhirStatucCode != null) {
				fhirFMH.setStatus(fhirStatucCode);
			} else {
				theTransformationContext.addWarning(FAMILY_HISTORY_ORGANIZER_ENTRY, STATUS_CODE_FIELD,
						MessageFormat.format(UNPARSEABLE_CODE_MESSAGE, cdaStatusCode));
			}
		} else {
			theTransformationContext.addWarning(FAMILY_HISTORY_ORGANIZER_ENTRY, STATUS_CODE_FIELD, MISSING_VALUE_MESSAGE);
		}

		// condition <-> familyHistoryObservation
		// also, deceased value is set by looking at
		// familyHistoryObservation.familyHistoryDeathObservation
		if (cdaFHO.getFamilyHistoryObservations() != null && !cdaFHO.getFamilyHistoryObservations().isEmpty()) {
			for (org.openhealthtools.mdht.uml.cda.consol.FamilyHistoryObservation familyHistoryObs : cdaFHO
					.getFamilyHistoryObservations()) {
				if (familyHistoryObs != null && !familyHistoryObs.isSetNullFlavor()) {

					// adding a new condition to fhirFMH
					FamilyMemberHistoryConditionComponent condition = fhirFMH.addCondition();

					// familyHistoryObservation.value[@xsi:type='CD'] -> code
					if (familyHistoryObs.getValues().isEmpty()) {
						theTransformationContext.addWarning(FAMILY_HISTORY_OBSERVATION_ENTRY, VALUE_FIELD, MIN_CARDINALITY_MESSAGE);
					} else if (familyHistoryObs.getValues().size() > 1) {
						theTransformationContext.addWarning(FAMILY_HISTORY_OBSERVATION_ENTRY, VALUE_FIELD,
								MessageFormat.format(MAX_CARDINALITY_MESSAGE, familyHistoryObs.getValues().size()));
					}
					for (ANY value : familyHistoryObs.getValues()) {
						if (value != null && !value.isSetNullFlavor()) {
							if (value instanceof CD) {
								condition.setCode(dtt.tCD2CodeableConcept((CD) value));
							}
						}
					}

					// NOTE: An alternative is to use the relatedSubject/subject/sdtc:deceasedInd
					// and relatedSubject/subject/sdtc:deceasedTime values
					// deceased
					if (familyHistoryObs.getFamilyHistoryDeathObservation() != null
							&& !familyHistoryObs.getFamilyHistoryDeathObservation().isSetNullFlavor()) {
						// if deathObservation exists, set fmh.deceased true
						fhirFMH.setDeceased(new BooleanType(true));

						// familyHistoryDeathObservation.value[@xsi:type='CD'] -> condition.outcome
						for (ANY value : familyHistoryObs.getFamilyHistoryDeathObservation().getValues()) {
							if (value != null && !value.isSetNullFlavor()) {
								if (value instanceof CD) {
									condition.setOutcome(dtt.tCD2CodeableConcept((CD) value));
								}
							}
						}
					}

					// familyHistoryObservation.ageObservation -> condition.onset
					if (familyHistoryObs.getAgeObservation() != null
							&& !familyHistoryObs.getAgeObservation().isSetNullFlavor()) {
						Age onset = tAgeObservation2Age(familyHistoryObs.getAgeObservation(), theTransformationContext);
						if (onset != null) {
							condition.setOnset(onset);
						}
					}
				}
			}
		} else {
			theTransformationContext.addWarning(FAMILY_HISTORY_ORGANIZER_ENTRY, COMPONENT_OBSERVATION_FIELD, MIN_CARDINALITY_MESSAGE);
		}

		// info from subject.relatedSubject
		if (cdaFHO.getSubject() != null && !cdaFHO.isSetNullFlavor() && cdaFHO.getSubject().getRelatedSubject() != null
				&& !cdaFHO.getSubject().getRelatedSubject().isSetNullFlavor()) {
			org.openhealthtools.mdht.uml.cda.RelatedSubject cdaRelatedSubject = cdaFHO.getSubject().getRelatedSubject();

			// subject.relatedSubject.code -> relationship
			if (cdaRelatedSubject.getCode() != null && !cdaRelatedSubject.getCode().isSetNullFlavor()) {
				fhirFMH.setRelationship(dtt.tCD2CodeableConcept(cdaRelatedSubject.getCode()));
			} else {
				theTransformationContext.addWarning(FAMILY_HISTORY_ORGANIZER_ENTRY, RELATED_SUBJECT_CODE_FIELD, MISSING_VALUE_MESSAGE);
			}

			// info from subject.relatedSubject.subject
			if (cdaRelatedSubject.getSubject() != null && !cdaRelatedSubject.getSubject().isSetNullFlavor()) {
				org.openhealthtools.mdht.uml.cda.SubjectPerson subjectPerson = cdaRelatedSubject.getSubject();

				// subject.relatedSubject.subject.name.text -> name
				for (EN en : subjectPerson.getNames()) {
					if (en != null && !en.isSetNullFlavor()) {
						if (en.getText() != null) {
							fhirFMH.setName(en.getText());
						}
					}
				}

				// subject.relatedSubject.subject.administrativeGenderCode -> gender
				if (subjectPerson.getAdministrativeGenderCode() != null
						&& !subjectPerson.getAdministrativeGenderCode().isSetNullFlavor()
						&& subjectPerson.getAdministrativeGenderCode().getCode() != null) {
					CodeableConcept fhirGenderCode = vst.tAdministrativeGenderCode2FamilyMemberHistorySex(
							subjectPerson.getAdministrativeGenderCode().getCode());
					if (fhirGenderCode != null) {
						fhirFMH.setSex(fhirGenderCode);
					} else {
						theTransformationContext.addWarning(FAMILY_HISTORY_ORGANIZER_ENTRY, RELATED_SUBJECT_GENDER_FIELD,
								MessageFormat.format(UNPARSEABLE_CODE_MESSAGE, subjectPerson.getAdministrativeGenderCode().getCode()));
					}
				} else {
					theTransformationContext.addWarning(FAMILY_HISTORY_ORGANIZER_ENTRY, RELATED_SUBJECT_GENDER_FIELD, MISSING_VALUE_MESSAGE);
				}

				// subject.relatedSubject.subject.birthTime -> born
				if (subjectPerson.getBirthTime() != null && !subjectPerson.getBirthTime().isSetNullFlavor()) {
					fhirFMH.setBorn(dtt.tTS2Date(subjectPerson.getBirthTime()));
				}
			}
		} else {
			theTransformationContext.addWarning(FAMILY_HISTORY_ORGANIZER_ENTRY, SUBJECT_FIELD, MISSING_VALUE_MESSAGE);
		}

		return fhirFMH;
	}

	/*
	 * Functional Status Section contains: 1- Functional Status Observation 2-
	 * Self-Care Activites Both of them have single Observation which needs mapping.
	 * Therefore, the parameter for the following
	 * method(tFunctionalStatus2Observation) chosen to be generic(Observation) .. to
	 * cover the content of the section. Also, notice that the transformation of
	 * those Observations are different from the generic Observation transformation
	 */

	@Override
	public EntryResult tFunctionalStatus2Observation(Observation cdaObservation,
													 IBundleInfo bundleInfo, ITransformationContext theTransformationContext) {
		EntryResult result = new EntryResult();

		if (cdaObservation == null || cdaObservation.isSetNullFlavor()) {
			return result;
		}

		org.hl7.fhir.r4.model.Observation fhirObs = new org.hl7.fhir.r4.model.Observation();
		result.addResource(fhirObs);

		// resource id
		IdType resourceId = new IdType("Observation", theTransformationContext.getUniqueId());
		fhirObs.setId(resourceId);

		// subject
		fhirObs.setSubject(theTransformationContext.getPatientRef());

		// statusCode -> status
		if (cdaObservation.getStatusCode() != null && !cdaObservation.getStatusCode().isSetNullFlavor()) {
			if (cdaObservation.getStatusCode().getCode() != null
					&& !cdaObservation.getStatusCode().getCode().isEmpty()) {
				fhirObs.setStatus(
						vst.tObservationStatusCode2ObservationStatus(cdaObservation.getStatusCode().getCode()));
			}
		} else {
			theTransformationContext.addWarning(FUNCTIONAL_STATUS_OBSERVATION_ENTRY, STATUS_CODE_FIELD, MISSING_VALUE_MESSAGE);
		}

		// id -> identifier
		if (cdaObservation.getIds() != null && !cdaObservation.getIds().isEmpty()) {
			for (II ii : cdaObservation.getIds()) {
				if (ii != null && !ii.isSetNullFlavor()) {
					fhirObs.addIdentifier(dtt.tII2Identifier(ii));
				}
			}
		} else {
			theTransformationContext.addWarning(FUNCTIONAL_STATUS_OBSERVATION_ENTRY, IDENTIFIER_FIELD, MIN_CARDINALITY_MESSAGE);
		}

		// code -> category
		if (cdaObservation.getCode() != null && !cdaObservation.isSetNullFlavor()) {
			fhirObs.addCategory(dtt.tCD2CodeableConcept(cdaObservation.getCode()));
		} else {
			theTransformationContext.addWarning(FUNCTIONAL_STATUS_OBSERVATION_ENTRY, CODE_FIELD, MISSING_VALUE_MESSAGE);
		}

		// value[@xsi:type='CD'] -> code
		if (cdaObservation.getValues() != null && !cdaObservation.getValues().isEmpty()) {
			if (cdaObservation.getValues().size() > 1) {
				theTransformationContext.addWarning(FUNCTIONAL_STATUS_OBSERVATION_ENTRY, VALUE_FIELD,
						MessageFormat.format(MAX_CARDINALITY_MESSAGE, cdaObservation.getValues().size()));
			}
			for (ANY value : cdaObservation.getValues()) {
				if (value != null && !value.isSetNullFlavor()) {
					if (value instanceof CD) {
						fhirObs.setCode(dtt.tCD2CodeableConcept((CD) value));
					}
				}
			}
		} else {
			theTransformationContext.addWarning(FUNCTIONAL_STATUS_OBSERVATION_ENTRY, VALUE_FIELD, MIN_CARDINALITY_MESSAGE);
		}

		// author -> performer
		if (cdaObservation.getAuthors() != null && !cdaObservation.getAuthors().isEmpty()) {
			for (org.openhealthtools.mdht.uml.cda.Author author : cdaObservation.getAuthors()) {
				if (author != null && !author.isSetNullFlavor()) {
					EntityResult entityResult = tAuthor2Practitioner(author, bundleInfo, theTransformationContext);
					result.updateFrom(entityResult);
					if (entityResult.hasPractitioner()) {
						fhirObs.addPerformer().setReference(entityResult.getPractitionerId());
					}
				}
			}
		}

		// effectiveTime -> effective
		if (cdaObservation.getEffectiveTime() != null && !cdaObservation.getEffectiveTime().isSetNullFlavor()) {
			fhirObs.setEffective(dtt.tIVL_TS2Period(cdaObservation.getEffectiveTime()));
		} else {
			theTransformationContext.addWarning(FUNCTIONAL_STATUS_OBSERVATION_ENTRY, EFFECTIVE_TIME_FIELD, MISSING_VALUE_MESSAGE);
		}

		// non-medicinal supply activity -> device
		if (cdaObservation.getEntryRelationships() != null && !cdaObservation.getEntryRelationships().isEmpty()) {
			for (EntryRelationship entryRelShip : cdaObservation.getEntryRelationships()) {
				if (entryRelShip != null && !entryRelShip.isSetNullFlavor()) {
					// supply
					org.openhealthtools.mdht.uml.cda.Supply cdaSupply = entryRelShip.getSupply();
					if (cdaSupply != null && !cdaSupply.isSetNullFlavor()) {
						if (cdaSupply instanceof NonMedicinalSupplyActivity) {
							// Non-Medicinal Supply Activity
							org.hl7.fhir.r4.model.Device fhirDev = tSupply2Device(cdaSupply, theTransformationContext);
							fhirObs.setDevice(getReference(fhirDev));
							result.addResource(fhirDev);
						}
					}
				}
			}
		}

		return result;
	}

	@Override
	public ContactComponent tGuardian2Contact(Guardian cdaGuardian, ITransformationContext theTransformationContext) {
		if (cdaGuardian == null || cdaGuardian.isSetNullFlavor()) {
			return null;
		}

		ContactComponent fhirContact = new ContactComponent();

		// addr -> address
		if (cdaGuardian.getAddrs() != null && !cdaGuardian.getAddrs().isEmpty()) {
			fhirContact.setAddress(dtt.AD2Address(cdaGuardian.getAddrs().get(0)));
		}

		// telecom -> telecom
		if (cdaGuardian.getTelecoms() != null && !cdaGuardian.getTelecoms().isEmpty()) {
			for (TEL tel : cdaGuardian.getTelecoms()) {
				if (tel != null && !tel.isSetNullFlavor()) {
					fhirContact.addTelecom(dtt.tTEL2ContactPoint(tel));
				}
			}
		}

		// guardianPerson/name -> name
		if (cdaGuardian.getGuardianPerson() != null && !cdaGuardian.getGuardianPerson().isSetNullFlavor()) {
			for (PN pn : cdaGuardian.getGuardianPerson().getNames()) {
				if (!pn.isSetNullFlavor()) {
					fhirContact.setName(dtt.tEN2HumanName(pn, theTransformationContext));
				}
			}
		}

		// code -> relationship
		if (cdaGuardian.getCode() != null && !cdaGuardian.getCode().isSetNullFlavor()) {
			// try to use IValueSetsTransformer method
			// tRoleCode2PatientContactRelationshipCode
			Coding relationshipCoding = null;
			if (cdaGuardian.getCode().getCode() != null && !cdaGuardian.getCode().getCode().isEmpty()) {
				relationshipCoding = vst.tRoleCode2PatientContactRelationshipCode(cdaGuardian.getCode().getCode());
			}
			// if tRoleCode2PatientContactRelationshipCode returns non-null value, add as
			// coding
			// otherwise, add relationship directly by making code
			// transformation(tCD2CodeableConcept)
			if (relationshipCoding != null) {
				fhirContact.addRelationship(new CodeableConcept().addCoding(relationshipCoding));
			} else {
				fhirContact.addRelationship(dtt.tCD2CodeableConcept(cdaGuardian.getCode()));
			}
		}
		return fhirContact;
	}

	@Override
	public EntryResult tImmunizationActivity2Immunization(ImmunizationActivity cdaImmunizationActivity,
														  IBundleInfo bundleInfo, ITransformationContext theTransformationContext) {
		EntryResult result = new EntryResult();
		LocalBundleInfo localBundleInfo = new LocalBundleInfo(bundleInfo);

		if (cdaImmunizationActivity == null || cdaImmunizationActivity.isSetNullFlavor()) {
			return result;
		}

		Immunization fhirImmunization = new Immunization();
		result.addResource(fhirImmunization);

		// resource id
		IdType resourceId = new IdType("Immunization", theTransformationContext.getUniqueId());
		fhirImmunization.setId(resourceId);

		// meta.profile
		if (Config.isGenerateDafProfileMetadata()) {
			fhirImmunization.getMeta().addProfile(Constants.PROFILE_DAF_IMMUNIZATION);
		}

		// patient
		fhirImmunization.setPatient(theTransformationContext.getPatientRef());

		// id -> identifier
		if (cdaImmunizationActivity.getIds() != null && !cdaImmunizationActivity.getIds().isEmpty()) {
			for (II ii : cdaImmunizationActivity.getIds()) {
				if (ii != null && !ii.isSetNullFlavor()) {
					fhirImmunization.addIdentifier(dtt.tII2Identifier(ii));
				}
			}
		} else {
			theTransformationContext.addWarning(IMMUNIZATION_ACTIVITY_ENTRY, IDENTIFIER_FIELD, MIN_CARDINALITY_MESSAGE);
		}

		// negationInd -> status.NOTDONE
		if (cdaImmunizationActivity.getNegationInd() != null && cdaImmunizationActivity.getNegationInd()) {
			fhirImmunization.setStatus(ImmunizationStatus.NOTDONE);
		}
		// statusCode -> status
		else if (cdaImmunizationActivity.getStatusCode() != null
				&& !cdaImmunizationActivity.getStatusCode().isSetNullFlavor()) {
			if (cdaImmunizationActivity.getStatusCode().getCode() != null
					&& !cdaImmunizationActivity.getStatusCode().getCode().isEmpty()) {

				ImmunizationStatus status = vst
						.tStatusCode2ImmunizationStatus(cdaImmunizationActivity.getStatusCode().getCode());
				if (status != null) {
					fhirImmunization.setStatus(status);
				} else {
					theTransformationContext.addError(IMMUNIZATION_ACTIVITY_ENTRY, STATUS_CODE_FIELD,
							MessageFormat.format(UNPARSEABLE_CODE_MESSAGE, cdaImmunizationActivity.getStatusCode().getCode()));
				}
			}
		} else {
			theTransformationContext.addWarning(IMMUNIZATION_ACTIVITY_ENTRY, STATUS_CODE_FIELD, MISSING_VALUE_MESSAGE);
		}

		// effectiveTime -> date
		if (cdaImmunizationActivity.getEffectiveTimes() != null
				&& !cdaImmunizationActivity.getEffectiveTimes().isEmpty()) {
			for (SXCM_TS effectiveTime : cdaImmunizationActivity.getEffectiveTimes()) {
				if (effectiveTime != null && !effectiveTime.isSetNullFlavor()) {
					// Asserting that at most one effective time exists
					fhirImmunization.setOccurrence(dtt.tTS2DateTime(effectiveTime));
				}
			}
		} else {
			theTransformationContext.addWarning(IMMUNIZATION_ACTIVITY_ENTRY, EFFECTIVE_TIME_FIELD, MISSING_VALUE_MESSAGE);
		}

		// lotNumber, vaccineCode, organization

		if (cdaImmunizationActivity.getConsumable() != null
				&& !cdaImmunizationActivity.getConsumable().isSetNullFlavor()) {
			if (cdaImmunizationActivity.getConsumable().getManufacturedProduct() != null
					&& !cdaImmunizationActivity.getConsumable().getManufacturedProduct().isSetNullFlavor()) {
				ManufacturedProduct manufacturedProduct = cdaImmunizationActivity.getConsumable()
						.getManufacturedProduct();

				if (manufacturedProduct.getManufacturedMaterial() != null
						&& !manufacturedProduct.getManufacturedMaterial().isSetNullFlavor()) {
					Material manufacturedMaterial = manufacturedProduct.getManufacturedMaterial();

					// consumable.manufacturedProduct.manufacturedMaterial.code -> vaccineCode
					CE vaccCode = manufacturedMaterial.getCode();
					if (vaccCode != null) {
						if (vaccCode.isSetNullFlavor()) {
							fhirImmunization.getVaccineCode().addExtension(dtt.tNullFlavor2DataAbsentReasonExtension(vaccCode.getNullFlavor()));
						} else {
							fhirImmunization.setVaccineCode(dtt.tCD2CodeableConcept(vaccCode, bundleInfo.getIdedAnnotations()));
						}
					} else {
						theTransformationContext.addWarning(IMMUNIZATION_MEDICATION_INFORMATION_ENTRY, MANUFACTURED_MATERIAL_CODE_FIELD, MISSING_VALUE_MESSAGE);
					}

					// consumable.manufacturedProduct.manufacturedMaterial.lotNumberText ->
					// lotNumber
					ST lotNumTxt = manufacturedMaterial.getLotNumberText();
					if (lotNumTxt != null && !lotNumTxt.isSetNullFlavor()) {
						fhirImmunization.setLotNumberElement(dtt.tST2String(lotNumTxt));
					}
				} else {
					theTransformationContext.addWarning(IMMUNIZATION_MEDICATION_INFORMATION_ENTRY, MANUFACTURED_MATERIAL_FIELD, MISSING_VALUE_MESSAGE);
				}

				// consumable.manufacturedProduct.manufacturerOrganization -> manufacturer
				if (manufacturedProduct.getManufacturerOrganization() != null
						&& !manufacturedProduct.getManufacturerOrganization().isSetNullFlavor()) {

					IEntryResult orgResult = tOrganization2Organization(
							manufacturedProduct.getManufacturerOrganization(), localBundleInfo, theTransformationContext);

					result.updateFrom(orgResult);
					localBundleInfo.updateFrom(orgResult);
					if (orgResult.getFullBundle() != null) {
						org.hl7.fhir.r4.model.Organization fhirOrganization = FHIRUtil.findFirstResource(
								orgResult.getFullBundle(), org.hl7.fhir.r4.model.Organization.class);
						if (fhirOrganization != null) {
							fhirImmunization.setManufacturer(getReference(fhirOrganization));
						}

					}

				}
			} else {
				theTransformationContext.addWarning(IMMUNIZATION_ACTIVITY_ENTRY, CONSUMABLE_PRODUCT_FIELD, MISSING_ENTRIES_MESSAGE);
			}
		} else {
			theTransformationContext.addWarning(IMMUNIZATION_ACTIVITY_ENTRY, CONSUMABLE_FIELD, MISSING_ENTRIES_MESSAGE);
		}

		// performer -> practitioner
		if (cdaImmunizationActivity.getPerformers() != null && !cdaImmunizationActivity.getPerformers().isEmpty()) {
			for (Performer2 performer : cdaImmunizationActivity.getPerformers()) {
				if (performer.getAssignedEntity() != null && !performer.getAssignedEntity().isSetNullFlavor()) {
					EntityResult entityResult = tPerformer22Practitioner(performer, localBundleInfo, theTransformationContext);
					result.updateFrom(entityResult);
					localBundleInfo.updateFrom(entityResult);
					if (entityResult.hasPractitioner()) {
						// TODO: verify the R4 mappings
						// TODO: find defined valueset/codesystem for immunization role
						// fhirImmunization.setPerformer(new Reference(entry.getResource().getId()));
						ImmunizationPerformerComponent perf = fhirImmunization.addPerformer();
						perf.getFunction().addCoding().setSystem("http://hl7.org/fhir/v2/0443").setCode("AP")
								.setDisplay("Administering Provider");
						perf.setActor(getReference(entityResult.getPractitioner()));
						fhirImmunization.setPrimarySource(true);
					}
				}
			}
		} else {
			// if no practitioner gets set, default to false.
			fhirImmunization.setPrimarySource(false);
		}

		// approachSiteCode -> site
		for (CD cd : cdaImmunizationActivity.getApproachSiteCodes()) {
			fhirImmunization.setSite(dtt.tCD2CodeableConcept(cd));
		}

		// routeCode -> route
		if (cdaImmunizationActivity.getRouteCode() != null
				&& !cdaImmunizationActivity.getRouteCode().isSetNullFlavor()) {
			fhirImmunization.setRoute(dtt.tCD2CodeableConcept(cdaImmunizationActivity.getRouteCode()));
		}

		// doseQuantity -> doseQuantity
		if (cdaImmunizationActivity.getDoseQuantity() != null
				&& !cdaImmunizationActivity.getDoseQuantity().isSetNullFlavor()) {
			SimpleQuantity dose = dtt.tPQ2SimpleQuantity(cdaImmunizationActivity.getDoseQuantity(), false);
			// manually set dose system, source object doesn't support it.
			dose.setSystem(vst.tOid2Url("2.16.840.1.113883.1.11.12839"));

			fhirImmunization.setDoseQuantity(dose);
		}

		// notGiven == true
		if (fhirImmunization.getStatus() == ImmunizationStatus.NOTDONE) {
			// immunizationRefusalReason.code -> immunization.statusReason
			if (cdaImmunizationActivity.getImmunizationRefusalReason() != null
					&& !cdaImmunizationActivity.getImmunizationRefusalReason().isSetNullFlavor()) {
				Observation observation = cdaImmunizationActivity.getImmunizationRefusalReason();
				if (observation.getCode() != null && !observation.getCode().isSetNullFlavor()) {
					fhirImmunization.addReasonCode(
							dtt.tCD2CodeableConcept(cdaImmunizationActivity.getImmunizationRefusalReason().getCode()));
					fhirImmunization.setStatusReason(dtt.tCD2CodeableConcept(cdaImmunizationActivity.getImmunizationRefusalReason().getCode()));
				}
			}
		}
		// notGiven == false
		else if (fhirImmunization.getStatus() != ImmunizationStatus.NOTDONE) {
			// indication.value -> explanation.reason
			if (cdaImmunizationActivity.getIndication() != null
					&& !cdaImmunizationActivity.getIndication().isSetNullFlavor()) {
				if (!cdaImmunizationActivity.getIndication().getValues().isEmpty()
						&& cdaImmunizationActivity.getIndication().getValues().get(0) != null
						&& !cdaImmunizationActivity.getIndication().getValues().get(0).isSetNullFlavor()) {
					fhirImmunization.addReasonCode(
							dtt.tCD2CodeableConcept((CD) cdaImmunizationActivity.getIndication().getValues().get(0)));
				}
			}
		}

		// reaction (i.e.
		// entryRelationship/observation[templateId/@root="2.16.840.1.113883.10.20.22.4.9"]
		// -> reaction
		if (cdaImmunizationActivity.getReactionObservation() != null
				&& !cdaImmunizationActivity.getReactionObservation().isSetNullFlavor()) {
			EntryResult er = tReactionObservation2Observation(cdaImmunizationActivity.getReactionObservation(),
					bundleInfo, theTransformationContext);
			Bundle reactionBundle = er.getBundle();
			org.hl7.fhir.r4.model.Observation fhirReactionObservation = null;
			for (BundleEntryComponent entry : reactionBundle.getEntry()) {
				result.addResource(entry.getResource());
				if (entry.getResource() instanceof org.hl7.fhir.r4.model.Observation) {
					fhirReactionObservation = (org.hl7.fhir.r4.model.Observation) entry.getResource();

					ImmunizationReactionComponent fhirReaction = fhirImmunization.addReaction();
					// reaction -> reaction.detail[ref=Observation]
					fhirReaction.setDetail(getReference(fhirReactionObservation));

					// reaction/effectiveTime/low -> reaction.date
					if (fhirReactionObservation.getEffective() != null) {
						Period reactionDate = (Period) fhirReactionObservation.getEffective();
						if (reactionDate.getStart() != null) {
							fhirReaction.setDateElement(reactionDate.getStartElement());
						}
					}

				}

			}
		}

		// TODO: in R4 this property at this level was removed. Figure out how
		// to map this to R4
		// fhirImmunization.setReported(Config.DEFAULT_IMMUNIZATION_REPORTED);
		return result;

	}

	private boolean conditionHasCategory(Condition condition, Coding otherCategoryCoding) {
		if (condition == null || otherCategoryCoding == null) {
			return false;
		}

		for (CodeableConcept category : condition.getCategory()) {
			if (category.hasCoding()) {

				Coding currentCategoryCoding = category.getCodingFirstRep();

				String code = currentCategoryCoding.getCode();

				if (code != null) {
					if (code.equals(otherCategoryCoding.getCode())) {
						return true;
					}
				}

			}
		}
		return false;
	}

	@Override
	public IEntryResult tIndication2ConditionEncounter(Indication cdaIndication, IBundleInfo bundleInfo, ITransformationContext theTransformationContext) {
		IEntryResult result = tIndication2Condition(cdaIndication, bundleInfo, theTransformationContext);
		Condition cond = (Condition) result.findResourceResult(Condition.class);

		Coding conditionCategory = new Coding().setSystem(vst.tOid2Url("2.16.840.1.113883.4.642.3.153"));
		conditionCategory.setCode("encounter-diagnosis");
		conditionCategory.setDisplay("Encounter Diagnosis");
		if (!conditionHasCategory(cond, conditionCategory)) {
			cond.addCategory().addCoding(conditionCategory);
		}
		return result;
	}

	@Override
	public IEntryResult tIndication2ConditionProblemListItem(Indication cdaIndication, IBundleInfo bundleInfo, ITransformationContext theTransformationContext) {
		IEntryResult result = tIndication2Condition(cdaIndication, bundleInfo, theTransformationContext);
		Condition cond = (Condition) result.findResourceResult(Condition.class);

		Coding conditionCategory = new Coding().setSystem(vst.tOid2Url("2.16.840.1.113883.4.642.3.153"));
		conditionCategory.setCode("problem-list-item");
		conditionCategory.setDisplay("Problem List Item");
		if (!conditionHasCategory(cond, conditionCategory)) {
			cond.addCategory().addCoding(conditionCategory);
		}
		return result;

	}

	private IEntryResult tIndication2Condition(Indication cdaIndication, IBundleInfo bundleInfo, ITransformationContext theTransformationContext) {
		EntryResult result = new EntryResult();

		if (cdaIndication == null || cdaIndication.isSetNullFlavor()) {
			return result;
		}

		Condition fhirCond = new Condition();

		// resource id
		IdType resourceId = new IdType("Condition", theTransformationContext.getUniqueId());
		fhirCond.setId(resourceId);

		// patient
		fhirCond.setSubject(theTransformationContext.getPatientRef());

		// meta.profile
		if (Config.isGenerateDafProfileMetadata()) {
			fhirCond.getMeta().addProfile(Constants.PROFILE_DAF_CONDITION);
		}

		List<II> ids = cdaIndication.getIds();

		if (ids != null) {
			Condition previous = (Condition) bundleInfo.findResourceResult(ids, Condition.class);
			if (previous != null) {
				result.addExistingResource(previous);
				return result;
			} else {
				result.putIIResource(ids, Condition.class, fhirCond);
			}
		}
		result.addResource(fhirCond);

		// id -> identifier
		if (ids != null && !cdaIndication.getIds().isEmpty()) {
			for (II ii : ids) {
				fhirCond.addIdentifier(dtt.tII2Identifier(ii));
			}
		} else {
			theTransformationContext.addWarning(INDICATION_ENTRY, IDENTIFIER_FIELD, MIN_CARDINALITY_MESSAGE);
		}

		// effectiveTime -> onset & abatement
		if (cdaIndication.getEffectiveTime() != null && !cdaIndication.getEffectiveTime().isSetNullFlavor()) {

			IVXB_TS low = cdaIndication.getEffectiveTime().getLow();
			IVXB_TS high = cdaIndication.getEffectiveTime().getHigh();
			String value = cdaIndication.getEffectiveTime().getValue();

			// low and high are both empty, and only the @value exists -> onset
			if (low == null && high == null && value != null && !value.equals("")) {
				fhirCond.setOnset(dtt.tString2DateTime(value));
			} else {
				// low -> onset
				if (low != null && !low.isSetNullFlavor()) {
					fhirCond.setOnset(dtt.tTS2DateTime(low));
				}
				// high -> abatement
				if (high != null && !high.isSetNullFlavor()) {
					fhirCond.setAbatement(dtt.tTS2DateTime(high));
				}
			}
		}

		// effectiveTime info -> clinicalStatus
		if (cdaIndication.getEffectiveTime() != null && !cdaIndication.getEffectiveTime().isSetNullFlavor()) {
			// high & low is present -> inactive
			if (cdaIndication.getEffectiveTime().getLow() != null
					&& !cdaIndication.getEffectiveTime().getLow().isSetNullFlavor()
					&& cdaIndication.getEffectiveTime().getHigh() != null
					&& !cdaIndication.getEffectiveTime().getHigh().isSetNullFlavor()) {
				fhirCond.setClinicalStatus(new CodeableConcept(new Coding("http://terminology.hl7.org/CodeSystem/condition-clinical", "inactive", "Inactive")));
			} else if (cdaIndication.getEffectiveTime().getLow() != null
					&& !cdaIndication.getEffectiveTime().getLow().isSetNullFlavor()) {
				// low is present, high is not present -> active
				fhirCond.setClinicalStatus(new CodeableConcept(new Coding("http://terminology.hl7.org/CodeSystem/condition-clinical", "active", "Active")));
			} else if (cdaIndication.getEffectiveTime().getValue() != null) {
				// value is present, low&high is not present -> active
				fhirCond.setClinicalStatus(new CodeableConcept(new Coding("http://terminology.hl7.org/CodeSystem/condition-clinical", "active", "Active")));
			}
		}

		// value[CD] -> code
		if (cdaIndication.getValues() != null && !cdaIndication.getValues().isEmpty()) {
			// There is only 1 value, but anyway...
			if (cdaIndication.getValues().size() > 1) {
				theTransformationContext.addWarning(INDICATION_ENTRY, VALUE_FIELD, MessageFormat.format(MAX_CARDINALITY_MESSAGE, cdaIndication.getValues().size()));
			}
			for (ANY value : cdaIndication.getValues()) {
				if (value != null && !value.isSetNullFlavor()) {
					if (value instanceof CD) {
						fhirCond.setCode(dtt.tCD2CodeableConcept((CD) value));
					}
				}
			}
		}

		return result;
	}

	@Override
	public PatientCommunicationComponent tLanguageCommunication2Communication(
			LanguageCommunication cdaLanguageCommunication) {
		if (cdaLanguageCommunication == null || cdaLanguageCommunication.isSetNullFlavor()) {
			return null;
		}

		PatientCommunicationComponent fhirCommunication = new PatientCommunicationComponent();

		// languageCode -> language
		if (cdaLanguageCommunication.getLanguageCode() != null
				&& !cdaLanguageCommunication.getLanguageCode().isSetNullFlavor()) {
			fhirCommunication.setLanguage(dtt.tCD2CodeableConcept(cdaLanguageCommunication.getLanguageCode()));
			// urn:ietf:bcp:47 -> language.codeSystem
			fhirCommunication.getLanguage().getCodingFirstRep().setSystem("urn:ietf:bcp:47");
		}

		// preferenceInd -> preferred
		if (cdaLanguageCommunication.getPreferenceInd() != null
				&& !cdaLanguageCommunication.getPreferenceInd().isSetNullFlavor()) {
			fhirCommunication.setPreferredElement(dtt.tBL2Boolean(cdaLanguageCommunication.getPreferenceInd()));
		}

		return fhirCommunication;

	}

	@Override
	public EntryResult tManufacturedProduct2Medication(ManufacturedProduct cdaManufacturedProduct,
													   IBundleInfo bundleInfo, ITransformationContext theTransformationContext) {
		EntryResult result = new EntryResult();

		if (cdaManufacturedProduct == null || cdaManufacturedProduct.isSetNullFlavor()) {
			return result;
		}

		Medication fhirMedication = new Medication();
		IMedicationsInformation medsInfo = null;
		CD cd = null;
		List<II> orgIds = null;
		Map<String, ManufacturedProduct> medDedupMap = bundleInfo.getMedicationDedupMap();

		if (cdaManufacturedProduct.getManufacturedMaterial() != null
				&& !cdaManufacturedProduct.getManufacturedMaterial().isSetNullFlavor()) {

			cd = cdaManufacturedProduct.getManufacturedMaterial().getCode();

			if (cd != null) {
				if (medDedupMap.containsKey(cd.getCode()) && cd.getTranslations().size() < medDedupMap.get(cd.getCode()).getManufacturedMaterial().getCode().getTranslations().size()) {
					cdaManufacturedProduct = medDedupMap.get(cd.getCode());
					medsInfo = bundleInfo.findResourceResult(medDedupMap.get(cd.getCode()).getManufacturedMaterial().getCode());
				} else {
					medsInfo = bundleInfo.findResourceResult(cd);
				}
				medDedupMap.put(cd.getCode(), cdaManufacturedProduct);
				// manufacturedMaterial.code -> code
				fhirMedication.setCode(dtt.tCD2CodeableConcept(
						cdaManufacturedProduct.getManufacturedMaterial().getCode(), bundleInfo.getIdedAnnotations()));
			}
		}

		// add fhir resource after we check for previous value

		// resource id
		IdType resourceId = new IdType("Medication", theTransformationContext.getUniqueId());
		fhirMedication.setId(resourceId);

		// meta.profile
		if (Config.isGenerateDafProfileMetadata()) {
			fhirMedication.getMeta().addProfile(Constants.PROFILE_DAF_MEDICATION);
		}

		// manufacturerOrganization -> manufacturer
		if (cdaManufacturedProduct.getManufacturerOrganization() != null
				&& !cdaManufacturedProduct.getManufacturerOrganization().isSetNullFlavor()) {

			IEntryResult orgResult = tOrganization2Organization(cdaManufacturedProduct.getManufacturerOrganization(),
					bundleInfo, theTransformationContext);

			org.hl7.fhir.r4.model.Organization fhirOrganization = (Organization) orgResult
					.findResourceResult(org.hl7.fhir.r4.model.Organization.class);

			result.updateFrom(orgResult);

			if (fhirOrganization != null) {
				fhirMedication.setManufacturer(getReference(fhirOrganization));

			}

		}

		if (medsInfo != null) {
			Medication previousMedication = medsInfo.getMedication(cd, orgIds);

			if (previousMedication != null) {
				result.addExistingResource(previousMedication);
			}
		} else {
			if (cd != null) {
				medsInfo = new MedicationsInformation(fhirMedication, cd, orgIds);
				result.putCDResource(cd, medsInfo);
			}
			result.addResource(fhirMedication);
		}
		return result;
	}

	@Override
	public EntryResult tManufacturedProduct2Medication(Product cdaProduct, IBundleInfo bundleInfo, ITransformationContext theTransformationContext) {

		if (cdaProduct == null || cdaProduct.isSetNullFlavor()) {
			return new EntryResult();
		}

		return tManufacturedProduct2Medication(cdaProduct.getManufacturedProduct(), bundleInfo, theTransformationContext);
	}

	@Override
	public EntryResult tManufacturedProduct2Medication(Consumable cdaConsumable, IBundleInfo bundleInfo, ITransformationContext theTransformationContext) {

		if (cdaConsumable == null || cdaConsumable.isSetNullFlavor()) {
			return new EntryResult();
		}

		return tManufacturedProduct2Medication(cdaConsumable.getManufacturedProduct(), bundleInfo, theTransformationContext);

	}

	@Override
	public EntryResult tMedicationActivity2MedicationStatement(MedicationActivity cdaMedicationActivity,
															   IBundleInfo bundleInfo, ITransformationContext theTransformationContext) {

		LocalBundleInfo localBundleInfo = new LocalBundleInfo(bundleInfo);
		EntryResult result = new EntryResult();

		if (cdaMedicationActivity == null || cdaMedicationActivity.isSetNullFlavor()) {
			return result;
		}

		MedicationStatement fhirMedSt = new MedicationStatement();
		org.hl7.fhir.r4.model.Dosage fhirDosage = fhirMedSt.addDosage();
		DosageDoseAndRateComponent fhirDosageAndRate = null;
		result.addResource(fhirMedSt);

		// resource id
		IdType resourceId = new IdType("MedicationStatement", theTransformationContext.getUniqueId());
		fhirMedSt.setId(resourceId);

		// meta.profile
		if (Config.isGenerateDafProfileMetadata()) {
			fhirMedSt.getMeta().addProfile(Constants.PROFILE_DAF_MEDICATION_STATEMENT);
		}

		// patient
		fhirMedSt.setSubject(theTransformationContext.getPatientRef());

		// id -> identifier
		if (cdaMedicationActivity.getIds() != null && !cdaMedicationActivity.getIds().isEmpty()) {
			for (II ii : cdaMedicationActivity.getIds()) {
				if (ii != null && !ii.isSetNullFlavor()) {
					fhirMedSt.addIdentifier(dtt.tII2Identifier(ii));
				}
			}
		} else {
			theTransformationContext.addWarning(MEDICATION_ACTIVITY_ENTRY, IDENTIFIER_FIELD, MIN_CARDINALITY_MESSAGE);
		}

		// statusCode -> status
		if (cdaMedicationActivity.getStatusCode() != null && !cdaMedicationActivity.getStatusCode().isSetNullFlavor()) {
			String cdaStatusCode = cdaMedicationActivity.getStatusCode().getCode();
			if (cdaStatusCode != null
					&& !cdaStatusCode.isEmpty()) {
				MedicationStatementStatus statusCode = vst.tStatusCode2MedicationStatementStatus(cdaStatusCode);
				if (statusCode != null) {
					fhirMedSt.setStatus(statusCode);
				} else {
					theTransformationContext.addWarning(MEDICATION_ACTIVITY_ENTRY, STATUS_CODE_FIELD,
							MessageFormat.format(UNPARSEABLE_CODE_MESSAGE, cdaStatusCode));
				}
			}
		}
		if (!fhirMedSt.hasStatus()) {
			theTransformationContext.addWarning(MEDICATION_ACTIVITY_ENTRY, STATUS_CODE_FIELD, MessageFormat.format(MISSING_CODE_WITH_DEFAULT_MESSAGE, Config.DEFAULT_MEDICATION_STATEMENT_STATUS));
			fhirMedSt.setStatus(Config.DEFAULT_MEDICATION_STATEMENT_STATUS);
		}

		// author[0] -> informationSource
		if (!cdaMedicationActivity.getAuthors().isEmpty()) {
			if (!cdaMedicationActivity.getAuthors().get(0).isSetNullFlavor()) {
				EntityResult entityResult = tAuthor2Practitioner(cdaMedicationActivity.getAuthors().get(0), bundleInfo, theTransformationContext);
				result.updateFrom(entityResult);
				localBundleInfo.updateFrom(entityResult);
				if (entityResult.hasPractitioner()) {
					fhirMedSt.setInformationSource(getReference(entityResult.getPractitioner()));
				}
			}
		}

		// consumable.manufacturedProduct -> medication
		if (cdaMedicationActivity.getConsumable() != null && !cdaMedicationActivity.getConsumable().isSetNullFlavor()) {
			EntryResult fhirMedicationResult = tManufacturedProduct2Medication(cdaMedicationActivity.getConsumable(),
					localBundleInfo, theTransformationContext);
			result.updateFrom(fhirMedicationResult);
			localBundleInfo.updateFrom(fhirMedicationResult);

			Medication medication = (Medication) fhirMedicationResult.findResourceResult(Medication.class);

			if (medication != null) {
				fhirMedSt.setMedication(getReference(medication));
			}

		} else {
			theTransformationContext.addWarning(MEDICATION_ACTIVITY_ENTRY, CONSUMABLE_FIELD, MISSING_ENTRIES_MESSAGE);
		}

		// getting info from effectiveTimes
		if (cdaMedicationActivity.getEffectiveTimes() != null && !cdaMedicationActivity.getEffectiveTimes().isEmpty()) {
			for (org.openhealthtools.mdht.uml.hl7.datatypes.SXCM_TS ts : cdaMedicationActivity.getEffectiveTimes()) {
				if (ts != null && !ts.isSetNullFlavor()) {
					// effectiveTime[@xsi:type='IVL_TS'] -> effective
					if (ts instanceof IVL_TS) {
						fhirMedSt.setEffective(dtt.tIVL_TS2Period((IVL_TS) ts));
					}
					// effectiveTime[@xsi:type='PIVL_TS'] -> dosage.timing
					if (ts instanceof PIVL_TS) {
						fhirDosage.setTiming(dtt.tPIVL_TS2Timing((PIVL_TS) ts));
					}
				}
			}
		} else {
			theTransformationContext.addWarning(MEDICATION_ACTIVITY_ENTRY, EFFECTIVE_TIME_FIELD, MIN_CARDINALITY_MESSAGE);
		}

		// doseQuantity -> dosage.quantity
		if (cdaMedicationActivity.getDoseQuantity() != null
				&& !cdaMedicationActivity.getDoseQuantity().isSetNullFlavor()) {
			if (fhirDosageAndRate == null) {
				fhirDosageAndRate = fhirDosage.addDoseAndRate();
			}
			SimpleQuantity dose = dtt.tPQ2SimpleQuantity(cdaMedicationActivity.getDoseQuantity(), false);
			// manually set dose system, source object doesn't support it.
			dose.setSystem(vst.tOid2Url("2.16.840.1.113883.1.11.12839"));
			fhirDosageAndRate.setDose(dose);
		} else {
			theTransformationContext.addWarning(MEDICATION_ACTIVITY_ENTRY, DOSE_QUANTITY_FIELD, MISSING_VALUE_MESSAGE);
		}

		// routeCode -> dosage.route
		if (cdaMedicationActivity.getRouteCode() != null && !cdaMedicationActivity.getRouteCode().isSetNullFlavor()) {
			fhirDosage.setRoute(dtt.tCD2CodeableConcept(cdaMedicationActivity.getRouteCode()));
		}

		// rateQuantity -> dosage.rate
		if (cdaMedicationActivity.getRateQuantity() != null
				&& !cdaMedicationActivity.getRateQuantity().isSetNullFlavor()) {
			if (fhirDosageAndRate == null) {
				fhirDosageAndRate = fhirDosage.addDoseAndRate();
			}
			fhirDosageAndRate.setRate(dtt.tIVL_PQ2Range(cdaMedicationActivity.getRateQuantity(), false));
		}

		// maxDoseQuantity -> dosage.maxDosePerPeriod
		if (cdaMedicationActivity.getMaxDoseQuantity() != null
				&& !cdaMedicationActivity.getMaxDoseQuantity().isSetNullFlavor()) {
			// cdaDataType.RTO does nothing but extends cdaDataType.RTO_PQ_PQ
			fhirDosage.setMaxDosePerPeriod(dtt.tRTO2Ratio((RTO) cdaMedicationActivity.getMaxDoseQuantity()));
		}

		if (cdaMedicationActivity.getEntryRelationships() != null
				&& !cdaMedicationActivity.getEntryRelationships().isEmpty()) {

			for (EntryRelationship er : cdaMedicationActivity.getEntryRelationships()) {

				if (er.getTypeCode() != null && er.getInversionInd() != null) {
					// If entry relationship contains frequency observation instruction
					if (er.getTypeCode().equals(x_ActRelationshipEntryRelationship.SUBJ) && er.getInversionInd()) {
						if (er.getObservation() != null && !er.getObservation().isSetNullFlavor()) {
							Observation obs = er.getObservation();
							if (obs.getClassCode() != null && obs.getMoodCode() != null) {
								if (obs.getClassCode().equals(ActClassObservation.OBS)
										&& obs.getMoodCode().equals(x_ActMoodDocumentObservation.EVN)) {
									if (obs.getCode() != null && obs.getCode().getCode().contentEquals("FREQUENCY")) {
										if (!obs.getValues().isEmpty()) {
											ANY valueElement = obs.getValues().get(0);
											// Instruction.Frequency -> Dosage.timing
											if (((ED) valueElement).getText() != null) {
												CodeableConcept timingCoding = new CodeableConcept();
												Timing timing = new Timing();
												fhirDosage.setTiming(timing);
												timingCoding.setText(((ED) valueElement).getText());
												timing.setCode(timingCoding);
											}
										}
									}
								}
							}
						}
					}
				} else if (er.getTypeCode() != null) {
					// if entry relationship contains Medication Free Text Signature
					if (er.getTypeCode().equals(x_ActRelationshipEntryRelationship.COMP)) {
						if (er.getSubstanceAdministration() != null
								&& !er.getSubstanceAdministration().isSetNullFlavor()) {
							SubstanceAdministration sa = er.getSubstanceAdministration();
							if (sa.getClassCode() != null && sa.getMoodCode() != null) {
								// substance administration is a Medication Free Text Sig
								if (sa.getClassCode().equals(ActClass.SBADM)
										&& (sa.getMoodCode().equals(x_DocumentSubstanceMood.EVN)
										|| sa.getMoodCode().equals(x_DocumentSubstanceMood.INT))) {

									String freeTextInstruction = dtt.tED2Annotation(sa.getText(),
											bundleInfo.getIdedAnnotations());

									if (freeTextInstruction != null) {
										// Medication Free Text Sig -> Dosage.text/Dosage.PatientInstructions
										fhirDosage.setText(freeTextInstruction);
										fhirDosage.setPatientInstruction(freeTextInstruction);
									}
								}
							}
						}
					}
				}
			}
		}

		// taken -> MedicationStatement.status.UNKNOWN
		// Add the extension to the resource
		fhirMedSt.addExtension("http://hl7.org/fhir/3.0/StructureDefinition/extension-MedicationStatement.status", new CodeType("UNKNOWN"));

		// indication -> reason
		for (Indication indication : cdaMedicationActivity.getIndications()) {
			IEntryResult condResult = tIndication2ConditionProblemListItem(indication, localBundleInfo, theTransformationContext);

			result.updateFrom(condResult);

			Condition fhirCondition = (Condition) condResult.findResourceResult(Condition.class);

			if (fhirCondition != null) {
				fhirMedSt.addReasonReference(getReference(fhirCondition));
			}
		}

		if (cdaMedicationActivity.getMedicationSupplyOrder() != null) {
			IEntryResult medRequestResult = medicationSupplyOrder2MedicationRequest(
					cdaMedicationActivity.getMedicationSupplyOrder(), localBundleInfo, theTransformationContext);
			localBundleInfo.updateFrom(medRequestResult);
			result.updateFrom(medRequestResult);
		}

		EList<MedicationDispense> dispenses = cdaMedicationActivity.getMedicationDispenses();
		if (dispenses != null && !dispenses.isEmpty()) {
			MedicationDispense dispense = dispenses.get(0); // Cardinality is 1 in spec
			IEntryResult medDispenseResult = tMedicationDispense2MedicationDispense(dispense, localBundleInfo, theTransformationContext);
			localBundleInfo.updateFrom(medDispenseResult);
			result.updateFrom(medDispenseResult);
		}

		return result;
	}

	@Override
	public EntryResult tMedicationDispense2MedicationDispense(
			MedicationDispense cdaMedicationDispense, IBundleInfo bundleInfo, ITransformationContext theTransformationContext) {

		LocalBundleInfo localBundleInfo = new LocalBundleInfo(bundleInfo);
		EntryResult result = new EntryResult();

		if (cdaMedicationDispense == null || cdaMedicationDispense.isSetNullFlavor()) {
			return result;
		}

		// NOTE: Following mapping doesn't really suit the mapping proposed by daf

		org.hl7.fhir.r4.model.MedicationDispense fhirMediDisp = new org.hl7.fhir.r4.model.MedicationDispense();
		result.addResource(fhirMediDisp);

		// patient
		fhirMediDisp.setSubject(theTransformationContext.getPatientRef());

		// resource id
		IdType resourceId = new IdType("MedicationDispense", theTransformationContext.getUniqueId());
		fhirMediDisp.setId(resourceId);

		// meta.profile
		if (Config.isGenerateDafProfileMetadata()) {
			fhirMediDisp.getMeta().addProfile(Constants.PROFILE_DAF_MEDICATION_DISPENSE);
		}

		// id -> identifier
		if (cdaMedicationDispense.getIds() != null & !cdaMedicationDispense.getIds().isEmpty()) {
			for (II ii : cdaMedicationDispense.getIds()) {
				if (ii != null && !ii.isSetNullFlavor()) {
					// Asserting at most one identifier exists
					fhirMediDisp.addIdentifier(dtt.tII2Identifier(ii));
				}
			}
		} else {
			theTransformationContext.addWarning(MEDICATION_DISPENSE_ENTRY, IDENTIFIER_FIELD, MIN_CARDINALITY_MESSAGE);
		}

		// statusCode -> status
		if (cdaMedicationDispense.getStatusCode() != null && !cdaMedicationDispense.getStatusCode().isSetNullFlavor()) {
			String cdaStatusCode = cdaMedicationDispense.getStatusCode().getCode();
			if (cdaStatusCode != null && !cdaStatusCode.isEmpty()) {
				org.hl7.fhir.r4.model.MedicationDispense.MedicationDispenseStatus mediDispStatEnum =
						vst.tStatusCode2MedicationDispenseStatus(cdaStatusCode);
				if (mediDispStatEnum != null) {
					fhirMediDisp.setStatus(mediDispStatEnum);
				} else {
					theTransformationContext.addWarning(MEDICATION_DISPENSE_ENTRY, STATUS_CODE_FIELD,
							MessageFormat.format(UNPARSEABLE_CODE_MESSAGE, cdaStatusCode));
				}
			}
		} else {
			theTransformationContext.addWarning(MEDICATION_DISPENSE_ENTRY, STATUS_CODE_FIELD, MISSING_VALUE_MESSAGE);
		}

		// product -> medication (reference)
		if (cdaMedicationDispense.getProduct() != null && !cdaMedicationDispense.getProduct().isSetNullFlavor()) {
			EntryResult fhirMedicationResult = tManufacturedProduct2Medication(cdaMedicationDispense.getProduct(),
					localBundleInfo, theTransformationContext);
			result.updateFrom(fhirMedicationResult);
			localBundleInfo.updateFrom(fhirMedicationResult);

			Medication medication = (Medication) fhirMedicationResult.findResourceResult(Medication.class);

			if (medication != null) {
				fhirMediDisp.setMedication(getReference(medication));
			}

		}

		// performer -> performer
		if (cdaMedicationDispense.getPerformers() != null && !cdaMedicationDispense.getPerformers().isEmpty()) {
			for (org.openhealthtools.mdht.uml.cda.Performer2 cdaPerformer : cdaMedicationDispense.getPerformers()) {
				if (cdaPerformer != null && !cdaPerformer.isSetNullFlavor()) {
					EntityResult entityResult = tPerformer22Practitioner(cdaPerformer, localBundleInfo, theTransformationContext);
					localBundleInfo.updateFrom(entityResult);
					result.updateFrom(entityResult);
					if (entityResult.hasPractitioner()) {
						fhirMediDisp.addPerformer().setActor(getReference(entityResult.getPractitioner()));
					}
				}
			}
		}

		// quantity -> quantity
		if (cdaMedicationDispense.getQuantity() != null && !cdaMedicationDispense.getQuantity().isSetNullFlavor()) {
			fhirMediDisp.setQuantity(dtt.tPQ2SimpleQuantity(cdaMedicationDispense.getQuantity(), false));
		}

		// whenPrepared and whenHandedOver
		// effectiveTime[0] -> whenPrepared, effectiveTime[1] -> whenHandedOver
		int effectiveTimeCount = 0;
		if (cdaMedicationDispense.getEffectiveTimes() != null && !cdaMedicationDispense.getEffectiveTimes().isEmpty()) {
			for (SXCM_TS ts : cdaMedicationDispense.getEffectiveTimes()) {
				if (effectiveTimeCount == 0) {
					// effectiveTime[0] -> whenPrepared
					if (ts != null && !ts.isSetNullFlavor()) {
						fhirMediDisp.setWhenPreparedElement(dtt.tTS2DateTime(ts));
					}
					effectiveTimeCount++;
				} else if (effectiveTimeCount == 1) {
					// effectiveTime[1] -> whenHandedOver
					if (ts != null && !ts.isSetNullFlavor()) {
						fhirMediDisp.setWhenHandedOverElement(dtt.tTS2DateTime(ts));
					}
					effectiveTimeCount++;
				}
			}
		}

		// Adding dosageInstruction
		org.hl7.fhir.r4.model.Dosage fhirDosageInstruction = fhirMediDisp.addDosageInstruction();

		// TODO: The information used for dosageInstruction is used for different
		// fields, too.
		// Determine which field the information should fit

		// effectiveTimes -> dosageInstruction.timing.event
		if (cdaMedicationDispense.getEffectiveTimes() != null && !cdaMedicationDispense.getEffectiveTimes().isEmpty()) {
			Timing fhirTiming = new Timing();

			// adding effectiveTimes to fhirTiming
			for (org.openhealthtools.mdht.uml.hl7.datatypes.SXCM_TS ts : cdaMedicationDispense.getEffectiveTimes()) {
				if (ts != null && !ts.isSetNullFlavor()) {
					fhirTiming.getEvent().add(dtt.tTS2DateTime(ts));
				} else if (ts.getValue() != null && !ts.getValue().isEmpty()) {
					fhirTiming.getEvent().add(dtt.tString2DateTime(ts.getValue()));
				}
			}

			// setting fhirTiming for dosageInstruction if it is not empty
			if (!fhirTiming.isEmpty()) {
				fhirDosageInstruction.setTiming(fhirTiming);
			}
		}

		// quantity -> dosageInstruction.dose
		if (cdaMedicationDispense.getQuantity() != null && !cdaMedicationDispense.getQuantity().isSetNullFlavor()) {
			fhirDosageInstruction.addDoseAndRate().setDose(dtt.tPQ2SimpleQuantity(cdaMedicationDispense.getQuantity(), false));
		}

		return result;
	}

	@Override
	public EntryResult medicationSupplyOrder2MedicationRequest(MedicationSupplyOrder cdaSupplyOrder,
															   IBundleInfo bundleInfo, ITransformationContext theTransformationContext) {

		LocalBundleInfo localBundleInfo = new LocalBundleInfo(bundleInfo);
		EntryResult result = new EntryResult();

		if (cdaSupplyOrder == null || cdaSupplyOrder.isSetNullFlavor()) {
			return result;
		}

		MedicationRequest fhirMedicationRequest = new MedicationRequest();
		result.addResource(fhirMedicationRequest);

		// subject
		fhirMedicationRequest.setSubject(theTransformationContext.getPatientRef());

		// resource id
		IdType resourceId = new IdType("MedicationRequest", theTransformationContext.getUniqueId());
		fhirMedicationRequest.setId(resourceId);

		// meta.profile
		if (Config.isGenerateDafProfileMetadata()) {
			fhirMedicationRequest.getMeta().addProfile(Constants.PROFILE_DAF_MEDICATION_REQUEST);
		}

		// id -> identifier
		if (cdaSupplyOrder.getIds() != null && !cdaSupplyOrder.getIds().isEmpty()) {
			for (II ii : cdaSupplyOrder.getIds()) {
				if (ii != null && !ii.isSetNullFlavor()) {
					fhirMedicationRequest.addIdentifier(dtt.tII2Identifier(ii));
				}
			}
		} else {
			addIdentifierDataAbsentReasonExtension(fhirMedicationRequest, NullFlavor.NI);
			theTransformationContext.addWarning(MEDICATION_SUPPLY_ORDER_ENTRY, IDENTIFIER_FIELD, MIN_CARDINALITY_MESSAGE);
		}

		// statusCode -> status
		if (cdaSupplyOrder.getStatusCode() != null && !cdaSupplyOrder.getStatusCode().isSetNullFlavor()) {
			fhirMedicationRequest.setStatus(vst.tActStatus2MedicationRequestStatus(cdaSupplyOrder.getStatusCode().getCode()));
		} else {
			theTransformationContext.addWarning(MEDICATION_SUPPLY_ORDER_ENTRY, STATUS_CODE_FIELD, MISSING_VALUE_MESSAGE);
		}

		// intent
		// hardcoded to "instance-order"
		fhirMedicationRequest.setIntent(MedicationRequestIntent.INSTANCEORDER);

		// product.manufacturedProduct(MedicationInformation ||
		// ImmunizationMedicationInformation) -> medication
		if (cdaSupplyOrder.getProduct() != null && !cdaSupplyOrder.getProduct().isSetNullFlavor()) {

			if (cdaSupplyOrder.getProduct().isSetNullFlavor()) {
				fhirMedicationRequest.getMedicationCodeableConcept().addExtension(dtt.tNullFlavor2DataAbsentReasonExtension(cdaSupplyOrder.getProduct().getNullFlavor()));
			} else {
				EntryResult fhirMedicationResult = tManufacturedProduct2Medication(cdaSupplyOrder.getProduct(),
						localBundleInfo, theTransformationContext);
				result.updateFrom(fhirMedicationResult);
				localBundleInfo.updateFrom(fhirMedicationResult);

				Medication medication = (Medication) fhirMedicationResult.findResourceResult(Medication.class);

				if (medication != null) {
					fhirMedicationRequest.setMedication(getReference(medication));
				}
			}
		}

		if (cdaSupplyOrder.getQuantity() != null || cdaSupplyOrder.getRepeatNumber() != null
				|| cdaSupplyOrder.getEffectiveTimes() != null) {
			MedicationRequestDispenseRequestComponent dispenseRequest = new MedicationRequestDispenseRequestComponent();
			fhirMedicationRequest.setDispenseRequest(dispenseRequest);
			// quantity -> dispenseRequest.quantity
			if (cdaSupplyOrder.getQuantity() != null && cdaSupplyOrder.getQuantity().getValue() != null
					&& !cdaSupplyOrder.getQuantity().isSetNullFlavor()) {
				SimpleQuantity sq = new SimpleQuantity();
				sq.setValue(cdaSupplyOrder.getQuantity().getValue());
				sq.setUnit(cdaSupplyOrder.getQuantity().getUnit());
				dispenseRequest.setQuantity(sq);
			}
			// repeatNumber -> dispenseRequest.numberOfRepeatsAllowed
			if (cdaSupplyOrder.getRepeatNumber() != null && !cdaSupplyOrder.isSetNullFlavor()) {
				if (cdaSupplyOrder.getRepeatNumber().getValue() != null) {
					dispenseRequest.setNumberOfRepeatsAllowed(cdaSupplyOrder.getRepeatNumber().getValue().intValue());
				}
			}

			// effectiveTime -> dispenseRequest.validityPeriod
			if (cdaSupplyOrder.getEffectiveTimes() != null && !cdaSupplyOrder.getEffectiveTimes().isEmpty()) {
				for (SXCM_TS ts : cdaSupplyOrder.getEffectiveTimes()) {
					if (ts instanceof IVL_TS) {
						Period period = dtt.tIVL_TS2Period((IVL_TS) ts, false);
						dispenseRequest.setValidityPeriod(period);
					}
				}
			}
		}

		// instructions -> notes
		if (cdaSupplyOrder.getInstructions() != null && !cdaSupplyOrder.getInstructions().isSetNullFlavor()) {
			Instructions instructions = cdaSupplyOrder.getInstructions();
			List<Annotation> annotations = new ArrayList<Annotation>();
			for (Act act : instructions.getActs()) {
				Annotation annotation = new Annotation();
				if (act.getText() != null) {
					annotation.setText(act.getText().getText());
					annotations.add(annotation);
				}
			}
			fhirMedicationRequest.setNote(annotations);
		}

		// author -> requester
		if (!cdaSupplyOrder.getAuthors().isEmpty()) {
			Author author = cdaSupplyOrder.getAuthors().get(0);
			EntityResult entityResult = tAuthor2Practitioner(author, localBundleInfo, theTransformationContext);
			localBundleInfo.updateFrom(entityResult);
			result.updateFrom(entityResult);
			if (entityResult.hasPractitioner()) {
				fhirMedicationRequest.setRequester(getReference(entityResult.getPractitioner()));
			}
		}

		return result;
	}

	@Override
	public IEntryResult tMedicationActivity2MedicationRequest(MedicationActivity theMedActivity,
															  IBundleInfo theBundleInfo,
															  ITransformationContext theTransformationContext) {

		LocalBundleInfo localBundleInfo = new LocalBundleInfo(theBundleInfo);
		EntryResult result = new EntryResult();

		if (theMedActivity == null || theMedActivity.isSetNullFlavor()) {
			return result;
		}

		MedicationRequest fhirMedicationRequest = new MedicationRequest();
		result.addResource(fhirMedicationRequest);

		// subject
		fhirMedicationRequest.setSubject(theTransformationContext.getPatientRef());

		// encounter
//		fhirMedicationRequest.setEncounter(theTransformationContext.getEncounterRef());

		// resource id
		IdType resourceId = new IdType("MedicationRequest", theTransformationContext.getUniqueId());
		fhirMedicationRequest.setId(resourceId);

		// meta.profile
		if (Config.isGenerateDafProfileMetadata()) {
			fhirMedicationRequest.getMeta().addProfile(Constants.PROFILE_DAF_MEDICATION_REQUEST);
		}

		// id -> identifier
		if (theMedActivity.getIds() != null && !theMedActivity.getIds().isEmpty()) {
			for (II ii : theMedActivity.getIds()) {
				if (ii != null && !ii.isSetNullFlavor()) {
					fhirMedicationRequest.addIdentifier(dtt.tII2Identifier(ii));
				}
			}
		} else {
			addIdentifierDataAbsentReasonExtension(fhirMedicationRequest, NullFlavor.NI);
			theTransformationContext.addWarning(MEDICATION_SUPPLY_ORDER_ENTRY, IDENTIFIER_FIELD, MIN_CARDINALITY_MESSAGE);
		}

		// statusCode -> status
		if (theMedActivity.getStatusCode() != null && !theMedActivity.getStatusCode().isSetNullFlavor()) {
			fhirMedicationRequest.setStatus(vst.tActStatus2MedicationRequestStatus(theMedActivity.getStatusCode().getCode()));
		} else {
			theTransformationContext.addWarning(MEDICATION_SUPPLY_ORDER_ENTRY, STATUS_CODE_FIELD, MISSING_VALUE_MESSAGE);
		}

		// (hardcoded to "order") -> intent
		fhirMedicationRequest.setIntent(MedicationRequestIntent.ORDER);

		// product.manufacturedProduct(MedicationInformation ||
		// ImmunizationMedicationInformation) -> medication
		MedicationSupplyOrder medicationSupplyOrder = theMedActivity.getMedicationSupplyOrder();
		if (medicationSupplyOrder.getProduct() != null) {

			if (medicationSupplyOrder.getProduct().isSetNullFlavor()) {
				fhirMedicationRequest.getMedicationCodeableConcept().addExtension(dtt.tNullFlavor2DataAbsentReasonExtension(medicationSupplyOrder.getProduct().getNullFlavor()));
			} else {
				EntryResult fhirMedicationResult = tManufacturedProduct2Medication(medicationSupplyOrder.getProduct(),
						localBundleInfo, theTransformationContext);
				result.updateFrom(fhirMedicationResult);
				localBundleInfo.updateFrom(fhirMedicationResult);

				Medication medication = (Medication) fhirMedicationResult.findResourceResult(Medication.class);

				if (medication != null) {
					fhirMedicationRequest.setMedication(getReference(medication));

				}
			}
		}

		if (medicationSupplyOrder.getQuantity() != null || theMedActivity.getRepeatNumber() != null
				|| theMedActivity.getEffectiveTimes() != null) {
			MedicationRequestDispenseRequestComponent dispenseRequest = new MedicationRequestDispenseRequestComponent();
			fhirMedicationRequest.setDispenseRequest(dispenseRequest);
			// quantity -> dispenseRequest.quantity
			if (medicationSupplyOrder.getQuantity() != null && medicationSupplyOrder.getQuantity().getValue() != null
					&& !medicationSupplyOrder.getQuantity().isSetNullFlavor()) {
				SimpleQuantity sq = new SimpleQuantity();
				sq.setValue(medicationSupplyOrder.getQuantity().getValue());
				sq.setUnit(medicationSupplyOrder.getQuantity().getUnit());
				dispenseRequest.setQuantity(sq);
			}
			// repeatNumber -> dispenseRequest.numberOfRepeatsAllowed
			if (theMedActivity.getRepeatNumber() != null && !theMedActivity.isSetNullFlavor()) {
				if (theMedActivity.getRepeatNumber().getValue() != null) {
					dispenseRequest.setNumberOfRepeatsAllowed(theMedActivity.getRepeatNumber().getValue().intValue());
				}
			}

			// effectiveTime -> dispenseRequest.validityPeriod
			if (theMedActivity.getEffectiveTimes() != null && !theMedActivity.getEffectiveTimes().isEmpty()) {
				for (SXCM_TS ts : theMedActivity.getEffectiveTimes()) {
					if (ts instanceof IVL_TS) {
						Period period = dtt.tIVL_TS2Period((IVL_TS) ts, false);
						dispenseRequest.setValidityPeriod(period);
					}
				}
			}
		}

		// instructions -> notes
		if (theMedActivity.getInstructions() != null && !theMedActivity.getInstructions().isSetNullFlavor()) {
			Instructions instructions = theMedActivity.getInstructions();
			List<Annotation> annotations = new ArrayList<Annotation>();
			for (Act act : instructions.getActs()) {
				Annotation annotation = new Annotation();
				if (act.getText() != null) {
					annotation.setText(act.getText().getText());
					annotations.add(annotation);
				}
			}
			fhirMedicationRequest.setNote(annotations);
		}

		// author -> requester
		if (!theMedActivity.getAuthors().isEmpty()) {
			Author author = theMedActivity.getAuthors().get(0);
			EntityResult entityResult = tAuthor2Practitioner(author, localBundleInfo, theTransformationContext);
			localBundleInfo.updateFrom(entityResult);
			result.updateFrom(entityResult);
			if (entityResult.hasPractitioner()) {
				fhirMedicationRequest.setRequester(getReference(entityResult.getPractitioner()));
			}

			// author.time -> authoredOn
			TS authoredOn = author.getTime();
			if (authoredOn != null && !authoredOn.isSetNullFlavor()) {
				fhirMedicationRequest.setAuthoredOn(dtt.tTS2Date(authoredOn).getValue());
			}
		}

		// (hard-coded to "false") -> reportedBoolean
		fhirMedicationRequest.setReported(new BooleanType(false));
		theTransformationContext.addWarning(MEDICATION_ACTIVITY_ENTRY, VALUE_FIELD, MEDICATION_REQUEST_HARD_CODED_MESSAGE);

		// substanceAdministration[2.16.840.1.113883.10.20.22.4.147] -> dosageInstruction
		for (SubstanceAdministration substanceAdmin : theMedActivity.getSubstanceAdministrations()) {
			if (substanceAdmin != null && !substanceAdmin.isSetNullFlavor()) {
				if (isFreeTextSig(substanceAdmin)) {
					String freeTextInstruction = dtt.tED2Annotation(substanceAdmin.getText(),
							theBundleInfo.getIdedAnnotations());

					if (freeTextInstruction != null) {
						// Medication Free Text Sig -> Dosage.text/Dosage.PatientInstructions
						fhirMedicationRequest.addDosageInstruction(
								new Dosage()
										.setText(freeTextInstruction)
										.setPatientInstruction(freeTextInstruction));
					}
				}
			}
		}

		return result;
	}

	private boolean isFreeTextSig(SubstanceAdministration substanceAdmin) {
		return substanceAdmin.getTemplateIds().stream().map(II::getRoot).anyMatch(MEDICATION_FREE_TEXT_SIG_TEMPLATE_OID::equals);
	}

	@Override
	public EntryResult tMedicationInformation2Medication(MedicationInformation cdaMedicationInformation,
														 IBundleInfo bundleInfo, ITransformationContext theTransformationContext) {
		/*
		 * Since MedicationInformation is a ManufacturedProduct instance with a specific
		 * templateId, tManufacturedProduct2Medication should satisfy the required
		 * mapping for MedicationInformation
		 */
		return tManufacturedProduct2Medication(cdaMedicationInformation, bundleInfo, theTransformationContext);
	}

	@Override
	public EntryResult tObservation2Observation(Observation cdaObservation,
												IBundleInfo bundleInfo, ITransformationContext theTransformationContext) {
		EntryResult result = new EntryResult();

		if (cdaObservation == null || cdaObservation.isSetNullFlavor()) {
			return result;
		}

		org.hl7.fhir.r4.model.Observation fhirObs = new org.hl7.fhir.r4.model.Observation();
		result.addResource(fhirObs);

		// resource id
		IdType resourceId = new IdType("Observation", theTransformationContext.getUniqueId());
		fhirObs.setId(resourceId);

		// subject
		fhirObs.setSubject(theTransformationContext.getPatientRef());

		// id -> identifier
		if (cdaObservation.getIds() != null && !cdaObservation.getIds().isEmpty()) {
			for (II ii : cdaObservation.getIds()) {
				if (ii != null && !ii.isSetNullFlavor()) {
					fhirObs.addIdentifier(dtt.tII2Identifier(ii));
				}
			}
		}

		// code -> code
		if (cdaObservation.getCode() != null) {
			fhirObs.setCode(dtt.tCD2CodeableConcept(cdaObservation.getCode(), bundleInfo.getIdedAnnotations()));
		}

		// statusCode -> status
		if (cdaObservation.getStatusCode() != null && !cdaObservation.getStatusCode().isSetNullFlavor()) {
			if (cdaObservation.getStatusCode().getCode() != null) {
				fhirObs.setStatus(
						vst.tObservationStatusCode2ObservationStatus(cdaObservation.getStatusCode().getCode()));

			}
		}

		// effectiveTime -> effective
		if (cdaObservation.getEffectiveTime() != null && !cdaObservation.getEffectiveTime().isSetNullFlavor()) {
			fhirObs.setEffective(dtt.tIVL_TS2Period(cdaObservation.getEffectiveTime()));
		}

		// targetSiteCode -> bodySite
		for (CD cd : cdaObservation.getTargetSiteCodes()) {
			if (cd != null && !cd.isSetNullFlavor()) {
				fhirObs.setBodySite(dtt.tCD2CodeableConcept(cd));
			}
		}

		// value or dataAbsentReason
		if (cdaObservation.getValues() != null && !cdaObservation.getValues().isEmpty()) {
			// We traverse the values in cdaObs
			for (ANY value : cdaObservation.getValues()) {
				if (value == null) {
					continue; // If the value is null, continue
				} else if (value.isSetNullFlavor()) {
					// If a null flavor exists, then we set dataAbsentReason by looking at the
					// null-flavor value
					Coding dataAbsentReasonCode = vst.tNullFlavor2DataAbsentReasonCode(value.getNullFlavor());
					if (dataAbsentReasonCode != null) {
						fhirObs.getDataAbsentReason().addCoding(dataAbsentReasonCode);
					} else {
						theTransformationContext.addWarning(OBSERVATION_ENTRY, VALUE_NULL_FLAVOR_FIELD,
								MessageFormat.format(UNPARSEABLE_CODE_MESSAGE, value.getNullFlavor()));
					}
				} else {
					// If a non-null value which has no null-flavor exists, then we can get the
					// value
					// Checking the type of value
					if (value instanceof CD) {
						fhirObs.setValue(dtt.tCD2CodeableConcept((CD) value));
					} else if (value instanceof IVL_PQ) {
						fhirObs.setValue(dtt.tIVL_PQ2Range((IVL_PQ) value, true));
					} else if (value instanceof PQ) {
						fhirObs.setValue(dtt.tPQ2Quantity((PQ) value, true));
					} else if (value instanceof ST) {
						fhirObs.setValue(dtt.tST2String((ST) value));
					} else if (value instanceof RTO) {
						fhirObs.setValue(dtt.tRTO2Ratio((RTO) value));
					} else if (value instanceof ED) {
						fhirObs.setValue(new StringType(dtt.tED2Annotation((ED) value, bundleInfo.getIdedAnnotations())));
					} else if (value instanceof TS) {
						fhirObs.setValue(dtt.tTS2DateTime((TS) value));
					} else if (value instanceof BL) {
						fhirObs.setValue(dtt.tBL2Boolean((BL) value));
					} else if (value instanceof REAL) {

						fhirObs.setValue(dtt.tREAL2Quantity((REAL) value));

						// Epic specific: attempt to get units from custom observation.
						handleEpicCustomObservationUnits(cdaObservation, fhirObs);
					} else {
						theTransformationContext.addWarning(OBSERVATION_ENTRY, OBSERVATION_VALUE_FIELD,
								MessageFormat.format(UNSUPPORTED_TYPE_MESSAGE, value.getClass().getName()));
					}
				}
			}
		}

		// author -> performer
		for (org.openhealthtools.mdht.uml.cda.Author author : cdaObservation.getAuthors()) {
			if (author != null && !author.isSetNullFlavor()) {
				EntityResult entityResult = tAuthor2Practitioner(author, bundleInfo, theTransformationContext);
				result.updateFrom(entityResult);
				if (entityResult.hasPractitioner()) {
					fhirObs.addPerformer().setReference(entityResult.getPractitionerId());
				}
			}
		}

		// methodCode -> method
		for (CE method : cdaObservation.getMethodCodes()) {
			if (method != null && !method.isSetNullFlavor()) {
				// Asserting that only one method exists
				fhirObs.setMethod(dtt.tCD2CodeableConcept(method));
			}
		}

		// author.time -> issued
		if (cdaObservation.getAuthors() != null && !cdaObservation.getAuthors().isEmpty()) {
			for (org.openhealthtools.mdht.uml.cda.Author author : cdaObservation.getAuthors()) {
				if (author != null && !author.isSetNullFlavor()) {
					// get time from author
					if (author.getTime() != null && !author.getTime().isSetNullFlavor()) {
						fhirObs.setIssuedElement(dtt.tTS2Instant(author.getTime()));
					}
				}
			}
		}

		// interpretationCode -> interpretation
		if (cdaObservation.getInterpretationCodes() != null && !cdaObservation.getInterpretationCodes().isEmpty()) {
			for (org.openhealthtools.mdht.uml.hl7.datatypes.CE cdaInterprCode : cdaObservation
					.getInterpretationCodes()) {
				if (cdaInterprCode != null && !cdaInterprCode.isSetNullFlavor()) {
					// Asserting that only one interpretation code exists
					fhirObs.addInterpretation(
							vst.tObservationInterpretationCode2ObservationInterpretationCode(cdaInterprCode, theTransformationContext));
				}
			}
		}

		// referenceRange -> referenceRange
		if (cdaObservation.getReferenceRanges() != null && !cdaObservation.getReferenceRanges().isEmpty()) {
			for (org.openhealthtools.mdht.uml.cda.ReferenceRange cdaReferenceRange : cdaObservation
					.getReferenceRanges()) {
				if (cdaReferenceRange != null && !cdaReferenceRange.isSetNullFlavor()) {
					fhirObs.addReferenceRange(tReferenceRange2ReferenceRange(cdaReferenceRange));
				}
			}
		}

		return result;
	}

	private void handleEpicCustomObservationUnits(Observation cdaObservation, org.hl7.fhir.r4.model.Observation fhirObs) {

		for (EntryRelationship er : cdaObservation.getEntryRelationships()) {
			Observation obs = er.getObservation();
			if (obs != null) {
				if (obs.getCode() != null) {
					if (obs.getCode().getCodeSystem() != null && obs.getCode().getCode() != null) {
						// Look for SNOMED unit encoding.
						if (obs.getCode().getCodeSystem().equals(SNOMED_OID)
								&& obs.getCode().getCode().equals(SNOMED_UNITS_VALUE)) {
							for (ANY val : obs.getValues()) {
								if (val instanceof ST) {
									ST stVal = (ST) val;
									String units = stVal.getText();
									Quantity fhirVal = (Quantity) fhirObs.getValue();

									fhirVal.setUnit(units)
											.setCode(units)
											.setSystem(UNITS_OF_MEASURE_URL);
									break;
								}
							}
						}
					}
				}
			}
		}
	}

	@Override
	public IEntryResult tOrganization2Organization(org.openhealthtools.mdht.uml.cda.Organization cdaOrganization,
												   IBundleInfo bundleInfo, ITransformationContext theTransformationContext) {
		EntryResult result = new EntryResult();
		Set<String> orgNameSet = bundleInfo.getOrganizationDedupMap();
		boolean inSet = false;
		if (cdaOrganization == null || cdaOrganization.isSetNullFlavor()) {
			return result;
		}

		org.hl7.fhir.r4.model.Organization fhirOrganization = new org.hl7.fhir.r4.model.Organization();

		// resource id
		IdType resourceId = new IdType("Organization", theTransformationContext.getUniqueId());
		fhirOrganization.setId(resourceId);

		// meta.profile
		if (Config.isGenerateDafProfileMetadata()) {
			fhirOrganization.getMeta().addProfile(Constants.PROFILE_DAF_ORGANIZATION);
		}

		// id -> identifier
		if (cdaOrganization.getIds() != null && !cdaOrganization.getIds().isEmpty()) {
			org.hl7.fhir.r4.model.Organization previousOrganization = (Organization) bundleInfo
					.findResourceResult(cdaOrganization.getIds(), org.hl7.fhir.r4.model.Organization.class);

			if (previousOrganization != null) {
				result.addExistingResource(previousOrganization);
				return result;
			} else {
				result.putIIResource(cdaOrganization.getIds(), org.hl7.fhir.r4.model.Organization.class,
						fhirOrganization);
			}

			for (II ii : cdaOrganization.getIds()) {
				if (ii != null && !ii.isSetNullFlavor()) {
					fhirOrganization.addIdentifier(dtt.tII2Identifier(ii));
				}
			}
		}

		// -> active
		fhirOrganization.setActive(true);

		// name -> name
		if (cdaOrganization.getNames() != null && !cdaOrganization.isSetNullFlavor()) {

			int namesLength = cdaOrganization.getNames().size();

			for (int iter = 0; iter < namesLength; ++iter) {
				ON name = cdaOrganization.getNames().get(iter);
				if (name != null && !name.isSetNullFlavor() && name.getText() != null && !name.getText().isEmpty()) {
					if (iter == 0) {
						fhirOrganization.setName(name.getText());
					} else {
						fhirOrganization.addAlias(name.getText());
					}
					if (orgNameSet.contains(name.getText())) {
						inSet = true;
					}

					orgNameSet.add(name.getText());
				}
			}

		}

		// telecom -> telecom
		if (cdaOrganization.getTelecoms() != null && !cdaOrganization.getTelecoms().isEmpty()) {
			for (TEL tel : cdaOrganization.getTelecoms()) {
				if (tel != null && !tel.isSetNullFlavor()) {
					fhirOrganization.addTelecom(dtt.tTEL2ContactPoint(tel));
				}
			}
		}

		// addr -> address
		if (cdaOrganization.getAddrs() != null && !cdaOrganization.getAddrs().isEmpty()) {
			for (AD ad : cdaOrganization.getAddrs()) {
				if (ad != null && !ad.isSetNullFlavor()) {
					fhirOrganization.addAddress(dtt.AD2Address(ad));
				}
			}
		}

		// organizations must have either a name or an identifier.
		if (!fhirOrganization.hasName() && !fhirOrganization.hasIdentifier()) {
			return result;
		}
		if (!inSet) {
			result.addResource(fhirOrganization);
		}

		return result;

	}

	@Override
	public IEntryResult tParticipantRole2Location(ParticipantRole cdaParticipantRole, IBundleInfo bundleInfo, ITransformationContext theTransformationContext) {
		EntryResult result = new EntryResult();

		if (cdaParticipantRole == null || cdaParticipantRole.isSetNullFlavor()) {
			return null;
		}

		org.hl7.fhir.r4.model.Location fhirLocation = new org.hl7.fhir.r4.model.Location();

		// id -> identifier
		if (cdaParticipantRole.getIds() != null && !cdaParticipantRole.getIds().isEmpty()) {
			org.hl7.fhir.r4.model.Location previousLocation = (Location) bundleInfo
					.findResourceResult(cdaParticipantRole.getIds(), org.hl7.fhir.r4.model.Location.class);

			if (previousLocation != null) {
				result.addExistingResource(previousLocation);
				return result;
			} else {
				result.putIIResource(cdaParticipantRole.getIds(), org.hl7.fhir.r4.model.Location.class, fhirLocation);
			}

			for (II ii : cdaParticipantRole.getIds()) {
				if (ii != null && !ii.isSetNullFlavor()) {
					fhirLocation.addIdentifier(dtt.tII2Identifier(ii));
				}
			}
		}

		// resource id
		IdType resourceId = new IdType("Location", theTransformationContext.getUniqueId());
		fhirLocation.setId(resourceId);

		// meta.profile
		if (Config.isGenerateDafProfileMetadata()) {
			fhirLocation.getMeta().addProfile(Constants.PROFILE_DAF_LOCATION);
		}

		// code -> type
		// TODO: Requires huge mapping work from HL7 HealthcareServiceLocation value set
		// to http://hl7.org/fhir/ValueSet/v3-ServiceDeliveryLocationRoleType
		if (cdaParticipantRole.getCode() != null && !cdaParticipantRole.getCode().isSetNullFlavor()) {
			logger.info(
					"Found location.code in the CDA document, which can be mapped to Location.type on the FHIR side. But this is skipped for the moment, as it requires huge mapping work from HL7 HealthcareServiceLocation value set to http://hl7.org/fhir/ValueSet/v3-ServiceDeliveryLocationRoleType");
			// fhirLocation.setType();
		}

		// playingEntity.name.text -> name
		if (cdaParticipantRole.getPlayingEntity() != null && !cdaParticipantRole.getPlayingEntity().isSetNullFlavor()) {
			if (cdaParticipantRole.getPlayingEntity().getNames() != null
					&& !cdaParticipantRole.getPlayingEntity().getNames().isEmpty()) {
				for (PN pn : cdaParticipantRole.getPlayingEntity().getNames()) {
					// Asserting that at most one name exists
					if (pn != null && !pn.isSetNullFlavor()) {
						fhirLocation.setName(pn.getText());
					}
				}
			}
		}

		// telecom -> telecom
		if (cdaParticipantRole.getTelecoms() != null && !cdaParticipantRole.getTelecoms().isEmpty()) {
			for (TEL tel : cdaParticipantRole.getTelecoms()) {
				if (tel != null) {
					if (tel.isSetNullFlavor()) {
						ContactPoint telecom = fhirLocation.addTelecom();
						telecom.addExtension(dtt.tNullFlavor2DataAbsentReasonExtension(tel.getNullFlavor()));
					} else {
						fhirLocation.addTelecom(dtt.tTEL2ContactPoint(tel));
					}
				}
			}
		}

		// addr -> address
		if (cdaParticipantRole.getAddrs() != null && !cdaParticipantRole.getAddrs().isEmpty()) {
			for (AD ad : cdaParticipantRole.getAddrs()) {
				// Asserting that at most one address exists
				if (ad != null && !ad.isSetNullFlavor()) {
					fhirLocation.setAddress(dtt.AD2Address(ad));
				}
			}
		}

		// Double check for duplicates - by business key
		if (fhirLocation.hasName()) {
			if (!bundleInfo.getLocationDedupMap().contains(fhirLocation.getName())) {
				result.addResource(fhirLocation);
				bundleInfo.getLocationDedupMap().add(fhirLocation.getName());
			}
		} else {
			result.addResource(fhirLocation);
		}

		return result;
	}

	@Override
	public IEntryResult tPatientRole2Patient(PatientRole cdaPatientRole, IBundleInfo bundleInfo, ITransformationContext theTransformationContext) {
		EntryResult result = new EntryResult();

		if (cdaPatientRole == null || cdaPatientRole.isSetNullFlavor()) {
			return result;
		}

		Patient fhirPatient = new Patient();

		result.addResource(fhirPatient);
		// resource id
		IdType resourceId = new IdType("Patient", theTransformationContext.getUniqueId());
		fhirPatient.setId(resourceId);

		// meta.profile
		if (Config.isGenerateDafProfileMetadata()) {
			fhirPatient.getMeta().addProfile(Constants.PROFILE_DAF_PATIENT);
		}

		// id -> identifier
		if (cdaPatientRole.getIds() != null && !cdaPatientRole.getIds().isEmpty()) {

			for (II id : cdaPatientRole.getIds()) {
				if (id != null && !id.isSetNullFlavor()) {
					fhirPatient.addIdentifier(dtt.tII2Identifier(id));
				}
			}
		}

		// addr -> address
		for (AD ad : cdaPatientRole.getAddrs()) {
			if (ad != null && !ad.isSetNullFlavor()) {
				fhirPatient.addAddress(dtt.AD2Address(ad));
			}
		}

		// telecom -> telecom
		for (TEL tel : cdaPatientRole.getTelecoms()) {
			if (tel != null && !tel.isSetNullFlavor()) {
				fhirPatient.addTelecom(dtt.tTEL2ContactPoint(tel));
			}
		}

		// providerOrganization -> managingOrganization
		if (cdaPatientRole.getProviderOrganization() != null
				&& !cdaPatientRole.getProviderOrganization().isSetNullFlavor()) {

			IEntryResult orgResult = tOrganization2Organization(cdaPatientRole.getProviderOrganization(), bundleInfo, theTransformationContext);

			org.hl7.fhir.r4.model.Organization fhirOrganization = (Organization) orgResult
					.findResourceResult(org.hl7.fhir.r4.model.Organization.class);
			result.updateFrom(orgResult);

			if (fhirOrganization != null) {
				fhirPatient.setManagingOrganization(getReference(fhirOrganization));
			}

		}

		org.openhealthtools.mdht.uml.cda.Patient cdaPatient = cdaPatientRole.getPatient();

		if (cdaPatient != null && !cdaPatient.isSetNullFlavor()) {
			// patient.name -> name
			for (PN pn : cdaPatient.getNames()) {
				if (pn != null && !pn.isSetNullFlavor()) {
					fhirPatient.addName(dtt.tEN2HumanName(pn, theTransformationContext));
				}
			}

			// patient.administrativeGenderCode -> gender
			if (cdaPatient.getAdministrativeGenderCode() != null
					&& !cdaPatient.getAdministrativeGenderCode().isSetNullFlavor()
					&& cdaPatient.getAdministrativeGenderCode().getCode() != null
					&& !cdaPatient.getAdministrativeGenderCode().getCode().isEmpty()) {
				AdministrativeGender administrativeGender = vst.tAdministrativeGenderCode2AdministrativeGender(
						cdaPatient.getAdministrativeGenderCode().getCode());
				fhirPatient.setGender(administrativeGender);
			}

			// patient.birthTime -> birthDate
			if (cdaPatient.getBirthTime() != null && !cdaPatient.getBirthTime().isSetNullFlavor()) {
				fhirPatient.setBirthDateElement(dtt.tTS2Date(cdaPatient.getBirthTime()));
			}

			// patient.maritalStatusCode -> maritalStatus
			if (cdaPatient.getMaritalStatusCode() != null && !cdaPatient.getMaritalStatusCode().isSetNullFlavor()) {
				if (cdaPatient.getMaritalStatusCode().getCode() != null
						&& !cdaPatient.getMaritalStatusCode().getCode().isEmpty()) {
					fhirPatient.getMaritalStatus().addCoding(
							vst.tMaritalStatusCode2MaritalStatusCode(cdaPatient.getMaritalStatusCode().getCode()));
				}
			}

			// patient.languageCommunication -> communication
			for (LanguageCommunication LC : cdaPatient.getLanguageCommunications()) {
				if (LC != null && !LC.isSetNullFlavor()) {
					fhirPatient.addCommunication(tLanguageCommunication2Communication(LC));
				}
			}

			// patient.guardian -> contact
			for (org.openhealthtools.mdht.uml.cda.Guardian guardian : cdaPatient.getGuardians()) {
				if (guardian != null && !guardian.isSetNullFlavor()) {
					fhirPatient.addContact(tGuardian2Contact(guardian, theTransformationContext));
				}
			}

			//patient.religiousAffiliationCode -> ext-patient-religion
			if (cdaPatient.getReligiousAffiliationCode() != null && !cdaPatient.getReligiousAffiliationCode().isSetNullFlavor()) {
				if (cdaPatient.getReligiousAffiliationCode().getCode() != null
						&& !cdaPatient.getReligiousAffiliationCode().getCode().isEmpty()) {
					Extension religionExtension = createExtension(FHIR_STRUCTURE_PATIENT_RELIGION_URI, dtt.tCD2CodeableConcept(cdaPatient.getReligiousAffiliationCode()));
					fhirPatient.addExtension(religionExtension);
				}
			}

			//ext-us-core-race
			if (cdaPatient.getRaceCode()!= null) {
				if (!cdaPatient.getRaceCode().isSetNullFlavor()) {
					if (cdaPatient.getRaceCode().getDisplayName() != null
							&& !cdaPatient.getRaceCode().getDisplayName().isEmpty()) {
						Extension raceExtension = new Extension();
						raceExtension.setUrl(FHIR_STRUCTURE_US_CORE_RACE_URI);
						//patient.sdtc:raceCode
						List<Coding> listOfDetailValues = new ArrayList<>();
						for (CE sdtRaceCode : cdaPatient.getSDTCRaceCodes()) {
							if (sdtRaceCode != null && !sdtRaceCode.isSetNullFlavor()) {
								listOfDetailValues.add(dtt.tCD2Coding(sdtRaceCode));
							}
						}
						createExtensionsWithinRootExtension(raceExtension, cdaPatient.getRaceCode().getDisplayName(), dtt.tCD2Coding(cdaPatient.getRaceCode()), listOfDetailValues);
						fhirPatient.addExtension(raceExtension);
					}
				} else {
					Extension raceExtension = new Extension().setUrl(FHIR_STRUCTURE_US_CORE_RACE_URI);

					Extension ombCategoryExtension = createExtension("ombCategory", null);
					ombCategoryExtension.addExtension(dtt.tNullFlavor2DataAbsentReasonExtension(
							cdaPatient.getRaceCode().getNullFlavor()));
					raceExtension.addExtension(ombCategoryExtension);

					fhirPatient.addExtension(raceExtension);
				}
			}

			//ext-us-core-ethnicity
			if (cdaPatient.getEthnicGroupCode() != null) {
				if (!cdaPatient.getEthnicGroupCode().isSetNullFlavor()) {
					if (cdaPatient.getEthnicGroupCode().getDisplayName() != null
							&& !cdaPatient.getEthnicGroupCode().getDisplayName().isEmpty()) {
						Extension ethnicityExtension = new Extension();
						ethnicityExtension.setUrl(FHIR_STRUCTURE_US_CORE_ETHNICITY_URI);
						createExtensionsWithinRootExtension(ethnicityExtension, cdaPatient.getEthnicGroupCode().getDisplayName(), dtt.tCD2Coding(cdaPatient.getEthnicGroupCode()), null);
						fhirPatient.addExtension(ethnicityExtension);
					}
				} else {
					Extension ethnicityExtension = new Extension().setUrl(FHIR_STRUCTURE_US_CORE_ETHNICITY_URI);

					Extension ombCategoryExtension = createExtension("ombCategory", null);
					ombCategoryExtension.addExtension(dtt.tNullFlavor2DataAbsentReasonExtension(
							cdaPatient.getEthnicGroupCode().getNullFlavor()));
					ethnicityExtension.addExtension(ombCategoryExtension);

					fhirPatient.addExtension(ethnicityExtension);
				}
			}

			//patient.birthPlace -> ext-patient-birthPlace
			if (cdaPatient.getBirthplace() != null && cdaPatient.getBirthplace().getPlace() != null) {
				if (cdaPatient.getBirthplace().getPlace().getAddr() != null
						&& !cdaPatient.getBirthplace().getPlace().getAddr().isSetNullFlavor()) {
					Extension birthPlaceExtension = createExtension(FHIR_STRUCTURE_PATIENT_BIRTH_PLACE_URI, dtt.AD2Address(cdaPatient.getBirthplace().getPlace().getAddr()));
					fhirPatient.addExtension(birthPlaceExtension);
				}
			}
		}

		return result;
	}

	private void createExtensionsWithinRootExtension(Extension theRootExtension, String theTextValue, Coding ombCategoryValue, List<Coding> detailedCodeValueList) {
		if (theTextValue == null || theTextValue.isEmpty()) {
			return;
		}
		//.displayName -> .ext-text
		Extension textExtension = createExtension("text", new StringType(theTextValue));
		theRootExtension.addExtension(textExtension);
		//.code -> .ext-ombCategory
		if (ombCategoryValue != null && !ombCategoryValue.isEmpty()) {
			Extension ombCategoryExtension = createExtension("ombCategory", ombCategoryValue);
			theRootExtension.addExtension(ombCategoryExtension);
		}
		//.sdtc:code -> .ext-detailed
		if (detailedCodeValueList != null && !detailedCodeValueList.isEmpty()) {
			for (Coding detailCode : detailedCodeValueList) {
				Extension detailExtension = createExtension("detailed", detailCode);
				theRootExtension.addExtension(detailExtension);
			}
		}
	}

	private Extension createExtension(String theUrl, Type theExtensionValue) {
		Extension newExtension = new Extension();
		newExtension.setUrl(theUrl);
		newExtension.setValue(theExtensionValue);
		return newExtension;
	}

	@Override
	public EntityResult tPerformer22Practitioner(Performer2 cdaPerformer2, IBundleInfo bundleInfo, ITransformationContext theTransformationContext) {
		if (cdaPerformer2 == null || cdaPerformer2.isSetNullFlavor()) {
			return new EntityResult();
		}

		return tAssignedEntity2Practitioner(cdaPerformer2.getAssignedEntity(), bundleInfo, theTransformationContext);
	}

	public EntityResult tPerformer12Practitioner(Performer1 cdaPerformer1, IBundleInfo bundleInfo, ITransformationContext theTransformationContext) {
		if (cdaPerformer1 == null || cdaPerformer1.isSetNullFlavor()) {
			return new EntityResult();
		}

		return tAssignedEntity2Practitioner(cdaPerformer1.getAssignedEntity(), bundleInfo, theTransformationContext);
	}

	@Override
	public EntryResult tProblemConcernAct2Condition(ProblemConcernAct cdaProblemConcernAct, IBundleInfo bundleInfo, ITransformationContext theTransformationContext) {
		EntryResult result = new EntryResult();

		if (cdaProblemConcernAct == null || cdaProblemConcernAct.isSetNullFlavor()) {
			return result;
		}

		// each problem observation instance -> FHIR Condition instance
		LocalBundleInfo localBundleInfo = new LocalBundleInfo(bundleInfo);

		if (cdaProblemConcernAct.getStatusCode() == null || cdaProblemConcernAct.getStatusCode().isSetNullFlavor()) {
			theTransformationContext.addWarning(PROBLEM_CONCERN_ACT_ENTRY, STATUS_CODE_FIELD, MISSING_VALUE_MESSAGE);
		}

		if (cdaProblemConcernAct.getProblemObservations().isEmpty()) {
			theTransformationContext.addWarning(PROBLEM_CONCERN_ACT_ENTRY, SUBJECT_OBSERVATION_RELATIONSHIP, MIN_CARDINALITY_MESSAGE);
		}
		for (ProblemObservation cdaProbObs : cdaProblemConcernAct.getProblemObservations()) {
			EntryResult er = tProblemObservation2Condition(cdaProbObs, localBundleInfo, theTransformationContext);
			localBundleInfo.updateFrom(er);
			result.updateEntitiesFrom(er);
			result.updateFrom(er);

			Bundle fhirProbObsBundle = er.getBundle();
			if (fhirProbObsBundle == null) {
				continue;
			}

			Condition cond = (Condition) er.findResourceResult(Condition.class);

			if (cond != null) {
				CS statusCode = cdaProblemConcernAct.getStatusCode();
				String statusCodeValue = statusCode == null || statusCode.isSetNullFlavor() ? null
						: statusCode.getCode();

				// Updating the mappings for Condition.clinicalStatus - see GL-4209 for details
				// statusCode -> clinicalStatus
				cond.setClinicalStatus(vst.tStatusCode2ConditionClinicalStatus(statusCodeValue));
			}

		}

		return result;
	}

	@Override
	public EntryResult tProblemObservation2Condition(ProblemObservation cdaProbObs, IBundleInfo bundleInfo, ITransformationContext theTransformationContext) {
		EntryResult result = new EntryResult();

		if (cdaProbObs == null || cdaProbObs.isSetNullFlavor()) {
			return result;
		}

		// NOTE: Although DAF requires the mapping for severity, this data is not
		// available on the C-CDA side.
		// NOTE: Problem status template is deprecated in C-CDA Release 2.1; hence
		// status data is not retrieved from this template.

		Condition fhirCondition = new Condition();

		// resource id
		IdType resourceId = new IdType("Condition", theTransformationContext.getUniqueId());
		fhirCondition.setId(resourceId);

		// meta.profile
		if (Config.isGenerateDafProfileMetadata()) {
			fhirCondition.getMeta().addProfile(Constants.PROFILE_DAF_CONDITION);
		}

		// patient
		fhirCondition.setSubject(theTransformationContext.getPatientRef());

		List<II> ids = cdaProbObs.getIds();

		if (ids != null) {
			Condition previousFhirCond = (Condition) bundleInfo.findResourceResult(ids, Condition.class);
			if (previousFhirCond != null) {
				result.addExistingResource(previousFhirCond);
				return result;
			} else {
				result.putIIResource(ids, Condition.class, fhirCondition);
			}
		} else {
			theTransformationContext.addWarning(PROBLEM_OBSERVATION_ENTRY, IDENTIFIER_FIELD, MIN_CARDINALITY_MESSAGE);
		}

		result.addResource(fhirCondition);
		// id -> identifier
		for (II id : ids) {
			if (!id.isSetNullFlavor()) {
				fhirCondition.addIdentifier(dtt.tII2Identifier(id));
			}
		}

		// category -> problem-list-item
		Coding conditionCategory = new Coding().setSystem(CODE_SYSTEM_CONDITION_CATEGORY);
		conditionCategory.setCode("problem-list-item");
		conditionCategory.setDisplay("Problem List Item");
		if (!conditionHasCategory(fhirCondition, conditionCategory)) {
			fhirCondition.addCategory().addCoding(conditionCategory);
		}

		// value -> code
		if (cdaProbObs.getValues() != null && !cdaProbObs.getValues().isEmpty()) {
			if (cdaProbObs.getValues().size() > 1) {
				theTransformationContext.addWarning(PROBLEM_OBSERVATION_ENTRY, VALUE_FIELD,
						MessageFormat.format(MAX_CARDINALITY_MESSAGE, cdaProbObs.getValues().size()));
			}
			for (ANY value : cdaProbObs.getValues()) {
				if (value != null) {
					if (value instanceof CD) {
						CodeableConcept conditionCode = dtt.tCD2CodeableConcept((CD) value, bundleInfo.getIdedAnnotations());
						adjustNoKnownProblemsRepresentation(conditionCode, cdaProbObs.getNegationInd());
						fhirCondition.setCode(conditionCode);
					}
				}
			}
		} else {
			theTransformationContext.addWarning(PROBLEM_OBSERVATION_ENTRY, VALUE_FIELD, MIN_CARDINALITY_MESSAGE);
		}

		// onset and abatement
		if (cdaProbObs.getEffectiveTime() != null && !cdaProbObs.getEffectiveTime().isSetNullFlavor()) {

			IVXB_TS low = cdaProbObs.getEffectiveTime().getLow();
			IVXB_TS high = cdaProbObs.getEffectiveTime().getHigh();

			// low -> onset (if doesn't exist, checking value)
			if (low != null && !low.isSetNullFlavor()) {
				fhirCondition.setOnset(dtt.tTS2DateTime(low));
			} else if (cdaProbObs.getEffectiveTime().getValue() != null
					&& !cdaProbObs.getEffectiveTime().getValue().isEmpty()) {
				fhirCondition.setOnset(dtt.tString2DateTime(cdaProbObs.getEffectiveTime().getValue()));
			}

			// high -> abatement
			if (high != null && !high.isSetNullFlavor()) {
				fhirCondition.setAbatement(dtt.tTS2DateTime(high));
			}
		} else {
			theTransformationContext.addWarning(PROBLEM_OBSERVATION_ENTRY, EFFECTIVE_TIME_FIELD, MISSING_VALUE_MESSAGE);
		}

		// onset and abatement -> clinicalStatus
		if (fhirCondition.getAbatement() != null) {
			fhirCondition.setClinicalStatus(new CodeableConcept(new Coding("http://terminology.hl7.org/CodeSystem/condition-clinical", "inactive", "Inactive")));
		} else {
			fhirCondition.setClinicalStatus(new CodeableConcept(new Coding("http://terminology.hl7.org/CodeSystem/condition-clinical", "active", "Active")));
		}

		// updating mapping for Condition.verificationStatus, because the change to override the Condition.clinicalStatus
		// in Problem Concern Act has left Condition.verificationStatus unmapped. See GL-4209 for details
		Coding verificationStatusCoding = new Coding().setSystem("http://terminology.hl7.org/CodeSystem/condition-ver-status");
		if (Objects.equals(Boolean.TRUE, cdaProbObs.getNegationInd())) {
			verificationStatusCoding.setCode("refuted").setDisplay("Refuted");
		} else {
			verificationStatusCoding.setCode("confirmed").setDisplay("Confirmed");
		}
		fhirCondition.setVerificationStatus(new CodeableConcept(verificationStatusCoding));

		// author[0] -> asserter
		if (!cdaProbObs.getAuthors().isEmpty()) {
			if (cdaProbObs.getAuthors().get(0) != null && !cdaProbObs.getAuthors().get(0).isSetNullFlavor()) {
				Author author = cdaProbObs.getAuthors().get(0);
				EntityResult entityResult = tAuthor2Practitioner(author, bundleInfo, theTransformationContext);
				result.updateFrom(entityResult);
				if (entityResult.hasPractitioner()) {
					fhirCondition.setAsserter(getReference(entityResult.getPractitioner()));
				}
				// author.time -> assertedDate
				if (author.getTime() != null && !author.getTime().isSetNullFlavor()) {
					fhirCondition.setRecordedDateElement(dtt.tTS2DateTime(author.getTime()));
				}
			}
		}

		return result;
	}

	private static void adjustNoKnownProblemsRepresentation(CodeableConcept theConditionCode, Boolean theNegationInd) {
		// 4150 For Tebra - special handling for No Known Problems case
		if (theNegationInd != null && theNegationInd
				&& theConditionCode != null && theConditionCode.hasCoding()
				&& Sets.newHashSet("64572001", "55607006").contains(theConditionCode.getCodingFirstRep().getCode())
				&& StringUtils.equals(SNOMED_URL, theConditionCode.getCodingFirstRep().getSystem())
		) {
			theConditionCode.getCodingFirstRep().setCode("160245001");
			theConditionCode.getCodingFirstRep().setDisplay("No current problems or disability (situation)");
		}
	}

	@Override
	public EntryResult tProcedure2Procedure(Procedure cdaProcedure,
											IBundleInfo bundleInfo, ITransformationContext theTransformationContext) {
		EntryResult result = new EntryResult();

		if (cdaProcedure == null || cdaProcedure.isSetNullFlavor()) {
			return result;
		}

		org.hl7.fhir.r4.model.Procedure fhirProc = new org.hl7.fhir.r4.model.Procedure();
		result.addResource(fhirProc);

		// resource id
		IdType resourceId = new IdType("Procedure", theTransformationContext.getUniqueId());
		fhirProc.setId(resourceId);

		// meta.profile
		if (Config.isGenerateDafProfileMetadata()) {
			fhirProc.getMeta().addProfile(Constants.PROFILE_DAF_PROCEDURE);
		}

		// subject
		fhirProc.setSubject(theTransformationContext.getPatientRef());

		// id -> identifier
		if (cdaProcedure.getIds() != null && !cdaProcedure.getIds().isEmpty()) {
			for (II id : cdaProcedure.getIds()) {
				if (id != null && !id.isSetNullFlavor()) {
					fhirProc.addIdentifier(dtt.tII2Identifier(id));
				}
			}
		} else {
			theTransformationContext.addWarning(PROCEDURE_ENTRY, IDENTIFIER_FIELD, MIN_CARDINALITY_MESSAGE);
		}

		// effectiveTime -> performed
		if (cdaProcedure.getEffectiveTime() != null) {
			if (!cdaProcedure.getEffectiveTime().isSetNullFlavor()) {
				fhirProc.setPerformed(dtt.tIVL_TS2Period(cdaProcedure.getEffectiveTime()));
			} else {
				Period period = new Period();
				period.addExtension(dtt.tNullFlavor2DataAbsentReasonExtension(cdaProcedure.getEffectiveTime().getNullFlavor()));
				fhirProc.setPerformed(period);
			}
		} else {
			Period period = new Period();
			period.addExtension(dtt.tNullFlavor2DataAbsentReasonExtension(NullFlavor.NP));
			fhirProc.setPerformed(period);
			theTransformationContext.addWarning(PROCEDURE_ENTRY, EFFECTIVE_TIME_FIELD, MISSING_VALUE_MESSAGE);
		}

		// targetSiteCode -> bodySite
		if (cdaProcedure.getTargetSiteCodes() != null && !cdaProcedure.getTargetSiteCodes().isEmpty()) {
			for (CD cd : cdaProcedure.getTargetSiteCodes()) {
				if (cd != null && !cd.isSetNullFlavor()) {
					fhirProc.addBodySite(dtt.tCD2CodeableConcept(cd));
				}
			}
		}

		// performer -> performer
		for (Performer2 performer : cdaProcedure.getPerformers()) {
			if (performer.getAssignedEntity() != null && !performer.getAssignedEntity().isSetNullFlavor()) {
				EntityResult entityResult = tPerformer22Practitioner(performer, bundleInfo, theTransformationContext);
				result.updateFrom(entityResult);
				if (!entityResult.isEmpty()) {
					ProcedurePerformerComponent fhirPerformer = new ProcedurePerformerComponent();
					fhirProc.addPerformer(fhirPerformer);
					if (entityResult.hasPractitioner()) {
						fhirPerformer.setActor(getReference(entityResult.getPractitioner()));
					}
					if (entityResult.hasOrganization()) {
						fhirPerformer.setOnBehalfOf(entityResult.getOrganizationReference());
					}
					if (entityResult.hasPractitionerRoleCode()) {
						fhirPerformer.setFunction(entityResult.getPractitionerRoleCode());
					}
				}
			}
		}

		// statusCode -> status
		if (cdaProcedure.getStatusCode() != null && !cdaProcedure.getStatusCode().isSetNullFlavor()
				&& cdaProcedure.getStatusCode().getCode() != null) {
			ProcedureStatus status = vst.tStatusCode2ProcedureStatus(cdaProcedure.getStatusCode().getCode());
			if (status != null) {
				fhirProc.setStatus(status);
			} else {
				theTransformationContext.addWarning(PROCEDURE_ENTRY, STATUS_CODE_FIELD,
						MessageFormat.format(UNPARSEABLE_CODE_MESSAGE, cdaProcedure.getStatusCode().getCode()));
			}
		}

		// code -> code
		CD code = cdaProcedure.getCode();
		if (code != null) {
			CodeableConcept cc = dtt.tCD2CodeableConcept(code, bundleInfo.getIdedAnnotations());
			if (cc != null) {
				fhirProc.setCode(cc);
			}
		}

		// encounter[0] -> context (per spec leave it to encounter section to create the
		// encounter resource)
		EList<org.openhealthtools.mdht.uml.cda.Encounter> encounters = cdaProcedure.getEncounters();
		if (!encounters.isEmpty()) {
			if (encounters.size() > 1) {
				logger.warn("Procedures cannot have multiple encounter. Only using first.");
			}
			org.openhealthtools.mdht.uml.cda.Encounter cdaEncounter = encounters.get(0);
			List<Identifier> identifiers = tIIs2Identifiers(cdaEncounter.getIds());
			if (identifiers != null && !identifiers.isEmpty()) {
				if (identifiers.size() > 1) {
					logger.warn("Procedures encounter cannot have multiple ids. Only using first.");
				}
				IDeferredReference deferredReference = new DeferredProcedureEncounterReference(fhirProc,
						identifiers.get(0));
				result.addDeferredReference(deferredReference);
			}
		}

		List<EntryRelationship> relationships = cdaProcedure.getEntryRelationships();
		if (relationships != null) {
			for (EntryRelationship relationship : relationships) {
				Observation observation = relationship.getObservation();
				if (observation != null && observation instanceof Indication) {
					CodeableConcept cc = dtt.tCD2CodeableConcept(observation.getCode());
					if (cc != null) {
						fhirProc.addReasonCode(cc);
					}
					continue;
				} else if (observation != null && observation instanceof ReactionObservation) {
					IEntryResult entryResult = tReactionObservation2AdverseEvent((ReactionObservation) observation, bundleInfo, theTransformationContext);
					AdverseEvent fhirAdverseEvent = (AdverseEvent) entryResult.findResourceResult(AdverseEvent.class);
					fhirAdverseEvent.addSuspectEntity().setInstance(getReference(fhirProc));
					result.updateFrom(entryResult);
					continue;
				}

				Act act = relationship.getAct();
				if (act != null && act instanceof CommentActivity) {
					String annotation = dtt.tED2Annotation(act.getText(), bundleInfo.getIdedAnnotations());
					if (annotation != null) {
						Annotation fhirAnnotation = new Annotation();
						fhirAnnotation.setText(annotation);
						fhirProc.addNote(fhirAnnotation);
					}
					continue;
				}

				SubstanceAdministration substanceAdministration = relationship.getSubstanceAdministration();
				if (substanceAdministration != null && substanceAdministration instanceof MedicationActivity) {
					EntryResult entryResult = tMedicationActivity2MedicationStatement((MedicationActivity) substanceAdministration, bundleInfo, theTransformationContext);
					MedicationStatement fhirMedicationStatement = (MedicationStatement) entryResult.findResourceResult(MedicationStatement.class);
					fhirMedicationStatement.addPartOf(getReference(fhirProc));
					result.updateFrom(entryResult);
				}
			}
		}

		ImplantedDeviceHelper implantedDeviceHelper = new ImplantedDeviceHelper(dtt, theTransformationContext, bundleInfo);

		if (cdaProcedure instanceof ProcedureActivityProcedure) {
			ProcedureActivityProcedure procedureActivityProcedure = (ProcedureActivityProcedure) cdaProcedure;
			for (ProductInstance productInstance : getProductInstances(procedureActivityProcedure)) {
				EntryResult entryResult = this.tProductInstance2Device(productInstance, bundleInfo, theTransformationContext);
				Device fhirDevice = (Device) entryResult.findResourceResult(Device.class);
				fhirProc.addFocalDevice()
						.setManipulated(getReference(fhirDevice));
				result.updateFrom(entryResult);

				implantedDeviceHelper.findAndAssignManufacturingInfo(procedureActivityProcedure, productInstance, fhirDevice);
			}
		}

		return result;
	}

	private List<ProductInstance> getProductInstances(ProcedureActivityProcedure cdaProcedure) {
		// For some reason, the getProductInstances() method of ProcedureActivityProcedure is always returning an empty
		//  list, even when there is data present. We need to implement our own process for fetching those elements.
		return cdaProcedure.getParticipants().stream()
				.filter(p -> ParticipationType.DEV.equals(p.getTypeCode()))
				.map(Participant2::getParticipantRole)
				.filter(Objects::nonNull)
				.filter(p -> ProductInstance.class.isAssignableFrom(p.getClass()))
				.map(p -> (ProductInstance) p)
				.collect(Collectors.toList());
	}

	@Override
	public EntryResult tProcedureActivityAct2Procedure(ProcedureActivityAct cdaProcedure,
													   IBundleInfo bundleInfo, ITransformationContext theTransformationContext) {
		EntryResult result = new EntryResult();

		if (cdaProcedure == null || cdaProcedure.isSetNullFlavor()) {
			return result;
		}

		org.hl7.fhir.r4.model.Procedure fhirProc = new org.hl7.fhir.r4.model.Procedure();
		result.addResource(fhirProc);

		// resource id
		IdType resourceId = new IdType("Procedure", theTransformationContext.getUniqueId());
		fhirProc.setId(resourceId);

		// meta.profile
		if (Config.isGenerateDafProfileMetadata()) {
			fhirProc.getMeta().addProfile(Constants.PROFILE_DAF_PROCEDURE);
		}

		// subject
		fhirProc.setSubject(theTransformationContext.getPatientRef());

		// id -> identifier
		if (cdaProcedure.getIds() != null && !cdaProcedure.getIds().isEmpty()) {
			for (II id : cdaProcedure.getIds()) {
				if (id != null && !id.isSetNullFlavor()) {
					fhirProc.addIdentifier(dtt.tII2Identifier(id));
				}
			}
		} else {
			theTransformationContext.addWarning(PROCEDURE_ACTIVITY_ACT_ENTRY, IDENTIFIER_FIELD, MIN_CARDINALITY_MESSAGE);
		}

		// effectiveTime -> performed
		if (cdaProcedure.getEffectiveTime() != null) {
			if (!cdaProcedure.getEffectiveTime().isSetNullFlavor()) {
				fhirProc.setPerformed(dtt.tIVL_TS2Period(cdaProcedure.getEffectiveTime()));
			} else {
				Period period = new Period();
				period.addExtension(dtt.tNullFlavor2DataAbsentReasonExtension(cdaProcedure.getEffectiveTime().getNullFlavor()));
				fhirProc.setPerformed(period);
			}
		} else {
			Period period = new Period();
			period.addExtension(dtt.tNullFlavor2DataAbsentReasonExtension(NullFlavor.NP));
			fhirProc.setPerformed(period);
			theTransformationContext.addWarning(PROCEDURE_ACTIVITY_ACT_ENTRY, EFFECTIVE_TIME_FIELD, MISSING_VALUE_MESSAGE);
		}

		// performer -> performer
		for (Performer2 performer : cdaProcedure.getPerformers()) {
			if (performer.getAssignedEntity() != null && !performer.getAssignedEntity().isSetNullFlavor()) {
				EntityResult entityResult = tPerformer22Practitioner(performer, bundleInfo, theTransformationContext);
				result.updateFrom(entityResult);
				if (!entityResult.isEmpty()) {
					ProcedurePerformerComponent fhirPerformer = new ProcedurePerformerComponent();
					fhirProc.addPerformer(fhirPerformer);
					if (entityResult.hasPractitioner()) {
						fhirPerformer.setActor(getReference(entityResult.getPractitioner()));
					}
					if (entityResult.hasOrganization()) {
						fhirPerformer.setOnBehalfOf(entityResult.getOrganizationReference());
					}
					if (entityResult.hasPractitionerRoleCode()) {
						fhirPerformer.setFunction(entityResult.getPractitionerRoleCode());
					}
				}
			}
		}

		// statusCode -> status
		if (cdaProcedure.getStatusCode() != null && !cdaProcedure.getStatusCode().isSetNullFlavor()
				&& cdaProcedure.getStatusCode().getCode() != null) {
			ProcedureStatus status = vst.tStatusCode2ProcedureStatus(cdaProcedure.getStatusCode().getCode());
			if (status != null) {
				fhirProc.setStatus(status);
			} else {
				theTransformationContext.addWarning(PROCEDURE_ACTIVITY_ACT_ENTRY, STATUS_CODE_FIELD,
						MessageFormat.format(UNPARSEABLE_CODE_MESSAGE, cdaProcedure.getStatusCode().getCode()));
			}
		} else {
			theTransformationContext.addWarning(PROCEDURE_ACTIVITY_ACT_ENTRY, STATUS_CODE_FIELD, MISSING_VALUE_MESSAGE);
		}

		// code -> code
		CD code = cdaProcedure.getCode();
		if (code != null) {
			if (code.isSetNullFlavor()) {
				fhirProc.getCode().addExtension(dtt.tNullFlavor2DataAbsentReasonExtension(code.getNullFlavor()));
			} else {
				CodeableConcept cc = dtt.tCD2CodeableConcept(code, bundleInfo.getIdedAnnotations());
				if (cc != null) {
					fhirProc.setCode(cc);
				}
			}
		} else {
			theTransformationContext.addWarning(PROCEDURE_ACTIVITY_ACT_ENTRY, CODE_FIELD, MISSING_VALUE_MESSAGE);
		}


		// encounter[0] -> context (per spec leave it to encounter section to create the
		// encounter resource)
		EList<org.openhealthtools.mdht.uml.cda.Encounter> encounters = cdaProcedure.getEncounters();
		if (!encounters.isEmpty()) {
			if (encounters.size() > 1) {
				theTransformationContext.addWarning(PROCEDURE_ACTIVITY_ACT_ENTRY, ENCOUNTER_RELATIONSHIP,
						MessageFormat.format(PROCEDURE_ENCOUNTER_CARDINALITY_MESSAGE, encounters.size()));
			}
			org.openhealthtools.mdht.uml.cda.Encounter cdaEncounter = encounters.get(0);
			List<Identifier> identifiers = tIIs2Identifiers(cdaEncounter.getIds());
			if (identifiers != null && !identifiers.isEmpty()) {
				IDeferredReference deferredReference = new DeferredProcedureEncounterReference(fhirProc,
						identifiers.get(0));
				result.addDeferredReference(deferredReference);
			}
		}

		List<EntryRelationship> relationships = cdaProcedure.getEntryRelationships();
		if (relationships != null) {
			for (EntryRelationship relationship : relationships) {
				Observation observation = relationship.getObservation();
				if (observation != null && observation instanceof Indication) {
					CodeableConcept cc = dtt.tCD2CodeableConcept(observation.getCode());
					if (cc != null) {
						fhirProc.addReasonCode(cc);
					}
					continue;
				}

				SubstanceAdministration substanceAdministration = relationship.getSubstanceAdministration();
				if (substanceAdministration != null && substanceAdministration instanceof MedicationActivity) {
					EntryResult entryResult = tMedicationActivity2MedicationStatement((MedicationActivity) substanceAdministration, bundleInfo, theTransformationContext);
					MedicationStatement fhirMedicationStatement = (MedicationStatement) entryResult.findResourceResult(MedicationStatement.class);
					fhirMedicationStatement.addPartOf(getReference(fhirProc));
					result.updateFrom(entryResult);
				}
			}
		}

		return result;
	}

	@Override
	public EntryResult tProductInstance2Device(ProductInstance theProductInstance, IBundleInfo theBundleInfo, ITransformationContext theTransformationContext) {
		EntryResult result = new EntryResult();

		if (theProductInstance == null || theProductInstance.isSetNullFlavor()) {
			return result;
		}

		org.hl7.fhir.r4.model.Device fhirDevice = new org.hl7.fhir.r4.model.Device();
		result.addResource(fhirDevice);

		// resource id
		IdType resourceId = new IdType("Device", theTransformationContext.getUniqueId());
		fhirDevice.setId(resourceId);

		// subject
		fhirDevice.setPatient(theTransformationContext.getPatientRef());

		if (theProductInstance.getIds().isEmpty()) {
			theTransformationContext.addWarning(PRODUCT_INSTANCE_ENTRY, IDENTIFIER_FIELD, NO_IDENTIFIERS_MESSAGE);
		} else {
			for (II id : theProductInstance.getIds()) {
				fhirDevice.addIdentifier(dtt.tII2Identifier(id));

				Device.DeviceUdiCarrierComponent udiCarrier = new Device.DeviceUdiCarrierComponent().setIssuer(id.getRoot());
				if (FDA_OID.equals(id.getRoot())) {
					udiCarrier.setCarrierHRF(id.getExtension());
				} else {
					udiCarrier.setDeviceIdentifier(id.getExtension());
				}

				if (!theProductInstance.getScopingEntity().getIds().isEmpty()) {
					udiCarrier.setJurisdiction(
							dtt.tII2Identifier(theProductInstance
											.getScopingEntity()
											.getIds().get(0))
									.getValue());
				} else {
					theTransformationContext.addWarning(PRODUCT_INSTANCE_ENTRY, SCOPING_ENTITY_ID_FIELD, NO_IDENTIFIERS_MESSAGE);
				}

				fhirDevice.addUdiCarrier(udiCarrier);
			}
		}

		fhirDevice.setType(dtt.tCD2CodeableConcept(theProductInstance.getPlayingDevice().getCode()));

		return result;
	}

	@Override
	public EntryResult tProcedureActivityObservation2Observation(ProcedureActivityObservation cdaObservation, IBundleInfo bundleInfo, ITransformationContext theTransformationContext) {
		EntryResult result = tObservation2Observation(cdaObservation, bundleInfo, theTransformationContext);

		org.hl7.fhir.r4.model.Observation fairObservation = (org.hl7.fhir.r4.model.Observation) result.findResourceResult(org.hl7.fhir.r4.model.Observation.class);

		// performer -> performer
		for(Performer2 cdaPerformer : cdaObservation.getPerformers()) {
			EntityResult entityResult = tPerformer22Practitioner(cdaPerformer, bundleInfo, theTransformationContext);
			if (entityResult.hasPractitioner()) {
				fairObservation.addPerformer(getReference(entityResult.getPractitioner()));
				result.updateFrom(entityResult);
			}
		}

		// encounter[0] -> context (per spec leave it to encounter section to create the
		// encounter resource)
		EList<org.openhealthtools.mdht.uml.cda.Encounter> encounters = cdaObservation.getEncounters();
		if (!encounters.isEmpty()) {
			if (encounters.size() > 1) {
				theTransformationContext.addWarning(PROCEDURE_ACTIVITY_OBSERVATION_ENTRY, ENCOUNTER_RELATIONSHIP,
						MessageFormat.format(OBSERVATION_ENCOUNTER_CARDINALITY_MESSAGE, encounters.size()));
			}
			org.openhealthtools.mdht.uml.cda.Encounter cdaEncounter = encounters.get(0);
			List<Identifier> identifiers = tIIs2Identifiers(cdaEncounter.getIds());
			if (identifiers != null && !identifiers.isEmpty()) {
				IDeferredReference deferredReference = new DeferredObservationEncounterReference(fairObservation,
						identifiers.get(0));
				result.addDeferredReference(deferredReference);
			}
		}

		return result;
	}

	@Override
	public EntryResult tReactionObservation2Observation(ReactionObservation cdaReactionObservation,
														IBundleInfo bundleInfo, ITransformationContext theTransformationContext) {
		return tObservation2Observation(cdaReactionObservation, bundleInfo, theTransformationContext);
	}

	@Override
	public IEntryResult tReactionObservation2AdverseEvent(ReactionObservation cdaReactionObservation, IBundleInfo bundleInfo, ITransformationContext theTransformationContext) {
		EntryResult result = new EntryResult();

		AdverseEvent fhirAdverseEvent = new AdverseEvent();
		result.addResource(fhirAdverseEvent);

		// resource id
		IdType resourceId = new IdType("AdverseEvent", theTransformationContext.getUniqueId());
		fhirAdverseEvent.setId(resourceId);

		// subject
		fhirAdverseEvent.setSubject(theTransformationContext.getPatientRef());

		// id -> identifier
		if (cdaReactionObservation.getIds() != null && !cdaReactionObservation.getIds().isEmpty()) {
			for (II id : cdaReactionObservation.getIds()) {
				if (id != null && !id.isSetNullFlavor()) {
					fhirAdverseEvent.setIdentifier(dtt.tII2Identifier(id));
					// The AdverseEvent class only accepts one identifier, so stop once we find a valid one.
					break;
				}
			}
		} else {
			theTransformationContext.addWarning(REACTION_OBSERVATION_ENTRY, IDENTIFIER_FIELD, MIN_CARDINALITY_MESSAGE);
		}

		// code -> actuality
		CD code = cdaReactionObservation.getCode();
		if (code != null) {
			AdverseEvent.AdverseEventActuality fhirActualityCode = vst.tReactionObservationCode2AdverseEventActuality(code.getCode());
			if (fhirActualityCode != null) {
				fhirAdverseEvent.setActuality(fhirActualityCode);
			} else {
				theTransformationContext.addWarning(REACTION_OBSERVATION_ENTRY, CODE_FIELD,
						MessageFormat.format(UNPARSEABLE_CODE_MESSAGE, code.getCode()));
			}
		} else {
			theTransformationContext.addWarning(REACTION_OBSERVATION_ENTRY, CODE_FIELD, MISSING_VALUE_MESSAGE);
		}

		// effectiveTime -> date
		if (cdaReactionObservation.getEffectiveTime() != null && !cdaReactionObservation.getEffectiveTime().isSetNullFlavor()) {
			fhirAdverseEvent.setDateElement(dtt.tIVL_TS2DateTime(cdaReactionObservation.getEffectiveTime()));
		}

		// value -> resultingCondition.code
		if (cdaReactionObservation.getValues() != null && !cdaReactionObservation.getValues().isEmpty()) {
			if (cdaReactionObservation.getValues().size() > 1) {
				theTransformationContext.addWarning(REACTION_OBSERVATION_ENTRY, VALUE_FIELD,
						MessageFormat.format(MAX_CARDINALITY_MESSAGE, cdaReactionObservation.getValues().size()));
			}
			for (ANY value : cdaReactionObservation.getValues()) {
				if (!value.isSetNullFlavor() && CD.class.isAssignableFrom(value.getClass())) {
					CD cdValue = (CD) value;
					Condition fhirCondition = new Condition();
					IdType conditionId = new IdType("Condition", theTransformationContext.getUniqueId());
					fhirCondition.setId(conditionId);
					fhirCondition.setSubject(theTransformationContext.getPatientRef());
					fhirCondition.setCode(dtt.tCD2CodeableConcept(cdValue));
					// TODO: Are there reasonable defaults to set for clinicalStatus, verificationStatus, category or severity?
					result.addResource(fhirCondition);

					fhirAdverseEvent.addResultingCondition(getReference(fhirCondition));
				}
			}
		} else {
			theTransformationContext.addWarning(REACTION_OBSERVATION_ENTRY, VALUE_FIELD, MIN_CARDINALITY_MESSAGE);
		}

		// entryRelationship.observation.value -> severity
		for (EntryRelationship entryRelationship : cdaReactionObservation.getEntryRelationships()) {
			Observation observation = entryRelationship.getObservation();
			if (observation != null && observation instanceof SeverityObservation) {
				SeverityObservation cdaSeverity = (SeverityObservation) observation;
				if (!cdaSeverity.isSetNullFlavor() && !cdaSeverity.getValues().isEmpty()) {
					cdaSeverity.getValues().stream()
							.filter(t -> t instanceof CD)
							.findFirst()
							.ifPresent(any ->
									fhirAdverseEvent.setSeverity(vst.tReactionObservationSeverity2Severity(((CD) any).getCode())));
				}
			}
		}

		return result;
	}

	@Override
	public ObservationReferenceRangeComponent tReferenceRange2ReferenceRange(
			org.openhealthtools.mdht.uml.cda.ReferenceRange cdaReferenceRange) {
		if (cdaReferenceRange == null || cdaReferenceRange.isSetNullFlavor()) {
			return null;
		}

		ObservationReferenceRangeComponent fhirRefRange = new ObservationReferenceRangeComponent();

		// Notice that we get all the desired information from
		// cdaRefRange.ObservationRange
		// We may think of transforming ObservationRange instead of ReferenceRange
		if (cdaReferenceRange.getObservationRange() != null && !cdaReferenceRange.isSetNullFlavor()) {

			// low - high
			if (cdaReferenceRange.getObservationRange().getValue() != null
					&& !cdaReferenceRange.getObservationRange().getValue().isSetNullFlavor()) {
				if (cdaReferenceRange.getObservationRange().getValue() instanceof IVL_PQ) {
					IVL_PQ cdaRefRangeValue = ((IVL_PQ) cdaReferenceRange.getObservationRange().getValue());
					// low
					if (cdaRefRangeValue.getLow() != null && !cdaRefRangeValue.getLow().isSetNullFlavor()) {
						fhirRefRange.setLow(dtt.tPQ2SimpleQuantity(cdaRefRangeValue.getLow(), false));
					}
					// high
					if (cdaRefRangeValue.getHigh() != null && !cdaRefRangeValue.getHigh().isSetNullFlavor()) {
						fhirRefRange.setHigh(dtt.tPQ2SimpleQuantity(cdaRefRangeValue.getHigh(), false));
					}
				}
			}

			// observationRange.interpretationCode -> type
			if (cdaReferenceRange.getObservationRange().getInterpretationCode() != null
					&& !cdaReferenceRange.getObservationRange().getInterpretationCode().isSetNullFlavor()) {
				fhirRefRange.setType(
						dtt.tCD2CodeableConcept(cdaReferenceRange.getObservationRange().getInterpretationCode()));
			}

			// text.text -> text
			if (cdaReferenceRange.getObservationRange().getText() != null
					&& !cdaReferenceRange.getObservationRange().getText().isSetNullFlavor()) {
				if (cdaReferenceRange.getObservationRange().getText().getText() != null
						&& !cdaReferenceRange.getObservationRange().getText().getText().isEmpty()) {
					fhirRefRange.setText(cdaReferenceRange.getObservationRange().getText().getText());
				}
			}
		}

		return fhirRefRange;
	}

	@Override
	public EntryResult tResultObservation2Observation(ResultObservation cdaResultObservation, IBundleInfo bundleInfo, ITransformationContext theTransformationContext) {
		EntryResult result = tObservation2Observation(cdaResultObservation, bundleInfo, theTransformationContext);
		Bundle fhirObservationBundle = result.getBundle();
		if (fhirObservationBundle == null) {
			return result;
		}

		// finding the observation resource and setting its meta.profile to result
		// observation's profile url
		if (Config.isGenerateDafProfileMetadata()) {
			for (BundleEntryComponent entry : fhirObservationBundle.getEntry()) {
				if (entry.getResource() instanceof org.hl7.fhir.r4.model.Observation) {
					(entry.getResource()).getMeta().addProfile(Constants.PROFILE_DAF_RESULT_OBS);
				}
			}
		}

		// set Observation.category
		// NOTE: tObservation2Observation is used by other resource, set Observation.category for the ResultSection only
		for (BundleEntryComponent entry : fhirObservationBundle.getEntry()) {
			if (entry.getResource() instanceof org.hl7.fhir.r4.model.Observation) {
				org.hl7.fhir.r4.model.Observation fhirObs = (org.hl7.fhir.r4.model.Observation)entry.getResource();
				CodeableConcept categoryCC = new CodeableConcept();
				categoryCC.getCodingFirstRep().setSystem(OBSERVATION_CATEGORY_URI).setCode("laboratory");
				fhirObs.addCategory(categoryCC);
			}
		}

		return result;
	}

	@Override
	public EntryResult tResultOrganizer2DiagnosticReport(ResultOrganizer cdaResultOrganizer, IBundleInfo bundleInfo, ITransformationContext theTransformationContext) {
		EntryResult result = new EntryResult();

		if (cdaResultOrganizer == null || cdaResultOrganizer.isSetNullFlavor()) {
			return result;
		}

		DiagnosticReport fhirDiagReport = new DiagnosticReport();
		result.addResource(fhirDiagReport);

		// resource id
		IdType resourceId = new IdType("DiagnosticReport", theTransformationContext.getUniqueId());
		fhirDiagReport.setId(resourceId);

		// meta.profile
		if (Config.isGenerateDafProfileMetadata()) {
			fhirDiagReport.getMeta().addProfile(Constants.PROFILE_DAF_DIAGNOSTIC_REPORT);
		}

		// subject
		fhirDiagReport.setSubject(theTransformationContext.getPatientRef());

		// encounter
		List<org.openhealthtools.mdht.uml.cda.Encounter> cdaEncounters = cdaResultOrganizer.getResultObservations().stream()
				.flatMap(t -> t.getEncounters().stream())
				.filter(t -> !t.getIds().isEmpty())
				.collect(Collectors.toList());
		if (cdaEncounters.size() > 1) {
			theTransformationContext.addWarning(RESULTS_ORGANIZER_OBSERVATION_ENTRY, ENCOUNTER_RELATIONSHIP,
					MessageFormat.format(OBSERVATION_ENCOUNTER_CARDINALITY_MESSAGE, cdaEncounters.size()));
		} else if (cdaEncounters.size() == 1) {
			org.openhealthtools.mdht.uml.cda.Encounter cdaEncounter = cdaEncounters.get(0);
			List<Identifier> identifiers = tIIs2Identifiers(cdaEncounter.getIds());
			IDeferredReference deferredReference = new DeferredDiagnosticReportEncounterReference(fhirDiagReport,
					identifiers.get(0));
			result.addDeferredReference(deferredReference);
		}

		// Although DiagnosticReport.request(DiagnosticOrder) is needed by daf, no
		// information exists in CDA side to fill that field.

		// id -> identifier
		if (cdaResultOrganizer.getIds() != null && !cdaResultOrganizer.getIds().isEmpty()) {
			for (II ii : cdaResultOrganizer.getIds()) {
				if (ii != null && !ii.isSetNullFlavor()) {
					fhirDiagReport.addIdentifier(dtt.tII2Identifier(ii));
				}
			}
		} else {
			theTransformationContext.addWarning(RESULT_ORGANIZER_ENTRY, IDENTIFIER_FIELD, MIN_CARDINALITY_MESSAGE);
		}

		// code -> code; note we make special exceptions here for certain values as mandated by the US Core IG
		// code -> category
		boolean isUsCoreDiagnosticReport = false;
		if (cdaResultOrganizer.getCode() != null) {
			CodeableConcept cd2CodeableConcept = dtt.tCD2CodeableConcept(cdaResultOrganizer.getCode(), bundleInfo.getIdedAnnotations());
			fhirDiagReport.setCode(cd2CodeableConcept);

			isUsCoreDiagnosticReport = usCoreDiagnosticReportCodingsContains(cd2CodeableConcept);
			if (isUsCoreDiagnosticReport) {
				fhirDiagReport.addCategory(cd2CodeableConcept);
			}
		} else {
			theTransformationContext.addWarning(RESULT_ORGANIZER_ENTRY, CODE_FIELD, MISSING_VALUE_MESSAGE);
		}

		if (fhirDiagReport.getCategory().isEmpty()) {
			fhirDiagReport.addCategory(new CodeableConcept(
					new Coding(DIAGNOSTIC_CATEGORY_URI,
							"LAB",
							"Laboratory")));
		}

		// statusCode -> status
		if (cdaResultOrganizer.getStatusCode() != null && !cdaResultOrganizer.isSetNullFlavor()) {
			DiagnosticReport.DiagnosticReportStatus fhirReportStatus = vst.tResultOrganizerStatusCode2DiagnosticReportStatus(cdaResultOrganizer.getStatusCode().getCode());
			if (fhirReportStatus != null) {
				fhirDiagReport.setStatus(fhirReportStatus);
			} else {
				theTransformationContext.addWarning(RESULT_ORGANIZER_ENTRY, STATUS_CODE_FIELD,
						MessageFormat.format(UNPARSEABLE_CODE_MESSAGE, cdaResultOrganizer.getStatusCode().getCode()));
			}
		} else {
			theTransformationContext.addWarning(RESULT_ORGANIZER_ENTRY, STATUS_CODE_FIELD, MISSING_VALUE_MESSAGE);
		}

		// effectiveTime -> effective, issued
		if (cdaResultOrganizer.getEffectiveTime() != null && !cdaResultOrganizer.getEffectiveTime().isSetNullFlavor()) {
			fhirDiagReport.setEffective(dtt.tIVL_TS2Period(cdaResultOrganizer.getEffectiveTime()));
			DateTimeType issueDate = dtt.tIVL_TS2DateTime(cdaResultOrganizer.getEffectiveTime());
			if (issueDate != null)
				fhirDiagReport.setIssued(issueDate.getValue());
		}

		// author -> performer
		if (cdaResultOrganizer.getAuthors() != null && !cdaResultOrganizer.getAuthors().isEmpty()) {
			for (org.openhealthtools.mdht.uml.cda.Author author : cdaResultOrganizer.getAuthors()) {
				// Asserting that at most one author exists
				if (author != null && !author.isSetNullFlavor()) {
					EntityResult entityResult = tAuthor2Practitioner(author, bundleInfo, theTransformationContext);
					result.updateFrom(entityResult);
					// TODO: what about role?
					if (entityResult.hasPractitioner()) {
						fhirDiagReport.addPerformer(getReference(entityResult.getPractitioner()));
					}
				}
			}

		}

		// ResultObservation -> result
		LocalBundleInfo localBundleInfo = new LocalBundleInfo(bundleInfo);
		if (cdaResultOrganizer.getResultObservations().isEmpty()) {
			theTransformationContext.addWarning(RESULT_ORGANIZER_ENTRY, COMPONENT_FIELD, MIN_CARDINALITY_MESSAGE);
		}
		for (ResultObservation cdaResultObs : cdaResultOrganizer.getResultObservations()) {
			if (!cdaResultObs.isSetNullFlavor()) {
				if (isUsCoreDiagnosticReport) {
					for (ANY any : cdaResultObs.getValues()) {
						if (any instanceof ED) {
							fhirDiagReport.addPresentedForm(dtt.tED2Attachment((ED) any));
						}
					}
				}

				EntryResult er = tResultObservation2Observation(cdaResultObs, localBundleInfo, theTransformationContext);
				localBundleInfo.updateFrom(er);
				result.updateEntitiesFrom(er);
				Bundle fhirObsBundle = er.getBundle();
				if (fhirObsBundle != null) {
					for (BundleEntryComponent entry : fhirObsBundle.getEntry()) {
						result.addResource(entry.getResource());
						if (entry.getResource() instanceof org.hl7.fhir.r4.model.Observation) {

							Reference resultRef = new Reference();
							resultRef.setReference(entry.getResource().getId());
							String referenceString = ReferenceInfo.getDisplay(entry.getResource());
							if (referenceString != null) {
								resultRef.setDisplay(referenceString);
							}
							fhirDiagReport.addResult(resultRef);

						}
					}
				}
			}
		}

		return result;
	}

	public boolean usCoreDiagnosticReportCodingsContains(CodeableConcept cd2CodeableConcept) {
		if (cd2CodeableConcept == null) {
			return false;
		}
		if (cd2CodeableConcept.getCoding().isEmpty()) {
			return false;
		}

		return cd2CodeableConcept.getCoding().stream()
				.anyMatch(coding -> US_CORE_DIAGNOSTIC_REPORT_CODINGS.stream()
						.anyMatch(coding::equalsShallow));
	}

	@Override
	public SectionComponent tSection2Section(Section cdaSection) {
		if (cdaSection == null || cdaSection.isSetNullFlavor()) {
			return null;
		}

		SectionComponent fhirSec = new SectionComponent();

		// title -> title.text
		if (cdaSection.getTitle() != null && !cdaSection.getTitle().isSetNullFlavor()) {
			if (cdaSection.getTitle().getText() != null && !cdaSection.getTitle().getText().isEmpty()) {
				fhirSec.setTitle(cdaSection.getTitle().getText());
			}
		}

		// code -> code
		if (cdaSection.getCode() != null && !cdaSection.getCode().isSetNullFlavor()) {
			fhirSec.setCode(dtt.tCD2CodeableConcept(cdaSection.getCode()));
		}

		// text -> text
		if (cdaSection.getText() != null) {
			Narrative fhirText = dtt.tStrucDocText2Narrative(cdaSection.getText());
			if (fhirText != null && Config.getGenerateNarrative()) {
				fhirSec.setText(fhirText);
			}
		}

		fhirSec.setMode(SectionMode.SNAPSHOT);

		return fhirSec;
	}

	@Override
	public IEntryResult tServiceDeliveryLocation2Location(ServiceDeliveryLocation cdaSDLOC, IBundleInfo theBundleInfo, ITransformationContext theTransformationContext) {
		/*
		 * ServiceDeliveryLocation is a ParticipantRole instance with a specific
		 * templateId Therefore, tParticipantRole2Location should satisfy the necessary
		 * mapping
		 */
		return tParticipantRole2Location(cdaSDLOC, theBundleInfo, theTransformationContext);
	}

	public IEntryResult tSocialHistoryObservation2Observation(org.openhealthtools.mdht.uml.cda.Observation cdaObservation,
															  IBundleInfo bundleInfo, ITransformationContext theTransformationContext) {
		IEntryResult result = tObservation2Observation(cdaObservation, bundleInfo, theTransformationContext);

		if (cdaObservation == null || cdaObservation.isSetNullFlavor()) {
			return result;
		}

		org.hl7.fhir.r4.model.Observation fhirObservation =
				(org.hl7.fhir.r4.model.Observation) result.findResourceResult(org.hl7.fhir.r4.model.Observation.class);

		if (PregnancyObservation.class.isAssignableFrom(cdaObservation.getClass())) {
			PregnancyObservation cdaPregnancyObservation = (PregnancyObservation) cdaObservation;
			EstimatedDateOfDelivery dateOfDelivery = cdaPregnancyObservation.getEstimatedDateOfDelivery();

			org.hl7.fhir.r4.model.Observation.ObservationComponentComponent fhirComponent = fhirObservation.addComponent();
			if (dateOfDelivery == null) {
				fhirComponent.getDataAbsentReason().addCoding(vst.tNullFlavor2DataAbsentReasonCode(NullFlavor.UNK));
			} else if (dateOfDelivery.isSetNullFlavor()) {
				fhirComponent.getDataAbsentReason().addCoding(vst.tNullFlavor2DataAbsentReasonCode(dateOfDelivery.getNullFlavor()));
			} else {
				fhirComponent.setCode(dtt.tCD2CodeableConcept(dateOfDelivery.getCode()));
				for (ANY timeStamp : dateOfDelivery.getValues()) {
					// If the document follows the spec, there should only be one value, and it must be TS
					if (TS.class.isAssignableFrom(timeStamp.getClass())) {
						fhirComponent.setValue(dtt.tTS2DateTime((TS) timeStamp));
						break;
					}
				}
			}
		}

		if (CaregiverCharacteristics.class.isAssignableFrom(cdaObservation.getClass())) {
			CaregiverCharacteristics cdaCaregiverCharacteristics = (CaregiverCharacteristics) cdaObservation;
			for (Participant2 participant : cdaCaregiverCharacteristics.getParticipants()) {
				org.hl7.fhir.r4.model.Observation.ObservationComponentComponent fhirComponent = fhirObservation.addComponent();
				IVL_TS participantTime = participant.getTime();
				if (participantTime == null) {
					fhirComponent.getDataAbsentReason().addCoding(vst.tNullFlavor2DataAbsentReasonCode(NullFlavor.UNK));
				} else if (participantTime.isSetNullFlavor()) {
					fhirComponent.getDataAbsentReason().addCoding(vst.tNullFlavor2DataAbsentReasonCode(participantTime.getNullFlavor()));
				} else {
					// TODO: identify a suitable code (LOINC is preferred) for this concept
					fhirComponent.getCode().setText("Period during which the caregiver plays this role");
					fhirComponent.setValue(dtt.tIVL_TS2Period(participantTime));
				}

				if (participant.getParticipantRole() != null && participant.getParticipantRole().getCode() != null) {
					fhirObservation.addFocus().setIdentifier(dtt.tCD2Identifier(participant.getParticipantRole().getCode()));
				}
			}
		}

		//-- issue 3425, add BirthSex to US Core birth sex extension
		if (cdaObservation != null && !cdaObservation.isSetNullFlavor()) {

			if (cdaObservation.getTemplateIds().stream().anyMatch(t -> CDASocialHistorySection.SOCIAL_HISTORY_OBSERVATION_BIRTH_SEX_TEMPLATE_ID.equals(t.getRoot()))) {

				EList<ANY> values = cdaObservation.getValues();
				if (values != null) {
					// We traverse the values in cdaObs
					for (ANY value : values) {
						Extension birthSexExt = dtt.tObservationBirthSexValue2BirthSexExtension(value);
						if (birthSexExt != null) {
							Patient patient = theTransformationContext.getPatient();
							patient.addExtension(birthSexExt);
							break; // Should only have one birth sex
						}
					}
				}
			}
		}

		return result;
	}

	@Override
	public org.hl7.fhir.r4.model.Device tSupply2Device(Supply cdaSupply, ITransformationContext theTransformationContext) {
		if (cdaSupply == null || cdaSupply.isSetNullFlavor()) {
			return null;
		}

		ProductInstance productInstance = null;
		// getting productInstance from cdaSupply.participant.participantRole
		if (cdaSupply.getParticipants() != null && !cdaSupply.getParticipants().isEmpty()) {
			for (Participant2 participant : cdaSupply.getParticipants()) {
				if (participant != null && !participant.isSetNullFlavor()) {
					if (participant.getParticipantRole() != null
							&& !participant.getParticipantRole().isSetNullFlavor()) {
						if (participant.getParticipantRole() instanceof ProductInstance) {
							productInstance = (ProductInstance) participant.getParticipantRole();
						}
					}
				}
			}
		}

		if (productInstance == null) {
			return null;
		}

		org.hl7.fhir.r4.model.Device fhirDev = new org.hl7.fhir.r4.model.Device();

		// resource id
		IdType resourceId = new IdType("Device", theTransformationContext.getUniqueId());
		fhirDev.setId(resourceId);

		// patient
		fhirDev.setPatient(theTransformationContext.getPatientRef());

		// productInstance.id -> identifier
		for (II id : productInstance.getIds()) {
			if (!id.isSetNullFlavor()) {
				fhirDev.addIdentifier(dtt.tII2Identifier(id));
			}
		}

		// productInstance.playingDevice.code -> type
		if (productInstance.getPlayingDevice() != null && !productInstance.getPlayingDevice().isSetNullFlavor()) {
			if (productInstance.getPlayingDevice().getCode() != null
					&& !productInstance.getPlayingDevice().getCode().isSetNullFlavor()) {
				fhirDev.setType(dtt.tCD2CodeableConcept(productInstance.getPlayingDevice().getCode()));
			}
		}

		return fhirDev;
	}

	@Override
	public EntryResult tVitalSignObservation2Observation(VitalSignObservation cdaVitalSignObservation,
														 IBundleInfo bundleInfo, ITransformationContext theTransformationContext) {
		EntryResult result = tObservation2Observation(cdaVitalSignObservation, bundleInfo, theTransformationContext);
		Bundle fhirObservationBundle = result.getBundle();
		if (fhirObservationBundle == null) {
			return result;
		}

		// finding the observation resource and setting its meta.profile to result
		// observation's profile url
		for (BundleEntryComponent entry : fhirObservationBundle.getEntry()) {
			if (entry.getResource() instanceof org.hl7.fhir.r4.model.Observation) {
				if (Config.isGenerateDafProfileMetadata()) {
					(entry.getResource()).getMeta().addProfile(Constants.PROFILE_DAF_VITAL_SIGNS);
				}

				ObservationCategory vitalSigns = ObservationCategory.VITALSIGNS;
				((org.hl7.fhir.r4.model.Observation) entry.getResource()).addCategory(new CodeableConcept(
						new Coding(vitalSigns.getSystem(),
								vitalSigns.toCode(),
								vitalSigns.getDisplay())));
			}
		}

		return result;
	}

	@Override
	public IEntryResult tNutritionalStatusObservation2Observation(Observation cdaObservation, IBundleInfo bundleInfo, ITransformationContext theTransformationContext) {
		return tObservation2Observation(cdaObservation, bundleInfo, theTransformationContext);
	}

	public DocumentReference tDocumentReference(String documentBody, ITransformationContext theTransformationContext) {

		DocumentReference docReference = new DocumentReference();

		// set id
		docReference.setId(new IdType("DocumentReference", theTransformationContext.getUniqueId()));

		// status -> current
		docReference.setStatus(DocumentReferenceStatus.CURRENT);

		// now -> indexed
		Date now = new Date();
		docReference.setDate(now);

		// type -> 34133-9 (hard-coded from specification)
		CodeableConcept docType = new CodeableConcept();
		Coding docTypeCoding = new Coding();
		docTypeCoding.setCode("34133-9");
		docTypeCoding.setSystem("2.16.840.1.113883.6.1");
		docTypeCoding.setDisplay("Summarization of Episode Note");
		docType.addCoding(docTypeCoding);
		docReference.setType(docType);

		// attachment
		Attachment docAttachment = new Attachment();
		docAttachment.setContentType("text/plain");

		// attachment doc
		Base64BinaryType doc64 = new Base64BinaryType(Base64.encode(documentBody.getBytes()));
		docAttachment.setDataElement(doc64);

		// attachment hash
		MessageDigest digest;
		try {
			digest = MessageDigest.getInstance("SHA-1");
			byte[] encodedhash = digest.digest(documentBody.getBytes());
			docAttachment.setHashElement(new Base64BinaryType(Base64.encode(encodedhash)));
		} catch (NoSuchAlgorithmException e) {
			logger.error(e.toString());
		}

		// set attachment
		DocumentReferenceContentComponent docContent = new DocumentReferenceContentComponent();
		docContent.setAttachment(docAttachment);
		docReference.addContent(docContent);

		return docReference;

	}

	public Device tDevice(Identifier assemblerDevice, ITransformationContext theTransformationContext) {
		Device device = new Device();
		device.setStatus(FHIRDeviceStatus.ACTIVE);
		device.addIdentifier(assemblerDevice);

		Narrative deviceNarrative = new Narrative().setStatus(NarrativeStatus.GENERATED);
		deviceNarrative.setDivAsString(assemblerDevice.getValue());
		device.setText(deviceNarrative);

		device.setId(new IdType("Device", theTransformationContext.getUniqueId()));
		return device;
	}

	@Override
	public Bundle tProvenance(Bundle bundle, String documentBody, Identifier assemblerDevice, ITransformationContext theTransformationContext) {

		Provenance provenance = new Provenance();
		provenance.setId(new IdType("Provenance", theTransformationContext.getUniqueId()));

		// recorded
		Date now = new Date();
		provenance.setRecorded(now);

		// document reference
		DocumentReference documentReference = tDocumentReference(documentBody, theTransformationContext);
		bundle.addEntry(new BundleEntryComponent().setResource(documentReference));

		ProvenanceEntityComponent pec = new ProvenanceEntityComponent();
		pec.setRole(ProvenanceEntityRole.SOURCE);
		pec.setWhat(getReference(documentReference));
		provenance.addEntity(pec);

		// -- For Author
		// author agent.type
		ProvenanceAgentComponent pacAuthor = new ProvenanceAgentComponent();
		Coding authorCode = new Coding(ProvenanceAgentType.AUTHOR.getSystem(), ProvenanceAgentType.AUTHOR.toCode(),
				ProvenanceAgentType.AUTHOR.getDisplay());
		pacAuthor.setType(new CodeableConcept().addCoding(authorCode));

		// author agent who/OnBehalfOf
		Composition compositon = FHIRUtil.findFirstResource(bundle, Composition.class);
		if (compositon != null) {
			// composition.author -> who
			pacAuthor.setWho(compositon.getAuthorFirstRep());
			// composition.custodian -> onBehalOf
			pacAuthor.setOnBehalfOf(compositon.getCustodian());
		}
		provenance.addAgent(pacAuthor);

		//-- Transmitter
		ProvenanceAgentComponent pacTransmitter = new ProvenanceAgentComponent();

		// transmitter type
		Coding transmitterCode = new Coding("http://hl7.org/fhir/us/core/CodeSystem/us-core-provenance-participant-type", "transmitter",
				"Transmitter");
		pacTransmitter.setType(new CodeableConcept().addCoding(transmitterCode));
		if (compositon != null) {
			// composition.author -> who
			pacTransmitter.setWho(compositon.getAuthorFirstRep());
			// composition.custodian -> onBehalOf
			pacTransmitter.setOnBehalfOf(compositon.getCustodian());
		}
		provenance.addAgent(pacTransmitter);

		//-- device
		if (assemblerDevice != null) {

			ProvenanceAgentComponent pacDevice = new ProvenanceAgentComponent();
			Device device = tDevice(assemblerDevice, theTransformationContext);
			bundle.addEntry(new BundleEntryComponent().setResource(device));

			// device agent type
			Coding deviceCode = new Coding(ProvenanceAgentType.ASSEMBLER.getSystem(), ProvenanceAgentType.ASSEMBLER.toCode(),
					ProvenanceAgentType.ASSEMBLER.getDisplay());
			pacDevice.setType(new CodeableConcept().addCoding(deviceCode));

			// device agent role
			Coding deviceRole = new Coding(ProvenanceAgentRole.ASSEMBLER.getSystem(),
					ProvenanceAgentRole.ASSEMBLER.toCode(), ProvenanceAgentRole.ASSEMBLER.getDisplay());
			pacDevice.addRole(new CodeableConcept().addCoding(deviceRole));

			// device who
			pacDevice.setWho(getReference(device));
			provenance.addAgent(pacDevice);
		}

		for (BundleEntryComponent bec : bundle.getEntry()) {
			provenance.addTarget(getReference(bec.getResource()));
		}

		bundle.addEntry(new BundleEntryComponent().setResource(provenance));
		return bundle;
	}

	@Override
	public PractitionerRole tParticipant2PractitionerRole(Participant2 cdaParticipant, ITransformationContext theTransformationContext) {

		if (cdaParticipant == null || cdaParticipant.isSetNullFlavor()) {
			return null;
		}

		ParticipantRole cdaParticipantRole = cdaParticipant.getParticipantRole();

		if (cdaParticipantRole == null || cdaParticipantRole.isSetNullFlavor())
			return null;

		PractitionerRole fhirPractitionerRole = new PractitionerRole();

		// 1. resource id
		IdType resourceId = new IdType("PractitionerRole", theTransformationContext.getUniqueId());
		fhirPractitionerRole.setId(resourceId);

		// 2. id -> identifier
		if (cdaParticipantRole.getIds() != null && !cdaParticipantRole.getIds().isEmpty()) {
			for (II ii : cdaParticipantRole.getIds()) {
				if (ii != null && !ii.isSetNullFlavor()) {
					fhirPractitionerRole.addIdentifier(dtt.tII2Identifier(ii));
				}
			}
		}

		return fhirPractitionerRole;
	}

	@Override
	public Practitioner tParticipant2Practitioner(Participant2 cdaParticipant, ITransformationContext theTransformationContext) {

		if (cdaParticipant == null || cdaParticipant.isSetNullFlavor()) {
			return null;
		}

		ParticipantRole cdaParticipantRole = cdaParticipant.getParticipantRole();

		if (cdaParticipantRole == null || cdaParticipantRole.isSetNullFlavor())
			return null;

		PlayingEntity playingEntity = cdaParticipantRole.getPlayingEntity();
		if (playingEntity == null || playingEntity.isSetNullFlavor())
			return null;

		Practitioner fhirPractitioner = new Practitioner();

		// 1. resource id
		IdType resourceId = new IdType("Practitioner", theTransformationContext.getUniqueId());
		fhirPractitioner.setId(resourceId);

		// 2. Practitioner.name
		List<PN> names = playingEntity.getNames();
		if (names != null && !names.isEmpty()) {
			for (PN name : names) {
				HumanName humanName = dtt.tPN2HumanName(name, theTransformationContext);
				if (humanName != null)
					fhirPractitioner.addName(humanName);
			}
		}
		return fhirPractitioner;
	}

	@Override
	public IEntryResult tAct2DocumentReference(StrucDocText theStrucDocText, Act theAct, IBundleInfo theBundleInfo, ITransformationContext theTransformationContext) {

		EntryResult result = new EntryResult();

		if (theAct == null || theAct.isSetNullFlavor()) {
			return result;
		}

		DocumentReference fhirDocRef = new DocumentReference();
		result.addResource(fhirDocRef);

		// 1. DocumentReference.id
		IdType resourceId = new IdType("DocumentReference", theTransformationContext.getUniqueId());
		fhirDocRef.setId(resourceId);

		// 2. text->DocumentReference.Narrative
		if (theStrucDocText != null) {
			Narrative narrative = dtt.tStrucDocText2Narrative(theStrucDocText);
			if (narrative != null && Config.getGenerateNarrative()) {
				fhirDocRef.setText(narrative);
			}
		}

		// 3. identifiers -> DocumentReference.identifier
		fhirDocRef.setIdentifier(tIIs2Identifiers(theAct.getIds()));

		// 4. DocumentReference.status - fix to current
		// changed to 'current' fix
		fhirDocRef.setStatus(DocumentReferenceStatus.CURRENT);

		// 5. statusCode -> DocumentReference.docStatus
		CS statusCode = theAct.getStatusCode();
		if (statusCode != null && !statusCode.isSetNullFlavor()) {
			fhirDocRef.setDocStatus(vst.tActStatus2ReferredDocumentStatus(statusCode.getCode()));
		}

		// 6. code.translation->DocumentReference.type {
		if (theAct.getCode() != null && !theAct.getCode().isSetNullFlavor()) {
			fhirDocRef.setType(dtt.tCDTranslation2CodeableConcept(theAct.getCode().getTranslations()));
		}

		// 7. DocumentReferece.category
		fhirDocRef.getCategoryFirstRep().getCodingFirstRep().setSystem(DOCUMENT_REFERENCE_CATEGORY_URI).setCode("clinical-note");

		// 8. subject reference to patient
		fhirDocRef.setSubject(theTransformationContext.getPatientRef());

		// 9. DocumentReferece.date, current date
		fhirDocRef.setDate(new Date());

		// 10. author->DocumentReference.author
		// 11. author.assignedAuthor.representedOrganization -> DocumentReference.custodian]
		DateType authorTime = null;
		EList<Author> authors = theAct.getAuthors();
		if (authors != null && !authors.isEmpty()) {
			for (org.openhealthtools.mdht.uml.cda.Author author : authors) {
				if (author != null && !author.isSetNullFlavor()) {
					authorTime = dtt.tTS2Date(author.getTime());

					EntityResult entityResult = tAuthor2Practitioner(author, theBundleInfo, theTransformationContext);
					result.updateFrom(entityResult);
					if (entityResult.hasPractitioner()) {
						fhirDocRef.addAuthor(getReference(entityResult.getPractitioner()));
					}
					if (entityResult.hasOrganization()) {
						fhirDocRef.setCustodian(getReference(entityResult.getOrganization()));
					}
				}
			}
		}

		// 12. participant->DocumentReference.authenticator
		EList<Participant2> cdaParticipentList = theAct.getParticipants();
		// For multiple participants, should have only one typeCode of 'LA', it's mapped to Authenticator
		int laCount=0;
		if (cdaParticipentList != null && !cdaParticipentList.isEmpty()) {

			for (Participant2 cdaParticipant : cdaParticipentList) {

				if (cdaParticipant == null || cdaParticipant.isSetNullFlavor())
					continue;

				if (cdaParticipant.getTypeCode() != ParticipationType.LA)
					continue;

				//-- Practitioner
				Practitioner fhirPractitioner = tParticipant2Practitioner(cdaParticipant,theTransformationContext);
				if (fhirPractitioner == null)
					continue;

				result.addResource(fhirPractitioner);
				laCount++;
				if (laCount>1) {
					theTransformationContext.addWarning(CONSULTATION_NOTE_ENTRY, PARTICIPANT_LA_TYPE_FIELD, MessageFormat.format(MAX_CARDINALITY_MESSAGE, laCount));
					break;
				}
				fhirDocRef.setAuthenticator(getReference(fhirPractitioner));

			}
		}

		// 13. reference->relatedTo.target
		EList<org.openhealthtools.mdht.uml.cda.Reference> cdaReferenceList = theAct.getReferences();
		if (cdaReferenceList != null && !cdaReferenceList.isEmpty()) {

			for (org.openhealthtools.mdht.uml.cda.Reference cdaReference : cdaReferenceList) {
				ExternalDocument cdaExternalDocument = cdaReference.getExternalDocument();
				if (cdaExternalDocument != null && !cdaExternalDocument.isSetNullFlavor()) {
					DocumentReference fhirRefDocRef = new DocumentReference();
					fhirRefDocRef.setId(new IdType("DocumentReference", theTransformationContext.getUniqueId()));
					result.addResource(fhirRefDocRef);
					List<Identifier> identifiers = tIIs2Identifiers(cdaExternalDocument.getIds());
					fhirRefDocRef.setIdentifier(identifiers);
					fhirRefDocRef.setType(dtt.tCD2CodeableConcept(cdaExternalDocument.getCode()));
					fhirDocRef.getRelatesToFirstRep().setTarget(getReference(fhirRefDocRef));
				}
			}
		}

		// 14. Document.content
		DocumentReferenceContentComponent docContent = fhirDocRef.getContentFirstRep();

		// content.format - fixed
		docContent.getFormat().setSystem("http://ihe.net/fhir/ValueSet/IHE.FormatCode.codesystem").setCode("urn:ihe:iti:xds:2017:mimeTypeSufficient");

		// content.attachment
		Attachment attachment = docContent.getAttachment();
		ED text = theAct.getText();
		String media = text.getMediaType();
		if (media != null) {
			attachment.setContentType(media);
		} else {
			attachment.setContentType("text/plain");
		}

		TEL reference = text.getReference();
		if (reference != null && !reference.isSetNullFlavor()) {
			String refValue = text.getReference().getValue();
			if (refValue != null && refValue.startsWith("#")) {
				String ref = refValue.substring(1);
				String content = theBundleInfo.getIdedAnnotations().get(ref);
				if (content != null) {
					Base64BinaryType doc64 = new Base64BinaryType(Base64.encode(content.getBytes()));
					attachment.setDataElement(doc64);
					// reference to the document itself
					attachment.setUrl(resourceId.getValue());
				}
			}
		}

		//-- author.time -> content.attachment.creation
		if (authorTime != null)
			attachment.setCreation(authorTime.getValue());

		// 15. DocumentReference.context
		//  effectiveTime -> DocumentReference.context.period
		DocumentReferenceContextComponent context = fhirDocRef.getContext();
		if (theAct.getEffectiveTime() != null && !theAct.getEffectiveTime().isSetNullFlavor()) {
			context.setPeriod(dtt.tIVL_TS2Period(theAct.getEffectiveTime()));
		}

		// 16. entryRelationship.encounter reference to encounter resource->context
		EList<EntryRelationship> entryRelationships = theAct.getEntryRelationships();
		for (EntryRelationship entryRelationship : entryRelationships) {
			Boolean negationInd = entryRelationship.getNegationInd();
			boolean includingEncounter; // only negationInd is NULL or negationInd is false
			if (negationInd == null) {
				includingEncounter = true;
			} else {
				includingEncounter = !negationInd.booleanValue();
			}

			if (!includingEncounter)
				continue;

			org.openhealthtools.mdht.uml.cda.Encounter cdaEncounter = entryRelationship.getEncounter();
			if (cdaEncounter == null || cdaEncounter.isSetNullFlavor())
				continue;

			EList<II> cdaEncounterIds = cdaEncounter.getIds();
			if (cdaEncounterIds == null || cdaEncounterIds.isEmpty())
				continue;

			List<Identifier> identifiers = tIIs2Identifiers(cdaEncounter.getIds());
			if (identifiers != null && !identifiers.isEmpty()) {
				IDeferredReference deferredReference = new DeferredDocumentReferenceEncounterReference(fhirDocRef,
						identifiers.get(0));
				result.addDeferredReference(deferredReference);
			}
		}

		return result;
	}


	@Override
	public IEntryResult tAssessment2ClinicalImpression(Section theSection, ITransformationContext theTransformationContext) {

		EntryResult result = new EntryResult();

		if (theSection == null || theSection.isSetNullFlavor()) {
			return result;
		}

		ClinicalImpression fhirClinicalDoc = new ClinicalImpression();
		result.addResource(fhirClinicalDoc);

		// 1. ClinicalImpression.id
		IdType resourceId = new IdType("ClinicalImpression", theTransformationContext.getUniqueId());
		fhirClinicalDoc.setId(resourceId);

		// 2. ClinicalImpression.code.Coding
		fhirClinicalDoc.setCode(dtt.tCD2CodeableConcept(theSection.getCode()));

		// 3. ClinicalImpression.summary
		String text = theSection.getText().getText();
		if (!("".equals(text) || null == text)) {
			fhirClinicalDoc.setSummary(text);
		} else {
			theTransformationContext.addError(ASSESSMENT_SECTION, SUMMARY_FIELD, MIN_CARDINALITY_MESSAGE);
		}

		// 4. ClinicalImpression.status
		fhirClinicalDoc.setStatus(ClinicalImpression.ClinicalImpressionStatus.COMPLETED);

		// 5. subject reference to patient
		fhirClinicalDoc.setSubject(theTransformationContext.getPatientRef());

		return result;
	}

	@Override
	public IEntryResult tGoalObservation2Goal(org.openhealthtools.mdht.uml.cda.Observation theCdaObs,
											  IBundleInfo theBundleInfo, ITransformationContext theTransformationContext) {

		EntryResult result = new EntryResult();

		if (theCdaObs == null || theCdaObs.isSetNullFlavor()) {
			return result;
		}

		Goal fhirGoal = new Goal();
		result.addResource(fhirGoal);

		// 1. Goal.id
		IdType resourceId = new IdType("Goal", theTransformationContext.getUniqueId());
		fhirGoal.setId(resourceId);

		// 2. id->identifier
		EList<II> ids = theCdaObs.getIds();
		if (ids != null && !ids.isEmpty()) {
			for (II id : ids) {
				if (id != null && !id.isSetNullFlavor()) {
					fhirGoal.addIdentifier(dtt.tII2Identifier(id));
				}
			}
		}

		// 3. GoalLifecycleStatus="Active" fixed
		fhirGoal.setLifecycleStatus(GoalLifecycleStatus.ACTIVE);

		Period period = dtt.tIVL_TS2Period(theCdaObs.getEffectiveTime());
		if (period != null) {
			// 4. EffectiveTime.low -> startDate
			if (period.hasStartElement()) {
				fhirGoal.setStart(new DateType(period.getStart()));
			}

			// 5. EffectiveTime.high -> target.dueDate
			if (period.hasEndElement()) {
				fhirGoal.getTargetFirstRep().setDue(new DateType(period.getEnd()));
			}
		}

		// 6. value->description.text
		EList<ANY> cdaValues = theCdaObs.getValues();
		if (cdaValues != null && cdaValues.size() > 1)
			theTransformationContext.addWarning(GOAL_OBSERVATION_ENTRY, VALUE_FIELD,MessageFormat.format(MAX_CARDINALITY_MESSAGE, cdaValues.size()));

		StringType valueStr = dtt.tValues2String(cdaValues);
		if (valueStr != null) {
			fhirGoal.getDescription().setText(valueStr.asStringValue());
		}

		// 7. author -> expressedBy
		if (theCdaObs.getAuthors() != null) {
			for (org.openhealthtools.mdht.uml.cda.Author author : theCdaObs.getAuthors()) {
				if (author != null && !author.isSetNullFlavor()) {
					EntityResult entityResult = tAuthor2Practitioner(author, theBundleInfo, theTransformationContext);
					result.updateFrom(entityResult);
					if (entityResult.hasPractitioner()) {
						fhirGoal.setExpressedBy(getReference(entityResult.getPractitioner()));
					}
				}
			}
		}

		// 8. subject reference to patient
		fhirGoal.setSubject(theTransformationContext.getPatientRef());

		// 9. EntryRelationship
		EList<EntryRelationship> erList = theCdaObs.getEntryRelationships();
		if (erList != null) {
			for (EntryRelationship er : erList) {

				x_ActRelationshipEntryRelationship erType = er.getTypeCode();

				if (erType == null)
					continue;

				if ("REFR".equalsIgnoreCase(erType.getName())) {
					// TODO: will handle Condition after Health Concern is done

					//-- Obs.priority to goal.priority
					org.openhealthtools.mdht.uml.cda.Observation priorityObs = er.getObservation();
					int priorityObsCount = 0;
					tCdaObservation2GoalPriority(priorityObs, priorityObsCount++, fhirGoal, theTransformationContext);
				} else if ("COMP".equalsIgnoreCase(erType.getName())) {
					// TODO: Will handle CarePlan after PLAN OF TREATMENT
				} else {
					theTransformationContext.addWarning(GOAL_OBSERVATION_ENTRY, ENTRY_RELATIONSHIP_TYPE_FIELD,
							"Unsupported typeCode : " + erType.getName());
				}
			}
		}

		return result;
	}

	private void tCdaObservation2GoalPriority(org.openhealthtools.mdht.uml.cda.Observation priorityObs, int priorityObsCount,
											  Goal fhirGoal, ITransformationContext theTransformationContext) {

		if (priorityObs == null || priorityObs.isSetNullFlavor())
			return;

		ActClassObservation obsClassCode = priorityObs.getClassCode();
		if (obsClassCode == null || !"OBS".equalsIgnoreCase(obsClassCode.getName()))
			return;

		x_ActMoodDocumentObservation obsMoodCode = priorityObs.getMoodCode();
		if (obsMoodCode == null || !"EVN".equalsIgnoreCase(obsMoodCode.getName()))
			return;

		if (!priorityObs.getTemplateIds().stream()
				.anyMatch(t -> CDAGoalSection.GOAL_OBSERVATION_PRIORITY_TEMPLATE_ID.equals(t.getRoot())))
			return;

		if (priorityObsCount > 1) {
			theTransformationContext.addWarning(GOAL_OBSERVATION_ENTRY, PRIORITY_PREFERENCE_FIELD,
					MessageFormat.format(MAX_CARDINALITY_MESSAGE, priorityObsCount));
			return;
		}

		// -- priorityObs.value[@xsi:type='CD'] -> goal.priority
		EList<ANY> values = priorityObs.getValues();
		if (values == null || values.isEmpty() || values.size() == 0) {
			theTransformationContext.addWarning(GOAL_OBSERVATION_ENTRY, VALUE_FIELD, MIN_CARDINALITY_MESSAGE);
			return;
		}

		int valueCDcount = 0;
		for (ANY value : values) {
			if (value == null || value.isSetNullFlavor())
				continue;

			if (!(value instanceof CD))
				continue;

			CodeableConcept priority = dtt.tCD2CodeableConceptExcludingTranslations((CD) value);
			if (priority != null) {
				valueCDcount++;
				fhirGoal.setPriority(priority);
			}
		}

		if (valueCDcount > 1) {
			theTransformationContext.addWarning(GOAL_OBSERVATION_ENTRY, VALUE_FIELD, MessageFormat.format(MAX_CARDINALITY_MESSAGE, valueCDcount));
		}

		// NOTE : priorityObs.getAuthors() should be same as the authors of outer Observation
		// since there is only one Goal.expressedBy, no need map here
	}

	@Override
	public IEntryResult tPlanOfCareSection2CarePlan(ST theTitle, StrucDocText theStrucDocText, List<PlanOfCareActivityAct> theActs, IBundleInfo theBundleInfo, ITransformationContext theTransformationContext) {

		EntryResult result = new EntryResult();

		CarePlan fhirCarePlan = new CarePlan();
		result.addResource(fhirCarePlan);

		// 1. CarePlan.id
		IdType resourceId = new IdType("CarePlan", theTransformationContext.getUniqueId());
		fhirCarePlan.setId(resourceId);

		// 2. title->title
		StringType titleType = dtt.tST2String(theTitle);
		if (titleType != null)
			fhirCarePlan.setTitle(titleType.getValue());

		// 3. text->description
		if (theStrucDocText != null) {
			String text = dtt.tStrucDocText2String(theStrucDocText);
			if (text != null)
				fhirCarePlan.setDescription(text);

			fhirCarePlan.setText(dtt.tStrucDocText2Narrative(theStrucDocText));
		}

		// 4. subject reference to patient
		fhirCarePlan.setSubject(theTransformationContext.getPatientRef());

		// 5. intent = "plan", status = "active" -- fixed
		fhirCarePlan.setIntent(CarePlanIntent.PLAN);
		fhirCarePlan.setStatus(CarePlanStatus.ACTIVE);

		// 6. act -> activity.reference(ServiceRequest)
		if (theActs != null) {
			for (PlanOfCareActivityAct act : theActs) {
				ServiceRequest serviceRequest = tPlanOfCareActivityAct2ServiceRequest(act, fhirCarePlan, result, theBundleInfo, theTransformationContext);
				if (serviceRequest != null) {
					result.addResource(serviceRequest);
					fhirCarePlan.addActivity().setReference(getReference(serviceRequest));
					// Tebra-specific business rule: they assure us that there will always be exactly one service request per plan
					for(Identifier serviceRequestIdentifier : serviceRequest.getIdentifier()) {
						fhirCarePlan.addIdentifier(serviceRequestIdentifier);
					}
					if (fhirCarePlan.getIdentifier().isEmpty()) {
						theTransformationContext.addWarning(CARE_PLAN_SECTION, IDENTIFIER_FIELD, POSSIBLE_DUPLICATION_MESSAGE);
					}
				}
			}
		}

		// 7.
		fhirCarePlan.addCategory(new CodeableConcept(
				new Coding().setCode(ASSESS_PLAN).setSystem(CAREPLAN_CATEGORY_URI)));

		return result;
	}

	@Override
	public ServiceRequest tPlanOfCareActivityAct2ServiceRequest(PlanOfCareActivityAct theAct, CarePlan thefhirCarePlan, EntryResult theResult, IBundleInfo theBundleInfo, ITransformationContext theTransformationContext) {

		if (theAct == null || theAct.isSetNullFlavor()) {
			return null;
		}

		ServiceRequest fhirSvcReq = new ServiceRequest();

		// 1. ServiceRequest.id
		IdType resourceId = new IdType("ServiceRequest", theTransformationContext.getUniqueId());
		fhirSvcReq.setId(resourceId);

		// 2. id->identifier
		List<Identifier> identifiers = tIIs2Identifiers(theAct.getIds());
		if (identifiers != null)
			fhirSvcReq.setIdentifier(identifiers);

		// 3. code->code
		CD code = theAct.getCode();
		CodeableConcept cc = dtt.tCD2CodeableConcept(code);
		if (cc != null)
			fhirSvcReq.setCode(cc);

		// 4. set status code to active
		fhirSvcReq.setStatus(ServiceRequestStatus.ACTIVE);

		// 5. effectiveDate->occurrenceDateTime
		DateTimeType effectiveDate = dtt.tIVL_TS2DateTime(theAct.getEffectiveTime());
		if (effectiveDate != null)
			fhirSvcReq.setOccurrence(effectiveDate);

		// 6. performer -> performer
		EList<Performer2> performers = theAct.getPerformers();
		if (performers != null) {
			for (Performer2 performer : performers) {

				if (performer == null || performer.isSetNullFlavor())
					continue;

				if (performer.getAssignedEntity() != null && !performer.getAssignedEntity().isSetNullFlavor()) {
					EntityResult entityResult = tPerformer22Practitioner(performer, theBundleInfo, theTransformationContext);
					theResult.updateFrom(entityResult);
					theBundleInfo.updateFrom(entityResult);
					if (!entityResult.isEmpty()) {
						if (entityResult.hasPractitioner()) {
							fhirSvcReq.addPerformer(getReference(entityResult.getPractitioner()));
						}
					}
				}
			}
		}

		// 7. author
		EList<Author> authors = theAct.getAuthors();

		int authorCount=0;
		if (authors != null) {
			for (Author author : authors) {
				if (author == null || author.isSetNullFlavor())
					continue;

				EntityResult entityResult = tAuthor2Practitioner(author, theBundleInfo, theTransformationContext);
				theResult.updateFrom(entityResult);
				theBundleInfo.updateFrom(entityResult);
				if (entityResult.hasPractitioner()) {
					authorCount++;
					Reference practitionerRef = getReference(entityResult.getPractitioner());
					// NOTE: since there is only one Requester of the ServiceRequest, set first one only
					if (authorCount == 1) {
						fhirSvcReq.setRequester(practitionerRef);
					}
					// avoid same practitioner reference to be added as a contributor
					if (!hasContributer(thefhirCarePlan, practitionerRef))
						thefhirCarePlan.addContributor(practitionerRef);
				}
			}
		}

		// 8. subject reference to patient
		fhirSvcReq.setSubject(theTransformationContext.getPatientRef());

		// 9. intent set to "plan"
		fhirSvcReq.setIntent(ServiceRequestIntent.PLAN);

		// 10. text->text (Narrative)
		Narrative narrative = dtt.tED2Narrative(theAct.getText(), theBundleInfo);
		if (narrative != null && Config.getGenerateNarrative()) {
			fhirSvcReq.setText(narrative);
		}

		return fhirSvcReq;
	}

	private boolean hasContributer(CarePlan thefhirCarePlan, Reference thePractitionerRef) {

		List<Reference> contributers = thefhirCarePlan.getContributor();

		for (Reference contributer : contributers) {
			if (contributer.getReference().equalsIgnoreCase(thePractitionerRef.getReference()))
				return true;
		}

		return false;
	}

	@Override
	public IEntryResult tHealthConcernObservation2Observation(org.openhealthtools.mdht.uml.cda.Observation theCdaObs, IBundleInfo theBundleInfo, ITransformationContext theTransformationContext) {

		EntryResult result = new EntryResult();

		org.hl7.fhir.r4.model.Observation fhirObs = new org.hl7.fhir.r4.model.Observation();
		result.addResource(fhirObs);

		// 1. Observation.id
		IdType resourceId = new IdType("Observation", theTransformationContext.getUniqueId());
		fhirObs.setId(resourceId);

		// 2. id->identifier
		List<Identifier> identifiers = tIIs2Identifiers(theCdaObs.getIds());
		if (identifiers != null)
			fhirObs.setIdentifier(identifiers);

		// 3. code->code
		CD code = theCdaObs.getCode();
		CodeableConcept cc = dtt.tCD2CodeableConcept(code);
		if (cc != null)
			fhirObs.setCode(cc);

		// 4. statusCode->status
		CS statusCode = theCdaObs.getStatusCode();
		if (statusCode != null && !statusCode.isSetNullFlavor()) {
			fhirObs.setStatus(vst.tObservationStatusCode2ObservationStatus(statusCode.getCode()));
		}

		// 5. value->CodeableConcept
		EList<ANY> cdaValues = theCdaObs.getValues();
		if (cdaValues != null && cdaValues.size() > 1)
			theTransformationContext.addWarning(HEALTH_CONCERN_OBSERVATION_ENTRY, VALUE_FIELD,MessageFormat.format(MAX_CARDINALITY_MESSAGE, cdaValues.size()));

		if (cdaValues == null) {
			theTransformationContext.addWarning(HEALTH_CONCERN_OBSERVATION_ENTRY, VALUE_FIELD, MIN_CARDINALITY_MESSAGE);
		} else {
			for (ANY value : cdaValues) {
				if (value == null || value.isSetNullFlavor())
					continue;
				if (!(value instanceof CD))
					continue;
				CD cd = (CD) value;
				cc = dtt.tCD2CodeableConcept(cd);
				if (cc != null) {
					fhirObs.setValue(cc);
					break;
				}
			}
		}

		// 6. subject reference to patient
		fhirObs.setSubject(theTransformationContext.getPatientRef());

		return result;
	}

	@Override
	public IEntryResult tHealthConcernAct2Condition(Act theAct, IBundleInfo theBundleInfo, ITransformationContext theTransformationContext) {

		EntryResult result = new EntryResult();

		Condition fhirCondition = new Condition();
		result.addResource(fhirCondition);

		// 1. Observation.id
		IdType resourceId = new IdType("Condition", theTransformationContext.getUniqueId());
		fhirCondition.setId(resourceId);

		// 1.5 id -> identifier
		List<II> ids = theAct.getIds();
		if (ids != null && !ids.isEmpty()) {
			for (II ii : ids) {
				fhirCondition.addIdentifier(dtt.tII2Identifier(ii));
			}
		} else {
			theTransformationContext.addWarning(HEALTH_CONCERN_ACT_STATUS_ENTRY, IDENTIFIER_FIELD, MIN_CARDINALITY_MESSAGE);
		}

		// 2. text->code (Tebra-specific mapping - see SMILE-5948 for details)
		ED text = theAct.getText();
		String concernText = dtt.tED2Annotation(text, theBundleInfo.getIdedAnnotations());
		if (StringUtils.isNotBlank(concernText)) {
			CodeableConcept cc = new CodeableConcept();
			cc.setText(concernText);
			fhirCondition.setCode(cc);
		}

		// 3. statusCode->status mapping
		CS statusCode = theAct.getStatusCode();
		if (statusCode != null && !statusCode.isSetNullFlavor()) {
			CodeableConcept clinicalStatus = vst.tActStatus2ConditionClinicalStatus(statusCode.getCode());
			if (clinicalStatus != null) {
				fhirCondition.setClinicalStatus(clinicalStatus);
			} else {
				theTransformationContext.addWarning(HEALTH_CONCERN_ACT_STATUS_ENTRY, VALUE_FIELD,
						MessageFormat.format(UNPARSEABLE_CODE_MESSAGE, statusCode.getCode()));
			}
		}

		// 4. effectiveDate->occurrenceDateTime
		DateTimeType effectiveDate = dtt.tIVL_TS2DateTime(theAct.getEffectiveTime());
		if (effectiveDate != null)
			fhirCondition.setOnset(effectiveDate);

		// 5. Author
		handleHealthConcernActAuthors(theAct.getAuthors(), fhirCondition, result, theBundleInfo, theTransformationContext);

		// 6. subject reference to patient
		fhirCondition.setSubject(theTransformationContext.getPatientRef());

		// 7. set Category to "health-concern"
		fhirCondition.addCategory(new CodeableConcept(new Coding(CODE_SYSTEM_US_CORE_CONDITION_CATEGORY, "health-concern", "Health Concern")));

		// 8. set verificationStatus to fixed value
		setVerificationStatusConfirmed(fhirCondition);

		return result;
	}

	private void setVerificationStatusConfirmed(Condition fhirCondition) {
		ConditionVerStatus confirmed = ConditionVerStatus.CONFIRMED;
		fhirCondition.setVerificationStatus(new CodeableConcept()
				.addCoding(new Coding()
						.setCode(confirmed.toCode())
						.setDisplay(confirmed.getDisplay())
						.setSystem(confirmed.getSystem())
				));
	}


	private void handleHealthConcernActAuthors(EList<Author> theAuthors, Condition theFhirCondition,
											   EntryResult theResult, IBundleInfo theBundleInfo, ITransformationContext theTransformationContext) {

		// Support only one author. If there is more than one author, take the first one and log an error too.
		// If author's is patient based on the identifier.value, setAsserter to Patient
		// otherwise setAsserter to Practitioner
		if (theAuthors == null)
			return;

		int authorCount = 0;
		Patient headPatient = theTransformationContext.getPatient();
		for (Author author : theAuthors) {
			if (author == null || author.isSetNullFlavor())
				continue;

			authorCount++;
			if (authorCount > 1) {
				break;
			}

			// Find the PatientRef by assignedAuthor.getIds()
			AssignedAuthor assignedAuthor = author.getAssignedAuthor();
			Reference patientRef = null;
			if (assignedAuthor != null && !assignedAuthor.isSetNullFlavor()) {
				List<Identifier> assignedAuthorIdentifiers = tIIs2Identifiers(assignedAuthor.getIds());
				if (FHIRUtil.isSameResource(headPatient.getIdentifier(), assignedAuthorIdentifiers)) {
					patientRef = theTransformationContext.getPatientRef();
				}
			}

			if (patientRef != null) {
				// Yes this is the patient
				theFhirCondition.setAsserter(patientRef);
			} else {
				// Practitioner Reference
				EntityResult entityResult = tAuthor2Practitioner(author, theBundleInfo, theTransformationContext);
				theResult.updateFrom(entityResult);
				if (entityResult.hasPractitioner()) {
					Reference pracRef = getReference(entityResult.getPractitioner());
					theFhirCondition.setAsserter(pracRef);
				}
			}
		}

		if (authorCount > 1) {
			theTransformationContext.addError(HEALTH_CONCERN_ACT_AUTHOR_ENTRY, AUTHOR_FIELD, MessageFormat.format(MAX_CARDINALITY_MESSAGE, authorCount));
		}
	}

	@Override
	public IEntryResult tOrganizer2CareTeam(StrucDocText theStrucDocText, Organizer theOrganizer,
											IBundleInfo theBundleInfo, ITransformationContext theTransformationContext) {

		EntryResult result = new EntryResult();

		CareTeam fhirCareTeam = new CareTeam();
		result.addResource(fhirCareTeam);

		// CareTeam.id
		IdType resourceId = new IdType("CareTeam", theTransformationContext.getUniqueId());
		fhirCareTeam.setId(resourceId);

		// id -> identifier
		EList<II> ids = theOrganizer.getIds();
		if (ids != null && !ids.isEmpty()) {
			for (II id : ids) {
				fhirCareTeam.addIdentifier(dtt.tII2Identifier(id));
			}
		} else {
			theTransformationContext.addWarning(CARE_TEAM_ORGANIZER_ENTRY, IDENTIFIER_FIELD, MIN_CARDINALITY_MESSAGE);
		}

		// text->CareTeam.Narrative
		if (theStrucDocText != null) {
			Narrative narrative = dtt.tStrucDocText2Narrative(theStrucDocText);
			if (narrative != null && Config.getGenerateNarrative()) {
				fhirCareTeam.setText(narrative);
			}
		}

		// code.displayName -> CareTeam.name
		CD code = theOrganizer.getCode();
		CodeableConcept cc = dtt.tCD2CodeableConcept(code);
		if (cc != null) {
			fhirCareTeam.setName(cc.getCodingFirstRep().getDisplay());
		}

		// statusCode -> CareTeam.status
		CS statusCode = theOrganizer.getStatusCode();
		if (statusCode != null && !statusCode.isSetNullFlavor()) {
			if (statusCode.getCode() != null)
				fhirCareTeam.setStatus(vst.tOrganizerStatus2CareTeamStatus(statusCode.getCode()));
		}

		// subject reference to patient
		fhirCareTeam.setSubject(theTransformationContext.getPatientRef());

		// effectiveTime -> CareTeam.period
		if (theOrganizer.getEffectiveTime() != null) {
			Period period = dtt.tIVL_TS2Period(theOrganizer.getEffectiveTime());
			fhirCareTeam.setPeriod(period);
		}

		EList<Component4> components = theOrganizer.getComponents();

		//-- process component.act
		for (Component4 component : components) {

			Act act = component.getAct();
			if (act != null && act.isSetNullFlavor()) {
				continue;
			}

			if (!act.getTemplateIds().stream().anyMatch(t -> CDACareTeamSection.CARE_TEAM_PERFORMER_ACT_TEMPLATE_ID.equals(t.getRoot()))) {
				continue;
            }

			CareTeamParticipantComponent participant = fhirCareTeam.addParticipant();

			// act.effectiveTime->participant.period
			if (act.getEffectiveTime() != null) {
				Period period = dtt.tIVL_TS2Period(theOrganizer.getEffectiveTime());
				participant.setPeriod(period);
			}

			// act.performer -> participant.member
			EList<Performer2> cdaPerformers = act.getPerformers();
			for (Performer2 cdaPerformer : cdaPerformers) {

				EntityResult entityResult = tPerformer22Practitioner(cdaPerformer, theBundleInfo, theTransformationContext);
				theBundleInfo.updateFrom(entityResult);
				result.updateFrom(entityResult);
				if (entityResult.hasPractitioner()) {
					participant.setMember(getReference(entityResult.getPractitioner()));
				}

				// act.perfomer.assignedEntity.code-> participant.role
				if (entityResult.hasPractitionerRole()) {
					PractitionerRole role = entityResult.getPractitionerRole();
					if (role != null)
						participant.addRole(role.getCodeFirstRep());
				}
			}

		}
		return result;
	}

}
