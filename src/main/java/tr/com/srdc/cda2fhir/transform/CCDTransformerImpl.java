package tr.com.srdc.cda2fhir.transform;

import java.io.FileInputStream;

/*
 * #%L
 * CDA to FHIR Transformer Library
 * %%
 * Copyright (C) 2016 SRDC Yazilim Arastirma ve Gelistirme ve Danismanlik Tic. A.S.
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import java.io.Serializable;
import java.text.MessageFormat;
import java.util.*;

import com.google.common.collect.Sets;
import org.apache.commons.lang3.StringUtils;
import org.hl7.fhir.r4.model.*;
import org.hl7.fhir.r4.model.Bundle.BundleEntryComponent;
import org.hl7.fhir.r4.model.Bundle.BundleType;
import org.hl7.fhir.r4.model.Composition.SectionComponent;
import org.hl7.fhir.r4.model.Enumeration;
import org.openhealthtools.mdht.uml.cda.Section;
import org.openhealthtools.mdht.uml.cda.consol.ConsolPackage;
import org.openhealthtools.mdht.uml.cda.consol.ContinuityOfCareDocument;
import org.openhealthtools.mdht.uml.cda.util.CDAUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import tr.com.srdc.cda2fhir.transform.entry.IEntryResult;
import tr.com.srdc.cda2fhir.transform.section.CDASectionTypeEnum;
import tr.com.srdc.cda2fhir.transform.section.ICDASection;
import tr.com.srdc.cda2fhir.transform.section.ISectionResult;
import tr.com.srdc.cda2fhir.transform.util.IDeferredReference;
import tr.com.srdc.cda2fhir.transform.util.IIdentifierMap;
import tr.com.srdc.cda2fhir.transform.util.ITransformationContext;
import tr.com.srdc.cda2fhir.transform.util.ITransformationResult;
import tr.com.srdc.cda2fhir.transform.util.IdentifierMapFactory;
import tr.com.srdc.cda2fhir.transform.util.impl.BundleInfo;
import tr.com.srdc.cda2fhir.transform.util.impl.BundleRequest;
import tr.com.srdc.cda2fhir.transform.util.impl.ReferenceInfo;
import tr.com.srdc.cda2fhir.transform.util.impl.TransformationContextImpl;
import tr.com.srdc.cda2fhir.transform.util.impl.TransformationResultImpl;
import tr.com.srdc.cda2fhir.util.EMFUtil;
import tr.com.srdc.cda2fhir.util.FHIRUtil;

import static tr.com.srdc.cda2fhir.transform.ResourceTransformerImpl.FHIR_STRUCTURE_US_CORE_RACE_URI;

public class CCDTransformerImpl implements ICDATransformer, Serializable {

	private static final long serialVersionUID = 1L;
	public static final String FHIR_RESOURCE_ALLERGY_INTOLERANCE = "AllergyIntolerance";
	public static final String ALLERGY_INTOLERANCE_CLINICAL_STATUS = FHIR_RESOURCE_ALLERGY_INTOLERANCE + ".clinicalStatus";
	public static final String FHIR_RESOURCE_CONDITION = "Condition";
	public static final String CONDITION_CLINICAL_STATUS = FHIR_RESOURCE_CONDITION + ".clinicalStatus";
	public static final String FHIR_RESOURCE_MEDICATION_REQUEST = "MedicationRequest";
	public static final String MEDICATION_REQUEST_STATUS = FHIR_RESOURCE_MEDICATION_REQUEST + ".status";
	public static final String FHIR_RESOURCE_PATIENT = "Patient";
	public static final String PATIENT_NAME = FHIR_RESOURCE_PATIENT + ".name";
	public static final String PATIENT_ADDRESS = FHIR_RESOURCE_PATIENT + ".address";

	public static final Set<String> UPDATABLE_RESOURCE_TYPES = Sets.newHashSet(FHIR_RESOURCE_ALLERGY_INTOLERANCE,
			FHIR_RESOURCE_CONDITION, FHIR_RESOURCE_MEDICATION_REQUEST, FHIR_RESOURCE_PATIENT);

	private IResourceTransformer resTransformer;

	private List<CDASectionTypeEnum> supportedSectionTypes = new ArrayList<>();

	private final Logger logger = LoggerFactory.getLogger(CCDTransformerImpl.class);

	/**
	 * Default constructor that initiates with a UUID resource id generator
	 */
	public CCDTransformerImpl() {
		resTransformer = new ResourceTransformerImpl();

		supportedSectionTypes.add(CDASectionTypeEnum.ALLERGIES_SECTION);
		supportedSectionTypes.add(CDASectionTypeEnum.IMMUNIZATIONS_SECTION);
		supportedSectionTypes.add(CDASectionTypeEnum.MEDICATIONS_SECTION);
		supportedSectionTypes.add(CDASectionTypeEnum.NUTRITION_SECTION);
		supportedSectionTypes.add(CDASectionTypeEnum.PROBLEM_SECTION);
		supportedSectionTypes.add(CDASectionTypeEnum.PROCEDURES_SECTION);
		supportedSectionTypes.add(CDASectionTypeEnum.ENCOUNTERS_SECTION);
		supportedSectionTypes.add(CDASectionTypeEnum.ENCOUNTERS_SECTION_ENTRIES_OPTIONAL);
		supportedSectionTypes.add(CDASectionTypeEnum.RESULTS_SECTION);
		supportedSectionTypes.add(CDASectionTypeEnum.SOCIAL_HISTORY_SECTION);
		supportedSectionTypes.add(CDASectionTypeEnum.VITAL_SIGNS_SECTION);
		supportedSectionTypes.add(CDASectionTypeEnum.CONSULTATION_SECTION);
		supportedSectionTypes.add(CDASectionTypeEnum.GOAL_SECTION);
		supportedSectionTypes.add(CDASectionTypeEnum.ASSESSMENT_SECTION);
		supportedSectionTypes.add(CDASectionTypeEnum.PLAN_OF_CARE_SECTION);
		supportedSectionTypes.add(CDASectionTypeEnum.HEALTH_CONCERN_SECTION);
		supportedSectionTypes.add(CDASectionTypeEnum.CARE_TEAM_SECTION);
	}

	public void setResourceTransformer(IResourceTransformer resTransformer) {
		this.resTransformer = resTransformer;
	}

	public void addSection(CDASectionTypeEnum sectionEnum) {
		supportedSectionTypes.add(sectionEnum);
	}

	public void setSection(CDASectionTypeEnum sectionEnum) {
		supportedSectionTypes.clear();
		supportedSectionTypes.add(sectionEnum);
	}

	/**
	 * @param bundle             A FHIR Bundle resource to be converted to a Transaction Bundle
	 * @param resourceProfileMap The mappings of default resource profiles to
	 *                           desired resource profiles. Used to set profile
	 *                           URI's of bundle entries or omit unwanted entries.
	 * @param addURLs            A flag controlling whether the fullUrl filed will be populated
	 *                           in the generated bundle
	 * @return A FHIR Bundle that contains a Composition corresponding to the CCD
	 *         document and all other resources but Patient that are referenced
	 *         within the Composition.
	 */
	public Bundle createTransactionBundle(Bundle bundle, Map<String, String> resourceProfileMap, boolean addURLs) {
		Bundle resultBundle = new Bundle();
		resultBundle.setType(BundleType.TRANSACTION);

		for (BundleEntryComponent entry : bundle.getEntry()) {
			// Patient resource will not be added
			if (entry != null) {
				// Add request and fullUrl fields to entries
				BundleRequest.addRequestToEntry(entry);
				if (addURLs) {
					addFullUrlToEntry(entry);
				}
				// if resourceProfileMap is specified omit the resources with no profiles given
				// Empty profileUri means add with no change
				if (resourceProfileMap != null) {
					String profileUri = resourceProfileMap.get(entry.getResource().getResourceType().name());
					if (profileUri != null) {
						if (!profileUri.isEmpty()) {
							entry.getResource().getMeta().addProfile(profileUri);
						}
						resultBundle.addEntry(entry);
					}
				} else {
					resultBundle.addEntry(entry);
				}

				createUpdateEntry(entry).ifPresent(resultBundle::addEntry);
			}
		}

		return resultBundle;
	}

	/**
	 * Creates a PATCH entry that matches a POST entry, to conditionally update a resource in the case where it already
	 * exists in the repository. These PATCH entries implement rules for updating resources that are specific to
	 * Kareo-Tebra. See tickets <a href="https://gitlab.com/simpatico.ai/cdr/-/issues/4209">4209</a> and
	 * <a href="https://gitlab.com/simpatico.ai/cdr/-/issues/4210">4210</a>.
	 *
	 * @param theCreateEntry an entry for conditionally creating a resource
	 * @return an entry for conditionally updating the same resource described in theCreateEntry, or empty if no update rules
	 * apply to this resource type.
	 */
	private Optional<BundleEntryComponent> createUpdateEntry(BundleEntryComponent theCreateEntry) {
		Optional<BundleEntryComponent> result;

		if (isValidForUpdate(theCreateEntry)) {
			switch (theCreateEntry.getResource().fhirType()) {
				case FHIR_RESOURCE_ALLERGY_INTOLERANCE: {
					result = createUpdateOperationForAllergyIntolerance(theCreateEntry);
					break;
				}
				case FHIR_RESOURCE_CONDITION: {
					result = createUpdateOperationForCondition(theCreateEntry);
					break;
				}
				case FHIR_RESOURCE_MEDICATION_REQUEST: {
					result = createUpdateOperationForMedicationRequest(theCreateEntry);
					break;
				}
				case FHIR_RESOURCE_PATIENT: {
					result = createUpdateOperationForPatient(theCreateEntry);

					break;
				}
				default:
					result = Optional.empty();
			}
		} else {
			result = Optional.empty();
		}

		return result;
	}

	private Optional<BundleEntryComponent> createUpdateOperationForAllergyIntolerance(BundleEntryComponent theCreateEntry) {
		AllergyIntolerance allergyIntolerance = (AllergyIntolerance) theCreateEntry.getResource();

		BundleEntryComponent updateEntry = createPatchEntry(theCreateEntry);

		Parameters updateParameters = createUpdateParameter(
				ALLERGY_INTOLERANCE_CLINICAL_STATUS, allergyIntolerance.getClinicalStatus());
		updateEntry.setResource(updateParameters);

		return Optional.of(updateEntry);
	}

	private Optional<BundleEntryComponent> createUpdateOperationForCondition(BundleEntryComponent theCreateEntry) {
		Condition condition = (Condition) theCreateEntry.getResource();

		BundleEntryComponent updateEntry = createPatchEntry(theCreateEntry);

		Parameters updateParameters = createUpdateParameter(
				CONDITION_CLINICAL_STATUS, condition.getClinicalStatus());
		updateEntry.setResource(updateParameters);

		return Optional.of(updateEntry);
	}

	private Optional<BundleEntryComponent> createUpdateOperationForMedicationRequest(BundleEntryComponent theCreateEntry) {
		MedicationRequest medicationRequest = (MedicationRequest) theCreateEntry.getResource();

		BundleEntryComponent updateEntry = createPatchEntry(theCreateEntry);

		Parameters updateParameters = createUpdateParameter(
				MEDICATION_REQUEST_STATUS, medicationRequest.getStatusElement());
		updateEntry.setResource(updateParameters);

		return Optional.of(updateEntry);
	}

	private Optional<BundleEntryComponent> createUpdateOperationForPatient(BundleEntryComponent theCreateEntry) {
		Patient patient = (Patient) theCreateEntry.getResource();

		BundleEntryComponent updateEntry = createPatchEntry(theCreateEntry);

		Parameters updateParameters = new Parameters();

		if (isNeedsNameUpdate(patient)) {
			updatePatientNames(patient, updateParameters);
		}

		if (patient.getAddress().size() == 2) {
			updatePatientAddresses(patient, updateParameters);
		}

		updatePatientRace(patient, updateParameters);

		updateEntry.setResource(updateParameters);
		return Optional.of(updateEntry);
	}

	private void updatePatientNames(Patient thePatient, Parameters theUpdateParameters) {
		for (HumanName name : thePatient.getName()) {
			if (name.hasPeriod()) {
				theUpdateParameters.addParameter(createUpdateOperation(
						formatNameUseUpdateFhirPath(name),
						new Enumeration<>(new HumanName.NameUseEnumFactory(), HumanName.NameUse.OLD)
				));
			} else {
				theUpdateParameters.addParameter(createAddOperation(FHIR_RESOURCE_PATIENT, "name", name));
			}
		}
	}

	private void updatePatientAddresses(Patient thePatient, Parameters theUpdateParameters) {
		// We expect the update scenario to contain exactly two address entries
		//  one with an end date but no start date, which is the old address
		//  one with a start date but no end date, which is the new address
		for (Address address : thePatient.getAddress()) {
			if (address.getPeriod().hasEndElement()) {
				theUpdateParameters.addParameter(createUpdateOperation(formatAddressUseUpdateFhirPath(address),
						new Enumeration<>(new Address.AddressUseEnumFactory(), Address.AddressUse.OLD)));
			} else {
				theUpdateParameters.addParameter(createAddOperation(FHIR_RESOURCE_PATIENT, "address", address));
			}
		}
	}

	private void updatePatientRace(Patient thePatient, Parameters theUpdateParameters) {
		theUpdateParameters.addParameter(createDeleteOperation(formatRaceExtensionFhirPath()));
		thePatient.getExtension().stream()
				.filter(t -> StringUtils.equals(FHIR_STRUCTURE_US_CORE_RACE_URI, t.getUrl()))
				.findFirst()
				.ifPresent(extension -> theUpdateParameters.addParameter(createAddOperation(
					FHIR_RESOURCE_PATIENT, "extension",	extension)));
	}

	private String formatNameUseUpdateFhirPath(HumanName theName) {
		List<String> conditions = new ArrayList<>();
		if (theName.hasFamily()) {
			conditions.add("family ~ '" + theName.getFamily() + "'");
		}

		List<String> givens = new ArrayList<>();
		for (StringType given : theName.getGiven()) {
			givens.add("'" + given.getValue() + "'");
		}
		if (!givens.isEmpty()) {
			conditions.add("given ~ (" + StringUtils.join(givens, "|") + ")");
		}

		return PATIENT_NAME + ".where(" + StringUtils.join(conditions, " and ") + " and use != 'old').use";
	}

	private String formatAddressUseUpdateFhirPath(Address theAddress) {
		return PATIENT_ADDRESS + ".where(line = '" + theAddress.getLine().get(0).toString() + "' and use != 'old').use";
	}

	private String formatRaceExtensionFhirPath() {
		return FHIR_RESOURCE_PATIENT + ".extension.where(url = '" + FHIR_STRUCTURE_US_CORE_RACE_URI + "')";
	}

	private boolean isNeedsNameUpdate(Patient thePatient) {
		// We recognize an "old" name due to the fact that is has a period. "New" names do not populate this field.
		return thePatient.getName().stream().anyMatch(HumanName::hasPeriod);
	}

	private boolean isValidForUpdate(BundleEntryComponent theCreateEntry) {
		return theCreateEntry.hasResource() &&
				UPDATABLE_RESOURCE_TYPES.contains(theCreateEntry.getResource().fhirType()) &&
				theCreateEntry.hasRequest() &&
				theCreateEntry.getRequest().hasIfNoneExist();
	}

	private BundleEntryComponent createPatchEntry(BundleEntryComponent theCreateEntry) {
		BundleEntryComponent updateEntry = new BundleEntryComponent();
		String resourceUrl = theCreateEntry.getResource().fhirType() + "?" + theCreateEntry.getRequest().getIfNoneExist();
		Bundle.BundleEntryRequestComponent patchRequest = new Bundle.BundleEntryRequestComponent()
				.setMethod(Bundle.HTTPVerb.PATCH).setUrl(resourceUrl);
		updateEntry.setRequest(patchRequest);
		return updateEntry;
	}

	private Parameters createUpdateParameter(String theFieldName, Type theNewValue) {
		Parameters updateParameters = new Parameters();
		updateParameters.addParameter(createUpdateOperation(theFieldName, theNewValue));
		return updateParameters;
	}

	private Parameters.ParametersParameterComponent createUpdateOperation(String thePathName, Type theNewValue) {
		Parameters.ParametersParameterComponent operation = new Parameters.ParametersParameterComponent();
		operation.setName("operation");
		operation.addPart().setName("type").setValue(new CodeType("replace"));
		operation.addPart().setName("path").setValue(new StringType(thePathName));
		operation.addPart().setName("value").setValue(theNewValue);
		return operation;
	}

	private Parameters.ParametersParameterComponent createAddOperation(String thePathName, String theFieldName, Type theNewValue) {
		Parameters.ParametersParameterComponent operation = new Parameters.ParametersParameterComponent();
		operation.setName("operation");
		operation.addPart().setName("type").setValue(new CodeType("add"));
		operation.addPart().setName("path").setValue(new StringType(thePathName));
		if (StringUtils.isNotBlank(theFieldName)) {
			operation.addPart().setName("name").setValue(new StringType(theFieldName));
		}
		operation.addPart().setName("value").setValue(theNewValue);
		return operation;
	}

	private Parameters.ParametersParameterComponent createDeleteOperation(String thePathName) {
		Parameters.ParametersParameterComponent operation = new Parameters.ParametersParameterComponent();
		operation.setName("operation");
		operation.addPart().setName("type").setValue(new CodeType("delete"));
		operation.addPart().setName("path").setValue(new StringType(thePathName));
		return operation;
	}

	/**
	 * Transforms a Consolidated CDA (C-CDA) 2.1 Continuity of Care Document (CCD)
	 * instance to a Bundle of corresponding FHIR resources
	 *
	 * @param filePath           The filepath to a file containing a
	 *                           Consolidated CDA (C-CDA) 2.1 Continuity of Care
	 *                           Document (CCD) instance to be transformed
	 * @param bundleType         The type of bundle to create, currently only
	 *                           supports transaction bundles.
	 * @param resourceProfileMap The mappings of default resource profiles to
	 *                           desired resource profiles. Used to set profile
	 *                           URI's of bundle entries or omit unwanted entries.
	 * @param documentBody       The decoded documentBody of the document, to be
	 *                           included in a provenance object.
	 * @return A FHIR Bundle that contains a Composition corresponding to the CCD
	 *         document and all other resources that are referenced within the
	 *         Composition.
	 * @throws Exception
	 */

	public Bundle transformDocument(String filePath, BundleType bundleType, Map<String, String> resourceProfileMap,
									String documentBody, Identifier assemblerDevice) throws Exception {
		ContinuityOfCareDocument cda = getClinicalDocument(filePath);
		ITransformationContext transformationContext = new TransformationContextImpl(bundleType);
		Bundle bundle = transformDocument(cda, true, transformationContext);
		bundle.setType(bundleType);
		// FT : 2022-09-07 issue: 3431 generate Provenance even the assemblerDevice.id is null
		if (!StringUtils.isEmpty(documentBody)) {
			bundle = resTransformer.tProvenance(bundle, documentBody, assemblerDevice, transformationContext);
		}

		if (bundleType.equals(BundleType.TRANSACTION)) {
			return createTransactionBundle(bundle, resourceProfileMap, false);
		}
		return bundle;
	}

	/**
	 * Transforms a Consolidated CDA (C-CDA) 2.1 Continuity of Care Document (CCD)
	 * instance to a Bundle of corresponding FHIR resources
	 *
	 * @param filePath A file path string to a Consolidated CDA (C-CDA) 2.1
	 *                 Continuity of Care Document (CCD) on file system
	 * @return A FHIR Bundle that contains a Composition corresponding to the CCD
	 *         document and all other resources that are referenced within the
	 *         Composition.
	 * @throws Exception
	 */
	public Bundle transformDocument(String filePath) throws Exception {
		ContinuityOfCareDocument cda = getClinicalDocument(filePath);
		return transformDocument(cda, true, new TransformationContextImpl());
	}

	/**
	 * Transforms a Consolidated CDA (C-CDA) 2.1 Continuity of Care Document (CCD)
	 * instance to a Bundle of corresponding FHIR resources
	 *
	 * @param cda A Consolidated CDA (C-CDA) 2.1 Continuity of Care Document (CCD)
	 *            instance to be transformed
	 * @return A FHIR Bundle that contains a Composition corresponding to the CCD
	 *         document and all other resources that are referenced within the
	 *         Composition.
	 */

	public Bundle transformDocument(ContinuityOfCareDocument cda) {
		ITransformationContext transformationContext = new TransformationContextImpl();
		return transformDocument(cda, true, transformationContext);
	}

	/**
	 * @param cda                A Consolidated CDA (C-CDA) 2.1 Continuity of Care
	 *                           fhir-r4 Document (CCD) instance to be transformed
	 * @param bundleType         The type of bundle to create, currently only
	 *                           supports transaction bundles.
	 * @param resourceProfileMap The mappings of default resource profiles to
	 *                           desired resource profiles. Used to set profile
	 *                           URI's of bundle entries or omit unwanted entries.
	 * @param documentBody       The decoded base64 document that would be included
	 *                           in the provenance object if provided.
	 * @return A {@link ITransformationResult} containing a FHIR Bundle that contains a Composition corresponding to the CCD
	 *         document and all other resources that are referenced within the Composition and any error or warning
	 *         messages detected during processing.
	 * @throws Exception
	 */

	@Override
	public ITransformationResult transformDocument(ContinuityOfCareDocument cda, BundleType bundleType,
												   Map<String, String> resourceProfileMap, String documentBody, Identifier assemblerDevice) throws Exception {
		ITransformationContext transformationContext = new TransformationContextImpl(bundleType);
		Bundle bundle = transformDocument(cda, true, transformationContext);
		bundle.setType(bundleType);
		// FT : 2022-09-07 issue: 3431 generate Provenance even the assemblerDevice.id is null
		if (!StringUtils.isEmpty(documentBody)) {
			bundle = resTransformer.tProvenance(bundle, documentBody, assemblerDevice, transformationContext);
		}

		if (bundleType.equals(BundleType.TRANSACTION)) {
			bundle = createTransactionBundle(bundle, resourceProfileMap, false);
		}
		return new TransformationResultImpl(bundle, transformationContext);
	}

	/**
	 * Transforms a Consolidated CDA (C-CDA) 2.1 Continuity of Care Document (CCD)
	 * instance to a Bundle of corresponding FHIR resources
	 *
	 * @param cda          A Consolidated CDA (C-CDA) 2.1 Continuity of Care
	 *                     Document (CCD) instance to be transformed.
	 * @param documentBody The decoded base64 document that would be included in the
	 *                     provenance object if provided.
	 * @return A {@link ITransformationResult} containing a FHIR Bundle that contains a Composition corresponding to the CCD
	 *         document and all other resources that are referenced within the Composition and any error or warning
	 *         messages detected during processing.
	 */
	@Override
	public ITransformationResult transformDocument(ContinuityOfCareDocument cda, String documentBody, Identifier assemblerDevice) {
		ITransformationContext transformationContext = new TransformationContextImpl();
		Bundle bundle = transformDocument(cda, true, transformationContext);
		// FT : 2022-09-07 issue: 3431 generate Provenance even the assemblerDevice.id is null
		if (!StringUtils.isEmpty(documentBody)) {
			bundle = resTransformer.tProvenance(bundle, documentBody, assemblerDevice, transformationContext);
		}
		return new TransformationResultImpl(bundle, transformationContext);
	}

	private ICDASection findCDASection(Section section) {
		for (CDASectionTypeEnum sectionType : supportedSectionTypes) {
			if (sectionType.supports(section)) {
				return sectionType.toCDASection(section);
			}
		}
		logger.info("Encountered unsupported section: " + section.getTitle().getText());
		return null;
	}

	/**
	 * Transforms a Consolidated CDA (C-CDA) 2.1 Continuity of Care Document (CCD)
	 * instance to a Bundle of corresponding FHIR resources
	 *
	 * @param ccd                A Consolidated CDA (C-CDA) 2.1 Continuity of Care
	 *                           Document (CCD) instance to be transformed
	 * @param includeComposition Flag to include composition (required for document
	 *                           type bundles)
	 * @param theTransformationContext
	 * @return A FHIR Bundle
	 */
	public Bundle transformDocument(ContinuityOfCareDocument ccd, boolean includeComposition, ITransformationContext theTransformationContext) { // TODO: Should be
		// bundle type based.
		if (ccd == null) {
			return null;
		}

		// init the global ccd bundle via a call to resource transformer, which handles
		// cda header data (in fact, all except the sections)
		IEntryResult entryResult = resTransformer.tClinicalDocument2Bundle(ccd, includeComposition, theTransformationContext);
		Bundle ccdBundle = entryResult.getBundle();
		if (ccdBundle == null) {
			theTransformationContext.addWarning("Document", "", "The document seems to be empty.");
			ccdBundle = new Bundle();
		}

		// the first bundle entry is always the composition
		Composition ccdComposition = includeComposition ? (Composition) ccdBundle.getEntry().get(0).getResource()
				: null;

		// init the patient id reference if it is not given externally.
		if (theTransformationContext.getPatientRef() == null) {
			List<Patient> patients = FHIRUtil.findResources(ccdBundle, Patient.class);
			if (patients.size() > 1) {
				theTransformationContext.addWarning("Document", "recordTarget",
						"The document contains more than one recordTarget. Using the first as the patient reference for other resources.");
			}
			if (patients.size() > 0) {
				theTransformationContext.setPatient(patients.get(0));
			} else {
				theTransformationContext.addWarning("Document", "recordTarget", "The document does not contain a recordTarget.");
			}
		} else if (ccdComposition != null) { // Correct the subject at composition with given patient reference.
			ccdComposition.setSubject(theTransformationContext.getPatientRef());
		}

		BundleInfo bundleInfo = new BundleInfo(resTransformer);
		bundleInfo.updateFrom(entryResult);
		List<IDeferredReference> deferredReferences = new ArrayList<>();

		// transform the sections
		for (Section cdaSec : ccd.getSections()) {
			ICDASection section = findCDASection(cdaSec);
			if (section != null) {

				SectionComponent fhirSec = resTransformer.tSection2Section(cdaSec);

				if (fhirSec == null) {
					continue;
				}

				if (ccdComposition != null) {
					ccdComposition.addSection(fhirSec);
				}

				// add text annotation lookups.
				if (cdaSec.getText() != null) {
					Map<String, String> idedAnnotations = EMFUtil.findReferences(cdaSec.getText());
					bundleInfo.mergeIdedAnnotations(idedAnnotations);
				}

				ISectionResult sectionResult = section.transform(bundleInfo, theTransformationContext);
				if (sectionResult != null) {
					FHIRUtil.mergeBundle(sectionResult.getBundle(), ccdBundle);
					if (fhirSec != null) {
						List<? extends Resource> resources = sectionResult.getSectionResources();

						for (Resource resource : resources) {
							Reference ref = fhirSec.addEntry();
							ref.setReference(resource.getId());
							String referenceString = ReferenceInfo.getDisplay(resource);
							if (referenceString != null) {
								ref.setDisplay(referenceString);
							}
						}
					}
					if (sectionResult.hasDeferredReferences()) {
						deferredReferences.addAll(sectionResult.getDeferredReferences());
					}
					bundleInfo.updateFrom(sectionResult);
				}
			}
		}

		IIdentifierMap<String> identifierMap = IdentifierMapFactory.bundleToIds(ccdBundle);

		// deferred references only present for procedure encounters.
		if (!deferredReferences.isEmpty()) {
			for (IDeferredReference dr : deferredReferences) {
				String id = identifierMap.get(dr.getFhirType(), dr.getIdentifier());
				if (id != null) {
					Reference reference = new Reference(id);
					String referenceString = ReferenceInfo.getDisplay(dr.getResource());
					if (referenceString != null) {
						reference.setDisplay(referenceString);
					}
					dr.resolve(reference);
				} else {
					theTransformationContext.addError("Document", "",
							MessageFormat.format("{0} {1} is referred but not found", dr.getFhirType(),
									dr.getIdentifier().getValue()));
				}
			}
		}

		// MedicationRequest resources need a populated encounter, but don't reference one; as such, IDeferredReference
		// will not do the trick.  Instead:
		setMedicationRequestEncounters(ccdBundle, theTransformationContext);

		return ccdBundle;
	}

	/**
	 * Adds the Encounter from the transformation context to MedicationRequest.encounter
	 *
	 * @param theBundle                     The Bundle resulting from transformation of a CCD
	 * @param theTransformationContext        The context object for the aforementioned transformation
	 */
	private void setMedicationRequestEncounters(Bundle theBundle, ITransformationContext theTransformationContext) {
		if (theTransformationContext.getEncounter() != null) {
			for (BundleEntryComponent entryComponent : theBundle.getEntry()) {
				Resource resource = entryComponent.getResource();
				if (resource != null && resource.getResourceType().equals(ResourceType.MedicationRequest)) {
					((MedicationRequest) resource).setEncounter(theTransformationContext.getEncounterRef());
				}
			}
		}
	}

	/**
	 * Adds fullUrl field to the entry using it's resource id.
	 *
	 * @param entry Entry which fullUrl field to be added.
	 */
	private void addFullUrlToEntry(BundleEntryComponent entry) {
		// entry.setFullUrl("urn:uuid:" + entry.getResource().getId().getIdPart());
		entry.setFullUrl("urn:uuid:" + entry.getResource().getIdElement().getIdPart());
	}

	private ContinuityOfCareDocument getClinicalDocument(String filePath) throws Exception {
		FileInputStream fis = new FileInputStream(filePath);
		// ClinicalDocument cda = CDAUtil.load(fis);
		ContinuityOfCareDocument cda = (ContinuityOfCareDocument) CDAUtil.loadAs(fis,
				ConsolPackage.eINSTANCE.getContinuityOfCareDocument());
		fis.close();
		return cda;
	}
}
