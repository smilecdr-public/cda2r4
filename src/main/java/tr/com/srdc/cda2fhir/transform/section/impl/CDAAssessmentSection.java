package tr.com.srdc.cda2fhir.transform.section.impl;

import org.eclipse.emf.common.util.EList;
import org.hl7.fhir.r4.model.ClinicalImpression;
import org.openhealthtools.mdht.uml.cda.Act;
import org.openhealthtools.mdht.uml.cda.Section;
import org.openhealthtools.mdht.uml.cda.StrucDocText;

import tr.com.srdc.cda2fhir.transform.IResourceTransformer;
import tr.com.srdc.cda2fhir.transform.entry.IEntryResult;
import tr.com.srdc.cda2fhir.transform.section.ICDASection;
import tr.com.srdc.cda2fhir.transform.util.IBundleInfo;
import tr.com.srdc.cda2fhir.transform.util.ITransformationContext;
import tr.com.srdc.cda2fhir.transform.util.impl.LocalBundleInfo;

public class CDAAssessmentSection implements ICDASection {

    public static final String ASSESSMENT_SECTION_TEMPLATE_ID = "2.16.840.1.113883.10.20.22.2.8";
    private Section section;

    public CDAAssessmentSection(Section theSection) {
        section = theSection;
    }

    @Override
    public SectionResultSingular<ClinicalImpression> transform(IBundleInfo bundleInfo, ITransformationContext theTransformationContext) {

        IResourceTransformer rt = bundleInfo.getResourceTransformer();
        SectionResultSingular<ClinicalImpression> result = SectionResultSingular.getInstance(ClinicalImpression.class);
        LocalBundleInfo localBundleInfo = new LocalBundleInfo(bundleInfo);

        if (section.getTemplateIds().stream().anyMatch(t -> ASSESSMENT_SECTION_TEMPLATE_ID.equals(t.getRoot()))) {
            IEntryResult er = rt.tAssessment2ClinicalImpression(section, theTransformationContext);
            result.updateFrom(er);
            localBundleInfo.updateFrom(er);
        }

        return result;
    }
}

