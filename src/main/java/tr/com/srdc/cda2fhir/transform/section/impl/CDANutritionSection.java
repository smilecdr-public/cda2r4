package tr.com.srdc.cda2fhir.transform.section.impl;

import org.hl7.fhir.r4.model.Observation;
import org.openhealthtools.mdht.uml.cda.Section;
import tr.com.srdc.cda2fhir.transform.IResourceTransformer;
import tr.com.srdc.cda2fhir.transform.entry.IEntryResult;
import tr.com.srdc.cda2fhir.transform.section.ICDASection;
import tr.com.srdc.cda2fhir.transform.util.IBundleInfo;
import tr.com.srdc.cda2fhir.transform.util.ITransformationContext;
import tr.com.srdc.cda2fhir.transform.util.impl.LocalBundleInfo;

public class CDANutritionSection implements ICDASection {
    public static final String NUTRITION_STATUS_OBSERVATION_TEMPLATE_ID = "2.16.840.1.113883.10.20.22.4.124";
    private Section mySection;

    public CDANutritionSection(Section theSection) {
        mySection = theSection;
    }

    @Override
    public SectionResultSingular<Observation> transform(IBundleInfo bundleInfo, ITransformationContext theTransformationContext) {
        IResourceTransformer rt = bundleInfo.getResourceTransformer();
        SectionResultSingular<Observation> result = SectionResultSingular.getInstance(Observation.class);
        LocalBundleInfo localBundleInfo = new LocalBundleInfo(bundleInfo);
        for (org.openhealthtools.mdht.uml.cda.Observation observation : mySection.getObservations()) {
            if (observation.getTemplateIds().stream().anyMatch(t -> NUTRITION_STATUS_OBSERVATION_TEMPLATE_ID.equals(t.getRoot()))) {
                IEntryResult er = rt.tNutritionalStatusObservation2Observation(observation, localBundleInfo, theTransformationContext);
                result.updateFrom(er);
                localBundleInfo.updateFrom(er);
            }
        }
        return result;
    }
}
