package tr.com.srdc.cda2fhir.transform.util.impl;

import tr.com.srdc.cda2fhir.transform.util.ITransformationMessage;

public class TransformationMessageImpl implements ITransformationMessage {

    private final Severity mySeverity;
    private final String myEntryName;
    private final String myFieldName;
    private final String myMessage;

    public TransformationMessageImpl(Severity theSeverity, String theEntryName, String theFieldName, String theMessage) {
        this.mySeverity = theSeverity;
        this.myEntryName = theEntryName;
        this.myFieldName = theFieldName;
        this.myMessage = theMessage;
    }

    @Override
    public Severity getSeverity() {
        return mySeverity;
    }

    @Override
    public String getEntryName() {
        return myEntryName;
    }

    @Override
    public String getFieldName() {
        return myFieldName;
    }

    @Override
    public String getMessage() {
        return myMessage;
    }
}
