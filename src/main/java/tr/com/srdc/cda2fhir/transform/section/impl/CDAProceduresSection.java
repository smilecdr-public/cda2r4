package tr.com.srdc.cda2fhir.transform.section.impl;

import org.hl7.fhir.r4.model.Device;
import org.hl7.fhir.r4.model.Observation;
import org.hl7.fhir.r4.model.Procedure;
import org.openhealthtools.mdht.uml.cda.consol.*;

import tr.com.srdc.cda2fhir.transform.IResourceTransformer;
import tr.com.srdc.cda2fhir.transform.entry.IEntryResult;
import tr.com.srdc.cda2fhir.transform.section.ICDASection;
import tr.com.srdc.cda2fhir.transform.util.IBundleInfo;
import tr.com.srdc.cda2fhir.transform.util.ITransformationContext;
import tr.com.srdc.cda2fhir.transform.util.impl.LocalBundleInfo;

public class CDAProceduresSection implements ICDASection {
	private ProceduresSection section;

	@SuppressWarnings("unused")
	private CDAProceduresSection() {
	}

	public CDAProceduresSection(ProceduresSection section) {
		this.section = section;
	}

	@Override
	public SectionResultDynamic transform(IBundleInfo bundleInfo, ITransformationContext theTransformationContext) {
		IResourceTransformer rt = bundleInfo.getResourceTransformer();
		SectionResultDynamic result = new SectionResultDynamic();
		LocalBundleInfo localBundleInfo = new LocalBundleInfo(bundleInfo);
		for (ProcedureActivityProcedure act : section.getConsolProcedureActivityProcedures()) {
			IEntryResult er = rt.tProcedure2Procedure(act, localBundleInfo, theTransformationContext);
			result.updateFrom(er, Procedure.class);
			localBundleInfo.updateFrom(er);
		}
		for (ProcedureActivityObservation observation : section.getProcedureActivityObservations()) {
			IEntryResult er = rt.tProcedureActivityObservation2Observation(observation, localBundleInfo, theTransformationContext);
			result.updateFrom(er, Observation.class);
			localBundleInfo.updateFrom(er);
		}
		for (ProcedureActivityAct act : section.getConsolProcedureActivityActs()) {
			IEntryResult er = rt.tProcedureActivityAct2Procedure(act, localBundleInfo, theTransformationContext);
			result.updateFrom(er, Procedure.class);
			localBundleInfo.updateFrom(er);
		}
		return result;
	}
}
