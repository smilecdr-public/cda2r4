package tr.com.srdc.cda2fhir.transform;

import org.apache.commons.lang3.StringUtils;
import org.hl7.fhir.r4.model.Device;
import org.openhealthtools.mdht.uml.cda.Component4;
import org.openhealthtools.mdht.uml.cda.Observation;
import org.openhealthtools.mdht.uml.cda.Organizer;
import org.openhealthtools.mdht.uml.cda.consol.ProcedureActivityProcedure;
import org.openhealthtools.mdht.uml.cda.consol.ProductInstance;
import org.openhealthtools.mdht.uml.hl7.datatypes.*;
import tr.com.srdc.cda2fhir.transform.util.IBundleInfo;
import tr.com.srdc.cda2fhir.transform.util.ITransformationContext;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import static tr.com.srdc.cda2fhir.transform.ResourceTransformerImpl.*;

public class ImplantedDeviceHelper {
    public static final String UDI_ORGANIZER_TEMPLATE_OID = "2.16.840.1.113883.10.20.22.4.311";
    public static final String CDA_R2_1_EFFECTIVE_DATE = "2019-06-21";
    public static final String LOT_OR_BATCH_NUMBER_TEMPLATE_ID = "2.16.840.1.113883.10.20.22.4.315";
    public static final String MANUFACTURING_DATE_TEMPLATE_ID = "2.16.840.1.113883.10.20.22.4.316";
    public static final String SERIAL_NUMBER_TEMPLATE_ID = "2.16.840.1.113883.10.20.22.4.319";
    public static final String EXPIRATION_DATE_TEMPLATE_ID = "2.16.840.1.113883.10.20.22.4.309";
    public static final String DISTINCT_IDENTIFICATION_CODE_TEMPLATE_ID = "2.16.840.1.113883.10.20.22.4.308";
    public static final String DEVICE_IDENTIFIER_TEMPLATE_ID = "2.16.840.1.113883.10.20.22.4.304";
    public static final List<String> MANUFACTURING_TEMPLATE_IDS = List.of(LOT_OR_BATCH_NUMBER_TEMPLATE_ID, MANUFACTURING_DATE_TEMPLATE_ID,
            SERIAL_NUMBER_TEMPLATE_ID, EXPIRATION_DATE_TEMPLATE_ID, DISTINCT_IDENTIFICATION_CODE_TEMPLATE_ID, DEVICE_IDENTIFIER_TEMPLATE_ID);
    private final ITransformationContext transformationContext;
    private final IDataTypesTransformer dtt;
    private final IBundleInfo bundleInfo;

    public ImplantedDeviceHelper(IDataTypesTransformer theDtt, ITransformationContext theTransformationContext, IBundleInfo theBundleInfo) {
        dtt = theDtt;
        transformationContext = theTransformationContext;
        bundleInfo = theBundleInfo;
    }

    void findAndAssignManufacturingInfo(ProcedureActivityProcedure procedureActivityProcedure, ProductInstance productInstance, Device fhirDevice) {
        List<Organizer> organizers = getUdiOrganizersMatchingProductInstance(procedureActivityProcedure, productInstance);

        if (organizers.size() > 1) {
            transformationContext.addWarning(PROCEDURE_ENTRY, UDI_ORGANIZER_RELATIONSHIP, "Found multiple Organizers matching ProductInstance ID");

        } else if (!organizers.isEmpty()) {
            Organizer organizer = organizers.get(0);
            for (Component4 component : organizer.getComponents()) {
                Observation observation = component.getObservation();
                if (observation != null) {
                    checkAndAssignManufacturingInfo(fhirDevice, observation);
                }
            }
        }
    }

    private void checkAndAssignManufacturingInfo(Device fhirDevice, Observation observation) {
        List<String> templateIdRoots = observation.getTemplateIds().stream().map(II::getRoot).collect(Collectors.toList());
        List<String> filteredTemplateIds = templateIdRoots.stream().filter(MANUFACTURING_TEMPLATE_IDS::contains).collect(Collectors.toList());
        boolean isManufacturingObservation = !filteredTemplateIds.isEmpty();

        if (isManufacturingObservation) {
            ANY value = getManufacturingValueVerbose(observation);
            for (String template : filteredTemplateIds) {
                switch (template) {
                    case LOT_OR_BATCH_NUMBER_TEMPLATE_ID:
                        fhirDevice.setLotNumber(dtt.tED2Annotation((ED) value, bundleInfo.getIdedAnnotations()));
                        break;
                    case MANUFACTURING_DATE_TEMPLATE_ID:
                        fhirDevice.setManufactureDate(dtt.tTS2DateTime((TS) value).getValue());
                        break;
                    case SERIAL_NUMBER_TEMPLATE_ID:
                        fhirDevice.setSerialNumber(dtt.tED2Annotation((ED) value, bundleInfo.getIdedAnnotations()));
                        break;
                    case EXPIRATION_DATE_TEMPLATE_ID:
                        fhirDevice.setExpirationDate(dtt.tTS2DateTime((TS) value).getValue());
                        break;
                    case DISTINCT_IDENTIFICATION_CODE_TEMPLATE_ID:
                        fhirDevice.setDistinctIdentifier(dtt.tED2Annotation((ED) value, bundleInfo.getIdedAnnotations()));
                        break;
                    case DEVICE_IDENTIFIER_TEMPLATE_ID:
                        String identifierValue = dtt.tII2Identifier((II) value).getValue();
                        Optional<Device.DeviceUdiCarrierComponent> udiCarrierComponent = fhirDevice.getUdiCarrier().stream()
                                .filter(t -> StringUtils.contains(t.getCarrierHRF(), identifierValue))
                                .findFirst();
                        udiCarrierComponent.ifPresent(theDeviceUdiCarrierComponent -> theDeviceUdiCarrierComponent.setDeviceIdentifier(identifierValue));
                        break;
                    default:
                        break;
                }
            }
        }
    }

    private ANY getManufacturingValueVerbose(Observation observation) {
        /*
         * we filter out CDs as in the context of device manufacture observations, there should only be two
         * values--a CD, and the value we actually care about
         */
        List<ANY> valueList = observation.getValues().stream()
                .filter(any1 -> !(any1 instanceof CD))
                .collect(Collectors.toList());

        if (valueList.isEmpty()) {
            transformationContext.addWarning(OBSERVATION_ENTRY, OBSERVATION_VALUE_FIELD, ResourceTransformerImpl.MIN_CARDINALITY_MESSAGE);
        } else if (valueList.size() > 1) {
            transformationContext.addWarning(OBSERVATION_ENTRY, OBSERVATION_VALUE_FIELD, ResourceTransformerImpl.MAX_CARDINALITY_MESSAGE);
        }

        return valueList.get(0);
    }

    private List<Organizer> getUdiOrganizersMatchingProductInstance(ProcedureActivityProcedure procedureActivityProcedure,
                                                                    ProductInstance productInstance) {
        return procedureActivityProcedure.getOrganizers().stream()
                .filter(this::matchesUdiOrganizerTemplate)
                .filter(org -> hasIIMatchingProductInstance(org, productInstance))
                .collect(Collectors.toList());
    }

    private boolean hasIIMatchingProductInstance(Organizer org, ProductInstance productInstance) {
        List<String> rootList = productInstance.getIds().stream()
                .map(II::getRoot)
                .collect(Collectors.toList());
        List<String> extensionList = productInstance.getIds().stream()
                .map(II::getExtension)
                .collect(Collectors.toList());

        return org.getIds().stream()
                .distinct()
                .anyMatch(ii -> rootList.contains(ii.getRoot()) && extensionList.contains(ii.getExtension()));
    }

    private boolean matchesUdiOrganizerTemplate(Organizer organizer) {
        return organizer.getTemplateIds().stream()
                .distinct()
                .anyMatch(ii -> (UDI_ORGANIZER_TEMPLATE_OID.equals(ii.getRoot())
                        && CDA_R2_1_EFFECTIVE_DATE.equals(ii.getExtension())));
    }
}