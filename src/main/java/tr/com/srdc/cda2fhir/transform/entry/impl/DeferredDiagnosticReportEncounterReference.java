package tr.com.srdc.cda2fhir.transform.entry.impl;

import org.hl7.fhir.r4.model.*;
import tr.com.srdc.cda2fhir.transform.util.IDeferredReference;

public class DeferredDiagnosticReportEncounterReference implements IDeferredReference {

    private DiagnosticReport diagnosticReport;
    private Identifier identifier;

    public DeferredDiagnosticReportEncounterReference(DiagnosticReport theDiagnosticReport, Identifier theIdentifier) {
        diagnosticReport = theDiagnosticReport;
        identifier = theIdentifier;
    }

    @Override
    public String getFhirType() {
        return "Encounter";
    }

    @Override
    public Identifier getIdentifier() {
        return identifier;
    }

    @Override
    public Resource getResource() {
        return diagnosticReport;
    }

    @Override
    public void resolve(Reference reference) {
        diagnosticReport.setEncounter(reference);
    }
}
