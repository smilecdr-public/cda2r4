package tr.com.srdc.cda2fhir.transform.entry.impl;

import org.hl7.fhir.r4.model.Identifier;
import org.hl7.fhir.r4.model.Observation;
import org.hl7.fhir.r4.model.Procedure;
import org.hl7.fhir.r4.model.Reference;
import org.hl7.fhir.r4.model.Resource;
import tr.com.srdc.cda2fhir.transform.util.IDeferredReference;

public class DeferredObservationEncounterReference implements IDeferredReference {
	private Observation observation;
	private Identifier identifier;

	public DeferredObservationEncounterReference(Observation observation, Identifier identifier) {
		this.observation = observation;
		this.identifier = identifier;
	}

	@Override
	public String getFhirType() {
		return "Encounter";
	}

	@Override
	public Identifier getIdentifier() {
		return identifier;
	}

	@Override
	public Resource getResource() {
		return observation;
	}

	@Override
	public void resolve(Reference reference) {
		observation.setEncounter(reference);
	}
}
