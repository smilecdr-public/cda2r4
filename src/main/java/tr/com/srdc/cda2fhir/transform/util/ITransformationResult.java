package tr.com.srdc.cda2fhir.transform.util;

import org.hl7.fhir.r4.model.Bundle;

import java.util.List;

public interface ITransformationResult {

    /**
     * Returns a bundle of FHIR resources corresponding to the contents of the document.
     *
     * @return the bundle
     */
    Bundle getBundle();

    /**
     * Indicates whether the transformation process encountered any errors.
     *
     * @return true is errors were present
     */
    boolean hasErrors();

    /**
     * Indicates whether the transformation process encountered any warnings.
     *
     * @return true if warnings were present
     */
    boolean hasWarnings();

    /**
     * Returns a list of error messages describing problems encountered during transformation.
     *
     * @return the list of messages
     */
    List<ITransformationMessage> getErrors();

    /**
     * Returns a list of warning messages describing problems encountered during transformation.
     *
     * @return the list of messages
     */
    List<ITransformationMessage> getWarnings();
}
