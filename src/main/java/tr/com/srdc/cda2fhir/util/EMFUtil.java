package tr.com.srdc.cda2fhir.util;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

import org.eclipse.emf.ecore.EReference;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.ecore.util.FeatureMap;
import org.eclipse.emf.ecore.util.FeatureMap.Entry;
import org.eclipse.emf.ecore.util.FeatureMapUtil;
import org.eclipse.emf.ecore.xml.type.AnyType;
import org.openhealthtools.mdht.uml.cda.StrucDocText;

public class EMFUtil {

	private static String[] supportedTypes = { "content", "td", "paragraph", "item" };

	static private String findAttribute(FeatureMap attributes, String name) {
		if (attributes != null) {
			for (Entry attribute : attributes) {
				String attrName = attribute.getEStructuralFeature().getName();
				if (name.equalsIgnoreCase(attrName)) {
					return attribute.getValue().toString();
				}
			}
		}
		return null;
	}

	static private void putReferences(FeatureMap featureMap, Map<String, String> result) {
		if (featureMap == null) {
			return;
		}

		for (Entry entry : featureMap) {
			EStructuralFeature feature = entry.getEStructuralFeature();
			if (feature instanceof EReference) {
				AnyType anyType = (AnyType) entry.getValue();

				if (Arrays.stream(supportedTypes).anyMatch(feature.getName().toLowerCase()::equals)) {
					String id = findAttribute(anyType.getAnyAttribute(), "id");
					if (id != null) {
						// JR 4053 Always get all text including the text of nested tags to any depth
						String textStr = getAllText(anyType.getMixed());
						if (textStr.length() > 0) {
							result.put(id, textStr);
						}
					}
				}
				putReferences(anyType.getMixed(), result);
			}
		}
	}

	/***
	 * Pulls text references out of source HTML for later use.
	 *
	 * @param text the structured text portion of the CCD document.
	 * @return map of ids and values from that section.
	 */
	static public Map<String, String> findReferences(StrucDocText text) {
		if (text != null) {
			Map<String, String> result = new HashMap<String, String>();
			FeatureMap featureMap = text.getMixed();
			putReferences(featureMap, result);
			return result;
		}
		return null;
	}

	/**
	 * if ID is at parent node item, to get text from sub node of paragraph 
	 * <item ID= "ConsultNote1"> <paragraph>Text Content</paragraph> </item>
	 * 
	 * @param featureMap the sub text node
	 * @return the text of sub node, e.g. Text Content
	 */
	private static String getSubText(FeatureMap featureMap) {

		if (featureMap == null || featureMap.isEmpty())
			return null;

		StringBuilder sb = new StringBuilder();

		for (FeatureMap.Entry entry : featureMap) {
			Object valueObject = entry.getValue();
			if (valueObject instanceof AnyType) {
				AnyType subType = (AnyType) valueObject;
				sb.append(getText(subType.getMixed()));
			}
		}
		return sb.toString();
	}

	private static String getText(FeatureMap featureMap) {
		StringBuilder sb = new StringBuilder();
		for (FeatureMap.Entry entry : featureMap) {
			if (FeatureMapUtil.isText(entry)) {
				sb.append(entry.getValue().toString());
			}
		}
		return sb.toString().trim();
	}

	private static String getAllText(FeatureMap featureMap) {
		StringBuilder sb = new StringBuilder();
		for (FeatureMap.Entry entry : featureMap) {
			if (FeatureMapUtil.isText(entry)) {
				sb.append(entry.getValue().toString());
			} else if (entry.getValue() instanceof AnyType) {
				sb.append(getAllText(((AnyType)entry.getValue()).getMixed()));
			}
		}
		return sb.toString().trim();
	}
}
